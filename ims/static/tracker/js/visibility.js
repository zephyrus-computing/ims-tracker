function toggle_visibility(id) {
    var e = document.getElementById(id);
    if (e.style.display == 'block')
        e.style.display = 'none';
    else
        e.style.display = 'block';
}

function ims_expand(elem) {
    var parent = elem.parentNode;
    var child_div = parent.getElementsByTagName("div")[0];
    if (elem.text === "+") {
        elem.text = "-";
        child_div.classList.remove("ims-hidden");
    } else {
        elem.text = "+";
        child_div.classList.add("ims-hidden");
    }
}
