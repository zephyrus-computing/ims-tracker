#!/bin/bash
folder=$(dirname $0) # Should be /etc/ims/scripts
parentfolder=$(dirname $folder) # Should be /etc/ims
logfile=$(grep "INSTALL_LOG_PATH" "$parentfolder/ims/settings.py" | cut -d'=' -f2)
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log"
fi
if [ ! $1 ]; then
    read -p "Select option (backend,broker): " selection
else
    selection=$1
fi
list=(backend broker)
if [[ ! " ${list[*]} " =~ " ${selection} " ]]; then
    echo "Invalid argument."
    echo "$0 [${list[*]}] [host] [port]"
    exit
fi
# Find the settings.py file
if [[ -f '/etc/ims/ims/settings.py' ]]; then
    settingsfile='/etc/ims/ims/settings.py'
else
    [[ -f "$parentfolder/ims/settings.py" ]] && settingsfile="$parentfolder/ims/settings.py" || settingsfile=''
    if [[ $settingsfile == '' ]]; then
        echo "ERROR: IMS settings.py File Not Found!" | tee -a $logfile
        exit 1
    fi
fi
if [ ! $6 ]; then
    if [ ! $3 ]; then
        read -p "Username: " username
        read -s -p "Password: " password
        read -p "Virtual Host [ims]: " vhost
        read -p "Target Host [localhost]: " targethost
        read -p "Target Port [5672]: " targetport
    else
        username = $2
        password = $3
        read -p "Virtual Host [ims]: " vhost
        read -p "Target Host [localhost]: " targethost
        read -p "Target Port [5672]: " targetport
else
    username = $2
    password = $3
    vhost = $4
    targethost = $5
    targetport = $6
fi
if [[ "$vhost" == "" ]]; then vhost = 'ims'; fi
if [[ "$targethost" == "" ]]; then targethost = 'localhost'; fi
if [[ "$targetport" == "" ]]; then targetport = '5672'; fi
# Update the settings.py with the specified Redis settings
echo "$(date)" >> $logfile
echo "Configuring ims settings ($settingsfile)" >> $logfile
if [ selection == "backend" ]; then
    EXISTING=$(awk '/CELERY_RESULT_BACKEND/{ print }' $settingsfile)
    LINEBACKEND=$(grep -n 'CELERY_RESULT_BACKEND = ' $settingsfile | cut -d: -f1)
    echo "$EXISTING" >> $logfile
    echo "Overwriting the Celery settings for RabbitMQ backend." >> $logfile
    sed -i "$LINEBACKEND s/.*/CELERY_RESULT_BACKEND = 'ampq:\/\/${username}:${password}@${targethost}:${targetport}'\/${vhost}/" $settingsfile
fi
if [ selection == "broker" ]; then
    EXISTING=$(awk '/CELERY_BROKER_URL/{ print }' $settingsfile)
    LINEBROKER=$(grep -n 'CELERY_BROKER_URL = ' $settingsfile | cut -d: -f1)
    echo "$EXISTING" >> $logfile
    echo "Overwriting the Celery settings for RabbitMQ broker." >> $logfile
    sed -i "$LINEBROKER s/.*/CELERY_BROKER_URL = 'ampq:\/\/${username}:${password}@${targethost}:${targetport}'\/${vhost}/" $settingsfile
    EXISTING=$(awk '/CELERY_BROKER_TRANSPORT_OPTIONS/{ print }' $settingsfile)
    if [ $EXISTING != "" ]; then
        LINEOPTIONS=$(grep -n 'CELERY_BROKER_TRANSPORT_OPTIONS = ' $settingsfile | cut -d: -f1)
        echo "$EXISTING" >> $logfile
        echo "Removing Transport Options." >> $logfile
        sed -i "$LINEOPTIONS s/.*//" $settingsfile
    fi
fi

# Run RabbitMQ Control commands
rabbitmqctl add_user ${username} ${password}
rabbitmqctl add_vhost ${vhost}
rabbitmqctl set_user_tags ${username} ims
rabbitmqctl set_permissions -p ${vhost} ${username} ".*" ".*" ".*"

