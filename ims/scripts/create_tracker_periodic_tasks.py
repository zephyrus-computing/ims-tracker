#!/usr/bin/env python
import logging
import os
from django.db.utils import IntegrityError
from django_celery_beat.models import IntervalSchedule, PeriodicTask, MINUTES, HOURS, now

logger = logging.getLogger('installer')

try:
    logger.info('creating IntervalSchedule for 5 minutes')
    i = IntervalSchedule.objects.get_or_create(every=5,period=MINUTES)[0]
    j = IntervalSchedule.objects.get_or_create(every=1,period=HOURS)[0]
except:
    logger.info('Unknown exception while adding IntervalSchedules to the database')
try:
    logger.info('creating PeriodicTask send_notifications with interval 5 minutes')
    PeriodicTask.objects.get_or_create(enabled=True,name='Builtin-Process-Notifications',description='Process and Send Notifications every 5 minutes',interval=i,task='process_notifications',start_time=now())[0]
    PeriodicTask.objects.get_or_create(enabled=True,name='Builtin-Cleanup-Tracker-Graphs',description='Clean up stale files every 1 hour',interval=j,task='clean_tracker_graphs',start_time=now())[0]
except:
    logger.info('Unknown exception while adding PeriodicTasks to the database')

