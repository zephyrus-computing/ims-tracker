#!/usr/bin/env python
import logging
import os
from django.db.utils import IntegrityError
from ims.addons.tracker.models import EmailTemplate

logger = logging.getLogger('installer')

templates = os.listdir('/etc/zc/ims/media/tracker/templates/')

for i in range(len(templates)):
    template = templates[i]
    templatename = template.replace('.html','')
    try:
        EmailTemplate.objects.create(name='builtin-' + templatename, subject='ims-' + templatename, template='tracker/templates/' + template)
    except IntegrityError:
        logger.info('The builtin template "{}" already exists in the database'.format(templatename))
    except:
        logger.info('Unknown exception while adding builtin template "{}" to the database'.format(templatename))

