#!/usr/bin/env python
import logging
import os
from django.conf import settings

logger = logging.getLogger('installer')
filepath = '/etc/zc/ims/ims/addons/tracker/celery.config'

# Get the CELERY environment variables from the tracker_settings.py file
# loaded from django.conf.settings and write them to a new file for the 
# ims-tracker.service file to reference
if os.path.exists(filepath):
    if os.path.exists('{}.old'.format(filepath)):
        os.remove('{}.old'.format(filepath))
    os.rename(filepath,'{}.old'.format(filepath))
with open(filepath, 'w') as config:
    print('# WARNING! This file is automatically generate by the generate_service_environment_file.py script.', file=config)
    print('# Any changes to this file will be overwritten the next time the script is executed', file=config)
    print('# Updates to these variables should be done via the .env file located in the application root directory', file=config)
    print('CELERY_APP="{}"'.format(settings.CELERY_APP), file=config)
    print('CELERY_RESULT_BACKEND="{}"'.format(settings.CELERY_RESULT_BACKEND), file=config)
    print('CELERY_BROKER_URL="{}"'.format(settings.CELERY_BROKER_URL), file=config)
    print('CELERY_BROKER_TRANSPORT_OPTIONS="{}"'.format(settings.CELERY_BROKER_TRANSPORT_OPTIONS), file=config)
    print('CELERY_TASK_SERIALIZER="{}"'.format(settings.CELERY_TASK_SERIALIZER), file=config)
    print('CELERY_ACCEPT_CONTENT="{}"'.format(settings.CELERY_ACCEPT_CONTENT), file=config)
    print('CELERY_RESULT_SERIALIZER="{}"'.format(settings.CELERY_RESULT_SERIALIZER), file=config)
    print('CELERY_TEST_RUNNER="{}"'.format(settings.CELERY_TEST_RUNNER), file=config)
    print('CELERYD_MULTI="{}"'.format(settings.CELERYD_MULTI), file=config)
    print('CELERYD_NODES="{}"'.format(settings.CELERYD_NODES), file=config)
    print('CELERYD_LOG_LEVEL="{}"'.format(settings.CELERYD_LOG_LEVEL), file=config)
    print('CELERYD_WORKDIR="{}"'.format(settings.CELERYD_WORKDIR), file=config)
    print('CELERYD_OPTS="{}"'.format(settings.CELERYD_OPTS), file=config)
    print('CELERYD_LOG_FILE="{}"'.format(settings.CELERYD_LOG_FILE), file=config)
    print('CELERYD_PID_FILE="{}"'.format(settings.CELERYD_PID_FILE), file=config)
    print('CELERYBEAT_SCHEDULER="{}"'.format(settings.CELERYBEAT_SCHEDULER), file=config)
    print('CELERYBEAT_LOG_LEVEL="{}"'.format(settings.CELERYBEAT_LOG_LEVEL), file=config)
    print('CELERYBEAT_LOG_FILE="{}"'.format(settings.CELERYBEAT_LOG_FILE), file=config)
    print('CELERYBEAT_PID_FILE="{}"'.format(settings.CELERYBEAT_PID_FILE), file=config)