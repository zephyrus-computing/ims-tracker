#!/bin/bash
folder=$(dirname $0) # Should be /etc/ims/scripts
parentfolder=$(dirname $folder) # Should be /etc/ims
logfile=$(grep "INSTALL_LOG_PATH" "$parentfolder/ims/settings.py" | cut -d'=' -f2)
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$parentfolder/logs/" ]] && logfile="$parentfolder/logs/install.log" || logfile="$folder/install.log"
fi
if [ ! $1 ]; then
    read -p "Select option (backend,broker): " selection
else
    selection=$1
fi
list=(backend broker)
if [[ ! " ${list[*]} " =~ " ${selection} " ]]; then
    echo "Invalid argument."
    echo "$0 [${list[*]}] [directory/path]"
    exit
fi
# Find the settings.py file
if [[ -f '/etc/ims/ims/settings.py' ]]; then
    settingsfile='/etc/ims/ims/settings.py'
else
    [[ -f "$parentfolder/ims/settings.py" ]] && settingsfile="$parentfolder/ims/settings.py" || settingsfile=''
    if [[ $settingsfile == '' ]]; then
        echo "ERROR: IMS settings.py File Not Found!" | tee -a $logfile
        exit 1
    fi
fi
if [ ! $2 ]; then
    read -p "Target Directory [/etc/zc/ims/ims/addons/tracker/celery/]: " targetdir
else
    targetdir = $2
fi
if [[ "$targetdir" == "" ]]; then targetdir='/etc/zc/ims/ims/addons/tracker/celery/'; fi
# Update the settings.py with the specified Redis settings
echo "$(date)" >> $logfile
echo "Configuring ims settings ($settingsfile)" >> $logfile
if [ $selection == "backend" ]; then
    EXISTING=$(awk '/CELERY_RESULT_BACKEND/{ print }' $settingsfile)
    LINEBACKEND=$(grep -n 'CELERY_RESULT_BACKEND = ' $settingsfile | cut -d: -f1)
    echo "$EXISTING" >> $logfile
    echo "Overwriting the Celery settings for File backend." >> $logfile
    sed -i "$LINEBACKEND s/.*/CELERY_RESULT_BACKEND = 'file:\/\/${targetdir}'/" $settingsfile
fi
if [ $selection == "broker" ]; then
    EXISTING=$(awk '/CELERY_BROKER_TRANSPORT_OPTIONS/{ print }' $settingsfile)
    if [ $EXISTING == "" ]; then
        EXISTING=$(awk '/CELERY_BROKER_URL/{ print }' $settingsfile)
        LINEBROKER=$(grep -n 'CELERY_BROKER_URL = ' $settingsfile | cut -d: -f1)
        echo "$EXISTING" >> $logfile
        echo "Overwriting the Celery settings for File broker." >> $logfile
        sed -i "$LINEBROKER s/.*/CELERY_BROKER_URL = 'filesystem:\/\/'\nCELERY_BROKER_TRANSPORT_OPTIONS = { 'data_folder_out': '${targetdir}', 'data_folder_in': '${targetdir}', 'processed_folder': '${targetdir}\/processed', 'store_processed': True }'/" $settingsfile
    else
        LINEOPTIONS=$(grep -n 'CELERY_BROKER_TRANSPORT_OPTIONS = ' $settingsfile | cut -d: -f1)
        echo "$EXISTING" >> $logfile
        EXISTING=$(awk '/CELERY_BROKER_URL/{ print }' $settingsfile)
        LINEBROKER=$(grep -n 'CELERY_BROKER_URL = ' $settingsfile | cut -d: -f1)
        echo "$EXISTING" >> $logfile
        echo "Overwriting the Celery settings for File broker." >> $logfile
        sed -i "$LINEOPTIONS s/.*/CELERY_BROKER_TRANSPORT_OPTIONS = { 'data_folder_out': '${targetdir}', 'data_folder_in': '${targetdir}', 'processed_folder': '${targetdir}\/processed', 'store_processed': True }'/" $settingsfile
        sed -i "$LINEBROKER s/.*/CELERY_BROKER_URL = 'filesystem:\/\/'/" $settingsfile
    fi
fi

