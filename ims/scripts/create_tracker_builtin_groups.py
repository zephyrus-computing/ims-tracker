#!/usr/bing/env python

from django.contrib.auth.models import Group, Permission

reportReadonly = Group.objects.get_or_create(name="Tracker Reports ReadOnly")[0]
reportManage = Group.objects.get_or_create(name="Tracker Reports Manage")[0]

readonlyPermissions = ['can_view_graph','can_view_recentadd','can_view_recentmod',
                       'can_view_useract','can_view_partcount','can_view_kitcount',
                       'can_view_contents','can_view_histpart','can_view_histkit',
                       'can_view_histps'
]
managePermissions = ['can_add_schdreport','can_delete_schdreport','can_view_schdreport',
                     'can_change_schdreport','can_subscribe_schdreport'
]

for i in range(0,len(readonlyPermissions)):
    p = Permission.objects.get_or_create(codename=readonlyPermissions[i])[0]
    reportReadonly.permissions.add(p)
    reportManage.permissions.add(p)

for i in range(0,len(managePermissions)):
    p = Permission.objects.get_or_create(codename=managePermissions[i])[0]
    reportManage.permissions.add(p)