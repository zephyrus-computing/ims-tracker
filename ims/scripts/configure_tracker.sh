#! /bin/bash
folder=$(dirname $(dirname $0)) # Should be /etc/zc/ims
logfile=$(grep "INSTALL_LOG_PATH" "$folder/ims/settings.py" | cut -d'=' -f2)
if [[ ! -d $(dirname $logfile) ]]; then
    [[ -d "$folder/logs/" ]] && logfile="$folder/logs/install.log" || logfile="$folder/install.log"
fi
if [ ! $1 ]; then
    main="Please make a selection (IMS-Tracker): "
    PS3=$main
    options=("celery" "email" "service" "enable" "disable" "back")
    select opt in "${options[@]}"
    do
        case $opt in
            "celery")
                PS3="Please select an option: "
                suboptions=("backend" "broker" "back")
                select sub in "${suboptions[@]}"
                do
                    case $sub in
                        "backend")
                            PS3="Please select a supported backend: "
                            suboptions=("redis" "rabbitmq" "file" "back")
                            select sub in "${suboptions[@]}"
                            do
                                case $sub in
                                    "redis")
                                        ${folder}/scripts/configure_redis.sh backend
                                        PS3=$main
                                        break
                                        ;;
                                    "rabbitmq")
                                        ${folder}/scripts/configure_rabbitmq.sh backend
                                        PS3=$main
                                        break
                                        ;;
                                    "file")
                                        ${folder}/scripts/configure_celeryfile.sh backend
                                        PS3=$main
                                        break
                                        ;;
                                    "back")
                                        PS3=$main
                                        break
                                        ;;
                                esac
                            done
                            break
                            ;;
                        "broker")
                            PS3="Please select a supported broker: "
                            suboptions=("redis" "rabbitmq" "file" "back")
                            select sub in "${suboptions[@]}"
                            do
                                case $sub in
                                    "redis")
                                        ${folder}/scripts/configure_redis.sh broker
                                        PS3=$main
                                        break
                                        ;;
                                    "rabbitmq")
                                        ${folder}/scripts/configure_rabbitmq.sh broker
                                        PS3=$main
                                        break
                                        ;;
                                    "file")
                                        ${folder}/scripts/configure_celeryfile.sh broker
                                        PS3=$main
                                        break
                                        ;;
                                    "back")
                                        PS3=$main
                                        break
                                        ;;
                                esac
                            done
                            break
                            ;;
                        "back")
                            PS3=$main
                            break
                            ;;
                    esac
                done
                ;;
            "email")
                PS3="Please select a support delivery system: "
                suboptions=("file" "smtp" "back")
                select sub in "${suboptions[@]}"
                do
                    case $sub in
                        "file")
                            ${folder}/scripts/configure_email.sh file
                            PS3=$main
                            break
                            ;;
                        "smtp")
                            ${folder}/scripts/configure_email.sh smtp
                            PS3=$main
                            break
                            ;;
                        "back")
                            PS3=$main
                            break
                            ;;
                    esac
                done
                ;;
            "service")
                PS3="Please select an action: "
                suboptions=("start" "stop" "restart" "status" "back")
                select sub in "${suboptions[@]}"
                do
                    case $sub in
                        "start")
                            systemctl start ims-tracker.service
                            systemctl start ims-tracker-beat.service
                            ;;
                        "stop")
                            systemctl stop ims-tracker.service
                            systemctl stop ims-tracker-beat.service
                            ;;
                        "restart")
                            systemctl stop ims-tracker.service
                            systemctl stop ims-tracker-beat.service
                            systemctl start ims-tracker-beat.service
                            systemctl start ims-tracker.service
                            ;;
                        "status")
                            systemctl status ims-tracker.service
                            systemctl status ims-tracker-beat.service
                            ;;
                        "back")
                            PS3=$main
                            break
                            ;;
                    esac
                done
                ;;
            "enable")
                sed -i "s/ADDON_TRACKER_ENABLED.*/ADDON_TRACKER_ENABLED = True/" $folder/ims/config/tracker.conf
                $folder/ims/config/generate.py
                echo "Restart the IMS Service for this to take effect."
                ;;
            "disable")
                sed -i "s/ADDON_TRACKER_ENABLED.*/ADDON_TRACKER_ENABLED = False/" $folder/ims/config/tracker.conf
                $folder/ims/config/generate.py
                echo "Restart the IMS Service for this to take effect."
                ;;
            "back")
                exit
                ;;
        esac
    done
else
    list=(celery email service enable disable)
    if [[ ! " ${list[*]} " =~ " ${1} " ]]; then
        echo "Invalid option."
        echo "$0 [${list[*]}]"
        exit
    fi
    if [ $1 == "disable" ]; then
        sed -i "s/ADDON_TRACKER_ENABLED.*/ADDON_TRACKER_ENABLED = False/" $folder/ims/config/tracker.conf
        $folder/ims/config/generate.py
    fi
    if [ $1 == "enable" ]; then
        sed -i "s/ADDON_TRACKER_ENABLED.*/ADDON_TRACKER_ENABLED = True/" $folder/ims/config/tracker.conf
        $folder/ims/config/generate.py
    fi
    if [ $1 == "service" ]; then
        list=(start stop restart status)
        if [ ! $2 ]; then
            echo "Missing required argument."
            echo "$0 $1 [${list[*]}]"
        else
            if [[ ! " ${list[*]} " =~ " ${2} " ]]; then
                if [ $2 == "start" ]; then
                    systemctl start ims-tracker.service
                fi
                if [ $2 == "stop" ]; then
                    systemctl stop ims-tracker.service
                fi
                if [ $2 == "restart" ]; then
                    systemctl restart ims-tracker.service
                fi
                if [ $2 == "status" ]; then
                    systemctl status ims-tracker.service
                fi
            else
                echo "Invalid argument."
                echo "$0 $1 [${list[*]}]"
            fi
        fi
    fi
    if [ $1 == "email" ]; then
        list=(smtp file)
        if [ ! $3 ]; then
            ${folder}/scripts/configure_email.sh
        else
            if [[ " ${list[*]} " =~ " ${3} " ]]; then
                if [ $3 == "smtp" ]; then
                    if [ $9 ]; then
                        ${folder}/scripts/configure_email.sh $3 $4 $5 $6 $7 $8 $9
                    else
                        if [ $7 ]; then
                            ${folder}/scripts/configure_email.sh $3 $4 $5 $6 $7
                        else
                            ${folder}/scripts/configure_email.sh $3
                        fi
                    fi
                fi
                if [ $3 == "file" ]; then
                    if [ $4 ]; then
                        ${folder}/scripts/configure_email.sh $3 $4
                    else
                        ${folder}/scripts/configure_email.sh $3
                    fi
                fi
            else
                echo "Invalid argument."
                echo "$0 $1 $2 [file] ([directory/path])"
                echo "$0 $1 $2 [smtp] ([host] [username] [password] [port] ([from_address] [use_tls]))"
            fi
        fi
    fi
    if [ $1 == "celery" ]; then
        list=(backend broker)
        list2=(redis rabbitmq file)
        if [ ! $4 ]; then
            echo "Missing required argument."
            echo "$0 $1 $2 [${list[*]}] [${list2[*]}]"
        else
            if [[ " ${list[*]} " =~ " ${3} " ]]; then
                if [[ " ${list2[*]} " =~ " ${4} " ]]; then
                    if [ $4 == "redis" ]; then
                        ${folder}/scripts/configure_redis.sh $3 $5 $6
                    fi
                    if [ $4 == "rabbitmq" ]; then
                        ${folder}/scripts/configure_rabbitmq.sh $3 $5 $6 $7 $8 $9
                    fi
                    if [ $4 == "file" ]; then
                        ${folder}/scripts/configure_celeryfile.sh $3 $5
                    fi
                else
                    echo "Invalid argument."
                    echo "$0 $1 $2 [${list[*]}] [${list2[*]}]"
                fi
            else
                echo "Invalid argument."
                echo "$0 $1 $2 [${list[*]}] [${list2[*]}]"
            fi
        fi
    fi
fi
