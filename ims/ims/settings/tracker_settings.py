import environ
import os
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
envroot = environ.Path(BASE_DIR)
env = environ.Env()
environ.Env.read_env(os.path.join(BASE_DIR, '.env'))

# TRACKER SETTINGS
ADDON_TRACKER_THRESHOLD_PAGE_SIZE = env.int('ADDON_TRACKER_THRESHOLD_PAGE_SIZE', default=20)
ADDON_TRACKER_SUMMARIZE = env.str('ADDON_TRACKER_SUMMARIZE', default='last')

# CELERY SETTINGS
CELERY_APP = 'ims'
CELERY_BIN = env.str('CELERY_BIN', default='/etc/zc/ims/venv/bin/celery')
CELERY_RESULT_BACKEND = env.str('CELERY_RESULT_BACKEND', default='file:///etc/zc/ims/ims/addons/tracker/celery/')
CELERY_BROKER_URL = env.str('CELERY_BROKER_URL', default='filesystem://')
CELERY_BROKER_TRANSPORT_OPTIONS = env.dict('CELERY_BROKER_TRANSPORT_OPTIONS', default={ 'data_folder_out': '/etc/zc/ims/ims/addons/tracker/celery/', 'data_folder_in': '/etc/zc/ims/ims/addons/tracker/celery/', 'processed_folder': '/etc/zc/ims/ims/addons/tracker/celery/processed/', 'store_processed': True })
CELERY_TASK_SERIALIZER = env.str('CELERY_TASK_SERIALIZER', default='json')
CELERY_ACCEPT_CONTENT = env.list('CELERY_ACCEPT_CONTENT', default='json')
CELERY_RESULT_SERIALIZER = env.str('CELERY_RESULT_SERIALIZER', default='json')
CELERY_TEST_RUNNER = env.str('CELERY_TEST_RUNNER', default='djcelery.contrib.test_runner.CeleryTestSuiteRunner')

CELERYD_MULTI = 'multi'
CELERYD_NODES = env.str('CELERYD_NODES', default='t1')
CELERYD_LOG_LEVEL = env.str('CELERYD_LOG_LEVEL', default='INFO')
CELERYD_WORKDIR = env.str('CELERYD_WORKDIR', default='/etc/zc/ims/')
CELERYD_OPTS = env.str('CELERYD_OPTS', default='--time-limit=300')
CELERYD_LOG_FILE = env.str('CELERYD_LOG_FILE', default='/var/log/zc/ims/celery%n%I.log')
CELERYD_PID_FILE = env.str('CELERYD_PID_FILE', default='/etc/zc/ims/run/celery%n.pid')

CELERYBEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'
CELERYBEAT_LOG_LEVEL = env.str('CELERYBEAT_LOG_LEVEL', default='INFO')
CELERYBEAT_LOG_FILE = env.str('CELERYBEAT_LOG_FILE', default='/var/log/zc/ims/celerybeat.log')
CELERYBEAT_PID_FILE = env.str('CELERYBEAT_PID_FILE', default='/etc/zc/ims/run/celerybeat.pid')