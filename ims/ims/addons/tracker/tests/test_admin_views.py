import django
django.setup()
from django.contrib.auth.models import AnonymousUser, User, Permission
from django.test import TestCase, override_settings
from random import randint
from rest_framework import status
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage
from ims.addons.tracker.models import PartThreshold, PartStorageThreshold, PartThresholdNotify, PartStorageThresholdNotify, PartThresholdTemplate, PartStorageThresholdTemplate, ObjectAction, ObjectActionTemplate, ObjectActionNotify, KitThreshold, KitThresholdNotify, KitThresholdTemplate, EmailRecipient, EmailTemplate, Notification, Threshold, KitThreshold, KitThresholdNotify, KitThresholdTemplate
from ims.addons.tracker.helpers import OPERATORS, OBJTYPES, ACTIONS

baseurl = '/tracker/admin/'

# Test module root view
class RootViewTests(TestCase):
    def test_view_root(self):
        response = self.client.get(baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(baseurl))
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get(baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/admin/home.html')

# Test PartThreshold views
class PartThresholdAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1121,1131):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
        for i in range(45,50):
            j = randint(1121,1130)
            k = randint(0,5)
            PartThreshold.objects.create(name='Test_View_PartThreshold_{}'.format(i), part=Part.objects.get(name='Test_View_Part_{}'.format(j)), threshold=5, operator=OPERATORS[k][0])
        self.baseurl = baseurl + 'partthreshold/'

    def test_view_partthreshold_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_partthreshold')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_partthreshold_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_partthreshold')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        self.client.get('{}create/?part={}'.format(self.baseurl, Part.objects.last().id))
        pt_list = PartThreshold.objects.all()
        ids = []
        for pt in pt_list:
            ids.append(pt.part.id)
        p = Part.objects.exclude(id__in=ids)[0]
        data = {'part': p.id, 'threshold': 3, 'operator':'<', 'name':'Test_View_PartThreshold_New'}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_partthreshold_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_partthreshold')
        pt = PartThreshold.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, pt.id), permission, 'tracker/admin/update.html')
        data = {'name':'Test_View_PartThreshold_Modified', 'part':pt.part.id, 'threshold':pt.threshold, 'operator':pt.operator}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, pt.id), data, permission)
        data = {'name':'Test_View_PartThreshold_Modified', 'partstorage':pt.part}
        self.client.post('{}change/{}/'.format(self.baseurl, pt.id), data)

    def test_view_partthreshold_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_partthreshold')
        pt = PartThreshold.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, pt.id), permission)

class PartStorageThresholdAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1131,1141):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
        for i in range(50,55):
            j = randint(1131,1140)
            k = randint(1131,1140)
            l = randint(0,5)
            PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j)), storage=Storage.objects.get(name='Test_View_Storage_{}'.format(k)), count=12)
            PartStorageThreshold.objects.create(name='Test_View_PartStorageThreshold_{}'.format(i), partstorage=PartStorage.objects.last(), threshold=5, operator=OPERATORS[l][0])
        self.baseurl = baseurl + 'partstoragethreshold/'

    def test_view_partstoragethreshold_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_partstoragethreshold')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_partstoragethreshold_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_partstoragethreshold')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        self.client.get('{}create/?partstorage={}'.format(self.baseurl, PartStorage.objects.last().id))
        pst_list = PartStorageThreshold.objects.all()
        ids = []
        for pst in pst_list:
            ids.append(pst.partstorage.id)
        ps = PartStorage.objects.exclude(id__in=ids)[0]
        data = {'partstorage': ps.id, 'threshold': 3, 'operator':'<', 'name':'Test_View_PartThreshold_New'}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_partstoragethreshold_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_partstoragethreshold')
        pst = PartStorageThreshold.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, pst.id), permission, 'tracker/admin/update.html')
        data = {'name':'Test_View_PartStorageThreshold_Modified', 'partstorage':pst.partstorage.id, 'threshold':pst.threshold, 'operator':pst.operator}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, pst.id), data, permission)
        data = {'name':'Test_View_PartStorageThreshold_Modified', 'partstorage':pst.partstorage}
        self.client.post('{}change/{}/'.format(self.baseurl, pst.id), data)

    def test_view_partstoragethreshold_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_partstoragethreshold')
        pst = PartStorageThreshold.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, pst.id), permission)

class PartThresholdNotifyAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1141,1151):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
        for i in range(55,60):
            j = randint(1141,1150)
            k = randint(0,5)
            PartThreshold.objects.create(name='Test_View_PartThreshold_{}'.format(i), part=Part.objects.get(name='Test_View_Part_{}'.format(j)), threshold=5, operator=OPERATORS[k][0])
        EmailRecipient.objects.get_or_create(name='Test_View_EmailRecipient',email='support@zephyruscomputing.com')
        for i in range(0,4):
            j = randint(55,59)
            PartThresholdNotify.objects.get_or_create(partthreshold=PartThreshold.objects.get(name='Test_View_PartThreshold_{}'.format(j)),emailrecipient=EmailRecipient.objects.first())
        self.baseurl = baseurl + 'partthresholdnotify/'

    def test_view_partthresholdnotify_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_partthresholdnotify')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_partthresholdnotify_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_partthresholdnotify')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        self.client.get('{}create/?partthreshold={}'.format(self.baseurl, PartThreshold.objects.last().id))
        self.client.get('{}create/?emailrecipient={}'.format(self.baseurl, EmailRecipient.objects.last().id))
        ptn_list = PartThresholdNotify.objects.all()
        ids = []
        for ptn in ptn_list:
            ids.append(ptn.partthreshold.id)
        pt = PartThreshold.objects.exclude(id__in=ids)[0]
        self.client.get('{}create/?partthreshold={}&emailrecipient={}'.format(self.baseurl, pt.id, EmailRecipient.objects.last().id))
        data = {'partthreshold': pt.id, 'emailrecipient': EmailRecipient.objects.first().id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'emailrecipient': EmailRecipient.objects.first().id}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_partthresholdnotify_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_partthresholdnotify')
        ptn = PartThresholdNotify.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, ptn.id), permission)

class PartThresholdTemplateAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1151,1161):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
        for i in range(60,65):
            j = randint(1151,1160)
            k = randint(0,5)
            PartThreshold.objects.create(name='Test_View_PartThreshold_{}'.format(i), part=Part.objects.get(name='Test_View_Part_{}'.format(j)), threshold=5, operator=OPERATORS[k][0])
        EmailTemplate.objects.get_or_create(name='Test_View_EmailTemplate', subject='Test View EmailTemplate', template='/media/tracker/templates/TestTemplate.html')
        for i in range(0,4):
            j = randint(60,64)
            PartThresholdTemplate.objects.get_or_create(partthreshold=PartThreshold.objects.get(name='Test_View_PartThreshold_{}'.format(j)), emailtemplate=EmailTemplate.objects.first())
        self.baseurl = baseurl + 'partthresholdtemplate/'

    def test_view_partthresholdtemplate_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_partthresholdtemplate')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_partthresholdtemplate_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_partthresholdtemplate')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        self.client.get('{}create/?partthreshold={}'.format(self.baseurl, PartThreshold.objects.last().id))
        self.client.get('{}create/?emailtemplate={}'.format(self.baseurl, EmailTemplate.objects.last().id))
        ptt_list = PartThresholdTemplate.objects.all()
        ids = []
        for ptt in ptt_list:
            ids.append(ptt.partthreshold.id)
        pt = PartThreshold.objects.exclude(id__in=ids)[0]
        self.client.get('{}create/?partthreshold={}&emailtemplate={}'.format(self.baseurl, pt.id, EmailTemplate.objects.last().id))
        data = {'partthreshold': pt.id, 'emailtemplate': EmailTemplate.objects.first().id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'emailtemplate': EmailTemplate.objects.first().id}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_partthresholdtemplate_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_partthresholdtemplate')
        ptt = PartThresholdTemplate.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, ptt.id), permission)

class PartStorageThresholdNotifyAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1161,1171):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
        for i in range(65,70):
            j = randint(1161,1170)
            k = randint(1161,1170)
            l = randint(0,5)
            PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j)), storage=Storage.objects.get(name='Test_View_Storage_{}'.format(k)), count=12)
            PartStorageThreshold.objects.create(name='Test_View_PartStorageThreshold_{}'.format(i), partstorage=PartStorage.objects.last(), threshold=5, operator=OPERATORS[l][0])
        EmailRecipient.objects.get_or_create(name='Test_View_EmailRecipient',email='support@zephyruscomputing.com')
        for i in range(0,4):
            j = randint(65,69)
            PartStorageThresholdNotify.objects.get_or_create(partstoragethreshold=PartStorageThreshold.objects.get(name='Test_View_PartStorageThreshold_{}'.format(j)),emailrecipient=EmailRecipient.objects.first())
        self.baseurl = baseurl + 'partstoragethresholdnotify/'

    def test_view_partstoragethresholdnotify_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_partstoragethresholdnotify')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_partstoragethresholdnotify_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_partstoragethresholdnotify')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        self.client.get('{}create/?partstoragethreshold={}'.format(self.baseurl, PartStorageThreshold.objects.last().id))
        self.client.get('{}create/?emailrecipient={}'.format(self.baseurl, EmailRecipient.objects.last().id))
        pstn_list = PartStorageThresholdNotify.objects.all()
        ids = []
        for pstn in pstn_list:
            ids.append(pstn.partstoragethreshold.id)
        pst = PartStorageThreshold.objects.exclude(id__in=ids)[0]
        self.client.get('{}create/?partstoragethreshold={}&emailrecipient={}'.format(self.baseurl, pst.id, EmailRecipient.objects.last().id))
        data = {'partstoragethreshold': pst.id, 'emailrecipient': EmailRecipient.objects.first().id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'emailrecipient': EmailRecipient.objects.first().id}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_partstoragethresholdnotify_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_partstoragethresholdnotify')
        pstn = PartStorageThresholdNotify.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, pstn.id), permission)

class PartStorageThresholdTemplateAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1171,1181):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
        for i in range(70,75):
            j = randint(1171,1180)
            k = randint(1171,1180)
            l = randint(0,5)
            PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j)), storage=Storage.objects.get(name='Test_View_Storage_{}'.format(k)), count=12)
            PartStorageThreshold.objects.create(name='Test_View_PartStorageThreshold_{}'.format(i), partstorage=PartStorage.objects.last(), threshold=5, operator=OPERATORS[l][0])
        EmailTemplate.objects.get_or_create(name='Test_View_EmailTemplate', subject='Test View EmailTemplate', template='/media/tracker/templates/TestTemplate.html')
        for i in range(0,4):
            j = randint(70,74)
            PartStorageThresholdTemplate.objects.get_or_create(partstoragethreshold=PartStorageThreshold.objects.get(name='Test_View_PartStorageThreshold_{}'.format(j)),emailtemplate=EmailTemplate.objects.first())
        self.baseurl = baseurl + 'partstoragethresholdtemplate/'

    def test_view_partstoragethresholdtemplate_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_partstoragethresholdtemplate')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_partstoragethresholdtemplate_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_partstoragethresholdtemplate')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        self.client.get('{}create/?partstoragethreshold={}'.format(self.baseurl, PartStorageThreshold.objects.last().id))
        self.client.get('{}create/?emailtemplate={}'.format(self.baseurl, EmailTemplate.objects.last().id))
        pstt_list = PartStorageThresholdTemplate.objects.all()
        ids = []
        for pstt in pstt_list:
            ids.append(pstt.partstoragethreshold.id)
        pst = PartStorageThreshold.objects.exclude(id__in=ids)[0]
        self.client.get('{}create/?partstoragethreshold={}&emailtemplate={}'.format(self.baseurl, pst.id, EmailTemplate.objects.last().id))
        data = {'partstoragethreshold': pst.id, 'emailtemplate': EmailTemplate.objects.first().id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'emailtemplate': EmailTemplate.objects.first().id}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_partstoragethresholdtemplate_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_partstoragethresholdtemplate')
        pstt = PartStorageThresholdTemplate.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, pstt.id), permission)

class KitThresholdAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1211,1221):
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a view TestCase test kit', sku=str(i), price=10.00)
        for i in range(75,80):
            j = randint(1211,1220)
            k = randint(0,5)
            KitThreshold.objects.create(name='Test_View_KitThreshold_{}'.format(i), kit=Kit.objects.get(name='Test_View_Kit_{}'.format(j)), threshold=5, operator=OPERATORS[k][0])
        self.baseurl = baseurl + 'kitthreshold/'

    def test_view_kittthreshold_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_kitthreshold')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_kitthreshold_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_kitthreshold')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        self.client.get('{}create/?kit={}'.format(self.baseurl, Kit.objects.last().id))
        kt_list = KitThreshold.objects.all()
        ids = []
        for kt in kt_list:
            ids.append(kt.kit.id)
        k = Kit.objects.exclude(id__in=ids)[0]
        data = {'kit': k.id, 'threshold': 3, 'operator':'<', 'name':'Test_View_KitThreshold_New'}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_kitthreshold_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_kitthreshold')
        kt = KitThreshold.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, kt.id), permission, 'tracker/admin/update.html')
        data = {'name':'Test_View_KitThreshold_Modified', 'kit':kt.kit.id, 'threshold':kt.threshold, 'operator':kt.operator}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, kt.id), data, permission)
        data = {'name':'Test_View_KitThreshold_Modified', 'partstorage':kt.kit}
        self.client.post('{}change/{}/'.format(self.baseurl, kt.id), data)

    def test_view_kitthreshold_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_kitthreshold')
        kt = KitThreshold.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, kt.id), permission)

class KitThresholdNotifyAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1221,1231):
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a view TestCase test kit', sku=str(i), price=10.00)
        for i in range(80,85):
            j = randint(1221,1230)
            k = randint(0,5)
            KitThreshold.objects.create(name='Test_View_KitThreshold_{}'.format(i), kit=Kit.objects.get(name='Test_View_Kit_{}'.format(j)), threshold=5, operator=OPERATORS[k][0])
        EmailRecipient.objects.get_or_create(name='Test_View_EmailRecipient',email='support@zephyruscomputing.com')
        for i in range(0,4):
            j = randint(80,84)
            KitThresholdNotify.objects.get_or_create(kitthreshold=KitThreshold.objects.get(name='Test_View_KitThreshold_{}'.format(j)),emailrecipient=EmailRecipient.objects.first())
        self.baseurl = baseurl + 'kitthresholdnotify/'

    def test_view_kitthresholdnotify_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_kitthresholdnotify')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_kitthresholdnotify_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_kitthresholdnotify')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        self.client.get('{}create/?kitthreshold={}'.format(self.baseurl, KitThreshold.objects.last().id))
        self.client.get('{}create/?emailrecipient={}'.format(self.baseurl, EmailRecipient.objects.last().id))
        ktn_list = KitThresholdNotify.objects.all()
        ids = []
        for ktn in ktn_list:
            ids.append(ktn.kitthreshold.id)
        kt = KitThreshold.objects.exclude(id__in=ids)[0]
        self.client.get('{}create/?kitthreshold={}&emailrecipient={}'.format(self.baseurl, kt.id, EmailRecipient.objects.last().id))
        data = {'kitthreshold': kt.id, 'emailrecipient': EmailRecipient.objects.first().id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'emailrecipient': EmailRecipient.objects.first().id}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_kitthresholdnotify_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_kitthresholdnotify')
        ktn = KitThresholdNotify.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, ktn.id), permission)

class KitThresholdTemplateAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1231,1241):
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a view TestCase test kit', sku=str(i), price=10.00)
        for i in range(85,90):
            j = randint(1231,1240)
            k = randint(0,5)
            KitThreshold.objects.create(name='Test_View_KitThreshold_{}'.format(i), kit=Kit.objects.get(name='Test_View_Kit_{}'.format(j)), threshold=5, operator=OPERATORS[k][0])
        EmailTemplate.objects.get_or_create(name='Test_View_EmailTemplate', subject='Test View EmailTemplate', template='/media/tracker/templates/TestTemplate.html')
        for i in range(0,4):
            j = randint(85,89)
            KitThresholdTemplate.objects.get_or_create(kitthreshold=KitThreshold.objects.get(name='Test_View_KitThreshold_{}'.format(j)),emailtemplate=EmailTemplate.objects.first())
        self.baseurl = baseurl + 'kitthresholdtemplate/'

    def test_view_kitthresholdtemplate_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_kitthresholdtemplate')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_kitthresholdtemplate_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_kitthresholdtemplate')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        self.client.get('{}create/?kitthreshold={}'.format(self.baseurl, KitThreshold.objects.last().id))
        self.client.get('{}create/?emailtemplate={}'.format(self.baseurl, EmailTemplate.objects.last().id))
        ktt_list = KitThresholdTemplate.objects.all()
        ids = []
        for ktt in ktt_list:
            ids.append(ktt.kitthreshold.id)
        kt = KitThreshold.objects.exclude(id__in=ids)[0]
        self.client.get('{}create/?kitthreshold={}&emailtemplate={}'.format(self.baseurl, kt.id, EmailTemplate.objects.last().id))
        data = {'kitthreshold': kt.id, 'emailtemplate': EmailTemplate.objects.first().id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'emailtemplate': EmailTemplate.objects.first().id}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_kitthresholdtemplate_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_kitthresholdtemplate')
        ktt = KitThresholdTemplate.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, ktt.id), permission)

class ObjectActionAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1181,1191):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=12)
            AlternateSKU.objects.create(sku=str(i).zfill(6), manufacturer='Zephyrus Computing, LLC')
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            if i != 1181:
                Assembly.objects.create(parent=Part.objects.first(), part=Part.objects.last(), count=1)

        for i in range(15,20):
            j = randint(1181,1190)
            k = OBJTYPES[randint(0,5)][0]
            l = ACTIONS[randint(0,4)][0]
            m = objtypeid(k, j)
            ObjectAction.objects.create(name='Test_View_ObjectAction_{}'.format(i), objtype=k, action=l, objid=m)
        self.baseurl = baseurl + 'objectaction/'

    def test_view_objectaction_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_objectaction')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_objectaction_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_objectaction')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        k = OBJTYPES[randint(0,5)][0]
        self.client.get('{}create/?type={}'.format(self.baseurl, k))
        l = ACTIONS[randint(0,4)][0]
        self.client.get('{}create/?action={}'.format(self.baseurl, l))
        m = objtypeid(k, randint(1181,1190))
        self.client.get('{}create/?id={}'.format(self.baseurl, m))
        data = {'name':'Test_View_ObjectAction_New', 'objtype': k, 'action': l, 'objid': m}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_objectaction_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_objectaction')
        oa = ObjectAction.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, oa.id), permission, 'tracker/admin/update.html')
        data = {'name':'Test_View_ObjectAction_Modified', 'action':oa.action, 'objid':oa.objid, 'objtype':oa.objtype}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, oa.id), data, permission)
        data = {'name':'Test_View_ObjectAction_Modified', 'action':-1}
        self.client.post('{}change/{}/'.format(self.baseurl, oa.id), data)

    def test_view_objectaction_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_objectaction')
        oa = ObjectAction.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, oa.id), permission)

class ObjectActionNotifyAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1191,1201):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=12)
            AlternateSKU.objects.create(sku=str(i).zfill(6), manufacturer='Zephyrus Computing, LLC')
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            if i != 1191:
                Assembly.objects.create(parent=Part.objects.first(), part=Part.objects.last(), count=1)

        for i in range(20,25):
            j = randint(1191,1200)
            k = OBJTYPES[randint(0,5)][0]
            l = ACTIONS[randint(0,4)][0]
            m = objtypeid(k, j)
            ObjectAction.objects.create(name='Test_View_ObjectAction_{}'.format(i), objtype=k, action=l, objid=m)
        EmailRecipient.objects.get_or_create(name='Test_View_EmailRecipient',email='support@zephyruscomputing.com')
        for i in range(0,4):
            ObjectActionNotify.objects.create(objectaction=ObjectAction.objects.all()[i], emailrecipient=EmailRecipient.objects.first())
        self.baseurl = baseurl + 'objectactionnotify/'

    def test_view_objectactionnotify_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_objectactionnotify')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_objectactionnotify_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_objectactionnotify')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        self.client.get('{}create/?objectaction={}'.format(self.baseurl, ObjectAction.objects.last().id))
        self.client.get('{}create/?emailrecipient={}'.format(self.baseurl, EmailRecipient.objects.last().id))
        oan_list = ObjectActionNotify.objects.all()
        ids = []
        for oan in oan_list:
            ids.append(oan.objectaction.id)
        oa = ObjectAction.objects.exclude(id__in=ids)[0]
        self.client.get('{}create/?objectaction={}&emailrecipient={}'.format(self.baseurl, oa.id, EmailRecipient.objects.last().id))
        data = {'objectaction': oa.id, 'emailrecipient': EmailRecipient.objects.last().id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'emailrecipient': EmailRecipient.objects.first().id}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_objectactionnotify_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_objectactionnotify')
        oan = ObjectActionNotify.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, oan.id), permission)

class ObjectActionTemplateAdminViewTests(TestCase):
    def setUp(self):
        for i in range(1201,1211):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=12)
            AlternateSKU.objects.create(sku=str(i).zfill(6), manufacturer='Zephyrus Computing, LLC')
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            if i != 1201:
                Assembly.objects.create(parent=Part.objects.first(), part=Part.objects.last(), count=1)

        for i in range(25,30):
            j = randint(1201,1210)
            k = OBJTYPES[randint(0,5)][0]
            l = ACTIONS[randint(0,4)][0]
            m = objtypeid(k, j)
            ObjectAction.objects.create(name='Test_View_ObjectAction_{}'.format(i), objtype=k, action=l, objid=m)
        EmailTemplate.objects.get_or_create(name='Test_View_EmailTemplate', subject='Test View EmailTemplate', template='/media/tracker/templates/TestTemplate.html')
        for i in range(0,4):
            ObjectActionTemplate.objects.create(objectaction=ObjectAction.objects.all()[i], emailtemplate=EmailTemplate.objects.first())
        self.baseurl = baseurl + 'objectactiontemplate/'

    def test_view_objectactiontemplate_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_objectactiontemplate')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_objectactiontemplate_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_objectactiontemplate')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        self.client.get('{}create/?objectaction={}'.format(self.baseurl, ObjectAction.objects.last().id))
        self.client.get('{}create/?emailtemplate={}'.format(self.baseurl, EmailTemplate.objects.last().id))
        oat_list = ObjectActionTemplate.objects.all()
        ids = []
        for oat in oat_list:
            ids.append(oat.objectaction.id)
        oa = ObjectAction.objects.exclude(id__in=ids)[0]
        self.client.get('{}create/?objectaction={}&emailtemplate={}'.format(self.baseurl, oa.id, EmailTemplate.objects.last().id))
        data = {'objectaction': oa.id, 'emailtemplate': EmailTemplate.objects.last().id}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'emailtemplate': EmailTemplate.objects.first().id}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_objectactiontemplate_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_objectactiontemplate')
        oat = ObjectActionTemplate.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, oat.id), permission)

class EmailRecipientAdminViewTests(TestCase):
    def setUp(self):
        for i in range(0,11):
            EmailRecipient.objects.create(name='Test_View_EmailRecipient_{}'.format(i), email='support_{}@zephyruscomputing.com'.format(i))
        self.baseurl = baseurl + 'emailrecipient/'

    def test_view_emailrecipient_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_emailrecipient')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_emailrecipient_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_emailrecipient')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        data = {'name':'Test_View_EmailRecipient_New', 'email':'support@zephyruscomputing.com'}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_emailrecipient_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_emailrecipient')
        er = EmailRecipient.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, er.id), permission, 'tracker/admin/update.html')
        data = {'name':'Test_View_EmailRecipient_Modified', 'email':er.email}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, er.id), data, permission)
        data = {'name':'Test_View_EmailRecipient_Modified', 'subject':'banana'}
        self.client.post('{}change/{}/'.format(self.baseurl, er.id), data)

    def test_view_emailrecipient_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_emailrecipient')
        er = EmailRecipient.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, er.id), permission)

class EmailTemplateAdminViewTests(TestCase):
    def setUp(self):
        for i in range(0,11):
            EmailTemplate.objects.create(name='Test_View_EmailTemplate_{}'.format(i), subject='Test {}'.format(i), template='/media/tracker/templates/TestTemplate.html')
        self.baseurl = baseurl + 'emailtemplate/'

    def test_view_emailtemplate_view(self):
        # Test the view view
        permission = Permission.objects.get(codename='view_emailtemplate')
        TestGenerics().get(self, self.baseurl, permission, 'tracker/admin/view.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.get('{}?page=10'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_emailtemplate_create(self):
        # Test the create view
        permission = Permission.objects.get(codename='add_emailtemplate')
        TestGenerics().get(self, '{}create/'.format(self.baseurl), permission, 'tracker/admin/create.html')
        data = {'name':'Test_View_EmailTemplate_New', 'subject':'New', 'template':'/media/tracker/templates/TestTemplate.html'}
        TestGenerics().post(self, '{}create/'.format(self.baseurl), data, permission)
        data = {'name': 'banana'}
        self.client.post('{}create/'.format(self.baseurl), data)

    def test_view_emailtemplate_change(self):
        # Test the change view
        permission = Permission.objects.get(codename='change_emailtemplate')
        et = EmailTemplate.objects.last()
        TestGenerics().get(self, '{}change/{}/'.format(self.baseurl, et.id), permission, 'tracker/admin/update.html')
        data = {'name':'Test_View_EmailTemplate_Modified', 'subject':et.subject}
        TestGenerics().post(self, '{}change/{}/'.format(self.baseurl, et.id), data, permission)
        data = {'name':'Test_View_EmailTemplate_Modified', 'email':'test@zephyruscomputing.com'}
        self.client.post('{}change/{}/'.format(self.baseurl, et.id), data)

    def test_view_emailtemplate_delete(self):
        # Test the delete view
        permission = Permission.objects.get(codename='delete_emailtemplate')
        et = EmailTemplate.objects.last()
        TestGenerics().delete(self, '{}delete/{}/'.format(self.baseurl, et.id), permission)

class TestGenerics():
    def get(self, case, url, perm, temp):
        response = case.client.get(url)
        case.assertRedirects(response, '/accounts/login/?next={}'.format(url))
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertTemplateUsed(response, temp)

    def post(self, case, url, data, perm):
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.post(url, data)
        case.assertNotEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        case.assertNotEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        case.assertNotEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        case.assertNotEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        case.assertNotEqual(response.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR)

    def delete(self, case, url, perm):
        user = User.objects.get_or_create(username='testuser')[0]
        user.user_permissions.add(perm)
        case.client.force_login(user)
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertTemplateUsed(response, 'tracker/ajax_result.html')
        case.assertContains(response, 'green')

def objtypeid(objtype, rand):
    if objtype == OBJTYPES[0][0]:
        return AlternateSKU.objects.get(sku=str(rand).zfill(6)).id
    if objtype == OBJTYPES[1][0]:
        try:
            return Assembly.objects.get(part=Part.objects.get(name='Test_View_Part_{}'.format(rand))).id
        except:
            return 0
        return Assembly.objects.first().id
    if objtype == OBJTYPES[2][0]:
        return Part.objects.get(name='Test_View_Part_{}'.format(rand)).id
    if objtype == OBJTYPES[3][0]:
        return PartAlternateSKU.objects.get(part=Part.objects.get(name='Test_View_Part_{}'.format(rand))).id
    if objtype == OBJTYPES[4][0]:
        return PartStorage.objects.get(part=Part.objects.get(name='Test_View_Part_{}'.format(rand))).id
    if objtype == OBJTYPES[5][0]:
        return Storage.objects.get(name='Test_View_Storage_{}'.format(rand)).id
    return 0
