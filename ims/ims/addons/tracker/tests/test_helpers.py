import django
django.setup()
from datetime import timedelta
from django.conf import settings
from django.contrib.auth.models import User
from django.db.models import Q
from django.test import TestCase, override_settings
from django.utils.timezone import now
from os import remove
from os.path import join
from random import randint, choice
from statistics import mean, median
from inventory.models import Part, PartStorage, Storage, Assembly, AlternateSKU, PartAlternateSKU, Kit, KitPartStorage
from ims.addons.tracker.models import PartThreshold, PartStorageThreshold, PartThresholdNotify, PartStorageThresholdNotify, PartThresholdTemplate, PartStorageThresholdTemplate, KitThreshold, KitThresholdNotify, KitThresholdTemplate, ObjectAction, ObjectActionTemplate, ObjectActionNotify, EmailRecipient, EmailTemplate, Notification, Threshold
import ims.addons.tracker.helpers as helpers
from log.models import Log



# Unit test for lookup methods
class LookupMethodsUnitTests(TestCase):
    def setUp(self):
        for i in range(1,10):
            AlternateSKU.objects.create(sku='test_sku_{}'.format(str(i).zfill(3)), manufacturer='Zephyrus Computing')
            Part.objects.create(name='test_part_{}'.format(str(i).zfill(3)), description='TestCase test part', sku=str(i).zfill(3), price=1.0, cost=0.0)
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(3)), description='TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)
            Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(3)), description='TestCase test kit', sku=str(i).zfill(3), price=1.0)
            KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.last(), count=1)
        for i in range(1,4):
            j = randint(1,len(Part.objects.all()) -1)
            Assembly.objects.create(parent=Part.objects.first(),part=Part.objects.all()[j], count=2)

    def test_get_operator(self):
        for i in helpers.OPERATORS:
            self.assertEqual(i[1], helpers.get_operator(i[0]))
        for i in ('999','411','911'):
            self.assertEqual('unknown', helpers.get_operator(i))

    def test_get_objtype(self):
        for i in helpers.OBJTYPES:
            self.assertEqual(i[1], helpers.get_objtype(i[0]))
        for i in ('large','small','round'):
            self.assertEqual('unknown', helpers.get_objtype(i))

    def test_get_action(self):
        for i in helpers.ACTIONS:
            self.assertEqual(i[1], helpers.get_action(i[0]))
        for i in ('jump','run','vault'):
            self.assertEqual('unknown', helpers.get_action(i))

    def test_get_notifytype(self):
        for i in helpers.NOTIFYTYPES:
            self.assertEqual(i[1], helpers.get_notifytype(i[0]))
        for i in ('immediate','eventually','postal'):
            self.assertEqual('unknown', helpers.get_notifytype(i))

    def test_get_status(self):
        for i in helpers.STATUSES:
            self.assertEqual(i[1], helpers.get_status(i[0]))
        for i in ('dead','alive','undead'):
            self.assertEqual('unknown', helpers.get_status(i))

    def test_get_thresholdtype(self):
        for i in helpers.NOTIFYTYPES:
            self.assertEqual(i[1], helpers.get_thresholdtype(i[0]))
        for i in ('email','post','rss'):
            self.assertEqual('unknown', helpers.get_thresholdtype(i))

    def test_get_model_choice(self):
        for i in helpers.MODEL_CHOICES:
            self.assertEqual(i[1], helpers.get_model_choice(i[0]))
        for i in ('plastic','metal','styrofoam'):
            self.assertEqual('unknown', helpers.get_thresholdtype(i))

    def test_get_timespan_type(self):
        for i in helpers.TIMESPAN_CHOICES:
            self.assertEqual(i[1], helpers.get_timespan_type(i[0]))
        for i in ('years','decades','microseconds'):
            self.assertEqual('unknown', helpers.get_timespan_type(i))

    def test_convert_name_type(self):
        for i in helpers.OBJTYPES:
            self.assertEqual(i[0], helpers.convert_name_type(i[1]))
        for i in ('l','sm','r'):
            self.assertEqual('', helpers.convert_name_type(i))

    def test_convert_name_action(self):
        for i in helpers.ACTIONS:
            self.assertEqual(i[0], helpers.convert_name_action(i[1]))
        for i in ('j','r','v'):
            self.assertEqual('', helpers.convert_name_action(i))

    def test_check_threshold(self):
        i = randint(1,100)
        j = randint(100,200)
        self.assertTrue(helpers.check_threshold(i, '<', j))
        self.assertTrue(helpers.check_threshold(i, '<=', j))
        self.assertTrue(helpers.check_threshold(j, '>', i))
        self.assertTrue(helpers.check_threshold(j, '>=', i))
        self.assertTrue(helpers.check_threshold(i, '==', i))
        self.assertTrue(helpers.check_threshold(i, '!=', j))
        i = randint(1,100)
        j = randint(100,200)
        self.assertFalse(helpers.check_threshold(i, '~', j))
        self.assertFalse(helpers.check_threshold(j, '<', i))
        self.assertFalse(helpers.check_threshold(j, '<=', i))
        self.assertFalse(helpers.check_threshold(i, '>', j))
        self.assertFalse(helpers.check_threshold(i, '>=', j))
        self.assertFalse(helpers.check_threshold(j, '==', i))
        self.assertFalse(helpers.check_threshold(i, '!=', i))

    def test_get_object(self):
        expected = AlternateSKU.objects.last()
        result = helpers.get_object('as', expected.id)
        self.assertEqual(expected, result)
        expected = Assembly.objects.last()
        result = helpers.get_object('a', expected.id)
        self.assertEqual(expected, result)
        expected = Part.objects.last()
        result = helpers.get_object('p', expected.id)
        self.assertEqual(expected, result)
        expected = Storage.objects.last()
        result = helpers.get_object('s', expected.id)
        self.assertEqual(expected, result)
        expected = PartStorage.objects.last()
        result = helpers.get_object('ps', expected.id)
        self.assertEqual(expected, result)
        expected = PartAlternateSKU.objects.last()
        result = helpers.get_object('pa', expected.id)
        self.assertEqual(expected, result)
        expected = Kit.objects.last()
        result = helpers.get_object('k', expected.id)
        self.assertEqual(expected, result)
        expected = KitPartStorage.objects.last()
        result = helpers.get_object('kp', expected.id)
        self.assertEqual(expected, result)
        result = helpers.get_object('cat', expected.id)
        self.assertEqual('', result)
        with self.assertRaises(AlternateSKU.DoesNotExist):
            helpers.get_object('as', 0)
        with self.assertRaises(Assembly.DoesNotExist):
            helpers.get_object('a', 0)
        with self.assertRaises(Part.DoesNotExist):
            helpers.get_object('p', 0)
        with self.assertRaises(Storage.DoesNotExist):
            helpers.get_object('s', 0)
        with self.assertRaises(PartStorage.DoesNotExist):
            helpers.get_object('ps', 0)
        with self.assertRaises(PartAlternateSKU.DoesNotExist):
            helpers.get_object('pa', 0)
        with self.assertRaises(Kit.DoesNotExist):
            helpers.get_object('k', 0)
        with self.assertRaises(KitPartStorage.DoesNotExist):
            helpers.get_object('kp', 0)

    def test_parse_querytype(self):
        result = helpers.parse_querytype(1)
        self.assertEqual(result, '^')
        result = helpers.parse_querytype('1')
        self.assertEqual(result, '^')
        result = helpers.parse_querytype(2)
        self.assertEqual(result, '=')
        result = helpers.parse_querytype('2')
        self.assertEqual(result, '=')
        result = helpers.parse_querytype(3)
        self.assertEqual(result, '')
        result = helpers.parse_querytype('3')
        self.assertEqual(result, '')
        result = helpers.parse_querytype('banana')
        self.assertEqual(result, '')
        result = helpers.parse_querytype([])
        self.assertEqual(result, '')
        result = helpers.parse_querytype(None)
        self.assertEqual(result, '')

class LogsAndListsUnitTests(TestCase):
    def setUp(self):
        for i in range(10,20):
            obj = AlternateSKU.objects.create(sku='test_alternatesku_{}'.format(str(i).zfill(3)), manufacturer='Zephyrus Computing')
            Log.objects.create(severity=20,module='inventory-api',user=None,message='Created AlternateSKU id {}'.format(obj.id))
            obj = Part.objects.create(name='test_part_{}'.format(str(i).zfill(3)), description='TestCase test part', sku=str(i).zfill(3), price=1.0, cost=0.0)
            Log.objects.create(severity=20,module='inventory-admin',user=None,message='Created Part id {}'.format(obj.id))
            obj = PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            Log.objects.create(severity=20,module='inventory-api',user=None,message='Created PartAlternateSKU id {}'.format(obj.id))
            obj = Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(3)), description='TestCase test storage')
            Log.objects.create(severity=20,module='inventory-api',user=None,message='Created Storage id {}'.format(obj.id))
            obj = PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)
            Log.objects.create(severity=20,module='inventory-admin',user=None,message='Created PartStorage id {}'.format(obj.id))
            obj = Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(3)), description='TestCase test kit', sku=str(i).zfill(3), price=1.0)
            Log.objects.create(severity=20,module='inventory-admin',user=None,message='Created Kit id {}'.format(obj.id))
            obj = KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.last(), count=1)
            Log.objects.create(severity=20,module='inventory-api',user=None,message='Created KitPartStorage id {}'.format(obj.id))
        for i in range(1,4):
            j = randint(1,len(Part.objects.all()) -1)
            obj = Assembly.objects.create(parent=Part.objects.first(),part=Part.objects.all()[j], count=2)
            Log.objects.create(severity=20,module='inventory-admin',user=None,message='Created Assembly id {}'.format(obj.id))

    def test_get_model_from_logs(self):
        since = now() - timedelta(hours=1)
        for model in Part, AlternateSKU, PartAlternateSKU, Storage, PartStorage, Assembly, Kit, KitPartStorage:
            logs = Log.objects.filter(Q(module__in=['inventory-admin','inventory-api']),message__istartswith='created {} '.format(model.__name__),created__gt=since)
            ids = helpers.get_model_from_logs(0,model,logs)
            self.assertIsNotNone(ids)
            self.assertGreater(len(ids),0)
            last = model.objects.last()
            bad_id = int(last.id) + 100
            log = Log.objects.create(severity=20,module='inventory-api',user=None,message='Created {} id {}'.format(model.__name__, bad_id))
            ids = helpers.get_model_from_logs(0,model,[log])
            self.assertEqual(ids,[])
        log = Log.objects.create(severity=20,module='Unknown',user=None,message='Banana Hammock')
        ids = helpers.get_model_from_logs(0,PartStorage,[log])
        self.assertEqual(ids,[])
        logs = Log.objects.filter(Q(module__in=['inventory-admin','inventory-api']),message__istartswith='created part ',created__gt=since)
        ids = helpers.get_model_from_logs(0,'Unknown',logs)
        self.assertEqual(ids,[])

    def test_recent_adds_list(self):
        for model in 'p','s','ps','as','pa','a','k','kp':
            results = helpers.recent_adds_list(0,model,1,'h')
            self.assertIsNotNone(results)
            self.assertGreater(len(results),0)
        results = helpers.recent_adds_list(0,['p','ps','s'],1,'h')
        self.assertTrue(Part.objects.first() in results)
        self.assertTrue(PartStorage.objects.first() in results)
        self.assertTrue(Storage.objects.first() in results)
        results = helpers.recent_adds_list(0,[],1,'h')
        results = helpers.recent_adds_list(0,'u',1,'h')

    def test_recent_mods_list(self):
        for i in range(0,20):
            for model in Part, AlternateSKU, PartAlternateSKU, Storage, PartStorage, Assembly, Kit, KitPartStorage:
                if i == 0:
                    obj = model.objects.first()
                else:
                    length = model.objects.all().count()
                    obj = model.objects.all()[randint(0,length -1)]
                Log.objects.create(severity=20,module='inventory-admin',user=None,message='{} Updated with id {}'.format(model.__name__, obj.id))
        for model in 'p','s','ps','as','pa','a','k','kp':
            results = helpers.recent_mods_list(0,model,1,'h')
            self.assertIsNotNone(results)
            self.assertGreater(len(results),0)
        results = helpers.recent_mods_list(0,['p','ps','s'],1,'h')
        self.assertTrue(Part.objects.first() in results)
        self.assertTrue(PartStorage.objects.first() in results)
        self.assertTrue(Storage.objects.first() in results)
        results = helpers.recent_mods_list(0,[],1,'h')
        results = helpers.recent_mods_list(0,'u',1,'h')
    
    def test_recent_user_list(self):
        objs = ('PartDetail API','PartStorageDetail API','AssemblyDetail API','KitDetail API')
        user = User.objects.get_or_create(username='testuser')[0]
        for i in range(0,20):
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Accessed {} for id 1'.format(objs[randint(0,len(objs)-1)]))
        results = helpers.recent_user_list(0,user,1,'h')
        self.assertGreaterEqual(len(results),1)
        for log in results:
            self.assertTrue(log.user == user)
        results = helpers.recent_user_list(0,user.id,1,'h')
        self.assertGreaterEqual(len(results),1)
        for log in results:
            self.assertTrue(log.user == user)
        results = helpers.recent_user_list(0,(user.id + 100),1,'h')
        self.assertEqual(results, ('',''))
        with self.assertRaises(Exception):
            results = helpers.recent_user_list(0,'',1,'h')

class CountsUnitTests(TestCase):
    def setUp(self):
        for i in range(20,30):
            part = Part.objects.create(name='test_part_{}'.format(str(i).zfill(3)), description='TestCase test part', sku=str(i).zfill(3), price=1.0, cost=0.0)
            storage = Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(3)), description='TestCase test storage')
            ps = PartStorage.objects.create(part=part, storage=storage, count=4)
            Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 0 from PartStorage id {}; current count: {}'.format(ps.id, ps.count))
            kit = Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(3)), description='TestCase test kit', sku=str(i).zfill(3), price=1.0)
            KitPartStorage.objects.create(kit=kit, partstorage=ps, count=1)
            Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 0 from Kit id {}; current count: {}'.format(kit.id, kit.get_available_count()))
        for i in range(0,10):
            if i == 0:
                ps = PartStorage.objects.first()
                kit = Kit.objects.first()
            else:
                j = randint(0,Part.objects.all().count()-1)
                k = randint(0,Storage.objects.all().count()-1)
                l = randint(0,Kit.objects.all().count()-1)
                ps = PartStorage.objects.get_or_create(part=Part.objects.all()[j], storage=Storage.objects.all()[k], count=4)[0]
                kit = Kit.objects.all()[l]
            Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 0 from PartStorage id {}; current count: {}'.format(ps.id, ps.count))
            KitPartStorage.objects.get_or_create(kit=kit,partstorage=ps,count=1)
            Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 0 from Kit id {}; current count: {}'.format(kit.id, kit.get_available_count()))
        for i in range(0,30):
            j = randint(0,PartStorage.objects.all().count()-1)
            ps = PartStorage.objects.all()[j]
            k = randint(0,1)
            if k == 0:
                ps.count += 1
                ps.save()
                ps.refresh_from_db()
                Log.objects.create(severity=20,module='inventory',user=None,message='Added 1 to PartStorage id {}; current count: {}'.format(ps.id, ps.count))
            else:
                try:
                    ps.count -= 1
                    ps.save()
                    ps.refresh_from_db()
                    Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 1 from PartStorage id {}; current count: {}'.format(ps.id, ps.count))
                except:
                    pass
        for i in range(0,8):
            j = randint(0,Kit.objects.all().count()-1)
            kit = Kit.objects.all()[j]
            try:
                kit.subtract(1)
                kit.save()
                kit.refresh_from_db()
                Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 1 from Kit id {}; current count: {}'.format(kit.id, kit.get_available_count()))
            except:
                pass

    def test_historic_part_count(self):
        part = Part.objects.first()
        timestamps, counts = helpers.historic_part_count(0,part,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertNotEqual(int(counts[0]), 0)
        self.assertTrue(counts[len(counts) -1] >= 0)
        part = Part.objects.create(name='new_test_part',description='newly generated test part',sku='new1',price=5,cost=1)
        timestamps, counts = helpers.historic_part_count(0,part,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertEqual(int(counts[0]), 0)
        ps = PartStorage.objects.create(part=part,storage=Storage.objects.first(),count=6)
        Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 1 from PartStorage id {}; current count: {}'.format(ps.id, ps.count))
        timestamps, counts = helpers.historic_part_count(0,part,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertNotEqual(int(counts[0]), 0)
        self.assertTrue(counts[len(counts) -1] == 6)
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Subtracted None from PartStorage id {}; current count: {}'.format(ps.id, ps.count))
        timestamps, counts = helpers.historic_part_count(0,part,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertNotEqual(int(counts[0]), 0)
        self.assertTrue(counts[len(counts) -1] == 6)
        ps.delete()
        timestamps, counts = helpers.historic_part_count(0,part,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertEqual(int(counts[0]), 0)
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Subtracted 0 from PartStorage that no longer exists; current count: 0')
        timestamps, counts = helpers.historic_part_count(0,part,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertEqual(int(counts[0]), 0)

    def test_historic_kit_count(self):
        kit = Kit.objects.first()
        timestamps, counts = helpers.historic_kit_count(0,kit,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertNotEqual(int(counts[0]), 0)
        self.assertTrue(counts[len(counts) -1] >= 0)
        kit = Kit.objects.create(name='new_test_kit',description='newly generated test kit',sku='new1',price=5)
        timestamps, counts = helpers.historic_kit_count(0,kit,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertEqual(int(counts[0]), 0)
        ps = PartStorage.objects.create(part=Part.objects.last(),storage=Storage.objects.first(),count=6)
        KitPartStorage.objects.create(kit=kit,partstorage=ps,count=1)
        Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 1 from Kit id {}; current count: {}'.format(kit.id, kit.get_available_count()))
        timestamps, counts = helpers.historic_kit_count(0,kit,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertNotEqual(int(counts[0]), 0)
        self.assertTrue(counts[len(counts) -1] == 6)
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Subtracted 0 from Kit id {}; current count: None'.format(kit.get_components()[0].id, kit.get_available_count()))
        timestamps, counts = helpers.historic_kit_count(0,kit,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertNotEqual(int(counts[0]), 0)
        self.assertTrue(counts[len(counts) -1] == 6)
        ps.delete()
        timestamps, counts = helpers.historic_kit_count(0,kit,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertEqual(int(counts[0]), 0)
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Subtracted 0 from Kit id {}; current count: None'.format(kit.id))
        timestamps, counts = helpers.historic_kit_count(0,kit,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertEqual(int(counts[0]), 0)

    def test_historic_partstorage_count(self):
        ps = PartStorage.objects.first()
        timestamps, counts = helpers.historic_partstorage_count(0,ps,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertGreater(len(counts),2)
        self.assertNotEqual(int(counts[0]), 0)
        self.assertTrue(counts[len(counts) -1] >= 0)
        ps = PartStorage.objects.create(part=Part.objects.last(),storage=Storage.objects.first(),count=6)
        timestamps, counts = helpers.historic_partstorage_count(0,ps,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertNotEqual(int(counts[0]), 0)
        self.assertTrue(counts[len(counts) -1] == 6)
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Subtracted 0 from PartStorage id {}; current count: None'.format(ps.id, ps.count))
        timestamps, counts = helpers.historic_partstorage_count(0,ps,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertNotEqual(int(counts[0]), 0)
        self.assertTrue(counts[len(counts) -1] == 6)
        ps.delete()
        timestamps, counts = helpers.historic_partstorage_count(0,ps,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertNotEqual(int(counts[0]), 0)
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Subtracted 0 from PartStorage that no longer exists; current count: 0'.format(ps.id))
        timestamps, counts = helpers.historic_partstorage_count(0,ps,1,'m')
        self.assertIsNotNone(timestamps)
        self.assertIsNotNone(counts)
        self.assertNotEqual(int(counts[0]), 0)

    def test_part_counts(self):
        part = Part.objects.last()
        results = helpers.part_counts(0,part)
        self.assertEqual(results,[])
        results = helpers.part_counts(0,[part])
        self.assertNotEqual(results,[])
        parts = Part.objects.all()
        results = helpers.part_counts(0,parts)
        for part in parts:
            self.assertIn(part.name, str(results))
        results = helpers.part_counts(0,[])
        self.assertEqual(results,[])
        results = helpers.part_counts(0,'')
        self.assertEqual(results,[])
        parts = Kit.objects.all()
        results = helpers.part_counts(0,parts)
        self.assertEqual(results,[])

    def test_kit_counts(self):
        kit = Kit.objects.last()
        results = helpers.kit_counts(0,kit)
        self.assertEqual(results,[])
        results = helpers.kit_counts(0,[kit])
        self.assertNotEqual(results,[])
        kits = Kit.objects.all()
        results = helpers.kit_counts(0,kits)
        for kit in kits:
            self.assertIn(kit.name, str(results))
        results = helpers.kit_counts(0,[])
        self.assertEqual(results,[])
        results = helpers.kit_counts(0,'')
        self.assertEqual(results,[])
        kits = Part.objects.all()
        results = helpers.kit_counts(0,kits)
        self.assertEqual(results,[])

    def test_storage_counts(self):
        storage = Storage.objects.last()
        results = helpers.storage_counts(0,[storage])
        self.assertEqual(results,[])
        results = helpers.storage_counts(0,storage)
        self.assertNotEqual(results,[])
        part = PartStorage.objects.filter(storage=storage).first().part
        self.assertIn(part.name, str(results))
        results = helpers.storage_counts(0,'')
        self.assertEqual(results,[])
        results = helpers.storage_counts(0,[])
        self.assertEqual(results,[])

class GraphsUnitTests(TestCase):
    def setUp(self):
        for i in range(30,40):
            Part.objects.create(name='test_part_{}'.format(str(i).zfill(3)), description='TestCase test part', sku=str(i).zfill(3), price=1.0, cost=0.0)
            Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(3)), description='TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)
            Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(3)), description='TestCase test kit', sku=str(i).zfill(3), price=1.0)
            KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.last(), count=1)

    def test_historic_part_count_graph(self):
        part = Part.objects.first()
        result = helpers.historic_part_count_graph(0,part,1,'h')
        self.assertIn('.png',result)
        file = result.split('graphs/')[1]
        remove(join(settings.BASE_DIR, 'media/tracker/graphs/', file))

    def test_historic_kit_count_graph(self):
        kit = Kit.objects.first()
        result = helpers.historic_kit_count_graph(0,kit,1,'h')
        self.assertIn('.png',result)
        file = result.split('graphs/')[1]
        remove(join(settings.BASE_DIR, 'media/tracker/graphs/', file))

    def test_historic_partstorage_count_graph(self):
        ps = PartStorage.objects.first()
        result = helpers.historic_partstorage_count_graph(0,ps,1,'h')
        self.assertIn('.png',result)
        file = result.split('graphs/')[1]
        remove(join(settings.BASE_DIR, 'media/tracker/graphs/', file))

    def test_part_counts_graph(self):
        part = Part.objects.first()
        result = helpers.part_counts_graph(0,[part])
        self.assertIn('.png',result)
        file = result.split('graphs/')[1]
        remove(join(settings.BASE_DIR, 'media/tracker/graphs/', file))
        parts = Part.objects.all()[0:5]
        result = helpers.part_counts_graph(0,parts)
        self.assertIn('.png',result)
        file = result.split('graphs/')[1]
        remove(join(settings.BASE_DIR, 'media/tracker/graphs/', file))

    def test_kit_counts_graph(self):
        kit = Kit.objects.first()
        result = helpers.kit_counts_graph(0,[kit])
        self.assertIn('.png',result)
        file = result.split('graphs/')[1]
        remove(join(settings.BASE_DIR, 'media/tracker/graphs/', file))
        kits = Kit.objects.all()[0:5]
        result = helpers.kit_counts_graph(0,kits)
        self.assertIn('.png',result)
        file = result.split('graphs/')[1]
        remove(join(settings.BASE_DIR, 'media/tracker/graphs/', file))

    def test_storage_count_graph(self):
        storage = Storage.objects.first()
        result = helpers.storage_counts_graph(0,storage)
        self.assertIn('.png',result)
        file = result.split('graphs/')[1]
        remove(join(settings.BASE_DIR, 'media/tracker/graphs/', file))

class PrivateUnitTests(TestCase):
    def setUp(self):
        Part.objects.create(name='test_part_sum', description='TestCase test part', sku='sum123', price=1.0, cost=0.0)
        Storage.objects.create(name='test_storage_sum', description='TestCase test storage')
        PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)

    def test_get_date_formatter(self):
        for t in helpers.TIMESPAN_CHOICES:
            for i in range(1,30):
                result = helpers._get_date_formatter(i, t[0])
                if t[0] == 'm':
                    self.assertEqual(result.fmt, '%H:%M:%S')
                if t[0] == 'h':
                    if i < 25:
                        self.assertEqual(result.fmt, '%H:%M:%S')
                    else:
                        self.assertEqual(result.fmt, '%Y-%m-%d %H:%M:%S')
                if t[0] == 'd':
                    if i == 1:
                        self.assertEqual(result.fmt, '%H:%M:%S')
                    elif i < 3:
                        self.assertEqual(result.fmt, '%Y-%m-%d %H:%M:%S')
                    else:
                        self.assertEqual(result.fmt, '%Y-%m-%d')
                if t[0] == 'w':
                        self.assertEqual(result.fmt, '%Y-%m-%d')
        result = helpers._get_date_formatter('two','words')
        self.assertEqual(result.fmt, '%Y-%m-%d')

    def test_get_time_ago(self):
        rightnow = now()
        for t in helpers.TIMESPAN_CHOICES:
            i = randint(1,30)
            result = helpers._get_time_ago(i,t[0])
            if t[1] == 'Weeks':
                self.assertGreater(result,(rightnow - timedelta(weeks=i)))
                self.assertLess(result,(rightnow - timedelta(weeks=i-1)))
            if t[1] == 'Days':
                self.assertGreater(result,(rightnow - timedelta(days=i)))
                self.assertLess(result,(rightnow - timedelta(days=i-1)))
            if t[1] == 'Hours':
                self.assertGreater(result,(rightnow - timedelta(hours=i)))
                self.assertLess(result,(rightnow - timedelta(hours=i-1)))
            if t[1] == 'Minutes':
                self.assertGreater(result,(rightnow - timedelta(minutes=i)))
                self.assertLess(result,(rightnow - timedelta(minutes=i-1)))
        result = helpers._get_time_ago('two','words')
        self.assertGreater(result,(rightnow - timedelta(hours=1)))
        self.assertLess(result,rightnow)

    def test_get_sample_size(self):
        for t in helpers.TIMESPAN_CHOICES:
            for i in range(1,randint(20,30)):
                result = helpers._get_sample_size(i,t[0])
                if t[1] == 'Weeks':
                    self.assertEqual(result, 'D')
                if t[1] == 'Days':
                    if i == 1:
                        self.assertEqual(result, 'H')
                    elif i == 2:
                        self.assertEqual(result, '3H')
                    elif i == 3:
                        self.assertEqual(result, '6H')
                    else:
                        self.assertEqual(result, 'D')
                if t[1] == 'Hours':
                    if i == 1:
                        self.assertEqual(result, '5T')
                    elif i < 7:
                        self.assertEqual(result, '30T')
                    elif i < 25:
                        self.assertEqual(result, 'H')
                    else:
                        self.assertEqual(result, '3H')
                if t[1] == 'Minutes':
                    if i < 16:
                        self.assertEqual(result, '')
                    else:
                        self.assertEqual(result, '5T')
        result = helpers._get_sample_size('two','words')
        self.assertEqual(result, '')

    def test_summerize_counts(self):
        counts = [1,2,3,4]
        right_now = now().timestamp()
        timestamps = [right_now,right_now -10,right_now -20, right_now -30]
        datetimes, rcounts = helpers._summarize_counts(timestamps, counts, 1, 'm', 'max')
        self.assertEqual(counts, rcounts)
        for i in range(0, len(datetimes) -1):
            self.assertEqual(timestamps[i],datetimes[i].timestamp())
        counts = []
        timestamps = []
        for i in range(0, 60):
            counts.append(randint(0,10))
            timestamps.append((now().replace(minute=0,second=0,microsecond=0) + timedelta(minutes=i)).timestamp())
        datetimes, rcounts = helpers._summarize_counts(timestamps, counts, 1, 'h', 'max')
        for i in range(0, 12):
            self.assertEqual(rcounts[i],max(counts[(i*5):(i*5)+5]))
        datetimes, rcounts = helpers._summarize_counts(timestamps, counts, 1, 'h', 'min')
        for i in range(0, 12):
            self.assertEqual(rcounts[i],min(counts[(i*5):(i*5)+5]))
        datetimes, rcounts = helpers._summarize_counts(timestamps, counts, 1, 'h', 'mean')
        for i in range(0, 12):
            self.assertEqual(rcounts[i],mean(counts[(i*5):(i*5)+5]))
        datetimes, rcounts = helpers._summarize_counts(timestamps, counts, 1, 'h', 'median')
        for i in range(0, 12):
            self.assertEqual(rcounts[i],median(counts[(i*5):(i*5)+5]))
        
class ApiHelperTests(TestCase):
    def setUp(self):
        for i in range(0, 100):
            AlternateSKU.objects.create(sku='test_sku_{}'.format(str(i).zfill(3)), manufacturer='Zephyrus Computing')
            Part.objects.create(name='test_part_{}'.format(str(i).zfill(3)), description='TestCase test part', sku=str(i).zfill(3), price=1.0, cost=0.0)
            PartThreshold.objects.create(name='test_partthreshold_{}'.format(i), part=Part.objects.last(), threshold=1)
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(3)), description='TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)
            PartStorageThreshold.objects.create(name='test_partstoragethreshold_{}'.format(i), partstorage=PartStorage.objects.last(), threshold=1)
            Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(3)), description='TestCase test kit', sku=str(i).zfill(3), price=1.0)
            KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.last(), count=1)
            KitThreshold.objects.create(name='test_kitthreshold_{}'.format(i), kit=Kit.objects.last(), threshold=1)
        for i in range(0,15):
            for j in range(0,randint(10,30)):
                k = randint(15,99)
                Assembly.objects.create(parent=Part.objects.all()[i],part=Part.objects.all()[k], count=2)

    def test_get_search_fields(self):
        result = helpers.get_search_fields('PartThreshold')
        self.assertEqual(result, ['name','part__name','operator','alarm','threshold'])
        result = helpers.get_search_fields('PartThresholdNotify')
        self.assertEqual(result, ['partthreshold__part__name','partthreshold__name','emailrecipient__name','emailrecipient__email'])
        result = helpers.get_search_fields('PartThresholdTemplate')
        self.assertEqual(result, ['partthreshold__part__name','partthreshold__name','emailtemplate__name','emailtemplate__subject'])
        result = helpers.get_search_fields('PartStorageThreshold')
        self.assertEqual(result, ['name','partstorage__part__name','partstorage__storage__name','operator','alarm','threshold'])
        result = helpers.get_search_fields('PartStorageThresholdNotify')
        self.assertEqual(result, ['partstoragethreshold__partstorage__part__name','partstoragethreshold__partstorage__storage__name','partstoragethreshold__name','emailrecipient__name','emailrecipient__email'])
        result = helpers.get_search_fields('PartStorageThresholdTemplate')
        self.assertEqual(result, ['partstoragethreshold__partstorage__part__name','partstoragethreshold__partstorage__storage__name','partstoragethreshold__name','emailtemplate__name','emailtemplate__subject'])
        result = helpers.get_search_fields('KitThreshold')
        self.assertEqual(result, ['name','kit__name','operator','alarm','threshold'])
        result = helpers.get_search_fields('KitThresholdNotify')
        self.assertEqual(result, ['kitthreshold__kit__name','kitthreshold__name','emailrecipient__name','emailrecipient__email'])
        result = helpers.get_search_fields('KitThresholdTemplate')
        self.assertEqual(result, ['kitthreshold__kit__name','kitthreshold__name','emailtemplate__name','emailtemplate__subject'])
        result = helpers.get_search_fields('ObjectAction')
        self.assertEqual(result, ['name','objtype','action','objid'])
        result = helpers.get_search_fields('ObjectActionNotify')
        self.assertEqual(result, ['objectaction__name','emailrecipient__name','emailrecipient__email'])
        result = helpers.get_search_fields('ObjectActionTemplate')
        self.assertEqual(result, ['objectaction__name','emailtemplate__name','emailtemplate__subject'])
        result = helpers.get_search_fields('EmailRecipient')
        self.assertEqual(result, ['name','email'])
        result = helpers.get_search_fields('EmailTemplate')
        self.assertEqual(result, ['name','subject'])
    
    def test_filter_api_request(self):
        request = mockrequest(GET={'search': 'banana'},user=1)
        logmsg, objects = helpers.filter_api_request(request, 'Accessed PartThresholdList API', 'PartThreshold', PartThreshold.objects.all())
        self.assertEqual(logmsg, 'Accessed PartThresholdList API query=banana, limit={}, offset=0'.format(settings.DEFAULT_API_RESULTS))
        self.assertEqual(len(objects), 0)
        request = mockrequest(GET={'operator': '==', 'limit': 100},user=1)
        logmsg, objects = helpers.filter_api_request(request, 'Accessed PartStorageThresholdList API', 'PartStorageThreshold', PartStorageThreshold.objects.all())
        self.assertEqual(logmsg, 'Accessed PartStorageThresholdList API operator===, limit=100, offset=0')
        self.assertEqual(len(objects), 0)
        request = mockrequest(GET={'threshold': '1', 'limit': 10, 'offset': 10},user=1)
        logmsg, objects = helpers.filter_api_request(request, 'Accessed KitThresholdList API', 'KitThreshold', KitThreshold.objects.all())
        self.assertEqual(logmsg, 'Accessed KitThresholdList API threshold=1, limit=10, offset=10')
        self.assertEqual(len(objects), 10)

class mockrequest():
    GET = dict()
    user = int()

    def __init__(self, GET, user):
        self.GET=GET
        self.user=user
        