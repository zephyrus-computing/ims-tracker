import django
django.setup()
from django.conf import settings
from django.contrib.auth.models import AnonymousUser, Permission, User
from django.test import TestCase, override_settings
from random import randint
from rest_framework import status
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage
from ims.addons.tracker.models import PartThreshold, PartStorageThreshold, PartThresholdNotify, PartStorageThresholdNotify, PartThresholdTemplate, PartStorageThresholdTemplate, ObjectAction, ObjectActionTemplate, ObjectActionNotify, KitThreshold, KitThresholdNotify, KitThresholdTemplate, EmailRecipient, EmailTemplate, Notification, Threshold
from ims.addons.tracker.helpers import OPERATORS


baseurl = '/tracker/'

# Test module root view
class RootViewTests(TestCase):
    def test_view_root(self):
        response = self.client.get(baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/home.html')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get(baseurl)
            self.assertRedirects(response, '/accounts/login/?next={}'.format(baseurl))
            self.client.force_login(User.objects.get_or_create(username='testuser')[0])
            response = self.client.get(baseurl)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

# Test PartThreshold views
class PartThresholdViewTests(TestCase):
    def setUp(self):
        for i in range(1101,1111):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
        for i in range(30,35):
            j = randint(1101,1110)
            k = randint(0,5)
            PartThreshold.objects.create(name='Test_View_PartThreshold_{}'.format(i), part=Part.objects.get(name='Test_View_Part_{}'.format(j)), threshold=5, operator=OPERATORS[k][0])
        self.baseurl = baseurl + 'partthreshold/'

    def test_view_partthresholds(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/partthreshold/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('{}?page=100'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=100')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get(self.baseurl)
            self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
            user = User.objects.get_or_create(username='testuser')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
            user.user_permissions.add(Permission.objects.get(name='Can view part threshold'))
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_partthreshold_details(self):
        # Test a valid partthreshold
        pt = PartThreshold.objects.first()
        response = self.client.get('{}{}/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/partthreshold/detail.html')
        # Test an invalid partthreshold
        response = self.client.get('{}{}/'.format(self.baseurl, '-15'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('{}{}/'.format(self.baseurl, pt.id))
            self.assertRedirects(response, '/accounts/login/?next={}{}/'.format(self.baseurl, pt.id))
            user = User.objects.get_or_create(username='testuser', email='testuser@zephyruscomputing.com')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, pt.id))
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
            user.user_permissions.add(Permission.objects.get(name='Can view part threshold'))
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            EmailRecipient.objects.get_or_create(name='testuser', email='testuser@zephyruscomputing.com')
            PartThresholdNotify.objects.create(partthreshold=pt, emailrecipient=EmailRecipient.objects.last())
            response = self.client.get('{}{}/'.format(self.baseurl, pt.id))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, 'Unsubscribe')

class PartStorageThresholdViewTests(TestCase):
    def setUp(self):
        for i in range(1111,1121):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
        for i in range(35,40):
            j = randint(1111,1120)
            k = randint(1111,1120)
            l = randint(0,5)
            PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j)), storage=Storage.objects.get(name='Test_View_Storage_{}'.format(k)), count=12)
            PartStorageThreshold.objects.create(name='Test_View_PartStorageThreshold_{}'.format(i), partstorage=PartStorage.objects.last(), threshold=5, operator=OPERATORS[l][0])
        self.baseurl = baseurl + 'partstoragethreshold/'

    def test_view_partstoragethresholds(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/partstoragethreshold/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('{}?page=100'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=100')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get(self.baseurl)
            self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
            user = User.objects.get_or_create(username='testuser')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
            user.user_permissions.add(Permission.objects.get(name='Can view part storage threshold'))
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_partstoragethreshold_details(self):
        # Test a valid partstoragethreshold
        pt = PartStorageThreshold.objects.first()
        response = self.client.get('{}{}/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/partstoragethreshold/detail.html')
        # Test an invalid partthreshold
        response = self.client.get('{}{}/'.format(self.baseurl, '-15'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('{}{}/'.format(self.baseurl, pt.id))
            self.assertRedirects(response, '/accounts/login/?next={}{}/'.format(self.baseurl, pt.id))
            user = User.objects.get_or_create(username='testuser', email='testuser@zephyruscomputing.com')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, pt.id))
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
            user.user_permissions.add(Permission.objects.get(name='Can view part storage threshold'))
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            EmailRecipient.objects.get_or_create(name='testuser', email='testuser@zephyruscomputing.com')
            PartStorageThresholdNotify.objects.create(partstoragethreshold=pt, emailrecipient=EmailRecipient.objects.last())
            response = self.client.get('{}{}/'.format(self.baseurl, pt.id))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, 'Unsubscribe')

class KitThresholdViewTests(TestCase):
    def setUp(self):
        for i in range(1151,1161):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a view TestCase test kit', sku=str(i), price=10.00)
        for i in range(115,120):
            j = randint(1151,1160)
            k = randint(1151,1160)
            l = randint(0,5)
            m = randint(1151,1160)
            part = Part.objects.get(name='Test_View_Part_{}'.format(j))
            storage = Storage.objects.get(name='Test_View_Storage_{}'.format(k))
            ps = PartStorage.objects.create(part=part, storage=storage, count=12)
            kit = Kit.objects.get(name='Test_View_Kit_{}'.format(m))
            KitPartStorage.objects.create(kit=kit, partstorage=ps, count=2)
            KitThreshold.objects.create(name='Test_View_KitThreshold_{}'.format(i), kit=kit, threshold=5, operator=OPERATORS[l][0])
        self.baseurl = baseurl + 'kitthreshold/'

    def test_view_kitthresholds(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/kitthreshold/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('{}?page=100'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=100')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get(self.baseurl)
            self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
            user = User.objects.get_or_create(username='testuser')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
            user.user_permissions.add(Permission.objects.get(name='Can view kit threshold'))
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_kitthreshold_details(self):
        # Test a valid partstoragethreshold
        kt = KitThreshold.objects.first()
        response = self.client.get('{}{}/'.format(self.baseurl, kt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/kitthreshold/detail.html')
        # Test an invalid partthreshold
        response = self.client.get('{}{}/'.format(self.baseurl, '-15'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('{}{}/'.format(self.baseurl, kt.id))
            self.assertRedirects(response, '/accounts/login/?next={}{}/'.format(self.baseurl, kt.id))
            user = User.objects.get_or_create(username='testuser', email='testuser@zephyruscomputing.com')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, kt.id))
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
            user.user_permissions.add(Permission.objects.get(name='Can view kit threshold'))
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            EmailRecipient.objects.get_or_create(name='testuser', email='testuser@zephyruscomputing.com')
            KitThresholdNotify.objects.create(kitthreshold=kt, emailrecipient=EmailRecipient.objects.last())
            response = self.client.get('{}{}/'.format(self.baseurl, kt.id))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, 'Unsubscribe')

class ObjectActionViewTests(TestCase):
    def setUp(self):
        for i in ['ps','p','a','as','k','kp','s']:
            ObjectAction.objects.create(name='{} c 0'.format(i), objtype=i, action='c', objid=0)
            ObjectAction.objects.create(name='{} d 0'.format(i), objtype=i, action='d', objid=0)
        self.baseurl = baseurl + 'objectaction/'

    def test_view_objectaction(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/objectaction/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('{}?page=100'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=100')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get(self.baseurl)
            self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
            user = User.objects.get_or_create(username='testuser')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
            user.user_permissions.add(Permission.objects.get(name='Can view object action'))
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_objectaction_details(self):
        # Test a valid objectaction
        oa = ObjectAction.objects.first()
        response = self.client.get('{}{}/'.format(self.baseurl, oa.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/objectaction/detail.html')
        # Test an invalid partthreshold
        response = self.client.get('{}{}/'.format(self.baseurl, '-15'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('{}{}/'.format(self.baseurl, oa.id))
            self.assertRedirects(response, '/accounts/login/?next={}{}/'.format(self.baseurl, oa.id))
            user = User.objects.get_or_create(username='testuser', email='testuser@zephyruscomputing.com')[0]
            self.client.force_login(user)
            response = self.client.get('{}{}/'.format(self.baseurl, oa.id))
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
            user.user_permissions.add(Permission.objects.get(name='Can view object action'))
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            EmailRecipient.objects.get_or_create(name='testuser', email='testuser@zephyruscomputing.com')
            ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.last())
            response = self.client.get('{}{}/'.format(self.baseurl, oa.id))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, 'Unsubscribe')

class UserProfileViewTests(TestCase):
    def setUp(self):
        er = EmailRecipient.objects.get_or_create(name='testuser', email='testuser@zephyruscomputing.com')[0]
        for i in range(1121,1131):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
        for i in range(100,105):
            j = randint(1121,1130)
            k = randint(1121,1130)
            l = randint(0,5)
            m = randint(1121,1130)
            p = Part.objects.get(name='Test_View_Part_{}'.format(j))
            s = Storage.objects.get(name='Test_View_Storage_{}'.format(k))
            PartStorage.objects.create(part=p, storage=s, count=12)
            PartStorageThreshold.objects.create(name='Test_View_PartStorageThreshold_{}'.format(i), partstorage=PartStorage.objects.last(), threshold=5, operator=OPERATORS[l][0])
            PartThreshold.objects.create(name='Test_View_PartThreshold_{}'.format(i), part=Part.objects.get(name='Test_View_Part_{}'.format(m)), threshold=5, operator=OPERATORS[l][0])
            ObjectAction.objects.create(name='Test_View_ObjectAction_P{}'.format(i), objid=p.id, objtype='p', action='d')
            ObjectAction.objects.create(name='Test_View_ObjectAction_S{}'.format(i), objid=s.id, objtype='s', action='d')
        PartThresholdNotify.objects.create(partthreshold=PartThreshold.objects.all()[randint(0,2)], emailrecipient=er)
        PartStorageThresholdNotify.objects.create(partstoragethreshold=PartStorageThreshold.objects.all()[randint(0,2)], emailrecipient=er)
        ObjectActionNotify.objects.create(objectaction=ObjectAction.objects.all()[randint(0,4)], emailrecipient=er)
        self.baseurl = '/accounts/profile/'

    def test_view_profile(self):
        user = User.objects.get_or_create(username='testuser')[0]
        response = self.client.get(self.baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
        self.client.force_login(user) 
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        user.mail = 'testuser@zephyruscomputing.com'
        user.save()
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

class EmailRecipientViewTests(TestCase):
    def setUp(self):
        er = EmailRecipient.objects.get_or_create(name='testuser', email='testuser@zephyruscomputing.com')[0]
        for i in range(1131,1141):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
        for i in range(105,110):
            j = randint(1131,1140)
            k = randint(1131,1140)
            l = randint(0,5)
            m = randint(1131,1140)
            p = Part.objects.get(name='Test_View_Part_{}'.format(j))
            s = Storage.objects.get(name='Test_View_Storage_{}'.format(k))
            PartStorage.objects.create(part=p, storage=s, count=12)
            PartStorageThreshold.objects.create(name='Test_View_PartStorageThreshold_{}'.format(i), partstorage=PartStorage.objects.last(), threshold=5, operator=OPERATORS[l][0])
            PartThreshold.objects.create(name='Test_View_PartThreshold_{}'.format(i), part=Part.objects.get(name='Test_View_Part_{}'.format(m)), threshold=5, operator=OPERATORS[l][0])
            ObjectAction.objects.create(name='Test_View_ObjectAction_P{}'.format(i), objid=p.id, objtype='p', action='d')
            ObjectAction.objects.create(name='Test_View_ObjectAction_S{}'.format(i), objid=s.id, objtype='s', action='d')
        PartThresholdNotify.objects.create(partthreshold=PartThreshold.objects.all()[randint(0,2)], emailrecipient=er)
        PartStorageThresholdNotify.objects.create(partstoragethreshold=PartStorageThreshold.objects.all()[randint(0,2)], emailrecipient=er)
        ObjectActionNotify.objects.create(objectaction=ObjectAction.objects.all()[randint(0,4)], emailrecipient=er)
        self.baseurl = baseurl + 'emailrecipient/'

    def test_view_emailrecipients(self):
        # Test the default view
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/emailrecipient/list.html')
        # Test the pagination of the view
        response = self.client.get('{}?page=-1'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=-1')
        response = self.client.get('{}?page=100'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'page=100')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.request.get('QUERY_STRING'), 'q=test')
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get(self.baseurl)
            self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))
            user = User.objects.get_or_create(username='testuser')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_302_FOUND)
            user.user_permissions.add(Permission.objects.get(name='Can view email recipient'))
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_view_emailrecipient_detail(self):
        # Test a valid emailrecipient
        er = EmailRecipient.objects.first()
        response = self.client.get('{}{}/'.format(self.baseurl, er.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/emailrecipient/detail.html')
        # Test an invalid emailrecipient
        response = self.client.get('{}{}/'.format(self.baseurl, '-15'))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Anonymous Access Restriction
        with override_settings(ALLOW_ANONYMOUS = False):
            response = self.client.get('{}{}/'.format(self.baseurl, er.id))
            self.assertRedirects(response, '/accounts/login/?next={}{}/'.format(self.baseurl, er.id))
            user = User.objects.get_or_create(username='testuser',email='testuser@zephyruscomputing.com')[0]
            self.client.force_login(user)
            user.user_permissions.add(Permission.objects.get(name='Can view email recipient'))
            response = self.client.get('{}{}/'.format(self.baseurl, er.id))
            self.assertEqual(response.status_code, status.HTTP_200_OK)

class AjaxViewTests(TestCase):
    def setUp(self):
        er = EmailRecipient.objects.get_or_create(name='testuser', email='testuser@zephyruscomputing.com')[0]
        for i in range(1141,1151):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is a view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is a view TestCase test storage')
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is a view TestCase test kit', sku=str(i), price=10.00)
        for i in range(110,115):
            j = randint(1141,1150)
            k = randint(1141,1150)
            l = randint(0,5)
            m = randint(1141,1150)
            p = Part.objects.get(name='Test_View_Part_{}'.format(j))
            s = Storage.objects.get(name='Test_View_Storage_{}'.format(k))
            k = Kit.objects.get(name='Test_View_Kit_{}'.format(m))
            ps = PartStorage.objects.create(part=p, storage=s, count=12)
            KitPartStorage.objects.create(kit=k, partstorage=ps, count=2)
            PartStorageThreshold.objects.create(name='Test_View_PartStorageThreshold_{}'.format(i), partstorage=PartStorage.objects.last(), threshold=5, operator=OPERATORS[l][0])
            PartThreshold.objects.create(name='Test_View_PartThreshold_{}'.format(i), part=Part.objects.get(name='Test_View_Part_{}'.format(m)), threshold=5, operator=OPERATORS[l][0])
            KitThreshold.objects.create(name='Test_View_KitThreshold_{}'.format(i), kit=k, threshold=5, operator=OPERATORS[l][0])
            ObjectAction.objects.create(name='Test_View_ObjectAction_P{}'.format(i), objid=p.id, objtype='p', action='d')
            ObjectAction.objects.create(name='Test_View_ObjectAction_S{}'.format(i), objid=s.id, objtype='s', action='d')
            ObjectAction.objects.create(name='Test_View_ObjectAction_K{}'.format(i), objid=k.id, objtype='k', action='d')
        PartThresholdNotify.objects.create(partthreshold=PartThreshold.objects.all()[randint(0,2)], emailrecipient=er)
        PartStorageThresholdNotify.objects.create(partstoragethreshold=PartStorageThreshold.objects.all()[randint(0,2)], emailrecipient=er)
        KitThresholdNotify.objects.create(kitthreshold=KitThreshold.objects.all()[randint(0,2)], emailrecipient=er)
        ObjectActionNotify.objects.create(objectaction=ObjectAction.objects.all()[randint(0,4)], emailrecipient=er)
        user = User.objects.get_or_create(username='testuser',email='testuser@zephyruscomputing.com')[0]
        for p in ('part threshold','part storage threshold','kit threshold','object action'):
            perm = Permission.objects.get(name='Can view {}'.format(p))
            user.user_permissions.add(perm.id)
        User.objects.get_or_create(username='testuser2')
        user = User.objects.get_or_create(username='teststaff',email='teststaff@zephyruscomputing.com',is_staff=True)[0]
        for p in ('part threshold notify','part storage threshold notify','kit threshold notify','object action notify'):
            perm = Permission.objects.get(name='Can delete {}'.format(p))
            user.user_permissions.add(perm.id)
        self.baseurl = baseurl
        
    def test_view_partthreshold_subscribe(self):
        ptn_list = PartThresholdNotify.objects.all()
        ids = []
        for ptn in ptn_list:
            ids.append(ptn.partthreshold.id)
        pt = PartThreshold.objects.exclude(id__in=ids)[0]
        response = self.client.get('{}partthreshold/{}/subscribe/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}partthreshold/{}/subscribe/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}partthreshold/{}/subscribe/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser2'))
        response = self.client.get('{}partthreshold/{}/subscribe/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

    def test_view_partthreshold_unsubscribe(self):
        ptn = PartThresholdNotify.objects.first()
        pt = ptn.partthreshold
        response = self.client.get('{}partthreshold/{}/unsubscribe/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}partthreshold/{}/unsubscribe/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}partthreshold/{}/unsubscribe/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser2'))
        response = self.client.get('{}partthreshold/{}/unsubscribe/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

    def test_view_partstoragethreshold_subscribe(self):
        pstn_list = PartStorageThresholdNotify.objects.all()
        ids = []
        for pstn in pstn_list:
            ids.append(pstn.partstoragethreshold.id)
        pst = PartStorageThreshold.objects.exclude(id__in=ids)[0]
        response = self.client.get('{}partstoragethreshold/{}/subscribe/'.format(self.baseurl, pst.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}partstoragethreshold/{}/subscribe/'.format(self.baseurl, pst.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}partstoragethreshold/{}/subscribe/'.format(self.baseurl, pst.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser2'))
        response = self.client.get('{}partstoragethreshold/{}/subscribe/'.format(self.baseurl, pst.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

    def test_view_partstoragethreshold_unsubscribe(self):
        pstn = PartStorageThresholdNotify.objects.first()
        pst = pstn.partstoragethreshold
        response = self.client.get('{}partstoragethreshold/{}/unsubscribe/'.format(self.baseurl, pst.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}partstoragethreshold/{}/unsubscribe/'.format(self.baseurl, pst.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}partstoragethreshold/{}/unsubscribe/'.format(self.baseurl, pst.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser2'))
        response = self.client.get('{}partstoragethreshold/{}/unsubscribe/'.format(self.baseurl, pst.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

    def test_view_kitthreshold_subscribe(self):
        ktn_list = KitThresholdNotify.objects.all()
        ids = []
        for ktn in ktn_list:
            ids.append(ktn.kitthreshold.id)
        kt = KitThreshold.objects.exclude(id__in=ids)[0]
        response = self.client.get('{}kitthreshold/{}/subscribe/'.format(self.baseurl, kt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}kitthreshold/{}/subscribe/'.format(self.baseurl, kt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}kitthreshold/{}/subscribe/'.format(self.baseurl, kt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser2'))
        response = self.client.get('{}kitthreshold/{}/subscribe/'.format(self.baseurl, kt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

    def test_view_kitthreshold_unsubscribe(self):
        ktn = KitThresholdNotify.objects.first()
        kt = ktn.kitthreshold
        response = self.client.get('{}kitthreshold/{}/unsubscribe/'.format(self.baseurl, kt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}kitthreshold/{}/unsubscribe/'.format(self.baseurl, kt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}kitthreshold/{}/unsubscribe/'.format(self.baseurl, kt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser2'))
        response = self.client.get('{}kitthreshold/{}/unsubscribe/'.format(self.baseurl, kt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

    def test_view_objectaction_subscribe(self):
        oan_list = ObjectActionNotify.objects.all()
        ids = []
        for oan in oan_list:
            ids.append(oan.objectaction.id)
        oa = ObjectAction.objects.exclude(id__in=ids)[0]
        response = self.client.get('{}objectaction/{}/subscribe/'.format(self.baseurl, oa.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}objectaction/{}/subscribe/'.format(self.baseurl, oa.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}objectaction/{}/subscribe/'.format(self.baseurl, oa.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser2'))
        response = self.client.get('{}objectaction/{}/subscribe/'.format(self.baseurl, oa.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

    def test_view_objectaction_unsubscribe(self):
        oan = ObjectActionNotify.objects.first()
        oa = oan.objectaction
        response = self.client.get('{}objectaction/{}/unsubscribe/'.format(self.baseurl, oa.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}objectaction/{}/unsubscribe/'.format(self.baseurl, oa.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}objectaction/{}/unsubscribe/'.format(self.baseurl, oa.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser2'))
        response = self.client.get('{}objectaction/{}/unsubscribe/'.format(self.baseurl, oa.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')

    def test_view_partthresholdnotify_delete(self):
        ptn = PartThresholdNotify.objects.first()
        response = self.client.get('{}partthresholdnotify/{}/delete/'.format(self.baseurl, ptn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}partthresholdnotify/{}/delete/'.format(self.baseurl, ptn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}partthresholdnotify/{}/delete/'.format(self.baseurl, ptn.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        ptn.save()
        self.client.force_login(User.objects.get(username='teststaff'))
        response = self.client.get('{}partthresholdnotify/{}/delete/'.format(self.baseurl, ptn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')

    def test_view_partstoragethresholdnotify_delete(self):
        pstn = PartStorageThresholdNotify.objects.first()
        response = self.client.get('{}partstoragethresholdnotify/{}/delete/'.format(self.baseurl, pstn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}partstoragethresholdnotify/{}/delete/'.format(self.baseurl, pstn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}partstoragethresholdnotify/{}/delete/'.format(self.baseurl, pstn.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        pstn.save()
        self.client.force_login(User.objects.get(username='teststaff'))
        response = self.client.get('{}partstoragethresholdnotify/{}/delete/'.format(self.baseurl, pstn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')

    def test_view_kitthresholdnotify_delete(self):
        ktn = KitThresholdNotify.objects.first()
        response = self.client.get('{}kitthresholdnotify/{}/delete/'.format(self.baseurl, ktn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}kitthresholdnotify/{}/delete/'.format(self.baseurl, ktn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}kitthresholdnotify/{}/delete/'.format(self.baseurl, ktn.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        ktn.save()
        self.client.force_login(User.objects.get(username='teststaff'))
        response = self.client.get('{}kitthresholdnotify/{}/delete/'.format(self.baseurl, ktn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')

    def test_view_objectactionnotify_delete(self):
        oan = ObjectActionNotify.objects.first()
        response = self.client.get('{}objectactionnotify/{}/delete/'.format(self.baseurl, oan.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'red')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}objectactionnotify/{}/delete/'.format(self.baseurl, oan.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')
        response = self.client.get('{}objectactionnotify/{}/delete/'.format(self.baseurl, oan.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        oan.save()
        self.client.force_login(User.objects.get(username='teststaff'))
        response = self.client.get('{}objectactionnotify/{}/delete/'.format(self.baseurl, oan.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'green')

    def test_ajax_profile(self):
        response = self.client.get('{}profile/'.format(baseurl))
        self.assertRedirects(response, '/accounts/login/?next={}profile/'.format(baseurl))
        self.client.force_login(User.objects.get(username='testuser2'))
        response = self.client.get('{}profile/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/ajax_profile.html')
        self.client.force_login(User.objects.get(username='testuser'))
        response = self.client.get('{}profile/'.format(baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/ajax_profile.html')
