import django
import json
django.setup()
from django.conf import settings
from django.contrib.auth.models import AnonymousUser, Permission, User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase, override_settings
from io import BytesIO
from os import remove
from random import randint
from rest_framework import status

from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage
from inventory.api.serializers import PartSerializer, PartStorageFullSerializer, KitSerializer
from ims.addons.tracker.models import PartThreshold, PartStorageThreshold, PartThresholdNotify, PartStorageThresholdNotify, PartThresholdTemplate, PartStorageThresholdTemplate, ObjectAction, ObjectActionTemplate, ObjectActionNotify, KitThreshold, KitThresholdNotify, KitThresholdTemplate, EmailRecipient, EmailTemplate, Notification, Threshold
from ims.addons.tracker.helpers import OPERATORS, OBJTYPES, ACTIONS
from ims.addons.tracker.api.serializers import EmailRecipientSerializer, EmailTemplateSerializer, PartThresholdSerializer, PartThresholdReadSerializer, PartStorageThresholdSerializer, PartStorageThresholdReadSerializer, PartThresholdNotifySerializer, PartThresholdTemplateSerializer, PartStorageThresholdNotifySerializer, PartStorageThresholdTemplateSerializer, KitThresholdSerializer, KitThresholdNotifySerializer, KitThresholdTemplateSerializer, ObjectActionSerializer, ObjectActionNotifySerializer, ObjectActionTemplateSerializer, ThresholdSerializer, NotificationSerializer

api_version = 'v1'
api_module = 'tracker'

api_baseurl = '/api/{}/{}/'.format(api_version, api_module)

# Test Tracker API PartThreshold View
class TrackerApiPartThresholdViewTests(TestCase):
    def setUp(self):
        for i in range(1000,1011):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
        for i in range(0,5):
            j = randint(1000,1010)
            k = randint(0,5)
            PartThreshold.objects.create(name='Test_View_PartThreshold_{}'.format(i), part=Part.objects.get(name='Test_View_Part_{}'.format(j)), threshold=5, operator=OPERATORS[k][0])
        self.baseurl = api_baseurl + 'partthreshold/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part threshold','Can add part threshold','Can change part threshold','Can delete part threshold']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_partthreshold_get(self):
        # Get Object
        pt = PartThreshold.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, pt)
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(pt.part).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?search={}'.format(self.baseurl, pt.name[-15:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(pt.part).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?search={}'.format(self.baseurl, pt.part.name[-9:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"part":' + json.dumps(PartSerializer(pt.part).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?name={}'.format(self.baseurl, pt.name))
        self.assertContains(response, pt.name)
        response = self.client.get('{}?part__name={}'.format(self.baseurl, pt.part.name))
        self.assertContains(response, pt.part.name)
        response = self.client.get('{}?operator={}'.format(self.baseurl, pt.operator))
        self.assertContains(response, pt.operator)
        response = self.client.get('{}?theshold={}'.format(self.baseurl, pt.threshold))
        self.assertContains(response, pt.threshold)
        response = self.client.get('{}?limit=2'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        response = self.client.get('{}?limit=2&offset=2'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        response = self.client.get('{}?limit=2&sort=name'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        self.assertContains(response, pt.name)
        response = self.client.get('{}?limit=2&sort=-name'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        self.assertNotContains(response, pt.name)
        response = self.client.get('{}?offset=10'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthreshold_post(self):
        # Get Objects and Make Data
        part = Part.objects.last()
        data = {'name': 'Test_View_PartThreshold_New', 'part': str(part.id), 'threshold': 5, 'operator': '=='}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthreshold_pk_get(self):
        # Get Object
        pt = PartThreshold.objects.first()
        # Run Generic Tests
        TestGenerics().pk_get(self, pt)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthreshold_pk_put(self):
        # Get Object
        pt = PartThreshold.objects.first()
        pt.threshold = 6
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, pt.id), PartThresholdSerializer(pt).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(pt.id), 'threshold': 0}
        response = self.client.put('{}{}/'.format(self.baseurl, pt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        pt.refresh_from_db()
        self.assertEqual(pt.threshold, 6)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.put('{}{}/'.format(self.baseurl, pt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthreshold_pk_patch(self):
        # Get Object and Make Data
        pt = PartThreshold.objects.first()
        data = {'id': pt.id, 'threshold': 4}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, pt.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': pt.id, 'part': 0}
        response = self.client.patch('{}{}/'.format(self.baseurl, pt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        pt.refresh_from_db()
        self.assertEqual(pt.threshold, 4)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, pt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthreshold_pk_delete(self):
        # Get Object
        pt = PartThreshold.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, pt)
        # Run Specific Tests
        self.assertIsNone(PartThreshold.objects.filter(id=pt.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API PartStorageThreshold View
class TrackerApiPartStorageThresholdViewTests(TestCase):
    def setUp(self):
        for i in range(1011,1021):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test storage')
        for i in range(5,10):
            j = randint(1011,1020)
            k = randint(1011,1020)
            l = randint(0,5)
            PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j)), storage=Storage.objects.get(name='Test_View_Storage_{}'.format(k)), count=12) 
            PartStorageThreshold.objects.create(name='Test_View_PartStorageThreshold_{}'.format(i), partstorage=PartStorage.objects.last(), threshold=5, operator=OPERATORS[l][0])
        self.baseurl = api_baseurl + 'partstoragethreshold/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part storage threshold','Can add part storage threshold','Can change part storage threshold','Can delete part storage threshold']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_partstoragethreshold_get(self):
        # Get Object
        pt = PartStorageThreshold.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, pt)
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"partstorage":' + json.dumps(PartStorageFullSerializer(pt.partstorage).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?search={}'.format(self.baseurl, pt.name[-15:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"partstorage":' + json.dumps(PartStorageFullSerializer(pt.partstorage).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?search={}'.format(self.baseurl, pt.partstorage.part.name[-9:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"partstorage":' + json.dumps(PartStorageFullSerializer(pt.partstorage).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?search={}'.format(self.baseurl, pt.partstorage.storage.name[-9:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"partstorage":' + json.dumps(PartStorageFullSerializer(pt.partstorage).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?name={}'.format(self.baseurl, pt.name))
        self.assertContains(response, pt.name)
        response = self.client.get('{}?partstorage__part__name={}'.format(self.baseurl, pt.partstorage.part.name))
        self.assertContains(response, pt.name)
        response = self.client.get('{}?operator={}'.format(self.baseurl, pt.operator))
        self.assertContains(response, pt.operator)
        response = self.client.get('{}?theshold={}'.format(self.baseurl, pt.threshold))
        self.assertContains(response, pt.threshold)
        response = self.client.get('{}?limit=2'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        response = self.client.get('{}?limit=2&offset=2'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        response = self.client.get('{}?limit=2&sort=name'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        self.assertContains(response, pt.name)
        response = self.client.get('{}?limit=2&sort=-name'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        self.assertNotContains(response, pt.name)
        response = self.client.get('{}?offset=10'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstoragethreshold_post(self):
        # Get Objects and Make Data
        ps = PartStorage.objects.last()
        data = {'name': 'Test_View_PartStorageThreshold_New', 'partstorage': str(ps.id), 'threshold': 5, 'operator': '=='}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstoragethreshold_pk_get(self):
        # Get Object
        pt = PartStorageThreshold.objects.first()
        # Run Generic Tests
        TestGenerics().pk_get(self, pt)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstoragethreshold_pk_put(self):
        # Get Object
        pt = PartStorageThreshold.objects.first()
        pt.threshold = 6
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, pt.id), PartStorageThresholdSerializer(pt).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(pt.id), 'threshold': 0}
        response = self.client.put('{}{}/'.format(self.baseurl, pt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        pt.refresh_from_db()
        self.assertEqual(pt.threshold, 6)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.put('{}{}/'.format(self.baseurl, pt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstoragethreshold_pk_patch(self):
        # Get Object and Make Data
        pt = PartStorageThreshold.objects.first()
        data = {'id': pt.id, 'threshold': 4}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, pt.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': pt.id, 'partstorage': 0}
        response = self.client.patch('{}{}/'.format(self.baseurl, pt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        pt.refresh_from_db()
        self.assertEqual(pt.threshold, 4)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, pt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstoragethreshold_pk_delete(self):
        # Get Object
        pt = PartStorageThreshold.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, pt)
        # Run Specific Tests
        self.assertIsNone(PartStorageThreshold.objects.filter(id=pt.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, pt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API PartThresoldNotify View
class TrackerApiPartThresholdNotifyViewTests(TestCase):
    def setUp(self):
        for i in range(1030,1041):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
        for i in range(10,15):
            j = randint(1030,1040)
            k = randint(0,5)
            PartThreshold.objects.create(name='Test_View_PartThreshold_{}'.format(i), part=Part.objects.get(name='Test_View_Part_{}'.format(j)), threshold=5, operator=OPERATORS[k][0])
        EmailRecipient.objects.get_or_create(name='Test_View_EmailRecipient',email='support@zephyruscomputing.com')
        for i in range(0,4):
            j = randint(10,14)
            PartThresholdNotify.objects.get_or_create(partthreshold=PartThreshold.objects.get(name='Test_View_PartThreshold_{}'.format(j)),emailrecipient=EmailRecipient.objects.first())
        self.baseurl = api_baseurl + 'partthresholdnotify/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part threshold notify','Can add part threshold notify','Can change part threshold notify','Can delete part threshold notify']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_partthresholdnotify_get(self):
        # Get Object
        ptn = PartThresholdNotify.objects.first()
        pt = json.loads(json.dumps(PartThresholdSerializer(ptn.partthreshold).data, separators=(",",":")))
        # Run Specific Tests
        for url in self.baseurl, '{}?search={}'.format(self.baseurl, ptn.emailrecipient.name[-15:]), '{}?search={}'.format(self.baseurl, ptn.partthreshold.name[-15:]):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            text = response.json()
            for field in 'name','part','threshold','operator':
                check = False
                if len(text) == 1:
                    if pt[field] == text[0]['partthreshold'][field]:
                        check = True
                else:
                    for i in range(0, len(text)-1):
                        if pt[field] == text[i]['partthreshold'][field]:
                            check = True
                            break
                self.assertTrue(check)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthresholdnotify_post(self):
        # Get Objects and Make Data
        pt = PartThreshold.objects.last()
        er = EmailRecipient.objects.first()
        for threshold in PartThreshold.objects.all():
            if len(PartThresholdNotify.objects.filter(partthreshold=threshold)) == 0:
                pt = threshold
                break
        data = {'partthreshold': str(pt.id), 'emailrecipient': str(er.id)}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthresholdnotify_pk_get(self):
        # Get Object
        ptn = PartThresholdNotify.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, ptn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        text = response.json()
        pt = json.loads(json.dumps(PartThresholdSerializer(ptn.partthreshold).data, separators=(",",":")))
        for field in 'name','part','threshold','operator':
            self.assertEqual(text['partthreshold'][field], pt[field])
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, ptn.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthresholdnotify_pk_delete(self):
        # Get Object
        ptn = PartThresholdNotify.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, ptn)
        # Run Specific Tests
        self.assertIsNone(PartThresholdNotify.objects.filter(id=ptn.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, ptn.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API PartThresoldTemplate View
class TrackerApiPartThresholdTemplateViewTests(TestCase):
    def setUp(self):
        for i in range(1040,1051):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
        for i in range(15,20):
            j = randint(1040,1050)
            k = randint(0,5)
            PartThreshold.objects.create(name='Test_View_PartThreshold_{}'.format(i), part=Part.objects.get(name='Test_View_Part_{}'.format(j)), threshold=5, operator=OPERATORS[k][0])
        EmailTemplate.objects.get_or_create(name='Test_View_EmailTemplate', subject='Test View EmailTemplate', template='/media/tracker/templates/TestTemplate.html')
        for i in range(0,4):
            j = randint(15,19)
            PartThresholdTemplate.objects.get_or_create(partthreshold=PartThreshold.objects.get(name='Test_View_PartThreshold_{}'.format(j)), emailtemplate=EmailTemplate.objects.first())
        self.baseurl = api_baseurl + 'partthresholdtemplate/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part threshold template','Can add part threshold template','Can change part threshold template','Can delete part threshold template']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_partthresholdtemplate_get(self):
        # Get Object
        ptt = PartThresholdTemplate.objects.first()
        pt = json.loads(json.dumps(PartThresholdSerializer(ptt.partthreshold).data, separators=(",",":")))
        # Run Specific Tests
        for url in self.baseurl, '{}?search={}'.format(self.baseurl, ptt.emailtemplate.name[-15:]), '{}?search={}'.format(self.baseurl, ptt.partthreshold.name[-15:]), '{}?search={}'.format(self.baseurl, ptt.emailtemplate.subject[-10:]):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            text = response.json()
            for field in 'name','part','threshold','operator':
                check = False
                if len(text) == 1:
                    if pt[field] == text[0]['partthreshold'][field]:
                        check = True
                else:
                    for i in range(0, len(text)-1):
                        if pt[field] == text[i]['partthreshold'][field]:
                            check = True
                            break
                self.assertTrue(check)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthresholdtemplate_post(self):
        # Get Objects and Make Data
        pt = PartThreshold.objects.last()
        et = EmailTemplate.objects.first()
        for threshold in PartThreshold.objects.all():
            if len(PartThresholdTemplate.objects.filter(partthreshold=threshold)) == 0:
                pt = threshold
                break
        data = {'partthreshold': str(pt.id), 'emailtemplate': str(et.id)}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthresholdtemplate_pk_get(self):
        # Get Object
        ptt = PartThresholdTemplate.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, ptt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        text = response.json()
        pt = json.loads(json.dumps(PartThresholdSerializer(ptt.partthreshold).data, separators=(",",":")))
        for field in 'name','part','threshold','operator':
            self.assertEqual(text['partthreshold'][field], pt[field])
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, ptt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthresholdnotify_pk_delete(self):
        # Get Object
        ptt = PartThresholdTemplate.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, ptt)
        # Run Specific Tests
        self.assertIsNone(PartThresholdTemplate.objects.filter(id=ptt.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, ptt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API PartStorageThresoldNotify View
class TrackerApiPartStorageThresholdNotifyViewTests(TestCase):
    def setUp(self):
        for i in range(1050,1061):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test storage')
        for i in range(20,25):
            j = randint(1050,1060)
            k = randint(1050,1060)
            l = randint(0,5)
            PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j)), storage=Storage.objects.get(name='Test_View_Storage_{}'.format(k)), count=12)
            PartStorageThreshold.objects.create(name='Test_View_PartStorageThreshold_{}'.format(i), partstorage=PartStorage.objects.last(), threshold=5, operator=OPERATORS[l][0])
        EmailRecipient.objects.get_or_create(name='Test_View_EmailRecipient',email='support@zephyruscomputing.com')
        for i in range(0,4):
            j = randint(20,24)
            PartStorageThresholdNotify.objects.get_or_create(partstoragethreshold=PartStorageThreshold.objects.get(name='Test_View_PartStorageThreshold_{}'.format(j)),emailrecipient=EmailRecipient.objects.first())
        self.baseurl = api_baseurl + 'partstoragethresholdnotify/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part storage threshold notify','Can add part storage threshold notify','Can change part storage threshold notify','Can delete part storage threshold notify']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_partstoragethresholdnotify_get(self):
        # Get Object
        ptn = PartStorageThresholdNotify.objects.first()
        pt = json.loads(json.dumps(PartStorageThresholdSerializer(ptn.partstoragethreshold).data, separators=(",",":")))
        # Run Specific Tests
        for url in self.baseurl, '{}?search={}'.format(self.baseurl, ptn.emailrecipient.name[-15:]), '{}?search={}'.format(self.baseurl, ptn.partstoragethreshold.name[-15:]):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            text = response.json()
            for field in 'name','partstorage','threshold','operator':
                check = False
                if len(text) == 1:
                    if pt[field] == text[0]['partstoragethreshold'][field]:
                        check = True
                else:
                    for i in range(0, len(text)-1):
                        if pt[field] == text[i]['partstoragethreshold'][field]:
                            check = True
                            break
                self.assertTrue(check)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstoragethresholdnotify_post(self):
        # Get Objects and Make Data
        pt = PartStorageThreshold.objects.last()
        er = EmailRecipient.objects.first()
        for threshold in PartStorageThreshold.objects.all():
            if len(PartStorageThresholdNotify.objects.filter(partstoragethreshold=threshold)) == 0:
                pt = threshold
                break
        data = {'partstoragethreshold': str(pt.id), 'emailrecipient': str(er.id)}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstoragethresholdnotify_pk_get(self):
        # Get Object
        ptn = PartStorageThresholdNotify.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, ptn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        text = response.json()
        pt = json.loads(json.dumps(PartStorageThresholdSerializer(ptn.partstoragethreshold).data, separators=(",",":")))
        for field in 'name','partstorage','threshold','operator':
            self.assertEqual(text['partstoragethreshold'][field], pt[field])
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, ptn.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partthresholdnotify_pk_delete(self):
        # Get Object
        ptn = PartStorageThresholdNotify.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, ptn)
        # Run Specific Tests
        self.assertIsNone(PartStorageThresholdNotify.objects.filter(id=ptn.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, ptn.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API PartStorageThresoldTemplate View
class TrackerApiPartStorageThresholdTemplateViewTests(TestCase):
    def setUp(self):
        for i in range(1060,1071):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test storage')
        for i in range(25,30):
            j = randint(1060,1070)
            k = randint(1060,1070)
            l = randint(0,5)
            PartStorage.objects.create(part=Part.objects.get(name='Test_View_Part_{}'.format(j)), storage=Storage.objects.get(name='Test_View_Storage_{}'.format(k)), count=12)
            PartStorageThreshold.objects.create(name='Test_View_PartStorageThreshold_{}'.format(i), partstorage=PartStorage.objects.last(), threshold=5, operator=OPERATORS[l][0])
        EmailTemplate.objects.get_or_create(name='Test_View_EmailTemplate', subject='Test View EmailTemplate', template='/media/tracker/templates/TestTemplate.html')
        for i in range(0,4):
            j = randint(25,29)
            PartStorageThresholdTemplate.objects.get_or_create(partstoragethreshold=PartStorageThreshold.objects.get(name='Test_View_PartStorageThreshold_{}'.format(j)), emailtemplate=EmailTemplate.objects.first())
        self.baseurl = api_baseurl + 'partstoragethresholdtemplate/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view part storage threshold template','Can add part storage threshold template','Can change part storage threshold template','Can delete part storage threshold template']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_partstoragethresholdtemplate_get(self):
        # Get Object
        ptt = PartStorageThresholdTemplate.objects.first()
        pt = json.loads(json.dumps(PartStorageThresholdSerializer(ptt.partstoragethreshold).data, separators=(",",":")))
        # Run Specific Tests
        for url in self.baseurl, '{}?search={}'.format(self.baseurl, ptt.emailtemplate.name[-15:]), '{}?search={}'.format(self.baseurl, ptt.partstoragethreshold.name[-15:]), '{}?search={}'.format(self.baseurl, ptt.emailtemplate.subject[-10:]), '{}?search={}'.format(self.baseurl, ptt.partstoragethreshold.partstorage.storage.name[-8:]), '{}?search={}'.format(self.baseurl,ptt.partstoragethreshold.partstorage.part.name[-8:]):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            text = response.json()
            for field in 'name','partstorage','threshold','operator':
                check = False
                if len(text) == 1:
                    if pt[field] == text[0]['partstoragethreshold'][field]:
                        check = True
                else:
                    for i in range(0, len(text)-1):
                        if pt[field] == text[i]['partstoragethreshold'][field]:
                            check = True
                            break
                self.assertTrue(check)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


    def test_apiview_partstoragethresholdtemplate_post(self):
        # Get Objects and Make Data
        pt = PartStorageThreshold.objects.last()
        et = EmailTemplate.objects.first()
        for threshold in PartStorageThreshold.objects.all():
            if len(PartStorageThresholdTemplate.objects.filter(partstoragethreshold=threshold)) == 0:
                pt = threshold
                break
        data = {'partstoragethreshold': str(pt.id), 'emailtemplate': str(et.id)}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstoragethresholdtemplate_pk_get(self):
        # Get Object
        ptt = PartStorageThresholdTemplate.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, ptt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        text = response.json()
        pt = json.loads(json.dumps(PartStorageThresholdSerializer(ptt.partstoragethreshold).data, separators=(",",":")))
        for field in 'name','partstorage','threshold','operator':
            self.assertEqual(text['partstoragethreshold'][field], pt[field])
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, ptt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_partstoragethresholdnotify_pk_delete(self):
        # Get Object 
        ptt = PartStorageThresholdTemplate.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, ptt)
        # Run Specific Tests
        self.assertIsNone(PartStorageThresholdTemplate.objects.filter(id=ptt.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, ptt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API ObjectAction View
class TrackerApiObjectActionViewTests(TestCase):
    def setUp(self):
        for i in range(1070,1081):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=12)
            AlternateSKU.objects.create(sku=str(i).zfill(6), manufacturer='Zephyrus Computing, LLC')
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            if i != 1070:
                Assembly.objects.create(parent=Part.objects.first(), part=Part.objects.last(), count=1)

        for i in range(0,5):
            j = randint(1070,1080)
            k = OBJTYPES[randint(0,5)][0]
            l = ACTIONS[randint(0,4)][0]
            m = 0
            if k == OBJTYPES[0][0]:
                m = AlternateSKU.objects.get(sku=str(j).zfill(6)).id
            if k == OBJTYPES[1][0]:
                m = Assembly.objects.all()[i].id
            if k == OBJTYPES[2][0]:
                m = Part.objects.get(name='Test_View_Part_{}'.format(j)).id
            if k == OBJTYPES[3][0]:
                m = PartAlternateSKU.objects.get(part=Part.objects.get(name='Test_View_Part_{}'.format(j))).id
            if k == OBJTYPES[4][0]:
                m = PartStorage.objects.get(part=Part.objects.get(name='Test_View_Part_{}'.format(j))).id
            if k == OBJTYPES[5][0]:
                m = Storage.objects.get(name='Test_View_Storage_{}'.format(j)).id
            ObjectAction.objects.create(name='Test_View_ObjectAction_{}'.format(i), objtype=k, action=l, objid=m)
        self.baseurl = api_baseurl + 'objectaction/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view object action','Can add object action','Can change object action','Can delete object action']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_objectaction_get(self):
        # Get Object
        oa = ObjectAction.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, oa)
        # Run Specific Tests
        response = self.client.get('{}?search={}'.format(self.baseurl, oa.name[-10:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, oa)
        response = self.client.get('{}?search={}'.format(self.baseurl, oa.objid))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, oa)
        response = self.client.get('{}?search={}'.format(self.baseurl, oa.objtype))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, oa)
        response = self.client.get('{}?search={}'.format(self.baseurl, oa.action))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, oa)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_objectaction_post(self):
        # Get Objects and Make Data
        Part.objects.create(name='Test_View_Part_New', description='This is an api view TestCase test part', sku='1070new', price=10.00, cost=5.00)
        data = {'name': 'Test_View_ObjectAction_New', 'objtype': OBJTYPES[2][0], 'action': ACTIONS[randint(0,4)][0], 'objid': Part.objects.last().id}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_objectaction_pk_get(self):
        # Get Object
        oa = ObjectAction.objects.first()
        # Run Generic Tests
        TestGenerics().pk_get(self, oa)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, oa.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_objectaction_pk_put(self):
        # Get Object
        oa = ObjectAction.objects.first()
        action = ACTIONS[randint(0,4)][0]
        while action == oa.action:
            action = ACTIONS[randint(0,4)][0]
        oa.action = action
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, oa.id), ObjectActionSerializer(oa).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        objid = oa.objid
        oa.objid = objid + 2
        response = self.client.put('{}{}/'.format(self.baseurl, oa.id), ObjectActionSerializer(oa).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(oa.id), 'action': 0}
        response = self.client.put('{}{}/'.format(self.baseurl, oa.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        oa.refresh_from_db()
        self.assertEqual(oa.action, action)
        self.assertEqual(oa.objid, objid)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.put('{}{}/'.format(self.baseurl, oa.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_objectaction_pk_patch(self):
        # Get Object
        oa = ObjectAction.objects.first()
        action = ACTIONS[randint(0,4)][0]
        while action == oa.action:
            action = ACTIONS[randint(0,4)][0]
        data = {'id': oa.id, 'action': action}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, oa.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        objid = oa.objid
        oa.objid = objid + 2
        data = {'id': oa.id, 'objid': oa.objid}
        response = self.client.patch('{}{}/'.format(self.baseurl, oa.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(oa.id), 'action': 0}
        response = self.client.put('{}{}/'.format(self.baseurl, oa.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        oa.refresh_from_db()
        self.assertEqual(oa.action, action)
        self.assertEqual(oa.objid, objid)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, oa.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        
    def test_apiview_objectaction_pk_delete(self):
        # Get Object
        oa = ObjectAction.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, oa)
        # Run Specific Tests
        self.assertIsNone(ObjectAction.objects.filter(id=oa.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, oa.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        
# Test Tracker API ObjectActionNotify View
class TrackerApiObjectActionNotifyViewTests(TestCase):
    def setUp(self):
        for i in range(1080,1091):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=12)
            AlternateSKU.objects.create(sku=str(i).zfill(6), manufacturer='Zephyrus Computing, LLC')
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            if i != 1080:
                Assembly.objects.create(parent=Part.objects.first(), part=Part.objects.last(), count=1)

        for i in range(5,10):
            j = randint(1080,1090)
            k = OBJTYPES[randint(0,5)][0]
            l = ACTIONS[randint(0,4)][0]
            m = 0
            if k == OBJTYPES[0][0]:
                m = AlternateSKU.objects.get(sku=str(j).zfill(6)).id
            if k == OBJTYPES[1][0]:
                m = Assembly.objects.all()[i].id
            if k == OBJTYPES[2][0]:
                m = Part.objects.get(name='Test_View_Part_{}'.format(j)).id
            if k == OBJTYPES[3][0]:
                m = PartAlternateSKU.objects.get(part=Part.objects.get(name='Test_View_Part_{}'.format(j))).id
            if k == OBJTYPES[4][0]:
                m = PartStorage.objects.get(part=Part.objects.get(name='Test_View_Part_{}'.format(j))).id
            if k == OBJTYPES[5][0]:
                m = Storage.objects.get(name='Test_View_Storage_{}'.format(j)).id
            ObjectAction.objects.create(name='Test_View_ObjectAction_{}'.format(i), objtype=k, action=l, objid=m)
        EmailRecipient.objects.get_or_create(name='Test_View_EmailRecipient',email='support@zephyruscomputing.com')
        for i in range(0,4):
            ObjectActionNotify.objects.create(objectaction=ObjectAction.objects.all()[i], emailrecipient=EmailRecipient.objects.first())
        self.baseurl = api_baseurl + 'objectactionnotify/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view object action notify','Can add object action notify','Can change object action notify','Can delete object action notify']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_objectactionnotify_get(self):
        # Get Object
        oan = ObjectActionNotify.objects.first()
        oa = json.loads(json.dumps(ObjectActionSerializer(oan.objectaction).data, separators=(",",":")))
        # Run Specific Tests
        for url in self.baseurl, '{}?search={}'.format(self.baseurl, oan.emailrecipient.name[-15:]), '{}?search={}'.format(self.baseurl, oan.objectaction.name[-15:]), '{}?search={}'.format(self.baseurl, oan.emailrecipient.email[10:]):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            text = response.json()
            for field in 'name','action','objtype','objid':
                check = False
                if len(text) == 1:
                    if oa[field] == text[0]['objectaction'][field]:
                        check = True
                else:
                    for i in range(0, len(text)-1):
                        if oa[field] == text[i]['objectaction'][field]:
                            check = True
                            break
                self.assertTrue(check)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_objectactionnotify_post(self):
        # Get Objects and Make Data
        oa = ObjectAction.objects.first()
        er = EmailRecipient.objects.first()
        for action in ObjectAction.objects.all():
            if len(ObjectActionNotify.objects.filter(objectaction=action)) == 0:
                oa = action
                break
        data = {'objectaction': oa.id, 'emailrecipient': er.id}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_objectactionnotify_pk_get(self):
        # Get Object
        oan = ObjectActionNotify.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, oan.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        text = response.json()
        oa = json.loads(json.dumps(ObjectActionSerializer(oan.objectaction).data, separators=(",",":")))
        for field in 'name','action','objtype','objid':
            self.assertTrue(text['objectaction'][field], oa[field])
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, oan.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_objectactionnotify_pk_delete(self):
        # Get Object
        oan = ObjectActionNotify.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, oan)
        # Run Specific Tests
        self.assertIsNone(ObjectActionNotify.objects.filter(id=oan.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, oan.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API ObjectActionTemplate View
class TrackerApiObjectActionTemplateViewTests(TestCase):
    def setUp(self):
        for i in range(1090,1101):
            Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=12)
            AlternateSKU.objects.create(sku=str(i).zfill(6), manufacturer='Zephyrus Computing, LLC')
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            if i != 1090:
                Assembly.objects.create(parent=Part.objects.first(), part=Part.objects.last(), count=1)

        for i in range(10,15):
            j = randint(1090,1100)
            k = OBJTYPES[randint(0,5)][0]
            l = ACTIONS[randint(0,4)][0]
            m = 0
            if k == OBJTYPES[0][0]:
                m = AlternateSKU.objects.get(sku=str(j).zfill(6)).id
            if k == OBJTYPES[1][0]:
                m = Assembly.objects.all()[i-10].id
            if k == OBJTYPES[2][0]:
                m = Part.objects.get(name='Test_View_Part_{}'.format(j)).id
            if k == OBJTYPES[3][0]:
                m = PartAlternateSKU.objects.get(part=Part.objects.get(name='Test_View_Part_{}'.format(j))).id
            if k == OBJTYPES[4][0]:
                m = PartStorage.objects.get(part=Part.objects.get(name='Test_View_Part_{}'.format(j))).id
            if k == OBJTYPES[5][0]:
                m = Storage.objects.get(name='Test_View_Storage_{}'.format(j)).id
            ObjectAction.objects.create(name='Test_View_ObjectAction_{}'.format(i), objtype=k, action=l, objid=m)
        EmailTemplate.objects.get_or_create(name='Test_View_EmailTemplate', subject='Test View EmailTemplate', template='/media/tracker/templates/TestTemplate.html')
        for i in range(0,4):
            ObjectActionTemplate.objects.create(objectaction=ObjectAction.objects.all()[i], emailtemplate=EmailTemplate.objects.first())
        self.baseurl = api_baseurl + 'objectactiontemplate/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view object action template','Can add object action template','Can change object action template','Can delete object action template']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_objectactiontemplate_get(self):
        # Get Object
        oat = ObjectActionTemplate.objects.first()
        oa = json.loads(json.dumps(ObjectActionSerializer(oat.objectaction).data, separators=(",",":")))
        # Run Specific Tests
        for url in self.baseurl, '{}?search={}'.format(self.baseurl, oat.emailtemplate.name[-15:]), '{}?search={}'.format(self.baseurl, oat.objectaction.name[-15:]):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            text = response.json()
            for field in 'name','action','objtype','objid':
                check = False
                if len(text) == 1:
                    if oa[field] == text[0]['objectaction'][field]:
                        check = True
                else:
                    for i in range(0, len(text)-1):
                        if oa[field] == text[i]['objectaction'][field]:
                            check = True
                            break
                self.assertTrue(check)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_objectactiontemplate_post(self):
        # Get Objects and Make Data
        oa = ObjectAction.objects.first()
        et = EmailTemplate.objects.first()
        for action in ObjectAction.objects.all():
            if len(ObjectActionTemplate.objects.filter(objectaction=action)) == 0:
                oa = action
                break
        data = {'objectaction': oa.id, 'emailtemplate': et.id}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_objectactiontemplate_pk_get(self):
        # Get Object
        oat = ObjectActionTemplate.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, oat.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        text = response.json()
        oa = json.loads(json.dumps(ObjectActionSerializer(oat.objectaction).data, separators=(",",":")))
        for field in 'name','action','objtype','objid':
            self.assertTrue(text['objectaction'][field], oa[field])
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, oat.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_objectactiontemplate_pk_delete(self):
        # Get Object
        oat = ObjectActionTemplate.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, oat)
        # Run Specific Tests
        self.assertIsNone(ObjectActionTemplate.objects.filter(id=oat.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, oat.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API KitThreshold View
class TrackerApiKitThresholdViewTests(TestCase):
    def setUp(self):
        for i in range(1100,1111):
            part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test part')
            PartStorage.objects.create(part=part,storage=storage,count=3)
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is an api view TestCase test kit', sku=str(i), price=10.00)
        for i in range(0,5):
            j = randint(1100,1110)
            k = randint(0,10)
            l = randint(0,5)
            KitPartStorage.objects.create(kit=Kit.objects.get(name='Test_View_Kit_{}'.format(j)),partstorage=PartStorage.objects.all()[k],count=1)
            KitThreshold.objects.create(name='Test_View_KitThreshold_{}'.format(i), kit=Kit.objects.get(name='Test_View_Kit_{}'.format(j)), threshold=3, operator=OPERATORS[l][0])
        self.baseurl = api_baseurl + 'kitthreshold/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view kit threshold','Can add kit threshold','Can change kit threshold','Can delete kit threshold']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_kitthreshold_get(self):
        # Get Object
        kt = KitThreshold.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, kt)
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"kit":' + json.dumps(KitSerializer(kt.kit).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?search={}'.format(self.baseurl, kt.name[-15:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"kit":' + json.dumps(KitSerializer(kt.kit).data, separators=(",",":"))[:-1])
        response = self.client.get('{}?search={}'.format(self.baseurl, kt.kit.name[-9:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, '"kit":' + json.dumps(KitSerializer(kt.kit).data, separators=(",",":"))[:-1])
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitthreshold_post(self):
        # Get Objects and Make Data
        kit = Kit.objects.last()
        data = {'name': 'Test_View_KitThreshold_New', 'kit': str(kit.id), 'threshold': 5, 'operator': '=='}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitthreshold_pk_get(self):
        # Get Object
        kt = KitThreshold.objects.first()
        # Run Generic Tests
        TestGenerics().pk_get(self, kt)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, kt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitthreshold_pk_put(self):
        # Get Object
        kt = KitThreshold.objects.first()
        kt.threshold = 6
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, kt.id), KitThresholdSerializer(kt).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(kt.id), 'threshold': 0}
        response = self.client.put('{}{}/'.format(self.baseurl, kt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        kt.refresh_from_db()
        self.assertEqual(kt.threshold, 6)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.put('{}{}/'.format(self.baseurl, kt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitthreshold_pk_patch(self):
        # Get Object and Make Data
        kt = KitThreshold.objects.first()
        data = {'id': kt.id, 'threshold': 4}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, kt.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': kt.id, 'kit': 0}
        response = self.client.patch('{}{}/'.format(self.baseurl, kt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        kt.refresh_from_db()
        self.assertEqual(kt.threshold, 4)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, kt.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitthreshold_pk_delete(self):
        # Get Object
        kt = KitThreshold.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, kt)
        # Run Specific Tests
        self.assertIsNone(KitThreshold.objects.filter(id=kt.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, kt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API KitThresoldNotify View
class TrackerApiKitThresholdNotifyViewTests(TestCase):
    def setUp(self):
        for i in range(1110,1121):
            part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test part')
            PartStorage.objects.create(part=part,storage=storage,count=3)
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is an api view TestCase test kit', sku=str(i), price=10.00)
        for i in range(5,10):
            j = randint(1110,1120)
            k = randint(0,10)
            l = randint(0,5)
            KitPartStorage.objects.create(kit=Kit.objects.get(name='Test_View_Kit_{}'.format(j)),partstorage=PartStorage.objects.all()[k],count=1)
            KitThreshold.objects.create(name='Test_View_KitThreshold_{}'.format(i), kit=Kit.objects.get(name='Test_View_Kit_{}'.format(j)), threshold=3, operator=OPERATORS[l][0])
        EmailRecipient.objects.get_or_create(name='Test_View_EmailRecipient',email='support@zephyruscomputing.com')
        for i in range(0,4):
            j = randint(5,9)
            KitThresholdNotify.objects.get_or_create(kitthreshold=KitThreshold.objects.get(name='Test_View_KitThreshold_{}'.format(j)),emailrecipient=EmailRecipient.objects.first())
        self.baseurl = api_baseurl + 'kitthresholdnotify/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view kit threshold notify','Can add kit threshold notify','Can change kit threshold notify','Can delete kit threshold notify']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_kitthresholdnotify_get(self):
        # Get Object
        ktn = KitThresholdNotify.objects.first()
        kt = json.loads(json.dumps(KitThresholdSerializer(ktn.kitthreshold).data, separators=(",",":")))
        # Run Specific Tests
        for url in self.baseurl, '{}?search={}'.format(self.baseurl, ktn.emailrecipient.name[-15:]), '{}?search={}'.format(self.baseurl, ktn.kitthreshold.name[-15:]):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            text = response.json()
            for field in 'name','kit','threshold','operator':
                check = False
                if len(text) == 1:
                    if kt[field] == text[0]['kitthreshold'][field]:
                        check = True
                else:
                    for i in range(0, len(text)-1):
                        if kt[field] == text[i]['kitthreshold'][field]:
                            check = True
                            break
                self.assertTrue(check)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitthresholdnotify_post(self):
        # Get Objects and Make Data
        kt = KitThreshold.objects.last()
        er = EmailRecipient.objects.first()
        for threshold in KitThreshold.objects.all():
            if len(KitThresholdNotify.objects.filter(kitthreshold=threshold)) == 0:
                kt = threshold
                break
        data = {'kitthreshold': str(kt.id), 'emailrecipient': str(er.id)}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitthresholdnotify_pk_get(self):
        # Get Object
        ktn = KitThresholdNotify.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, ktn.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        text = response.json()
        kt = json.loads(json.dumps(KitThresholdSerializer(ktn.kitthreshold).data, separators=(",",":")))
        for field in 'name','kit','threshold','operator':
            self.assertEqual(text['kitthreshold'][field], kt[field])
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, ktn.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitthresholdnotify_pk_delete(self):
        # Get Object
        ktn = KitThresholdNotify.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, ktn)
        # Run Specific Tests
        self.assertIsNone(KitThresholdNotify.objects.filter(id=ktn.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, ktn.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API KitThresoldTemplate View
class TrackerApiKitThresholdTemplateViewTests(TestCase):
    def setUp(self):
        for i in range(1120,1131):
            part = Part.objects.create(name='Test_View_Part_{}'.format(i), description='This is an api view TestCase test part', sku=str(i), price=10.00, cost=5.00)
            storage = Storage.objects.create(name='Test_View_Storage_{}'.format(i), description='This is an api view TestCase test part')
            PartStorage.objects.create(part=part,storage=storage,count=3)
            Kit.objects.create(name='Test_View_Kit_{}'.format(i), description='This is an api view TestCase test kit', sku=str(i), price=10.00)
        for i in range(10,15):
            j = randint(1120,1130)
            k = randint(0,10)
            l = randint(0,5)
            KitPartStorage.objects.create(kit=Kit.objects.get(name='Test_View_Kit_{}'.format(j)),partstorage=PartStorage.objects.all()[k],count=1)
            KitThreshold.objects.create(name='Test_View_KitThreshold_{}'.format(i), kit=Kit.objects.get(name='Test_View_Kit_{}'.format(j)), threshold=3, operator=OPERATORS[l][0])
        EmailTemplate.objects.get_or_create(name='Test_View_EmailTemplate', subject='Test View EmailTemplate', template='/media/tracker/templates/TestTemplate.html')
        for i in range(0,4):
            j = randint(10,14)
            KitThresholdTemplate.objects.get_or_create(kitthreshold=KitThreshold.objects.get(name='Test_View_KitThreshold_{}'.format(j)), emailtemplate=EmailTemplate.objects.first())
        self.baseurl = api_baseurl + 'kitthresholdtemplate/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view kit threshold template','Can add kit threshold template','Can change kit threshold template','Can delete kit threshold template']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_kitthresholdtemplate_get(self):
        # Get Object
        ktt = KitThresholdTemplate.objects.first()
        kt = json.loads(json.dumps(KitThresholdSerializer(ktt.kitthreshold).data, separators=(",",":")))
        # Run Specific Tests
        for url in self.baseurl, '{}?search={}'.format(self.baseurl, ktt.emailtemplate.name[-15:]), '{}?search={}'.format(self.baseurl, ktt.kitthreshold.name[-15:]), '{}?search={}'.format(self.baseurl, ktt.emailtemplate.subject[-10:]):
            response = self.client.get(url)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            text = response.json()
            for field in 'name','kit','threshold','operator':
                check = False
                if len(text) == 1:
                    if kt[field] == text[0]['kitthreshold'][field]:
                        check = True
                else:
                    for i in range(0, len(text)-1):
                        if kt[field] == text[i]['kitthreshold'][field]:
                            check = True
                            break
                self.assertTrue(check)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitthresholdtemplate_post(self):
        # Get Objects and Make Data
        kt = KitThreshold.objects.last()
        et = EmailTemplate.objects.first()
        for threshold in KitThreshold.objects.all():
            if len(KitThresholdTemplate.objects.filter(kitthreshold=threshold)) == 0:
                kt = threshold
                break
        data = {'kitthreshold': str(kt.id), 'emailtemplate': str(et.id)}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitthresholdtemplate_pk_get(self):
        # Get Object
        ktt = KitThresholdTemplate.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, ktt.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        text = response.json()
        kt = json.loads(json.dumps(KitThresholdSerializer(ktt.kitthreshold).data, separators=(",",":")))
        for field in 'name','kit','threshold','operator':
            self.assertEqual(text['kitthreshold'][field], kt[field])
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, ktt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_kitthresholdnotify_pk_delete(self):
        # Get Object
        ktt = KitThresholdTemplate.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, ktt)
        # Run Specific Tests
        self.assertIsNone(KitThresholdTemplate.objects.filter(id=ktt.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, ktt.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API EmailRecipient View
class TrackerApiEmailRecipientViewTests(TestCase):
    def setUp(self):
        for i in range(1000,1011):
            EmailRecipient.objects.create(name='Test_View_EmailRecipient_{}'.format(i),email='support+{}@zephyruscomputing.com'.format(i))
        self.baseurl = api_baseurl + 'emailrecipient/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view email recipient','Can add email recipient','Can change email recipient','Can delete email recipient']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_emailrecipient_get(self):
        # Get Object
        er = EmailRecipient.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, er.name)
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, er.name)
        response = self.client.get('{}?search={}'.format(self.baseurl, er.name[-15:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, er.name)
        response = self.client.get('{}?search={}'.format(self.baseurl, er.email[-9:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, er.name)
        response = self.client.get('{}?name={}'.format(self.baseurl, er.name))
        self.assertContains(response, er.name)
        response = self.client.get('{}?email={}'.format(self.baseurl, er.email.replace('+','%2B')))
        self.assertContains(response, er.name)
        response = self.client.get('{}?limit=2'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        response = self.client.get('{}?limit=2&offset=2'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        response = self.client.get('{}?limit=2&sort=name'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        self.assertContains(response, er.name)
        response = self.client.get('{}?limit=2&sort=-name'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        self.assertNotContains(response, er.name)
        response = self.client.get('{}?offset=20'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_emailrecipient_post(self):
        # Get Objects and Make Data
        data = {'name': 'Test_View_EmailRecipient_New', 'email': 'support+new@zephyruscomputing.com'}
        # Run Generic Tests
        TestGenerics().post(self, data, False)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.post(self.baseurl, data=data)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_emailrecipient_pk_get(self):
        # Get Object
        er = EmailRecipient.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, er.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, er.name)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, er.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_emailrecipient_pk_put(self):
        # Get Object
        er = EmailRecipient.objects.first()
        er.name = 'Test_View_EmailRecipient_NewName'
        # Run Specific Tests
        response = self.client.put('{}{}/'.format(self.baseurl, er.id), EmailRecipientSerializer(er).data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': str(er.id), 'email': 0}
        response = self.client.put('{}{}/'.format(self.baseurl, er.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        er.refresh_from_db()
        self.assertEqual(er.name, 'Test_View_EmailRecipient_NewName')
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.put('{}{}/'.format(self.baseurl, er.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_emailrecipient_pk_patch(self):
        # Get Object and Make Data
        er = EmailRecipient.objects.first()
        data = {'id': er.id, 'email': 'support+newemail@zephyruscomputing.com'}
        # Run Specific Tests
        response = self.client.patch('{}{}/'.format(self.baseurl, er.id), data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        bad_data = {'id': er.id, 'email': ''}
        response = self.client.patch('{}{}/'.format(self.baseurl, er.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        er.refresh_from_db()
        self.assertEqual(er.email, 'support+newemail@zephyruscomputing.com')
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, er.id), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_emailrecipient_pk_delete(self):
        # Get Object
        er = EmailRecipient.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, er)
        # Run Specific Tests
        self.assertIsNone(EmailRecipient.objects.filter(id=er.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, er.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

# Test Tracker API EmailTemplate View
class TrackerApiEmailTemplateViewTests(TestCase):
    def setUp(self):
        for i in range(1000,1011):
            EmailTemplate.objects.create(name='Test_View_EmailTemplate_{}'.format(i),subject='Test_View_EmailTemplate_{}'.format(i),template='/media/tracker/templates/general.html')
        self.baseurl = api_baseurl + 'emailtemplate/'
        self.user = User.objects.get_or_create(username='testuser')[0]
        for name in ['Can view email template','Can add email template','Can change email template','Can delete email template']:
            p = Permission.objects.get(name=name)
            self.user.user_permissions.add(p)
        self.client.force_login(self.user)

    def test_apiview_emailtemplate_get(self):
        # Get Object
        et = EmailTemplate.objects.first()
        # Run Generic Tests
        TestGenerics().get(self, et.name)
        # Run Specific Tests
        response = self.client.get(self.baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, et.name)
        response = self.client.get('{}?search={}'.format(self.baseurl, et.name[-15:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, et.name)
        response = self.client.get('{}?search={}'.format(self.baseurl, et.subject[-9:]))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, et.name)
        response = self.client.get('{}?name={}'.format(self.baseurl, et.name))
        self.assertContains(response, et.name)
        response = self.client.get('{}?subject={}'.format(self.baseurl, et.subject))
        self.assertContains(response, et.name)
        response = self.client.get('{}?limit=2'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        response = self.client.get('{}?limit=2&offset=2'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        response = self.client.get('{}?limit=2&sort=name'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        self.assertContains(response, et.name)
        response = self.client.get('{}?limit=2&sort=-name'.format(self.baseurl))
        self.assertEqual(2, len(json.loads(response.content)))
        self.assertNotContains(response, et.name)
        response = self.client.get('{}?offset=20'.format(self.baseurl))
        self.assertEqual(0, len(json.loads(response.content)))
        with override_settings(ALLOW_ANONYMOUS=False):
            user = User.objects.get_or_create(username='testuser2')[0]
            self.client.force_login(user)
            response = self.client.get(self.baseurl)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_emailtemplate_post(self):
        # Make Data
        filedata = '<html><body><p>This is a test file</p></body></html>'
        # Run Specific Tests
        try:
            with open('{}/tracker/temp.html'.format(settings.MEDIA_ROOT), 'w') as file:
                file.write(filedata)
            with open('{}/tracker/temp.html'.format(settings.MEDIA_ROOT),'rb') as file:
                data = {'name': 'Test_View_EmailTemplate_New', 'subject': 'Test_View_EmailTemplate_New', 'template': file}
                response = self.client.post(self.baseurl, data=data)
                self.assertEqual(response.status_code, status.HTTP_201_CREATED)
                user = User.objects.get_or_create(username='testuser2')[0]
                self.client.force_login(user)
                response = self.client.post(self.baseurl, data=data)
                self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
            with open('{}/tracker/templates/temp.html'.format(settings.MEDIA_ROOT), 'r') as file:
                self.assertEqual(filedata, file.read())
        finally:
            remove('{}/tracker/temp.html'.format(settings.MEDIA_ROOT))
            remove('{}/tracker/templates/temp.html'.format(settings.MEDIA_ROOT))

    def test_apiview_emailtemplate_pk_get(self):
        # Get Object
        et = EmailTemplate.objects.first()
        # Run Specific Tests
        response = self.client.get('{}{}/'.format(self.baseurl, et.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, et.name)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.get('{}{}/'.format(self.baseurl, et.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_emailtemplate_pk_put(self):
        # Get Object
        et = EmailTemplate.objects.first()
        # Run Specific Tests
        boundary = '**'
        content_type = f'multipart/form-data; boundary={boundary}'
        try:
            # SimpleUploadedFile was not working correctly, resorted to doing raw requests
            data = (
                f'--{boundary}\r\n'
                f'Content-Disposition: form-data; name="name"\r\n\r\n'
                f'{et.name}\r\n'
                f'--{boundary}\r\n'
                f'Content-Disposition: form-data; name="subject"\r\n\r\n'
                f'Test_View_EmailTemplate_Updated\r\n'
                f'--{boundary}\r\n'
                f'Content-Disposition: form-data; name="template"; filename=temp.html\r\n\r\n'
                f'<html><body><p>This is a new test file</p></body></html>\r\n'
                f'--{boundary}--\r\n'
            )
            response = self.client.generic('PUT','{}{}/'.format(self.baseurl, et.pk), data=data, content_type=content_type)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            with open('{}/tracker/templates/temp.html'.format(settings.MEDIA_ROOT), 'r') as file:
                self.assertEqual('<html><body><p>This is a new test file</p></body></html>', file.read())
        finally:
            remove('{}/tracker/templates/temp.html'.format(settings.MEDIA_ROOT))
        bad_data = (
            f'--{boundary}\r\n'
            f'Content-Disposition: form-data; name="name"\r\n\r\n'
            f'{et.name}\r\n'
            f'--{boundary}\r\n'
            f'Content-Disposition: form-data; name="subject"\r\n\r\n'
            f'Test_View_EmailTemplate_Updated\r\n'
            f'--{boundary}\r\n'
        )
        response = self.client.put('{}{}/'.format(self.baseurl, et.pk), bad_data, content_type=content_type)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.put('{}{}/'.format(self.baseurl, et.pk), bad_data, content_type=content_type)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_emailtemplate_pk_patch(self):
        # Get Object
        et = EmailTemplate.objects.first()
        # Run Specific Tests
        boundary = '**'
        content_type = f'multipart/form-data; boundary={boundary}'
        try:
            # SimpleUploadedFile was not working correctly, resorted to doing raw requests
            data = (
                f'--{boundary}\r\n'
                f'Content-Disposition: form-data; name="subject"\r\n\r\n'
                f'Test_View_EmailTemplate_Patched\r\n'
                f'--{boundary}\r\n'
                f'Content-Disposition: form-data; name="template"; filename=temp.html\r\n\r\n'
                f'<html><body><p>This is a new test file</p></body></html>\r\n'
                f'--{boundary}--\r\n'
            )
            response = self.client.generic('PATCH','{}{}/'.format(self.baseurl, et.pk), data=data, content_type=content_type)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            with open('{}/tracker/templates/temp.html'.format(settings.MEDIA_ROOT), 'r') as file:
                self.assertEqual('<html><body><p>This is a new test file</p></body></html>', file.read())
        finally:
            remove('{}/tracker/templates/temp.html'.format(settings.MEDIA_ROOT))
        bad_data = {'banana': 'yellow'}
        response = self.client.patch('{}{}/'.format(self.baseurl, et.pk), bad_data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.patch('{}{}/'.format(self.baseurl, et.pk), bad_data, content_type=content_type)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_apiview_emailtemplate_pk_delete(self):
        # Get Object
        et = EmailTemplate.objects.first()
        # Run Generic Tests
        TestGenerics().pk_delete(self, et)
        # Run Specific Tests
        self.assertIsNone(EmailTemplate.objects.filter(id=et.id).first())
        user = User.objects.get_or_create(username='testuser2')[0]
        self.client.force_login(user)
        response = self.client.delete('{}{}/'.format(self.baseurl, et.id))
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class TestGenerics():
    def get_serialized_data(self, obj):
        if obj.__class__.__name__ == 'PartThreshold':
            return PartThresholdSerializer(obj).data
        if obj.__class__.__name__ == 'PartStorageThreshold':
            return PartStorageThresholdSerializer(obj).data
        if obj.__class__.__name__ == 'KitThreshold':
            return KitThresholdSerializer(obj).data
        if obj.__class__.__name__ == 'EmailTemplate':
            return EmailTemplateSerializer(obj).data
        if obj.__class__.__name__ == 'EmailRecipient':
            return EmailRecipientSerializer(obj).data
        if obj.__class__.__name__ == 'PartThresholdNotify':
            return PartThresholdNotifySerializer(obj).data
        if obj.__class__.__name__ == 'PartThresholdTemplate':
            return PartThresholdTemplateSerializer(obj).data
        if obj.__class__.__name__ == 'PartStorageThresholdNotify':
            return PartStorageThresholdNotifySerializer(obj).data
        if obj.__class__.__name__ == 'PartStorageThresholdTemplate':
            return PartStorageThresholdTemplateSerializer(obj).data
        if obj.__class__.__name__ == 'KitThresholdNotify':
            return KitThresholdNotifySerializer(obj).data
        if obj.__class__.__name__ == 'KitThresholdTemplate':
            return KitThresholdTemplateSerializer(obj).data
        if obj.__class__.__name__ == 'ObjectAction':
            return ObjectActionSerializer(obj).data
        if obj.__class__.__name__ == 'ObjectActionNotify':
            return ObjectActionNotifySerializer(obj).data
        if obj.__class__.__name__ == 'ObjectActionTemplate':
            return ObjectActionTemplateSerializer(obj).data
        if obj.__class__.__name__ == 'Threshold':
            return ThresholdSerializer(obj).data
        if obj.__class__.__name__ == 'Notification':
            return NotificationSerializer(obj).data
        if obj.__class__.__name__ == 'EmailRecipient':
            return EmailRecipientSerializer(obj).data
        if obj.__class__.__name__ == 'EmailTemplate':
            return EmailTemplateSerializer(obj).data
        return json.dumps(obj)
    
    def get_completed_url(self, url, obj_or_data):
        serialized = self.get_serialized_data(obj_or_data)
        if isinstance(serialized, str):
            serialized = json.loads(serialized)
        return '{}{}/'.format(url, serialized.get('id'))

    def get(self, case, obj, url=''):
        if url == '':
            url = case.baseurl
        # GET Valid Request
        response = case.client.get(url)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertContains(response, obj)

    def post(self, case, data, dup=True, url=''):
        if url == '':
            url = case.baseurl
        # POST Valid Data
        response = case.client.post(url, data)
        case.assertEqual(response.status_code, status.HTTP_201_CREATED)
        if dup:
            # POST Duplicate Data
            response = case.client.post(url, data)
            case.assertEqual(response.status_code, status.HTTP_200_OK)
        # POST Invalid Data
        bad_data = {'name': 'bad'}
        response = case.client.post(url, bad_data)
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def pk_get(self, case, obj, url=''):
        if url == '':
            url = case.baseurl
        # GET Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.get(valid)
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        case.assertContains(response, obj)
        # GET Invalid Request
        response = case.client.get(url + '0/')
        case.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def pk_put(self, case, obj, url=''):
        if url == '':
            url = case.baseurl
        serialized = self.get_serialized_data(obj)
        # PUT Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.put(valid, serialized, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        # PUT Invalid Request
        bad_data = {'id': obj.id, 'name': obj.name}
        response = case.client.put(valid, bad_data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def pk_patch(self, case, data, url=''):
        if url == '':
            url = case.baseurl
        # PATCH Valid Data
        valid = self.get_completed_url(url, data)
        response = case.client.patch(valid, data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_200_OK)
        # PATCH Invalid Data
        bad_data = {'name': 'bad'.zfill(1000)}
        response = case.client.patch(valid, bad_data, content_type='application/json')
        case.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def pk_delete(self, case, obj, url=''):
        if url == '':
            url = case.baseurl
        # DELETE Valid Request
        valid = self.get_completed_url(url, obj)
        response = case.client.delete(valid)
        case.assertEqual(response.status_code, status.HTTP_200_OK)


