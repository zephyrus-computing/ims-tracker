import django
django.setup()
from django.conf import settings
from django.test import TestCase, override_settings
from os import listdir
from os.path import join
from random import randint
from inventory.models import Part, PartStorage, Storage, AlternateSKU, PartAlternateSKU, Assembly, Kit, KitPartStorage
from ims.addons.tracker.models import PartThreshold, PartStorageThreshold, PartThresholdNotify, PartStorageThresholdNotify, PartThresholdTemplate, PartStorageThresholdTemplate, ObjectAction, ObjectActionTemplate, ObjectActionNotify, KitThreshold, KitThresholdNotify, KitThresholdTemplate, EmailRecipient, EmailTemplate, Notification, Threshold
from ims.addons.tracker.helpers import OBJTYPES, ACTIONS, REPORTS, TIMESPAN_CHOICES
from ims.addons.tracker.tasks import send_email, process_notifications, send_notifications, part_threshold_check, partstorage_threshold_check, kit_threshold_check, object_action_check, clean_tracker_graphs, scheduled_report
from log.models import Log
import json

# Tests for send_email
class SendEmailTaskTests(TestCase):
    def test_send_email(self):
        subject = 'Test Email '
        message = '<html><body><b>This is a test email, please ignore.</b><p>Test Number $ of 4</p></body></html>'
        addr = 'support@zephyruscomputing.com'
        to = ['jmeyer@zephyruscomputing.com']
        for i in range(1,7):
            if i == 1:
                result = send_email('{}{}'.format(subject,i), message.replace('$',str(i)), addr, to)
                self.assertEqual(result, 1)
            elif i == 2:
                result = send_email('','','','')
                self.assertEqual(result, 0)
            elif i == 3:
                result = send_email('',message.replace('$',str(i)),addr,to)
                self.assertEqual(result, 1)
            elif i == 4:
                result = send_email('{}{}'.format(subject,i),'',addr,to)
                self.assertEqual(result, 1)
            elif i == 5:
                result = send_email('{}{}'.format(subject,i),message.replace('$',str(i)),'',to)
                self.assertEqual(result, 1)
            else:
                result = send_email(subject,message,addr,'')
                self.assertEqual(result, 0)

# Tests for PartThreshold
class PartThresholdTaskTests(TestCase):
    def setUp(self):
        EmailRecipient.objects.create(name='Zephyrus Marketing', email='marketing@zephyruscomputing.com')
        EmailTemplate.objects.create(name='Part Threshold Check Test', subject='test', template='tracker/templates/general.html')
        for i in range(1,6):
            Part.objects.create(name='test_part_{}'.format(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        for i in range(1,6):
            j = randint(1,5)
            PartThreshold.objects.create(name='test_partthreshold_{}'.format(i), part=Part.objects.get(name='test_part_{}'.format(j)), threshold=6)
            PartThresholdTemplate.objects.create(partthreshold=PartThreshold.objects.get(name='test_partthreshold_{}'.format(i)), emailtemplate=EmailTemplate.objects.first())
            PartThresholdNotify.objects.create(partthreshold=PartThreshold.objects.get(name='test_partthreshold_{}'.format(i)), emailrecipient=EmailRecipient.objects.first())
        Storage.objects.create(name='test_storage_threshold_task', description='This is a TestCase test storage')
        for i in range(1,6):
            PartStorage.objects.create(part=Part.objects.get(name='test_part_{}'.format(i)), storage=Storage.objects.first(), count=6)

    def test_part_threshold_check(self):
        #setUp
        for ps in PartStorage.objects.all():
            ps.count = 4
            ps.save()
            Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 2 from PartStorage id {}'.format(ps.id))
        part_threshold_check()
        for pt in PartThreshold.objects.all():
            self.assertTrue(pt.alarm)
        for ps in PartStorage.objects.all():
            ps.count = 6
            ps.save()
            Log.objects.create(severity=20,module='inventory',user=None,message='Added 2 from PartStorage id {}'.format(ps.id))
        part_threshold_check()
        for pt in PartThreshold.objects.all():
            self.assertFalse(pt.alarm)
        for ps in PartStorage.objects.all():
            Log.objects.create(severity=20,module='inventory',user=None,message='Added 2 from PartStorage id {}'.format(ps.id))
        PartStorage.objects.first().delete()
        Log.objects.create(severity=20,module='inventory',user=None,message='Added nonesense for a random PartStorage')
        part_threshold_check()
        for pt in PartThreshold.objects.all():
            self.assertFalse(pt.alarm)


# Tests for PartStorageThreshold
class PartStorageThresholdTaskTests(TestCase):
    def setUp(self):
        EmailRecipient.objects.create(name='Zephyrus Marketing', email='marketing@zephyruscomputing.com')
        EmailTemplate.objects.create(name='PartStorage Threshold Check Test', subject='test', template='tracker/templates/general.html')
        for i in range(1,6):
            Part.objects.create(name='test_part_' + str(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
            Storage.objects.create(name='test_storage_{}'.format(i), description='This is a TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=12)
        for i in range(1,6):
            j = randint(0,len(PartStorage.objects.all()) -1)
            PartStorageThreshold.objects.create(name='test_partstoragethreshold_{}'.format(i), partstorage=PartStorage.objects.all()[j], threshold=6)
            PartStorageThresholdTemplate.objects.create(partstoragethreshold=PartStorageThreshold.objects.get(name='test_partstoragethreshold_{}'.format(i)), emailtemplate=EmailTemplate.objects.first())
            PartStorageThresholdNotify.objects.create(partstoragethreshold=PartStorageThreshold.objects.get(name='test_partstoragethreshold_{}'.format(i)), emailrecipient=EmailRecipient.objects.first())

    def test_partstorage_threshold_check(self):
        #setUp
        for ps in PartStorage.objects.all():
            ps.count = 4
            ps.save()
            Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 2 from PartStorage id {}'.format(ps.id))
        partstorage_threshold_check()
        for pst in PartStorageThreshold.objects.all():
            self.assertTrue(pst.alarm)
        for ps in PartStorage.objects.all():
            ps.count = 6
            ps.save()
            Log.objects.create(severity=20,module='inventory',user=None,message='Added 2 from PartStorage id {}'.format(ps.id))
        partstorage_threshold_check()
        for pst in PartStorageThreshold.objects.all():
            self.assertFalse(pst.alarm)
        for ps in PartStorage.objects.all():
            Log.objects.create(severity=20,module='inventory',user=None,message='Added 2 from PartStorage id {}'.format(ps.id))
        PartStorage.objects.first().delete()
        Log.objects.create(severity=20,module='inventory',user=None,message='Added nonesense for a random PartStorage')
        partstorage_threshold_check()
        for pst in PartStorageThreshold.objects.all():
            self.assertFalse(pst.alarm)

# Tests for ObjectAction
class ObjectActionTaskTests(TestCase):
    def setUp(self):
        EmailRecipient.objects.create(name='Zephyrus Marketing', email='marketing@zephyruscomputing.com')
        EmailTemplate.objects.create(name='ObjectAction Check Test', subject='test', template='tracker/templates/general.html')
        ObjectAction.objects.create(name='test_ObjectAction_1', objtype='p', action='c', objid=0)
        ObjectAction.objects.create(name='test_ObjectAction_2', objtype='a', action='m', objid=1)
        ObjectAction.objects.create(name='test_ObjectAction_3', objtype='as', action='d', objid=2)
        ObjectAction.objects.create(name='test_ObjectAction_4', objtype='s', action='c', objid=3)
        ObjectAction.objects.create(name='test_ObjectAction_5', objtype='ps', action='a', objid=4)
        for i in range(1,6):
            ObjectActionNotify.objects.create(objectaction=ObjectAction.objects.get(name='test_ObjectAction_{}'.format(i)), emailrecipient=EmailRecipient.objects.first())
        for i in range(20,26):
            Part.objects.create(name='test_part_' + str(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
            Storage.objects.create(name='test_storage_{}'.format(i), description='This is a TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=12)
            Kit.objects.create(name='test_kit_{}'.format(i), description='This is a TestCast test kit', sku=str(i).zfill(4), price=29.99)
            KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.last(), count=2)
            AlternateSKU.objects.create(manufacturer='Zephyrus Computing',sku=str(i*2).zfill(5))
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            if i > 20:
                Assembly.objects.create(parent=Part.objects.first(), part=Part.objects.last(), count=1)

    def test_object_action_check(self):
        part = Part.objects.first()
        storage = Storage.objects.first()
        ps = PartStorage.objects.first()
        kit = Kit.objects.first()
        kps = KitPartStorage.objects.first()
        altsku = AlternateSKU.objects.first()
        pa = PartAlternateSKU.objects.first()
        assembly = Assembly.objects.first()
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_c_p', objtype='p', action='c', objid=0)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_c_s', objtype='s', action='c', objid=0)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_c_ps', objtype='ps', action='c', objid=0)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_c_k', objtype='k', action='c', objid=0)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_c_kp', objtype='kp', action='c', objid=0)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_c_as', objtype='as', action='c', objid=0)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_c_pa', objtype='pa', action='c', objid=0)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_c_a', objtype='a', action='c', objid=0)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        Log.objects.create(severity=20,module='inventory-admin',user=None,message='Created Part id {}'.format(part.id))
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Created Storage id {}'.format(storage.id))
        Log.objects.create(severity=20,module='inventory-admin',user=None,message='Created PartStorage id {}'.format(ps.id))
        Log.objects.create(severity=20,module='inventory-admin',user=None,message='Created Kit id {}'.format(kit.id))
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Created KitPartStorage id {}'.format(kps.id))
        Log.objects.create(severity=20,module='inventory-admin',user=None,message='Created AlternateSKU id {}'.format(altsku.id))
        Log.objects.create(severity=20,module='inventory-admin',user=None,message='Created PartAltSKU id {}'.format(pa.id))
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Created Assembly id {}'.format(assembly.id))
        Log.objects.create(severity=20,module='inventory-admin',user=None,message='Created Banana id 12')
        object_action_check()
        for oa in ObjectAction.objects.filter(name__icontains='test_objectaction_c_'):
            notifies = Notification.objects.filter(notifyid=oa.id)
            self.assertTrue(len(notifies)>0)
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_a_ps', objtype='ps', action='a', objid=ps.id)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_a_k', objtype='k', action='a', objid=kit.id)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_s_ps', objtype='ps', action='s', objid=ps.id)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_s_k', objtype='k', action='s', objid=kit.id)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        Log.objects.create(severity=20,module='inventory',user=None,message='Added 1 to PartStorage id {}'.format(ps.id))
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Added 1 to Kit id {}'.format(kit.id))
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Subtracted 1 from PartStorage id {}'.format(ps.id))
        Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 1 from Kit id {}'.format(kit.id))
        object_action_check()
        for oa in ObjectAction.objects.filter(name__icontains='test_objectaction_a_'):
            notifies = Notification.objects.filter(notifyid=oa.id)
            self.assertTrue(len(notifies)>0)
        for oa in ObjectAction.objects.filter(name__icontains='test_objectaction_s_'):
            notifies = Notification.objects.filter(notifyid=oa.id)
            self.assertTrue(len(notifies)>0)
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_u_p', objtype='p', action='m', objid=part.id)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        oa = ObjectAction.objects.get_or_create(name='test_ObjectAction_u_s', objtype='s', action='m', objid=storage.id)[0]
        ObjectActionNotify.objects.create(objectaction=oa, emailrecipient=EmailRecipient.objects.first())
        Log.objects.create(severity=20,module='inventory',user=None,message='Gonna Added 1 from Part id {} and mess everything up'.format(part.id))
        Log.objects.create(severity=20,module='inventory-admin',user=None,message='Part updated successfully for id {}'.format(part.id))
        Log.objects.create(severity=20,module='inventory-api',user=None,message='Storage updated successfully for id {}'.format(storage.id))
        object_action_check()
        for oa in ObjectAction.objects.filter(name__icontains='test_objectaction_u_'):
            notifies = Notification.objects.filter(notifyid=oa.id)
            self.assertTrue(len(notifies)>0)

class KitThresholdTaskTests(TestCase):
    def setUp(self):
        EmailRecipient.objects.create(name='Zephyrus Marketing', email='marketing@zephyruscomputing.com')
        EmailTemplate.objects.create(name='PartStorage Threshold Check Test', subject='test', template='tracker/templates/general.html')
        for i in range(13,19):
            Part.objects.create(name='test_part_{}'.format(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
            Storage.objects.create(name='test_storage_{}'.format(i), description='This is a TestCase test storage')
            PartStorage.objects.create(part=Part.objects.get(name='test_part_{}'.format(i)), storage=Storage.objects.get(name='test_storage_{}'.format(i)), count=12)
            Kit.objects.create(name='test_kit_{}'.format(i), description='This is a TestCast test kit', sku=str(i).zfill(4), price=29.99)
            KitPartStorage.objects.create(kit=Kit.objects.get(name='test_kit_{}'.format(i)), partstorage=PartStorage.objects.get(part=Part.objects.get(name='test_part_{}'.format(i)), storage=Storage.objects.get(name='test_storage_{}'.format(i))), count=2)
        for i in range(1,6):
            j = randint(0,len(Kit.objects.all()) -1)
            KitThreshold.objects.create(name='test_kitthreshold_{}'.format(i), kit=Kit.objects.all()[j], threshold=6)
            KitThresholdTemplate.objects.create(kitthreshold=KitThreshold.objects.get(name='test_kitthreshold_{}'.format(i)), emailtemplate=EmailTemplate.objects.first())
            KitThresholdNotify.objects.create(kitthreshold=KitThreshold.objects.get(name='test_kitthreshold_{}'.format(i)), emailrecipient=EmailRecipient.objects.first())

    @override_settings(ADDON_TRACKER_NOTIFY_ONCE=False)
    def test_kit_threshold_check(self):
        #setUp
        for k in Kit.objects.all():
            for kps in k.get_components():
                kps.partstorage.count = 2
                kps.partstorage.save()
            Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 2 from Kit id {}'.format(k.id))
        kit_threshold_check()
        for kt in KitThreshold.objects.all():
            self.assertTrue(kt.alarm)
        for k in Kit.objects.all():
            for kps in k.get_components():
                kps.partstorage.count = 12
                kps.partstorage.save()
                Log.objects.create(severity=20,module='inventory',user=None,message='Added 10 from PartStorage id {}'.format(kps.partstorage.id))
        kit_threshold_check()
        for kt in KitThreshold.objects.all():
            self.assertFalse(kt.alarm)
        for k in Kit.objects.all():
            for kps in k.get_components():
                kps.partstorage.count = 2
                kps.partstorage.save()
            Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 2 from Kit id {}'.format(k.id))
        Kit.objects.first().delete()
        Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted nonesense for a random Kit')
        kit_threshold_check()
        for kt in KitThreshold.objects.all():
            self.assertTrue(kt.alarm)
        Log.objects.create(severity=20,module='inventory',user=None,message='Added nonesense for a random PartStorage')
        Log.objects.create(severity=20,module='inventory',user=None,message='Added 2 for Kit id -1')
        Log.objects.create(severity=20,module='inventory',user=None,message='Subtracted 45 for PartStorage id 123456789')
        kit_threshold_check()

class NotificationsUnitTests(TestCase):
    def setUp(self):
        EmailRecipient.objects.create(name='Zephyrus Marketing', email='marketing@zephyruscomputing.com')
        EmailTemplate.objects.create(name='Notification Check Test', subject='test', template='tracker/templates/general.html')
        for i in range(1,10):
            AlternateSKU.objects.create(sku='test_sku_{}'.format(str(i).zfill(2)), manufacturer='Zephyrus Computing')
            Part.objects.create(name='test_part_{}'.format(str(i).zfill(2)), description='TestCase test part', sku=str(i).zfill(2), price=1.0, cost=0.0)
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(2)), description='TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)
            Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(2)), description='TestCast test kit', sku=str(i).zfill(2), price=1.0)
        for i in range(1,4):
            j = randint(1,len(Part.objects.all()) -1)
            k = randint(1,len(Kit.objects.all()) -1)
            Assembly.objects.create(parent=Part.objects.first(),part=Part.objects.all()[j], count=2)
            KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.all()[k], count=2)
        for i in range(1,4):
            j = randint(0,len(Assembly.objects.all()) -1)
            k = randint(0,len(ACTIONS) -1)
            l = randint(0,len(OBJTYPES) -1)
            ObjectAction.objects.create(name='test_ObjectAction_{}'.format(str(i).zfill(3)), objtype=OBJTYPES[l], action=ACTIONS[k], objid=_get_object_number(OBJTYPES[l][0], j).id)
            ObjectActionTemplate.objects.create(objectaction=ObjectAction.objects.last(), emailtemplate=EmailTemplate.objects.first())
            ObjectActionNotify.objects.create(objectaction=ObjectAction.objects.last(), emailrecipient=EmailRecipient.objects.first())
        for i in range(1,4):
            j = randint(0,len(Part.objects.all()) -1)
            PartThreshold.objects.create(name='test_PartThreshold_{}'.format(str(i).zfill(3)), part=Part.objects.all()[j], threshold=6)
            PartThresholdTemplate.objects.create(partthreshold=PartThreshold.objects.last(), emailtemplate=EmailTemplate.objects.first())
            PartThresholdNotify.objects.create(partthreshold=PartThreshold.objects.last(), emailrecipient=EmailRecipient.objects.first())
        for i in range(1,4):
            j = randint(0,len(PartStorage.objects.all()) -1)
            PartStorageThreshold.objects.create(name='test_PartStorageThreshold_{}'.format(str(i).zfill(3)), partstorage=PartStorage.objects.all()[j], threshold=6)
            PartStorageThresholdTemplate.objects.create(partstoragethreshold=PartStorageThreshold.objects.last(), emailtemplate=EmailTemplate.objects.first())
            PartStorageThresholdNotify.objects.create(partstoragethreshold=PartStorageThreshold.objects.last(), emailrecipient=EmailRecipient.objects.first())
        for i in range(1,4):
            j = randint(0,len(Kit.objects.all()) -1)
            KitThreshold.objects.create(name='test_KitThreshold_{}'.format(str(i).zfill(3)), kit=Kit.objects.all()[j], threshold=6)
            KitThresholdTemplate.objects.create(kitthreshold=KitThreshold.objects.last(), emailtemplate=EmailTemplate.objects.first())
            KitThresholdNotify.objects.create(kitthreshold=KitThreshold.objects.last(), emailrecipient=EmailRecipient.objects.first())

    def test_send_notifications(self):
        er = EmailRecipient.objects.first()
        et = EmailTemplate.objects.first()
        pt = PartThreshold.objects.first()
        tpt = Threshold.objects.create(thresholdtype='pt', thresholdid=pt.id, count=6, current=4, status=True)
        ptn = PartThresholdNotify.objects.get(partthreshold=pt, emailrecipient=er)
        pst = PartStorageThreshold.objects.first()
        tpst = Threshold.objects.create(thresholdtype='pst', thresholdid=ptn.id, count=6, current=4, status=True)
        pstn = PartStorageThresholdNotify.objects.get(partstoragethreshold=pst, emailrecipient=er)
        oa = ObjectAction.objects.first()
        oan = ObjectActionNotify.objects.get(objectaction=oa, emailrecipient=er)
        kt = KitThreshold.objects.first()
        tkt = Threshold.objects.create(thresholdtype='kt', thresholdid=kt.id, count=6, current=4, status=True)
        ktn = KitThresholdNotify.objects.get(kitthreshold=kt, emailrecipient=er)
        Notification.objects.create(notifytype='pt', notifyid=ptn.id, threshold=tpt.id, status='p', emailrecipient=er)
        Notification.objects.create(notifytype='pst', notifyid=pstn.id, threshold=tpst.id, status='p', emailrecipient=er)
        Notification.objects.create(notifytype='oa', notifyid=oan.id, threshold=0, status='p', emailrecipient=er)
        Notification.objects.create(notifytype='kt', notifyid=ktn.id, threshold=0, status='p', emailrecipient=er)
        for n in Notification.objects.all():
            self.assertEqual(n.status, 'p')
        n = Notification.objects.get(notifytype='oa', notifyid=oan.id, threshold=0, status='p')
        send_notifications(er.id, et.subject, 'This is a test message', [n.id])
        n.refresh_from_db()
        self.assertEqual(n.status, 's')
        ids = []
        for n in Notification.objects.filter(status='p'):
            ids.append(n.id)
        send_notifications(er.id, et.subject, 'This is test message', ids)
        for n in Notification.objects.all():
            self.assertEqual(n.status, 's')
        send_notifications(0, et.subject, et.template, 0)
        n = Notification.objects.create(notifytype='oa', notifyid=oan.id, threshold=0, status='p', emailrecipient=er)
        # send_notifications(er.id, None, None, [n.id])
        # n.refresh_from_db()
        # self.assertEqual(n.status, 'e')

    def test_process_notifications(self):
        er = EmailRecipient.objects.first()
        et = EmailTemplate.objects.first()
        pt = PartThreshold.objects.last()
        tpt = Threshold.objects.create(thresholdtype='pt', thresholdid=pt.id, count=6, current=7, status=False)
        ptn = PartThresholdNotify.objects.get(partthreshold=pt, emailrecipient=er)
        pst = PartStorageThreshold.objects.last()
        tpst = Threshold.objects.create(thresholdtype='pst', thresholdid=pst.id, count=6, current=7, status=False)
        pstn = PartStorageThresholdNotify.objects.get(partstoragethreshold=pst, emailrecipient=er)
        oa = ObjectAction.objects.last()
        oan = ObjectActionNotify.objects.get(objectaction=oa, emailrecipient=er)
        kt = KitThreshold.objects.last()
        tkt = Threshold.objects.create(thresholdtype='kt', thresholdid=kt.id, count=6, current=4, status=True)
        ktn = KitThresholdNotify.objects.get(kitthreshold=kt, emailrecipient=er)
        Notification.objects.create(notifytype='pt', notifyid=ptn.id, threshold=tpt.id, status='q', emailrecipient=er)
        Notification.objects.create(notifytype='pst', notifyid=pstn.id, threshold=tpst.id, status='q', emailrecipient=er)
        Notification.objects.create(notifytype='oa', notifyid=oan.id, threshold=0, status='q', emailrecipient=er)
        Notification.objects.create(notifytype='kt', notifyid=ktn.id, threshold=tkt.id, status='q', emailrecipient=er)
        process_notifications()
        for n in Notification.objects.all():
            self.assertEqual(n.status, 'p')
        for t in Threshold.objects.all():
            t.delete()
        for n in Notification.objects.all():
            n.status = 'q'
            n.save()
        process_notifications()
        for ptt in PartThresholdTemplate.objects.all():
            ptt.delete()
        for pstt in PartStorageThresholdTemplate.objects.all():
            pstt.delete()
        for oat in ObjectActionTemplate.objects.all():
            oat.delete()
        for ktt in KitThresholdTemplate.objects.all():
            ktt.delete()
        for n in Notification.objects.all():
            n.status = 'q'
            n.save()
        process_notifications()
        for n in Notification.objects.all():
            self.assertEqual(n.status, 'e')
        for pst in PartStorageThreshold.objects.all():
            pst.delete()
        for pt in PartThreshold.objects.all():
            pt.delete()
        for oa in ObjectAction.objects.all():
            oa.delete()
        for kt in KitThreshold.objects.all():
            kt.delete()
        for n in Notification.objects.all():
            n.status = 'q'
            n.save()
        process_notifications()
        for n in Notification.objects.all():
            self.assertEqual(n.status, 'e')

class CleanupTasksUnitTests(TestCase):
    def setUp(self):
        self.directory = join(settings.BASE_DIR, 'media/tracker/graphs/tests')
        for i in range(0,randint(5,25)):
            with open(join(self.directory,'test_{}.png'.format(i)),'w') as file:
                file.write('0')
    
    def test_clean_tracker_graphs(self):
        files = listdir(self.directory)
        self.assertGreater(len(files), 0)
        clean_tracker_graphs(self.directory)
        files = listdir(self.directory)
        self.assertEqual(len(files), 0)

class ScheduledReportUnitTests(TestCase):
    def setUp(self):
        # need tasks, reportnotify, emailrecipient
        EmailRecipient.objects.create(name='Zephyrus Marketing', email='marketing@zephyruscomputing.com')
        EmailTemplate.objects.create(name='Notification Check Test', subject='test', template='tracker/templates/general.html')
        for i in range(1,10):
            AlternateSKU.objects.create(sku='test_sku_{}'.format(str(i).zfill(2)), manufacturer='Zephyrus Computing')
            Part.objects.create(name='test_part_{}'.format(str(i).zfill(2)), description='TestCase test part', sku=str(i).zfill(2), price=1.0, cost=0.0)
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(2)), description='TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)
            Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(2)), description='TestCast test kit', sku=str(i).zfill(2), price=1.0)
        for i in range(1,4):
            j = randint(1,len(Part.objects.all()) -1)
            k = randint(1,len(Kit.objects.all()) -1)
            Assembly.objects.create(parent=Part.objects.first(),part=Part.objects.all()[j], count=2)
            KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.all()[k], count=2)
            j = randint(0,len(Assembly.objects.all()) -1)
            k = randint(0,len(ACTIONS) -1)
            l = randint(0,len(OBJTYPES) -1)
            ObjectAction.objects.create(name='test_ObjectAction_{}'.format(str(i).zfill(3)), objtype=OBJTYPES[l], action=ACTIONS[k], objid=_get_object_number(OBJTYPES[l][0], j).id)
            j = randint(0,len(Part.objects.all()) -1)
            PartThreshold.objects.create(name='test_PartThreshold_{}'.format(str(i).zfill(3)), part=Part.objects.all()[j], threshold=6)
            j = randint(0,len(PartStorage.objects.all()) -1)
            PartStorageThreshold.objects.create(name='test_PartStorageThreshold_{}'.format(str(i).zfill(3)), partstorage=PartStorage.objects.all()[j], threshold=6)
            j = randint(0,len(Kit.objects.all()) -1)
            KitThreshold.objects.create(name='test_KitThreshold_{}'.format(str(i).zfill(3)), kit=Kit.objects.all()[j], threshold=6)

    def test_scheduled_report(self):
        for report in REPORTS:
            scheduled_report(report=report[0],ids=[i for i in range(1,randint(3,10))],duration=randint(1,5),span=TIMESPAN_CHOICES[randint(0,len(TIMESPAN_CHOICES)-1)],subject='Test Report')

def _get_object_number(objtype, num):
    if objtype == 'as':
        return Assembly.objects.all()[num]
    if objtype == 'a':
        return AlternateSKU.objects.all()[num]
    if objtype == 'p':
        return Part.objects.all()[num]
    if objtype == 's':
        return Storage.objects.all()[num]
    if objtype == 'ps':
        return PartStorage.objects.all()[num]
    if objtype == 'pa':
        return PartAlternateSKU.objects.all()[num]
    if objtype == 'k':
        return Kit.objects.all()[num]
    if objtype == 'kp':
        return KitPartStorage.objects.all()[num]
    return {'id':0}
