import django
django.setup()
from django.contrib.auth.models import User
from django.test import TestCase, override_settings
from random import randint
from inventory.models import Part, PartStorage, Storage, Kit
from ims.addons.tracker.models import PartThreshold, PartStorageThreshold, PartThresholdNotify, PartStorageThresholdNotify, PartThresholdTemplate, PartStorageThresholdTemplate, ObjectAction, ObjectActionTemplate, ObjectActionNotify, KitThreshold, KitThresholdNotify, KitThresholdTemplate, EmailRecipient, EmailTemplate, Notification, Threshold
from ims.addons.tracker.helpers import OBJTYPES, ACTIONS, OPERATORS, NOTIFYTYPES, STATUSES, get_status

# Unit tests for PartThreshold
class PartThresholdUnitTests(TestCase):
    def setUp(self):
        for i in range(1,11):
            Part.objects.create(name='test_part_{}'.format(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        for i in range(1,6):
            j = randint(1,10)
            pt = PartThreshold.objects.create(name='test_partthreshold_{}'.format(i), part=Part.objects.get(name='test_part_{}'.format(j)), threshold=6)

    def test_partthreshold_str(self):
        i = randint(1,5)
        p = PartThreshold.objects.get(name='test_partthreshold_{}'.format(i))
        self.assertEqual(p.__str__(), 'test_partthreshold_{}'.format(i))

    def test_partthreshold_absoluteurl(self):
        p = PartThreshold.objects.get(name='test_partthreshold_{}'.format(randint(1,5)))
        self.assertEqual(p.get_absolute_url(), '/tracker/partthreshold/{}/'.format(p.id))

    def test_partthreshold_get_last_triggered(self):
        p = PartThreshold.objects.get(name='test_partthreshold_{}'.format(randint(1,5)))
        self.assertEqual(p.get_last_triggered(), '')
        t = Threshold.objects.create(thresholdtype='pt',thresholdid=p.id,count=6,current=5,status=True)
        self.assertEqual(p.get_last_triggered(), t)
        t.delete()

    def test_partthreshold_get_last_triggered_datetime(self):
        p = PartThreshold.objects.get(name='test_partthreshold_{}'.format(randint(1,5)))
        self.assertEqual(p.get_last_triggered_datetime(), 'Never')
        t = Threshold.objects.create(thresholdtype='pt',thresholdid=p.id,count=6,current=5,status=True)
        self.assertEqual(p.get_last_triggered_datetime(), t.created)
        t.delete()

    def test_partthreshold_get_last_triggered_status(self):
        p = PartThreshold.objects.get(name='test_partthreshold_{}'.format(randint(1,5)))
        self.assertEqual(p.get_last_triggered_status(), 'unknown')
        t = Threshold.objects.create(thresholdtype='pt',thresholdid=p.id,count=6,current=5,status=True)
        self.assertEqual(p.get_last_triggered_status(), t.status)
        t.delete()

# Unit test for PartThresholdNotify
class PartThresholdNotifyUnitTests(TestCase):
    def setUp(self):
        user = User.objects.get_or_create(username='testuser',email='support@zephyruscomputing.com')[0]
        er = EmailRecipient.objects.get_or_create(name='testuser',email='support@zephyruscomputing.com')[0]
        for i in range(31,41):
            Part.objects.create(name='test_part_{}'.format(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        for i in range(12,18):
            j = randint(0,2)
            pt = PartThreshold.objects.create(name='test_partthreshold_{}'.format(i), part=Part.objects.all()[j], threshold=6)
            PartThresholdNotify.objects.create(partthreshold=pt,emailrecipient=er)

    def test_partthresholdnotify_str(self):
        er = EmailRecipient.objects.first()
        ptn = PartThresholdNotify.objects.all()[randint(0,PartThresholdNotify.objects.all().count() -1)]
        self.assertEqual(ptn.__str__(), '{} to {}'.format(ptn.partthreshold.name, er.email))

    def test_partthresholdnotify_get_last_triggered(self):
        er = EmailRecipient.objects.first()
        ptn = PartThresholdNotify.objects.all()[randint(0,PartThresholdNotify.objects.all().count() -1)]
        self.assertEqual(ptn.get_last_triggered(), '')
        n = Notification.objects.create(notifytype='pt',notifyid=ptn.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(ptn.get_last_triggered(), n)
        n.delete()

    def test_partthresholdnotify_get_last_triggered_datetime(self):
        er = EmailRecipient.objects.first()
        ptn = PartThresholdNotify.objects.all()[randint(0,PartThresholdNotify.objects.all().count() -1)]
        self.assertEqual(ptn.get_last_triggered_datetime(), 'Never')
        n = Notification.objects.create(notifytype='pt',notifyid=ptn.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(ptn.get_last_triggered_datetime(), n.lastmodified)
        n.delete()

    def test_partthresholdnotify_get_last_triggered_status(self):
        er = EmailRecipient.objects.first()
        ptn = PartThresholdNotify.objects.all()[randint(0,PartThresholdNotify.objects.all().count() -1)]
        self.assertEqual(ptn.get_last_triggered_status(), 'unknown')
        n = Notification.objects.create(notifytype='pt',notifyid=ptn.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(ptn.get_last_triggered_status(), get_status(n.status))
        n.delete()
        
# Unit tests for PartStorageThreshold
class PartStorageThresholdUnitTests(TestCase):
    def setUp(self):
        for i in range(11,21):
            Part.objects.create(name='test_part_{}'.format(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        for i in range(1,4):
            Storage.objects.create(name='test_storage_{}'.format(i), description='This is a TestCase test storage')
            j = randint(11,20)
            PartStorage.objects.create(part=Part.objects.get(name='test_part_{}'.format(j)), storage=Storage.objects.get(name='test_storage_{}'.format(i)), count=5)
        for i in range(1,6):
            j = randint(0,2)
            PartStorageThreshold.objects.create(name='test_partstoragethreshold_{}'.format(i), partstorage=PartStorage.objects.all()[j], threshold=6)

    def test_partstoragethreshold_str(self):
        i = randint(1,5)
        p = PartStorageThreshold.objects.get(name='test_partstoragethreshold_{}'.format(i))
        self.assertEqual(p.__str__(), 'test_partstoragethreshold_{}'.format(i))

    def test_partstoragethreshold_absoluteurl(self):
        p = PartStorageThreshold.objects.get(name='test_partstoragethreshold_{}'.format(randint(1,5)))
        self.assertEqual(p.get_absolute_url(), '/tracker/partstoragethreshold/{}/'.format(p.id))

    def test_partstoragethreshold_get_last_triggered(self):
        p = PartStorageThreshold.objects.get(name='test_partstoragethreshold_{}'.format(randint(1,5)))
        self.assertEqual(p.get_last_triggered(), '')
        t = Threshold.objects.create(thresholdtype='pst',thresholdid=p.id,count=6,current=5,status=True)
        self.assertEqual(p.get_last_triggered(), t)
        t.delete()

    def test_partstoragethreshold_get_last_triggered_datetime(self):
        p = PartStorageThreshold.objects.get(name='test_partstoragethreshold_{}'.format(randint(1,5)))
        self.assertEqual(p.get_last_triggered_datetime(), 'Never')
        t = Threshold.objects.create(thresholdtype='pst',thresholdid=p.id,count=6,current=5,status=True)
        self.assertEqual(p.get_last_triggered_datetime(), t.created)
        t.delete()

    def test_partstoragethreshold_get_last_triggered_status(self):
        p = PartStorageThreshold.objects.get(name='test_partstoragethreshold_{}'.format(randint(1,5)))
        self.assertEqual(p.get_last_triggered_status(), 'unknown')
        t = Threshold.objects.create(thresholdtype='pst',thresholdid=p.id,count=6,current=5,status=True)
        self.assertEqual(p.get_last_triggered_status(), t.status)
        t.delete()

# Unit test for PartStorageThresholdNotify
class PartStorageThresholdNotifyUnitTests(TestCase):
    def setUp(self):
        user = User.objects.get_or_create(username='testuser',email='support@zephyruscomputing.com')[0]
        er = EmailRecipient.objects.get_or_create(name='testuser',email='support@zephyruscomputing.com')[0]
        for i in range(21,31):
            Part.objects.create(name='test_part_{}'.format(i), description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        for i in range(4,7):
            Storage.objects.create(name='test_storage_{}'.format(i), description='This is a TestCase test storage')
            j = randint(21,30)
            PartStorage.objects.create(part=Part.objects.get(name='test_part_{}'.format(j)), storage=Storage.objects.get(name='test_storage_{}'.format(i)), count=5)
        for i in range(6,12):
            j = randint(0,2)
            pst = PartStorageThreshold.objects.create(name='test_partstoragethreshold_{}'.format(i), partstorage=PartStorage.objects.all()[j], threshold=6)
            PartStorageThresholdNotify.objects.create(partstoragethreshold=pst,emailrecipient=er)

    def test_partstoragethresholdnotify_str(self):
        er = EmailRecipient.objects.first()
        pstn = PartStorageThresholdNotify.objects.all()[randint(0,PartStorageThresholdNotify.objects.all().count() -1)]
        self.assertEqual(pstn.__str__(), '{} to {}'.format(pstn.partstoragethreshold.name, er.email))

    def test_partstoragethresholdnotify_get_last_triggered(self):
        er = EmailRecipient.objects.first()
        pstn = PartStorageThresholdNotify.objects.all()[randint(0,PartStorageThresholdNotify.objects.all().count() -1)]
        self.assertEqual(pstn.get_last_triggered(), '')
        n = Notification.objects.create(notifytype='pst',notifyid=pstn.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(pstn.get_last_triggered(), n)
        n.delete()

    def test_partstoragethresholdnotify_get_last_triggered_datetime(self):
        er = EmailRecipient.objects.first()
        pstn = PartStorageThresholdNotify.objects.all()[randint(0,PartStorageThresholdNotify.objects.all().count() -1)]
        self.assertEqual(pstn.get_last_triggered_datetime(), 'Never')
        n = Notification.objects.create(notifytype='pst',notifyid=pstn.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(pstn.get_last_triggered_datetime(), n.lastmodified)
        n.delete()

    def test_partstoragethresholdnotify_get_last_triggered_status(self):
        er = EmailRecipient.objects.first()
        pstn = PartStorageThresholdNotify.objects.all()[randint(0,PartStorageThresholdNotify.objects.all().count() -1)]
        self.assertEqual(pstn.get_last_triggered_status(), 'unknown')
        n = Notification.objects.create(notifytype='pst',notifyid=pstn.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(pstn.get_last_triggered_status(), get_status(n.status))
        n.delete()

class KitThresholdUnitTests(TestCase):
    def setUp(self):
        for i in range(1,11):
            Kit.objects.create(name='test_kit_{}'.format(i), description='This is a TestCase test kit', sku=str(i).zfill(4), price=9.99)
        for i in range(0,6):
            j = randint(0,2)
            KitThreshold.objects.create(name='test_kitthreshold_{}'.format(i), kit=Kit.objects.all()[j], threshold=6)

    def test_kitthreshold_str(self):
        i = randint(1,5)
        kt = KitThreshold.objects.get(name='test_kitthreshold_{}'.format(i))
        self.assertEqual(kt.__str__(), 'test_kitthreshold_{}'.format(i))

    def test_kitthreshold_absoluteurl(self):
        kt = KitThreshold.objects.get(name='test_kitthreshold_{}'.format(randint(1,5)))
        self.assertEqual(kt.get_absolute_url(), '/tracker/kitthreshold/{}/'.format(kt.id))

    def test_kitthreshold_get_last_triggered(self):
        kt = KitThreshold.objects.get(name='test_kitthreshold_{}'.format(randint(1,5)))
        self.assertEqual(kt.get_last_triggered(), '')
        t = Threshold.objects.create(thresholdtype='kt',thresholdid=kt.id,count=6,current=5,status=True)
        self.assertEqual(kt.get_last_triggered(), t)
        t.delete()

    def test_kitthreshold_get_last_triggered_datetime(self):
        kt = KitThreshold.objects.get(name='test_kitthreshold_{}'.format(randint(1,5)))
        self.assertEqual(kt.get_last_triggered_datetime(), 'Never')
        t = Threshold.objects.create(thresholdtype='kt',thresholdid=kt.id,count=6,current=5,status=True)
        self.assertEqual(kt.get_last_triggered_datetime(), t.created)
        t.delete()

    def test_kitthreshold_get_last_triggered_status(self):
        kt = KitThreshold.objects.get(name='test_kitthreshold_{}'.format(randint(1,5)))
        self.assertEqual(kt.get_last_triggered_status(), 'unknown')
        t = Threshold.objects.create(thresholdtype='kt',thresholdid=kt.id,count=6,current=5,status=True)
        self.assertEqual(kt.get_last_triggered_status(), t.status)
        t.delete()

class KitThresholdNotifyUnitTests(TestCase):
    def setUp(self):
        user = User.objects.get_or_create(username='testuser',email='support@zephyruscomputing.com')[0]
        er = EmailRecipient.objects.get_or_create(name='testuser',email='support@zephyruscomputing.com')[0]
        for i in range(1,11):
            Kit.objects.create(name='test_kit_{}'.format(i), description='This is a TestCase test kit', sku=str(i).zfill(4), price=9.99)
        for i in range(0,6):
            j = randint(0,2)
            kt = KitThreshold.objects.create(name='test_kitthreshold_{}'.format(i), kit=Kit.objects.all()[j], threshold=6)
            KitThresholdNotify.objects.create(kitthreshold=kt,emailrecipient=er)

    def test_objectactionnotify_str(self):
        er = EmailRecipient.objects.first()
        ktn = KitThresholdNotify.objects.all()[randint(0,KitThresholdNotify.objects.all().count() -1)]
        self.assertEqual(ktn.__str__(), '{} to {}'.format(ktn.kitthreshold.name, er.email))

    def test_objectactionnotify_get_last_triggered(self):
        er = EmailRecipient.objects.first()
        ktn = KitThresholdNotify.objects.all()[randint(0,KitThresholdNotify.objects.all().count() -1)]
        self.assertEqual(ktn.get_last_triggered(), '')
        n = Notification.objects.create(notifytype='kt',notifyid=ktn.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(ktn.get_last_triggered(), n)
        n.delete()

    def test_objectactionnotify_get_last_triggered_datetime(self):
        er = EmailRecipient.objects.first()
        ktn = KitThresholdNotify.objects.all()[randint(0,KitThresholdNotify.objects.all().count() -1)]
        self.assertEqual(ktn.get_last_triggered_datetime(), 'Never')
        n = Notification.objects.create(notifytype='kt',notifyid=ktn.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(ktn.get_last_triggered_datetime(), n.lastmodified)
        n.delete()

    def test_objectactionnotify_get_last_triggered_status(self):
        er = EmailRecipient.objects.first()
        ktn = KitThresholdNotify.objects.all()[randint(0,KitThresholdNotify.objects.all().count() -1)]
        self.assertEqual(ktn.get_last_triggered_status(), 'unknown')
        n = Notification.objects.create(notifytype='kt',notifyid=ktn.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(ktn.get_last_triggered_status(), get_status(n.status))
        n.delete()

# Unit test for EmailRecipient
class EmailRecipientUnitTests(TestCase):
    def setUp(self):
        for i in range(1,11):
            EmailRecipient.objects.create(name='test_recipient_{}'.format(i), email='test{}@zephyruscomputing.com'.format(i))

    def test_emailrecipient_str(self):
        i = randint(1,10)
        r = EmailRecipient.objects.get(name='test_recipient_{}'.format(i))
        self.assertEqual(r.__str__(), 'test_recipient_{} (test{}@zephyruscomputing.com)'.format(i, i))

    def test_emailrecipient_absoluteurl(self):
        r = EmailRecipient.objects.get(name='test_recipient_{}'.format(randint(1,10)))
        self.assertEqual(r.get_absolute_url(), '/tracker/emailrecipient/{}/'.format(r.id))

    def test_emailrecipient_partthresholds(self):
        i = randint(1,10)
        r = EmailRecipient.objects.get(name='test_recipient_{}'.format(i))
        self.assertFalse(r.has_partthresholds())
        Part.objects.get_or_create(name='test_part_r', description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        PartThreshold.objects.create(name='test_partthreshold_r', part=Part.objects.get(name='test_part_r'), threshold=6)
        PartThresholdNotify.objects.create(partthreshold=PartThreshold.objects.get(name='test_partthreshold_r'), emailrecipient=r)
        self.assertTrue(r.has_partthresholds())

    def test_emailrecipient_partstoragethresholds(self):
        i = randint(1,10)
        r = EmailRecipient.objects.get(name='test_recipient_{}'.format(i))
        self.assertFalse(r.has_partstoragethresholds())
        Part.objects.get_or_create(name='test_part_r', description='This is a TestCase test part', sku=str(i).zfill(4), price=9.99, cost=3.14)
        Storage.objects.get_or_create(name='test_storage_r', description='This is a TestCase test storage')
        PartStorage.objects.get_or_create(part=Part.objects.get(name='test_part_r'), storage=Storage.objects.get(name='test_storage_r'), count=1)
        PartStorageThreshold.objects.create(name='test_partstoragethreshold_r', partstorage=PartStorage.objects.last(), threshold=6)
        PartStorageThresholdNotify.objects.create(partstoragethreshold=PartStorageThreshold.objects.get(name='test_partstoragethreshold_r'), emailrecipient=r)
        self.assertTrue(r.has_partstoragethresholds())

    def test_emailrecipient_objectactions(self):
        r = EmailRecipient.objects.get(name='test_recipient_{}'.format(randint(1,10)))
        self.assertFalse(r.has_objectactions())
        ObjectAction.objects.get_or_create(name='test_objectaction_r', objtype='a', action='c', objid=0)
        ObjectActionNotify.objects.get_or_create(objectaction=ObjectAction.objects.get(name='test_objectaction_r'), emailrecipient=r)
        self.assertTrue(r.has_objectactions())

    def test_emailrecipient_notifications(self):
        EmailRecipient.objects.create(name='test_recipient_r', email='testr@zephyruscomputing.com')
        r = EmailRecipient.objects.get(name='test_recipient_r')
        self.assertFalse(r.has_notifications())

# Unit test for EmailTemplate
class EmailTemplateUnitTests(TestCase):
    def setUp(self):
        for i in range(1,11):
            EmailTemplate.objects.create(name='test_template_{}'.format(i), subject='test{}'.format(i), template='<html><body>Test{}</body></html>'.format(i))

    def test_emailtemplate_str(self):
        i = randint(1,10)
        t = EmailTemplate.objects.get(name='test_template_{}'.format(i))
        self.assertEqual(t.__str__(), 'test_template_{}'.format(i))

# Unit test for ObjectAction
class ObjectActionUnitTests(TestCase):
    def setUp(self):
        for i in range(1,4):
            j = randint(0, len(ACTIONS) -1)
            k = randint(0, len(OBJTYPES) -1)
            l = randint(0, 1)
            ObjectAction.objects.create(name='test_objectaction_{}'.format(i), objtype=OBJTYPES[k], action=ACTIONS[j], objid=l)

    def test_objectaction_str(self):
        o = ObjectAction.objects.get(name='test_objectaction_{}'.format(randint(1,3)))
        self.assertEqual(o.__str__(), 'test_objectaction_{}'.format(o.id))

    def test_objectaction_get_absolute_url(self):
        o = ObjectAction.objects.get(name='test_objectaction_{}'.format(randint(1,3)))
        self.assertEqual(o.get_absolute_url(), '/tracker/objectaction/{}/'.format(o.id))

    def test_objectaction_get_last_triggered(self):
        o = ObjectAction.objects.get(name='test_objectaction_{}'.format(randint(1,3)))
        self.assertEqual(o.get_last_triggered(), '')
        t = Threshold.objects.create(thresholdtype='oa',thresholdid=o.id,count=0,current=0,status=True)
        self.assertEqual(o.get_last_triggered(), t)
        t.delete()

    def test_objectaction_get_last_triggered_datetime(self):
        o = ObjectAction.objects.get(name='test_objectaction_{}'.format(randint(1,3)))
        self.assertEqual(o.get_last_triggered_datetime(), 'Never')
        t = Threshold.objects.create(thresholdtype='oa',thresholdid=o.id,count=0,current=0,status=True)
        self.assertEqual(o.get_last_triggered_datetime(), t.created)
        t.delete()

    def test_objectaction_get_last_triggered_status(self):
        o = ObjectAction.objects.get(name='test_objectaction_{}'.format(randint(1,3)))
        self.assertEqual(o.get_last_triggered_status(), 'unknown')
        t = Threshold.objects.create(thresholdtype='oa',thresholdid=o.id,count=0,current=0,status=True)
        self.assertEqual(o.get_last_triggered_status(), t.status)
        t.delete()

    def test_objectaction_get_objtype_name(self):
        o = ObjectAction.objects.create(name='test_objectaction_cp0', objtype='p', action='c', objid=0)
        self.assertEqual(o.get_objtype_name(), 'Part')

    def test_objectaction_get_action_name(self):
        o = ObjectAction.objects.create(name='test_objectaction_dps0', objtype='ps', action='d', objid=0)
        self.assertEqual(o.get_action_name(), 'Deleted')

    def test_objectaction_get_objid_name(self):
        o = ObjectAction.objects.create(name='test_objectaction_akp0', objtype='kp', action='a', objid=0)
        self.assertEqual(o.get_objid_name(), 'All')

# Unit test for ObjectActionNotify
class ObjectActionNotifyUnitTests(TestCase):
    def setUp(self):
        user = User.objects.get_or_create(username='testuser',email='support@zephyruscomputing.com')[0]
        er = EmailRecipient.objects.get_or_create(name='testuser',email='support@zephyruscomputing.com')[0]
        for i in range(1,4):
            j = randint(0, len(ACTIONS) -1)
            k = randint(0, len(OBJTYPES) -1)
            l = randint(0, 1)
            oa = ObjectAction.objects.create(name='test_objectaction_{}'.format(i), objtype=OBJTYPES[k], action=ACTIONS[j], objid=l)
            ObjectActionNotify.objects.create(objectaction=oa,emailrecipient=er)

    def test_objectactionnotify_str(self):
        er = EmailRecipient.objects.first()
        oan = ObjectActionNotify.objects.all()[randint(0,ObjectActionNotify.objects.all().count() -1)]
        self.assertEqual(oan.__str__(), '{} to {}'.format(oan.objectaction.name, er.email))

    def test_objectactionnotify_get_last_triggered(self):
        er = EmailRecipient.objects.first()
        oan = ObjectActionNotify.objects.all()[randint(0,ObjectActionNotify.objects.all().count() -1)]
        self.assertEqual(oan.get_last_triggered(), '')
        n = Notification.objects.create(notifytype='oa',notifyid=oan.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(oan.get_last_triggered(), n)
        n.delete()

    def test_objectactionnotify_get_last_triggered_datetime(self):
        er = EmailRecipient.objects.first()
        oan = ObjectActionNotify.objects.all()[randint(0,ObjectActionNotify.objects.all().count() -1)]
        self.assertEqual(oan.get_last_triggered_datetime(), 'Never')
        n = Notification.objects.create(notifytype='oa',notifyid=oan.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(oan.get_last_triggered_datetime(), n.lastmodified)
        n.delete()

    def test_objectactionnotify_get_last_triggered_status(self):
        er = EmailRecipient.objects.first()
        oan = ObjectActionNotify.objects.all()[randint(0,ObjectActionNotify.objects.all().count() -1)]
        self.assertEqual(oan.get_last_triggered_status(), 'unknown')
        n = Notification.objects.create(notifytype='oa',notifyid=oan.id,threshold=0,emailrecipient=er,status='p')
        self.assertEqual(oan.get_last_triggered_status(), get_status(n.status))
        n.delete()

# Unit test for Notification
class NotificationUnitTests(TestCase):
    def setUp(self):
        for i in range(11,21):
            EmailRecipient.objects.create(name='test_recipient_{}'.format(i), email='test{}@zephyruscomputing.com'.format(i))
        for i in range(1,3):
            j = randint(11,20)
            Notification.objects.create(notifytype='pt',notifyid=0,threshold=0,emailrecipient=EmailRecipient.objects.get(name='test_recipient_{}'.format(j)),status='p')

    def test_notification_str(self):
        n = Notification.objects.last()
        self.assertEqual(n.__str__(), 'Part Threshold 0 Pending')

# Unit test for Threshold
class ThresholdUnitTests(TestCase):
    def setUp(self):
        for i in range(1,3):
            Threshold.objects.create(thresholdtype='pt',thresholdid=0,count=0,current=0,status=False)

    def test_threshold_str(self):
        t = Threshold.objects.last()
        self.assertEqual(t.__str__(), 'Part Threshold 0 False')


