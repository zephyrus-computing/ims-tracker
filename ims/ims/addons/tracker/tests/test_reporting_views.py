import django
django.setup()
from django.conf import settings
from django.contrib.auth.models import AnonymousUser, Permission, User
from django.test import TestCase, override_settings
from django.utils.timezone import now
from django_celery_beat.models import ClockedSchedule, CrontabSchedule, IntervalSchedule, PeriodicTask, SolarSchedule, MINUTES, HOURS
from random import randint, choice
from rest_framework import status
from inventory.models import Part, Storage, AlternateSKU, PartAlternateSKU, PartStorage, Assembly, Kit, KitPartStorage
from ims.addons.tracker.models import PartThreshold, PartStorageThreshold, PartThresholdNotify, PartStorageThresholdNotify, PartThresholdTemplate, PartStorageThresholdTemplate, ObjectAction, ObjectActionTemplate, ObjectActionNotify, KitThreshold, KitThresholdNotify, KitThresholdTemplate, EmailRecipient, EmailTemplate, Notification, Threshold, ReportNotify
from ims.addons.tracker.helpers import OPERATORS, REPORTS, TIMESPAN_CHOICES, SCHEDULER_CHOICES, SOLAR_CHOICES, MODEL_CHOICES
from log.models import Log


baseurl = '/tracker/reporting/'

class ReportingHomeViewTests(TestCase):
    def test_view_reporting_home(self):
        response = self.client.get(baseurl)
        self.assertRedirects(response, '/accounts/login/?next={}'.format(baseurl))
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        response = self.client.get(baseurl)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/reporting/home.html')

class ReportingRecentViewTests(TestCase):
    def setUp(self):
        user = User.objects.get_or_create(username='testuser')[0]
        for i in range(1241,1250):
            obj = AlternateSKU.objects.create(sku='test_alternatesku_{}'.format(str(i).zfill(3)), manufacturer='Zephyrus Computing')
            Log.objects.create(severity=20,module='inventory-api',user=user,message='Created AlternateSKU id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='AlternateSKU Updated id {}'.format(obj.id))
            obj = Part.objects.create(name='test_part_{}'.format(str(i).zfill(3)), description='TestCase test part', sku=str(i).zfill(3), price=1.0, cost=0.0)
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Created Part id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Part Updated id {}'.format(obj.id))
            obj = PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            Log.objects.create(severity=20,module='inventory-api',user=user,message='Created PartAlternateSKU id {}'.format(obj.id))
            obj = Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(3)), description='TestCase test storage')
            Log.objects.create(severity=20,module='inventory-api',user=user,message='Created Storage id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Storage Updated id {}'.format(obj.id))
            obj = PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Created PartStorage id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='PartStorage Updated id {}'.format(obj.id))
            obj = Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(3)), description='TestCase test kit', sku=str(i).zfill(3), price=1.0)
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Created Kit id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Kit Updated id {}'.format(obj.id))
            obj = KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.last(), count=1)
            Log.objects.create(severity=20,module='inventory-api',user=user,message='Created KitPartStorage id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='KitPartStorage Updated id {}'.format(obj.id))
        for i in range(1,4):
            j = randint(1,len(Part.objects.all()) -1)
            obj = Assembly.objects.create(parent=Part.objects.first(),part=Part.objects.all()[j], count=2)
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Created Assembly id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Assembly Updated id {}'.format(obj.id))
        self.baseurl = '{}recent/'.format(baseurl)
        user = User.objects.get_or_create(username='testuser')[0]
        for p in ['can_view_recentadd','can_view_recentmod','can_view_useract']:
            user.user_permissions.add(Permission.objects.get(codename=p))
        self.client.force_login(user)

    def test_reporting_recent_adds(self):
        part = Part.objects.first()
        response = self.client.get('{}adds/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.client.force_login(User.objects.get_or_create(username='testuser')[0])
        self.assertTemplateUsed(response, 'tracker/reporting/recent/adds-mods.html')
        response = self.client.get('{}adds/?id=p&duration=1&span=m'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, part.name)
        response = self.client.get('{}adds/?id=s&duration=1&span=m'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, part.name)
        response = self.client.get('{}adds/?id=s&banana=1&hammock=m'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, part.name)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}adds/'.format(self.baseurl))
        self.assertRedirects(response, '/accounts/login/?next={}adds/'.format(self.baseurl))

    def test_reporting_recent_mods(self):
        part = Part.objects.first()
        response = self.client.get('{}mods/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/reporting/recent/adds-mods.html')
        response = self.client.get('{}mods/?id=p&duration=1&span=m'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, part.name)
        response = self.client.get('{}mods/?id=s&duration=1&span=m'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, part.name)
        response = self.client.get('{}mods/?id=s&banana=1&hammock=m'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, part.name)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}adds/'.format(self.baseurl))
        self.assertRedirects(response, '/accounts/login/?next={}adds/'.format(self.baseurl))

    def test_reporting_recent_user(self):
        user = User.objects.get_or_create(username='testuser')[0]
        part = Part.objects.first()
        response = self.client.get('{}user/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/reporting/recent/user-act.html')
        response = self.client.get('{}user/?user={}&duration=1&span=m'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Created Part id {}'.format(part.id))
        response = self.client.get('{}user/?user={}&duration=1&span=m'.format(self.baseurl, User.objects.get_or_create(username='testuser2')[0].id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Created Part id {}'.format(part.id))
        response = self.client.get('{}user/?user={}&banana=1&hammock=m'.format(self.baseurl, user.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, 'Created Part id {}'.format(part.id))
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}adds/'.format(self.baseurl))
        self.assertRedirects(response, '/accounts/login/?next={}adds/'.format(self.baseurl))

class ReportingCountsViewTests(TestCase):
    def setUp(self):
        for i in range(1251,1260):
            AlternateSKU.objects.create(sku='test_alternatesku_{}'.format(str(i).zfill(3)), manufacturer='Zephyrus Computing')
            Part.objects.create(name='test_part_{}'.format(str(i).zfill(3)), description='TestCase test part', sku=str(i).zfill(3), price=1.0, cost=0.0)
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(3)), description='TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)
            Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(3)), description='TestCase test kit', sku=str(i).zfill(3), price=1.0)
            KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.last(), count=1)
        for i in range(1,4):
            j = randint(1,len(Part.objects.all()) -1)
            Assembly.objects.create(parent=Part.objects.first(),part=Part.objects.all()[j], count=2)
        self.baseurl = '{}counts/'.format(baseurl)
        user = User.objects.get_or_create(username='testuser')[0]
        for p in ['can_view_partcount','can_view_kitcount','can_view_contents']:
            user.user_permissions.add(Permission.objects.get(codename=p))
        self.client.force_login(user)

    def test_reporting_counts_part(self):
        response = self.client.get('{}part/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/reporting/count/parts.html')
        params = '?part={}'.format('&part='.join([str(p.id) for p in Part.objects.all()]))
        response = self.client.get('{}part/{}'.format(self.baseurl, params))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for p in Part.objects.all():
            self.assertContains(response, p.name, 2)
        response = self.client.get('{}part/?&banana=1&hammock=m'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, Part.objects.first().name, 1)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}part/'.format(self.baseurl))
        self.assertRedirects(response, '/accounts/login/?next={}part/'.format(self.baseurl))

    def test_reporting_counts_kit(self):
        response = self.client.get('{}kit/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/reporting/count/kits.html')
        params = '?kit={}'.format('&kit='.join([str(k.id) for k in Kit.objects.all()]))
        response = self.client.get('{}kit/{}'.format(self.baseurl, params))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for k in Kit.objects.all():
            self.assertContains(response, k.name, 2)
        response = self.client.get('{}kit/?&banana=1&hammock=m'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, Kit.objects.first().name, 1)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}kit/'.format(self.baseurl))
        self.assertRedirects(response, '/accounts/login/?next={}kit/'.format(self.baseurl))

    def test_reporting_counts_storage(self):
        storage = Storage.objects.first()
        response = self.client.get('{}storage/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/reporting/count/storage.html')
        response = self.client.get('{}storage/?id={}'.format(self.baseurl, storage.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for p in storage.get_parts():
            self.assertContains(response, p.name)
        response = self.client.get('{}storage/?&banana=1&hammock=m'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, Part.objects.first().name)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}storage/'.format(self.baseurl))
        self.assertRedirects(response, '/accounts/login/?next={}storage/'.format(self.baseurl))

class ReportingAjaxViewTests(TestCase):
    def setUp(self):
        User.objects.get_or_create(username='testuser',email='support@zephyruscomputing.com')
        EmailRecipient.objects.get_or_create(name='testuser',email='support@zephyruscomputing.com')
        for i in range(1261,1270):
            AlternateSKU.objects.create(sku='test_alternatesku_{}'.format(str(i).zfill(3)), manufacturer='Zephyrus Computing')
            Part.objects.create(name='test_part_{}'.format(str(i).zfill(3)), description='TestCase test part', sku=str(i).zfill(3), price=1.0, cost=0.0)
            PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(3)), description='TestCase test storage')
            PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)
            Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(3)), description='TestCase test kit', sku=str(i).zfill(3), price=1.0)
            KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.last(), count=1)
        for i in range(1,4):
            j = randint(1,len(Part.objects.all()) -1)
            Assembly.objects.create(parent=Part.objects.first(),part=Part.objects.all()[j], count=2)
        for i in range(20,40):
            kwargs = '{}"report":"{}","ids":{},"duration":"{}","span":"{}","subject":"{}"{}'.format('{',REPORTS[randint(0,len(REPORTS)-1)],[i for i in range(0,randint(3,10))],randint(1,5),TIMESPAN_CHOICES[randint(0,len(TIMESPAN_CHOICES)-1)],'Test Report','}')
            j = choice(['c','i','s','t'])
            if j == 'c': 
                schedule = ClockedSchedule.objects.get_or_create(clocked_time=now())[0]
                PeriodicTask.objects.create(enabled=True,name='Test_Task_{}'.format(i),description='This is a test task',clocked=schedule,one_off=True,task='scheduled_report',start_time=now(),kwargs=kwargs) 
            if j == 'i':
                schedule = IntervalSchedule.objects.get_or_create(every=randint(1,20),period=MINUTES)[0]
                PeriodicTask.objects.create(enabled=True,name='Test_Task_{}'.format(i),description='This is a test task',interval=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs) 
            if j == 's':
                schedule = SolarSchedule.objects.get_or_create(event=SOLAR_CHOICES[randint(0,len(SOLAR_CHOICES)-1)],longitude=randint(-180,180),latitude=randint(-90,90))[0]
                PeriodicTask.objects.create(enabled=True,name='Test_Task_{}'.format(i),description='This is a test task',solar=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs)
            if j == 't':
                schedule = CrontabSchedule.objects.get_or_create(minute=choice(['*',range(0,60)]),hour=choice(['*',range(0,60)]),day_of_month=choice(['*',range(0,30)]),month_of_year=choice(['*',range(0,12)]),day_of_week=choice(['*',range(0,7)]),timezone=choice(['UTC','US/Eastern','US/Central','US/Mountain','US/Pacific','US/Arizona','US/Hawaii','US/Alaska']))[0]
                PeriodicTask.objects.create(enabled=True,name='Test_Task_{}'.format(i),description='This is a test task',crontab=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs)
        self.baseurl = '{}'.format(baseurl)
        user = User.objects.get_or_create(username='testuser')[0]
        for p in ['can_view_recentadd','can_view_recentmod','can_view_useract','can_view_partcount','can_view_kitcount','can_view_contents','can_view_histpart','can_view_histkit','can_view_histps','can_change_schdreport','can_delete_schdreport','can_subscribe_schdreport']:
            user.user_permissions.add(Permission.objects.get(codename=p))
        self.client.force_login(user)

    def test_ajax_graph_models(self):
        response = self.client.get('{}graph_models/p/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, Part.objects.first().name)
        response = self.client.get('{}graph_models/s/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, Storage.objects.first().name)
        for model in ['a','as','hacl','hkcl','hpcl','hpscl','k','kcb','kps','p','pa','pcb','ps','s','scb']:
            response = self.client.get('{}graph_models/{}/'.format(self.baseurl, model))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}graph_models/scb/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, Storage.objects.first().name)

    def test_ajax_report_dropdown_options(self):
        response = self.client.get('{}report_options/recent_adds_list/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, MODEL_CHOICES[0][1])
        response = self.client.get('{}report_options/recent_mods_list/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, MODEL_CHOICES[0][1])
        response = self.client.get('{}report_options/recent_user_list/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, User.objects.first().username)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}report_options/recent_mods_list/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotContains(response, MODEL_CHOICES[0][1])

    def test_ajax_scheduler_delete(self):
        task = PeriodicTask.objects.last()
        response = self.client.get('{}scheduler/delete/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertNotIn(task, PeriodicTask.objects.all())
        response = self.client.get('{}scheduler/delete/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}scheduler/delete/{}/'.format(self.baseurl, task.id))
        self.assertRedirects(response, '/accounts/login/?next={}scheduler/delete/{}/'.format(self.baseurl, task.id))
    
    def test_ajax_scheduler_assign(self):
        task = PeriodicTask.objects.first()
        er = EmailRecipient.objects.get(name='testuser')
        response = self.client.get('{}scheduler/assign/{}/{}/'.format(self.baseurl, task.id, er.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Successfully assigned {} to {}.'.format(er.name, task.name))
        response = self.client.get('{}scheduler/assign/{}/{}/'.format(self.baseurl, task.id, er.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Subscription already exists.')
        response = self.client.get('{}scheduler/assign/{}/{}/'.format(self.baseurl, task.id, 1001))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.client.get('{}scheduler/assign/{}/{}/'.format(self.baseurl, 1001, er.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}scheduler/assign/{}/{}/'.format(self.baseurl, task.id, er.id))
        self.assertRedirects(response, '/accounts/login/?next={}scheduler/assign/{}/{}/'.format(self.baseurl, task.id, er.id))
    
    def test_ajax_scheduler_unassign(self):
        task = PeriodicTask.objects.first()
        er = EmailRecipient.objects.get(name='testuser')
        ReportNotify.objects.get_or_create(report=task,emailrecipient=er)[0]
        response = self.client.get('{}scheduler/unassign/{}/{}/'.format(self.baseurl, task.id, er.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Successfully unassigned {} from {}.'.format(er.name, task.name))
        response = self.client.get('{}scheduler/unassign/{}/{}/'.format(self.baseurl, task.id, er.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Subscription does not exist.')
        response = self.client.get('{}scheduler/unassign/{}/{}/'.format(self.baseurl, task.id, 1001))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        response = self.client.get('{}scheduler/unassign/{}/{}/'.format(self.baseurl, 1001, er.id))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}scheduler/unassign/{}/{}/'.format(self.baseurl, task.id, er.id))
        self.assertRedirects(response, '/accounts/login/?next={}scheduler/unassign/{}/{}/'.format(self.baseurl, task.id, er.id))
    
    def test_ajax_scheduler_subscribe(self):
        task = PeriodicTask.objects.first()
        er = EmailRecipient.objects.get(name='testuser')
        response = self.client.get('{}scheduler/subscribe/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Successfully Subscribed to {}.'.format(task.name))
        response = self.client.get('{}scheduler/subscribe/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Subscription already exists.')
        er.delete()
        response = self.client.get('{}scheduler/subscribe/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'No matching EmailRecipient found.')
        response = self.client.get('{}scheduler/subscribe/{}/'.format(self.baseurl, 1001))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}scheduler/subscribe/{}/'.format(self.baseurl, task.id))
        self.assertRedirects(response, '/accounts/login/?next={}scheduler/subscribe/{}/'.format(self.baseurl, task.id))

    def test_ajax_scheduler_unsubscribe(self):
        task = PeriodicTask.objects.first()
        er = EmailRecipient.objects.get(name='testuser')
        ReportNotify.objects.get_or_create(report=task,emailrecipient=er)[0]
        response = self.client.get('{}scheduler/unsubscribe/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Successfully Unsubscribed from {}.'.format(task.name))
        response = self.client.get('{}scheduler/unsubscribe/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'Subscription does not exist.')
        er.delete()
        response = self.client.get('{}scheduler/unsubscribe/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, 'No matching EmailRecipient found.')
        response = self.client.get('{}scheduler/unsubscribe/{}/'.format(self.baseurl, 1001))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}scheduler/unsubscribe/{}/'.format(self.baseurl, task.id))
        self.assertRedirects(response, '/accounts/login/?next={}scheduler/unsubscribe/{}/'.format(self.baseurl, task.id))

class ReportingSchedulerViewTests(TestCase):
    def setUp(self):
        for i in range(0,20):
            kwargs = '{}"report":"{}","ids":{},"duration":"{}","span":"{}","subject":"{}"{}'.format('{',REPORTS[randint(0,len(REPORTS)-1)],[i for i in range(0,randint(3,10))],randint(1,5),TIMESPAN_CHOICES[randint(0,len(TIMESPAN_CHOICES)-1)],'Test Report','}')
            j = choice(['c','i','s','t'])
            if j == 'c': 
                schedule = ClockedSchedule.objects.get_or_create(clocked_time=now())[0]
                PeriodicTask.objects.create(enabled=True,name='Test_Task_{}'.format(i),description='This is a test task',clocked=schedule,one_off=True,task='scheduled_report',start_time=now(),kwargs=kwargs) 
            if j == 'i':
                schedule = IntervalSchedule.objects.get_or_create(every=randint(1,20),period=MINUTES)[0]
                PeriodicTask.objects.create(enabled=True,name='Test_Task_{}'.format(i),description='This is a test task',interval=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs) 
            if j == 's':
                schedule = SolarSchedule.objects.get_or_create(event=SOLAR_CHOICES[randint(0,len(SOLAR_CHOICES)-1)],longitude=randint(-180,180),latitude=randint(-90,90))[0]
                PeriodicTask.objects.create(enabled=True,name='Test_Task_{}'.format(i),description='This is a test task',solar=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs)
            if j == 't':
                schedule = CrontabSchedule.objects.get_or_create(minute=choice(['*',range(0,60)]),hour=choice(['*',range(0,60)]),day_of_month=choice(['*',range(0,30)]),month_of_year=choice(['*',range(0,12)]),day_of_week=choice(['*',range(0,7)]),timezone=choice(['UTC','US/Eastern','US/Central','US/Mountain','US/Pacific','US/Arizona','US/Hawaii','US/Alaska']))[0]
                PeriodicTask.objects.create(enabled=True,name='Test_Task_{}'.format(i),description='This is a test task',crontab=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs)
        self.baseurl = '{}scheduler/'.format(baseurl)
        user = User.objects.get_or_create(username='testuser',email='support@zephyruscomputing.com')[0]
        for p in ['can_view_schdreport','can_change_schdreport','can_add_schdreport']:
            user.user_permissions.add(Permission.objects.get(codename=p))
        self.client.force_login(user)

    def test_reporting_scheduler(self):
        response = self.client.get('{}'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/reporting/scheduler/list.html')
        response = self.client.get('{}?q=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, PeriodicTask.objects.first().name)
        response = self.client.get('{}?page=test'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = self.client.get('{}?page=100'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}'.format(self.baseurl))
        self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))

    def test_reporting_scheduler_view(self):
        task = PeriodicTask.objects.first()
        response = self.client.get('{}view/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/reporting/scheduler/view.html')
        kwargs = '{}"report":"{}","ids":{},"subject":"{}"{}'.format('{',REPORTS[randint(0,len(REPORTS)-1)],[i for i in range(0,randint(3,10))],'Test Report','}')
        schedule = IntervalSchedule.objects.get_or_create(every=(randint(1,4)*7),period='days')[0]
        task = PeriodicTask.objects.create(enabled=True,name='Test_Task_View-interval',description='This ia a test task',interval=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs) 
        er = EmailRecipient.objects.create(name='testuser',email='support@zephyruscomputing.com')
        ReportNotify.objects.create(report=task, emailrecipient=er)
        response = self.client.get('{}view/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schedule = IntervalSchedule.objects.get_or_create(every=(randint(1,6)),period='days')[0]
        task = PeriodicTask.objects.create(enabled=True,name='Test_Task_View-interval2',description='This ia a test task',interval=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs) 
        response = self.client.get('{}view/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schedule = CrontabSchedule.objects.get_or_create(minute=0,hour='*',day_of_month='*',month_of_year='*',day_of_week='*',timezone='UTC')[0]
        task = PeriodicTask.objects.create(enabled=True,name='Test_Task_View-crontab',description='This ia a test task',crontab=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs) 
        response = self.client.get('{}view/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schedule = ClockedSchedule.objects.get_or_create(clocked_time=now())[0]
        task = PeriodicTask.objects.create(enabled=True,name='Test_Task_View-clocked',description='This ia a test task',one_off=True,clocked=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs) 
        response = self.client.get('{}view/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        schedule = SolarSchedule.objects.get_or_create(event='sunset',latitude=45,longitude=-120)[0]
        task = PeriodicTask.objects.create(enabled=True,name='Test_Task_View-solar',description='This ia a test task',solar=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs) 
        response = self.client.get('{}view/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}view/{}/'.format(self.baseurl, task.id))
        self.assertRedirects(response, '/accounts/login/?next={}view/{}/'.format(self.baseurl, task.id))

    def test_reporting_scheduler_create(self):
        response = self.client.get('{}create/'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/reporting/scheduler/create.html')
        for scheduler in SCHEDULER_CHOICES:
            data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-{}'.format(scheduler[0]), 'description':'A Test Scheduled Report','schedule':scheduler[0]}
            response = self.client.post('{}create/'.format(self.baseurl),data)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        for scheduler in SCHEDULER_CHOICES:
            data = {}
            if scheduler[0] == 'c':
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-clocked', 'description':'A Test Scheduled Report','schedule':scheduler[0],'date':now().date(),'time':now().time(),'timezone':'America/Los Angeles','duration':5,'span':'h'}
            if scheduler[0] == 't':
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-crontab', 'description':'A Test Scheduled Report','schedule':scheduler[0],'minute':'*','hour':'*','day':'*','month':'*','day_of_week':'*','timezone':'US/Pacific','duration':3,'span':'d'}
            if scheduler[0] == 'i':
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-interval', 'description':'A Test Scheduled Report','schedule':scheduler[0],'number':6,'period':'h','duration':6,'span':'h'}
            if scheduler[0] == 's':
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-solar', 'description':'A Test Scheduled Report','schedule':scheduler[0],'event':'sr','latitude':45,'longitude':-120,'duration':15,'span':'m'}
            response = self.client.post('{}create/'.format(self.baseurl),data)
            self.assertRedirects(response, self.baseurl)
        data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-weeks', 'description':'A Test Scheduled Report','schedule':'i','number':6,'period':'h','duration':2,'span':'w'}
        response = self.client.post('{}create/'.format(self.baseurl),data)
        self.assertRedirects(response, self.baseurl)
        data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-bad','schedule':'i','number':6,'period':'h','duration':2,'span':'d'}
        response = self.client.post('{}create/'.format(self.baseurl),data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}create/'.format(self.baseurl))
        self.assertRedirects(response, '/accounts/login/?next={}create/'.format(self.baseurl))

    def test_reporting_scheduler_change(self):
        task = PeriodicTask.objects.filter(task='scheduled_report').first()
        er = EmailRecipient.objects.create(name='testuser',email='support@zephyruscomputing.com')
        rn = ReportNotify.objects.create(report=task,emailrecipient=er)
        response = self.client.get('{}change/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/reporting/scheduler/update.html')
        rn.delete()
        response = self.client.get('{}change/{}/'.format(self.baseurl, task.id))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        for scheduler in SCHEDULER_CHOICES:
            task.refresh_from_db()
            if task.clocked != None: current = 'c'
            if task.crontab != None: current = 't'
            if task.interval != None: current = 'i'
            if task.solar != None: current = 's'
            data = {}
            if scheduler[0] == 'c':
                othertask = PeriodicTask.objects.filter(task='scheduled_report',solar=None,interval=None,crontab=None).first()
                response = self.client.get('{}change/{}/'.format(self.baseurl, othertask.id))
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-clocked', 'description':'A Test Scheduled Report','schedule':scheduler[0],'duration':5,'span':'h'}
                response = self.client.post('{}change/{}/'.format(self.baseurl, task.id),data)
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                if current != 'c':
                    task.refresh_from_db()
                    self.assertIsNone(task.clocked)
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-clocked', 'description':'A Test Scheduled Report','schedule':scheduler[0],'date':now().date(),'time':now().time(),'timezone':'Etc/GMT-8','duration':5,'span':'h'}
                response = self.client.post('{}change/{}/'.format(self.baseurl, task.id),data)
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                task.refresh_from_db()
                self.assertIsNotNone(task.clocked)
                self.assertIsNone(task.crontab)
                self.assertIsNone(task.interval)
                self.assertIsNone(task.solar)
            if scheduler[0] == 't':
                othertask = PeriodicTask.objects.filter(task='scheduled_report',solar=None,interval=None,clocked=None).first()
                response = self.client.get('{}change/{}/'.format(self.baseurl, othertask.id))
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-crontab', 'description':'A Test Scheduled Report','schedule':scheduler[0],'duration':3,'span':'d'}
                response = self.client.post('{}change/{}/'.format(self.baseurl, task.id),data)
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                if current != 't':
                    task.refresh_from_db()
                    self.assertIsNone(task.crontab)
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-crontab', 'description':'A Test Scheduled Report','schedule':scheduler[0],'minute':'*','hour':'*','day':'*','month':'*','day_of_week':'*','timezone':'Etc/Universal','duration':3,'span':'d'}
                response = self.client.post('{}change/{}/'.format(self.baseurl, task.id),data)
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                task.refresh_from_db()
                self.assertIsNone(task.clocked)
                self.assertIsNotNone(task.crontab)
                self.assertIsNone(task.interval)
                self.assertIsNone(task.solar)
            if scheduler[0] == 'i':
                othertask = PeriodicTask.objects.filter(task='scheduled_report',solar=None,clocked=None,crontab=None).first()
                response = self.client.get('{}change/{}/'.format(self.baseurl, othertask.id))
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-interval', 'description':'A Test Scheduled Report','schedule':scheduler[0],'duration':6,'span':'h'}
                response = self.client.post('{}change/{}/'.format(self.baseurl, task.id),data)
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                if current != 'i':
                    task.refresh_from_db()
                    self.assertIsNone(task.interval)
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-interval', 'description':'A Test Scheduled Report','schedule':scheduler[0],'number':6,'period':'h','duration':6,'span':'h'}
                response = self.client.post('{}change/{}/'.format(self.baseurl, task.id),data)
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                task.refresh_from_db()
                self.assertIsNone(task.clocked)
                self.assertIsNone(task.crontab)
                self.assertIsNotNone(task.interval)
                self.assertIsNone(task.solar)
            if scheduler[0] == 's':
                othertask = PeriodicTask.objects.filter(task='scheduled_report',clocked=None,interval=None,crontab=None).first()
                response = self.client.get('{}change/{}/'.format(self.baseurl, othertask.id))
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-solar', 'description':'A Test Scheduled Report','schedule':scheduler[0],'duration':15,'span':'m'}
                response = self.client.post('{}change/{}/'.format(self.baseurl, task.id),data)
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                if current != 's':
                    task.refresh_from_db()
                    self.assertIsNone(task.solar)
                data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-solar', 'description':'A Test Scheduled Report','schedule':scheduler[0],'event':'sr','latitude':45,'longitude':-120,'duration':15,'span':'m'}
                response = self.client.post('{}change/{}/'.format(self.baseurl, task.id),data)
                self.assertEqual(response.status_code, status.HTTP_200_OK)
                task.refresh_from_db()
                self.assertIsNone(task.clocked)
                self.assertIsNone(task.crontab)
                self.assertIsNone(task.interval)
                self.assertIsNotNone(task.solar)
        data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-weeks', 'description':'A Test Scheduled Report','schedule':'i','duration':5,'span':'w'}
        response = self.client.post('{}change/{}/'.format(self.baseurl, task.id),data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = {'report': REPORTS[randint(0,len(REPORTS)-1)][0], 'name':'Test_View_Scheduled_Report-weeks','schedule':'i','duration':5,'span':'m'}
        response = self.client.post('{}change/{}/'.format(self.baseurl, task.id),data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}change/{}/'.format(self.baseurl, task.id))
        self.assertRedirects(response, '/accounts/login/?next={}change/{}/'.format(self.baseurl, task.id))

class ReportingGraphViewTests(TestCase):
    def setUp(self):
        user = User.objects.get_or_create(username='testuser')[0]
        for i in range(1271,1280):
            obj = AlternateSKU.objects.create(sku='test_alternatesku_{}'.format(str(i).zfill(3)), manufacturer='Zephyrus Computing')
            Log.objects.create(severity=20,module='inventory-api',user=user,message='Created AlternateSKU id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='AlternateSKU Updated id {}'.format(obj.id))
            obj = Part.objects.create(name='test_part_{}'.format(str(i).zfill(3)), description='TestCase test part', sku=str(i).zfill(3), price=1.0, cost=0.0)
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Created Part id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Part Updated id {}'.format(obj.id))
            obj = PartAlternateSKU.objects.create(part=Part.objects.last(), alt_sku=AlternateSKU.objects.last())
            Log.objects.create(severity=20,module='inventory-api',user=user,message='Created PartAlternateSKU id {}'.format(obj.id))
            obj = Storage.objects.create(name='test_storage_{}'.format(str(i).zfill(3)), description='TestCase test storage')
            Log.objects.create(severity=20,module='inventory-api',user=user,message='Created Storage id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Storage Updated id {}'.format(obj.id))
            obj = PartStorage.objects.create(part=Part.objects.last(), storage=Storage.objects.last(), count=4)
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Created PartStorage id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='PartStorage Updated id {}'.format(obj.id))
            obj = Kit.objects.create(name='test_kit_{}'.format(str(i).zfill(3)), description='TestCase test kit', sku=str(i).zfill(3), price=1.0)
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Created Kit id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Kit Updated id {}'.format(obj.id))
            obj = KitPartStorage.objects.create(kit=Kit.objects.last(), partstorage=PartStorage.objects.last(), count=1)
            Log.objects.create(severity=20,module='inventory-api',user=user,message='Created KitPartStorage id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='KitPartStorage Updated id {}'.format(obj.id))
        for i in range(1,4):
            j = randint(1,len(Part.objects.all()) -1)
            obj = Assembly.objects.create(parent=Part.objects.first(),part=Part.objects.all()[j], count=2)
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Created Assembly id {}'.format(obj.id))
            Log.objects.create(severity=20,module='inventory-admin',user=user,message='Assembly Updated id {}'.format(obj.id))
        for i in range(0,100):
            j = choice([PartStorage, Kit])
            k = choice(['inventory','inventory-api'])
            l = choice(['Added','Subtracted'])
            obj = j.objects.all()[randint(0,j.objects.all().count() -1)]
            Log.objects.create(severity=20,module=k,user=user,message='{} {} to {} id {}'.format(l,randint(1,5),obj.__class__.__name__,obj.id))
        self.baseurl = '{}graph/'.format(baseurl)
        for p in ['can_view_graph']:
            user.user_permissions.add(Permission.objects.get(codename=p))
        self.client.force_login(user)

    def test_reporting_graph(self):
        response = self.client.get('{}'.format(self.baseurl))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'tracker/reporting/graph.html')
        for model in ['hkcl','hpcl','hpscl','kcb','pcb','scb']:
            response = self.client.get('{}?graph={}&duration=1&span=m'.format(self.baseurl, model))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertNotContains(response, '.png')
            if model == 'kcb' or model == 'pcb':
                ids = ''
                for i in range(0,randint(1,5)):
                    ids += 'id={}&'.format(randint(1,9))
                ids = ids.rstrip('&')
            else:
                ids = 'id={}'.format(randint(1,9))
            response = self.client.get('{}?graph={}&duration=1&span=m&{}'.format(self.baseurl, model, ids))
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertContains(response, '.png')
        # Test Access Restriction
        self.client.force_login(User.objects.get_or_create(username='testuser2')[0])
        response = self.client.get('{}'.format(self.baseurl))
        self.assertRedirects(response, '/accounts/login/?next={}'.format(self.baseurl))

