from django.conf import settings
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django_celery_beat.models import IntervalSchedule, PeriodicTask
from itertools import chain
import logging

import ims.addons.tracker.helpers as helpers

logger = logging.getLogger(__name__)
from log.logger import getLogger
logger2 = getLogger('tracker')

class EmailRecipient(models.Model):
    name = models.CharField(max_length=256)
    email = models.EmailField(max_length=256,unique=True)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name','email']

    def __str__(self):
        return self.name + ' (' + self.email + ')'

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for PartThreshold id {}'.format(self.id))
        return reverse('ims.addons.tracker:emailrecipient_detail', args=[self.id])

    def get_partthresholds(self):
        logger.debug('Generating pt_list of PartThreshold for EmailRecipient id {}'.format(self.id))
        object_list = PartThresholdNotify.objects.filter(emailrecipient=self)
        pt_list = []
        for obj in object_list:
            if obj.partthreshold not in pt_list:
                pt_list.append(obj.partthreshold)
        return pt_list

    def has_partthresholds(self):
        logger.debug('Checking if PartThreshold exists for EmailRecipient id {}'.format(self.id))
        object_list = self.get_partthresholds()
        return len(object_list) > 0

    def get_partstoragethresholds(self):
        logger.debug('Generating pst_list of PartStorageThreshold for EmailRecipient id {}'.format(self.id))
        object_list = PartStorageThresholdNotify.objects.filter(emailrecipient=self)
        pst_list = []
        for obj in object_list:
            if obj.partstoragethreshold not in pst_list:
                pst_list.append(obj.partstoragethreshold)
        return pst_list

    def has_partstoragethresholds(self):
        logger.debug('Checking if PartStorageThreshold exists for EmailRecipient id {}'.format(self.id))
        object_list = self.get_partstoragethresholds()
        return len(object_list) > 0

    def get_objectactions(self):
        logger.debug('Generating oa_list of ObjectAction for EmailRecipient id {}'.format(self.id))
        object_list = ObjectActionNotify.objects.filter(emailrecipient=self)
        oa_list = []
        for obj in object_list:
            if obj.objectaction not in oa_list:
                oa_list.append(obj.objectaction)
        return oa_list

    def has_objectactions(self):
        logger.debug('Checking if ObjectAction exists for EmailRecipient id {}'.format(self.id))
        object_list = self.get_objectactions()
        return len(object_list) > 0
    
    def get_kitthresholds(self):
        logger.debug('Generating kt_list of KitThreshold for EmailRecipient id {}'.format(self.id))
        object_list = KitThresholdNotify.objects.filter(emailrecipient=self)
        kt_list = []
        for obj in object_list:
            if obj.kitthreshold not in kt_list:
                kt_list.append(obj.kitthreshold)
        return kt_list
    
    def has_kitthresholds(self):
        logger.debug('Checking if KitThreshold exists for EmailRecipient id {}'.format(self.id))
        object_list = self.get_kitthresholds()
        return len(object_list) > 0

    def get_notifications(self):
        logger.debug('Generating list of *model*Notify for EmailRecipient id {}'.format(self.id))
        raw = chain(self.get_partthresholds(), self.get_partstoragethresholds(), self.get_kitthresholds(), self.get_objectactions())
        results = sorted(list(raw), key=lambda r: str(r))
        return results

    def has_notifications(self):
        logger.debug('Checking if *model*Notify exists for EmailRecipient id {}'.format(self.id))
        object_list = self.get_notifications()
        return len(object_list) > 0

class PartThreshold(models.Model):
    name = models.CharField(max_length=256,unique=True)
    part = models.ForeignKey('inventory.Part',on_delete=models.CASCADE,related_name='+')
    threshold = models.PositiveIntegerField(default=0)
    operator = models.CharField(max_length=2,choices=helpers.OPERATORS,default=helpers.LESSTHAN)
    alarm = models.BooleanField(default=False,editable=False)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name','part','threshold']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for PartThreshold id {}'.format(self.id))
        return reverse('ims.addons.tracker:status_partthreshold_detail', args=[self.id])

    def get_last_triggered(self):
        logger.debug('Getting last triggered Threshold for PartThreshold id {}'.format(self.id))
        try:
            last = Threshold.objects.filter(thresholdtype=helpers.PARTTHRESHOLD,thresholdid=self.id).latest('created')
            return last
        except:
            logger.debug('No Thresholds found for PartThreshold id {}'.format(self.id))
            return ''

    def get_last_triggered_datetime(self):
        logger.debug('Getting last triggered datetime for PartThreshold id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'Never'
        else:
            return last.created

    def get_last_triggered_status(self):
        logger.debug('Getting last triggered status for PartThreshold id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'unknown'
        return last.status

class PartThresholdNotify(models.Model):
    partthreshold = models.ForeignKey('PartThreshold',on_delete=models.CASCADE)
    emailrecipient = models.ForeignKey('EmailRecipient',on_delete=models.CASCADE)

    class Meta:
        ordering = ['partthreshold','emailrecipient']
        unique_together = ('partthreshold','emailrecipient')

    def __str__(self):
        return '{} to {}'.format(self.partthreshold.name, self.emailrecipient.email)

    def get_last_triggered(self):
        logger.debug('Getting Notifications to determine last triggered for PartThresholdNotify id {}'.format(self.id))
        try:
            last = Notification.objects.filter(notifytype=helpers.PARTTHRESHOLD,notifyid=self.id).latest('lastmodified')
            return last
        except:
            logger.debug('No Notifications found for PartThresholdNotify id {}'.format(self.id))
            return ''
    
    def get_last_triggered_datetime(self):
        logger.debug('Getting last triggered datetime for PartThresholdNotify id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'Never'
        else:
            return last.lastmodified
    
    def get_last_triggered_status(self):
        logger.debug('Getting last triggered status for PartThresholdNotify id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'unknown'
        return helpers.get_status(last.status)

class EmailTemplate(models.Model):
    name = models.CharField(max_length=256,unique=True)
    subject = models.CharField(max_length=50)
    template = models.FileField(upload_to='tracker/templates/',max_length=50)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']
        
    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        logger.debug('Generating absolute URL for PartThreshold id {}'.format(self.id))
        return reverse('ims.addons.tracker:emailtemplate_detail', args=[self.id])

    def get_partthresholds(self):
        logger.debug('Generating pt_list of PartThreshold for EmailTemplate id {}'.format(self.id))
        object_list = PartThresholdTemplate.objects.filter(emailtemplate=self)
        pt_list = []
        for obj in object_list:
            if obj.partthreshold not in pt_list:
                pt_list.append(obj.partthreshold)
        return pt_list

    def has_partthresholds(self):
        logger.debug('Checking if PartThreshold exists for EmailTemplate id {}'.format(self.id))
        object_list = self.get_partthresholds()
        return len(object_list) > 0

    def get_partstoragethresholds(self):
        logger.debug('Generating pst_list of PartStorageThreshold for EmailTemplate id {}'.format(self.id))
        object_list = PartStorageThresholdTemplate.objects.filter(emailtemplate=self)
        pst_list = []
        for obj in object_list:
            if obj.partstoragethreshold not in pst_list:
                pst_list.append(obj.partstoragethreshold)
        return pst_list

    def has_partstoragethresholds(self):
        logger.debug('Checking if PartStorageThreshold exists for EmailTemplate id {}'.format(self.id))
        object_list = self.get_partstoragethresholds()
        return len(object_list) > 0

    def get_objectactions(self):
        logger.debug('Generating oa_list of ObjectAction for EmailTemplate id {}'.format(self.id))
        object_list = ObjectActionTemplate.objects.filter(emailtemplate=self)
        oa_list = []
        for obj in object_list:
            if obj.objectaction not in oa_list:
                oa_list.append(obj.objectaction)
        return oa_list

    def has_objectactions(self):
        logger.debug('Checking if ObjectAction exists for EmailTemplate id {}'.format(self.id))
        object_list = self.get_objectactions()
        return len(object_list) > 0
    
    def get_kitthresholds(self):
        logger.debug('Generating kt_list of KitThreshold for EmailTemplate id {}'.format(self.id))
        object_list = KitThresholdTemplate.objects.filter(emailtemplate=self)
        kt_list = []
        for obj in object_list:
            if obj.kitthreshold not in kt_list:
                kt_list.append(obj.kitthreshold)
        return kt_list
    
    def has_kitthresholds(self):
        logger.debug('Checking if KitThreshold exists for EmailTemplate id {}'.format(self.id))
        object_list = self.get_kitthresholds()
        return len(object_list) > 0

class PartThresholdTemplate(models.Model):
    partthreshold = models.ForeignKey('PartThreshold',on_delete=models.CASCADE)
    emailtemplate = models.ForeignKey('EmailTemplate',on_delete=models.CASCADE)
    
    class Meta:
        ordering = ['partthreshold','emailtemplate']
        unique_together = ('partthreshold','emailtemplate')

    def __str__(self):
        return '{} render {}'.format(self.emailtemplate.name, self.partthreshold.name)

class PartStorageThreshold(models.Model):
    name = models.CharField(max_length=256,unique=True)
    partstorage = models.ForeignKey('inventory.PartStorage',on_delete=models.CASCADE,related_name='+')
    threshold = models.PositiveIntegerField(default=0)
    operator = models.CharField(max_length=2,choices=helpers.OPERATORS,default=helpers.LESSTHAN)
    alarm = models.BooleanField(default=False,editable=False)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name','partstorage','threshold']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for PartStorageThreshold id ' + str(self.id))
        return reverse('ims.addons.tracker:status_partstoragethreshold_detail', args=[self.id])

    def get_last_triggered(self):
        logger.debug('Getting Thresholds to determine last triggered for PartStorageThreshold id {}'.format(self.id))
        try:
            last = Threshold.objects.filter(thresholdtype=helpers.PARTSTORAGETHRESHOLD,thresholdid=self.id).latest('created')
            return last
        except:
            logger.debug('No Thresholds found for PartStorageThreshold id {}'.format(self.id))
            return ''
    
    def get_last_triggered_datetime(self):
        logger.debug('Getting last triggered datetime for PartStorageThreshold id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'Never'
        else:
            return last.created
    
    def get_last_triggered_status(self):
        logger.debug('Getting last triggered status for PartStorageThreshold id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'unknown'
        return last.status

class PartStorageThresholdNotify(models.Model):
    partstoragethreshold = models.ForeignKey('PartStorageThreshold',on_delete=models.CASCADE)
    emailrecipient = models.ForeignKey('EmailRecipient',on_delete=models.CASCADE)

    class Meta:
        ordering = ['partstoragethreshold','emailrecipient']
        unique_together = ('partstoragethreshold','emailrecipient')

    def __str__(self):
        return '{} to {}'.format(self.partstoragethreshold.name, self.emailrecipient.email)

    def get_last_triggered(self):
        logger.debug('Getting Notifications to determine last triggered for PartStorageThresholdNotify id {}'.format(self.id))
        try:
            last = Notification.objects.filter(notifytype=helpers.PARTSTORAGETHRESHOLD,notifyid=self.partstoragethreshold.id).latest('lastmodified')
            return last
        except:
            logger.debug('No Notifications found for PartStorageThresholdNotify id {}'.format(self.id))
            return ''
    
    def get_last_triggered_datetime(self):
        logger.debug('Getting last triggered datetime for PartStorageThresholdNotify id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'Never'
        else:
            return last.lastmodified
    
    def get_last_triggered_status(self):
        logger.debug('Getting last triggered status for PartStorageThresholdNotify id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'unknown'
        return helpers.get_status(last.status)

class PartStorageThresholdTemplate(models.Model):
    partstoragethreshold = models.ForeignKey('PartStorageThreshold',on_delete=models.CASCADE)
    emailtemplate = models.ForeignKey('EmailTemplate',on_delete=models.CASCADE)
    
    class Meta:
        ordering = ['partstoragethreshold','emailtemplate']
        unique_together = ('partstoragethreshold','emailtemplate')

    def __str__(self):
        return '{} render {}'.format(self.emailtemplate.name, self.partstoragethreshold.name)

class ObjectAction(models.Model):
    name = models.CharField(max_length=256,unique=True)
    objtype = models.CharField(max_length=2, choices=helpers.OBJTYPES)
    action = models.CharField(max_length=2, choices=helpers.ACTIONS)
    objid = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for ObjectAction id ' + str(self.id))
        return reverse('ims.addons.tracker:status_objectaction_detail', args=[self.id])

    def get_last_triggered(self):
        logger.debug('Getting Thresholds to determine last triggered for ObjectAction id {}'.format(self.id))
        try:
            last = Threshold.objects.filter(thresholdtype=helpers.OBJECTACTION,thresholdid=self.id).latest('created')
            return last
        except:
            logger.debug('No Thresholds found for ObjectAction id {}'.format(self.id))
            return ''

    def get_last_triggered_datetime(self):
        logger.debug('Getting last triggered datetime for ObjectAction id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'Never'
        else:
            return last.created
    
    def get_last_triggered_status(self):
        logger.debug('Getting last triggered status for ObjectAction id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'unknown'
        return last.status
    
    def get_objtype_name(self):
        logger.debug('Getting Object Type Name for ObjectAction id {}'.format(self.id))
        return helpers.get_objtype(self.objtype)

    def get_action_name(self):
        logger.debug('Getting Action Name for ObjectAction id {}'.format(self.id))
        return helpers.get_action(self.action)

    def get_objid_name(self):
        logger.debug('Getting Object Id Name for ObjectAction id {}'.format(self.id))
        name = 'All'
        try:
            name = helpers.get_object(self.objtype, self.objid)
            if name == '':
                return 'All'
            name = str(name)
        except:
            pass
        return name


class ObjectActionNotify(models.Model):
    objectaction = models.ForeignKey('ObjectAction',on_delete=models.CASCADE)
    emailrecipient = models.ForeignKey('EmailRecipient',on_delete=models.CASCADE)

    class Meta:
        ordering = ['objectaction','emailrecipient']
        unique_together = ('objectaction','emailrecipient')

    def __str__(self):
        return '{} to {}'.format(self.objectaction.name, self.emailrecipient.email)

    def get_last_triggered(self):
        logger.debug('Getting Notifications to determine last triggered for ObjectActionNotify id {}'.format(self.id))
        try:
            last = Notification.objects.filter(notifytype=helpers.OBJECTACTION,notifyid=self.id).latest('lastmodified')
            return last
        except:
            logger.debug('No Notifications found for ObjectActionNotify id {}'.format(self.id))
            return ''
    
    def get_last_triggered_datetime(self):
        logger.debug('Getting last triggered datetime for ObjectActionNotify id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'Never'
        else:
            return last.lastmodified
    
    def get_last_triggered_status(self):
        logger.debug('Getting last triggered status for ObjectActionNotify id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'unknown'
        return helpers.get_status(last.status)

class ObjectActionTemplate(models.Model):
    objectaction = models.ForeignKey('ObjectAction',on_delete=models.CASCADE)
    emailtemplate = models.ForeignKey('EmailTemplate',on_delete=models.CASCADE)

    class Meta:
        ordering = ['objectaction','emailtemplate']
        unique_together = ('objectaction','emailtemplate')

    def __str__(self):
        return '{} render {}'.format(self.emailtemplate.name, self.objectaction.name)

class KitThreshold(models.Model):
    name = models.CharField(max_length=256,unique=True)
    kit = models.ForeignKey('inventory.Kit',on_delete=models.CASCADE,related_name='+')
    threshold = models.PositiveIntegerField(default=0)
    operator = models.CharField(max_length=2,choices=helpers.OPERATORS,default=helpers.LESSTHAN)
    alarm = models.BooleanField(default=False,editable=False)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['name','kit','threshold']
    
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        logger.debug('Generating absolute URL for kit id ' + str(self.id))
        return reverse('ims.addons.tracker:status_kitthreshold_detail', args=[self.id])

    def get_last_triggered(self):
        logger.debug('Getting Thresholds to determine last triggered for KitThreshold id {}'.format(self.id))
        try:
            last = Threshold.objects.filter(thresholdtype=helpers.KITTHRESHOLD,thresholdid=self.id).latest('created')
            return last
        except:
            logger.debug('No Thresholds found for KitThreshold id {}'.format(self.id))
        return ''

    def get_last_triggered_datetime(self):
        logger.debug('Getting last triggered datetime for KitThreshold id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'Never'
        else:
            return last.created
    
    def get_last_triggered_status(self):
        logger.debug('Getting last triggered status for KitThreshold id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'unknown'
        return last.status

class KitThresholdTemplate(models.Model):
    kitthreshold = models.ForeignKey('KitThreshold',on_delete=models.CASCADE)
    emailtemplate = models.ForeignKey('EmailTemplate',on_delete=models.CASCADE)

    class Meta:
        ordering = ['kitthreshold','emailtemplate']
        unique_together = ('kitthreshold','emailtemplate')
    
    def __str__(self):
        return '{} render {}'.format(self.emailtemplate.name, self.kitthreshold.name)

class KitThresholdNotify(models.Model):
    kitthreshold = models.ForeignKey('KitThreshold',on_delete=models.CASCADE)
    emailrecipient = models.ForeignKey('EmailRecipient',on_delete=models.CASCADE)

    class Meta:
        ordering = ['kitthreshold','emailrecipient']
        unique_together = ('kitthreshold','emailrecipient')
    
    def __str__(self):
        return '{} to {}'.format(self.kitthreshold.name, self.emailrecipient.email)

    def get_last_triggered(self):
        logger.debug('Getting Notifications to determine last triggered for KitThresholdNotify id {}'.format(self.id))
        try:
            last = Notification.objects.filter(notifytype=helpers.KITTHRESHOLD,notifyid=self.kitthreshold.id).latest('lastmodified')
            return last
        except:
            logger.debug('No Notifications found for KitThresholdNotify id {}'.format(self.id))
            return ''
    
    def get_last_triggered_datetime(self):
        logger.debug('Getting last triggered datetime for KitThresholdNotify id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'Never'
        else:
            return last.lastmodified
    
    def get_last_triggered_status(self):
        logger.debug('Getting last triggered status for KitThresholdNotify id {}'.format(self.id))
        last = self.get_last_triggered()
        if last == '':
            return 'unknown'
        return helpers.get_status(last.status)

class ReportNotify(models.Model):
    report = models.ForeignKey(PeriodicTask,on_delete=models.CASCADE)
    emailrecipient = models.ForeignKey('EmailRecipient',on_delete=models.CASCADE)
    
class Report(ReportNotify):
    class Meta:
        proxy = True
        permissions = [
            ('can_view_graph', 'Can View Graphs'),
            ('can_view_recentadd','Can View Recently Added'),
            ('can_view_recentmod','Can View Recently Modified'),
            ('can_view_useract','Can View User Activity'),
            ('can_view_partcount','Can View Part Counts'),
            ('can_view_kitcount','Can View Kit Counts'),
            ('can_view_contents','Can View Storage Contents'),
            ('can_view_histpart','Can View Historic Part Counts'),
            ('can_view_histkit','Can View Historic Kit Counts'),
            ('can_view_histps','Can View Historic PartStorage Counts'),
            ('can_add_schdreport', 'Can Add Scheduled Reports'),
            ('can_delete_schdreport', 'Can Delete Scheduled Reports'),
            ('can_view_schdreport', 'Can View Scheduled Reports'),
            ('can_change_schdreport', 'Can Change Scheduled Reports'),
            ('can_subscribe_schdreport', 'Can (Un)Subscribe Scheduled Reports'),
        ]

class Notification(models.Model):
    notifytype = models.CharField(max_length=3, choices=helpers.NOTIFYTYPES)
    notifyid = models.PositiveIntegerField(default=0)
    threshold = models.PositiveIntegerField(default=0)
    emailrecipient = models.ForeignKey('EmailRecipient',on_delete=models.CASCADE)
    status = models.CharField(max_length=3, choices=helpers.STATUSES)
    created = models.DateTimeField(auto_now_add=True,editable=False)
    lastmodified = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ['notifytype','notifyid','status']

    def __str__(self):
        return '{} {} {}'.format(helpers.get_notifytype(self.notifytype), self.notifyid, helpers.get_status(self.status))

class Threshold(models.Model):
    thresholdtype = models.CharField(max_length=3, choices=helpers.NOTIFYTYPES)
    thresholdid = models.PositiveIntegerField(default=0)
    count = models.PositiveIntegerField(default=0)
    current = models.PositiveIntegerField(default=0)
    status = models.BooleanField(default=False,editable=False)
    created = models.DateTimeField(auto_now_add=True,editable=False)

    class Meta:
        ordering = ['created']

    def __str__(self):
        return '{} {} {}'.format(helpers.get_thresholdtype(self.thresholdtype), self.thresholdid, self.status)
    

