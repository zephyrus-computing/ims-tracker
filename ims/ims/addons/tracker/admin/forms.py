from django.conf import settings
from django.forms import ModelForm, IntegerField

from ims.addons.tracker.models import PartThreshold, PartThresholdTemplate, PartThresholdNotify, PartStorageThreshold, PartStorageThresholdTemplate, PartStorageThresholdNotify, ObjectAction, ObjectActionTemplate, ObjectActionNotify, EmailRecipient, EmailTemplate, KitThreshold, KitThresholdNotify, KitThresholdTemplate

class KitThresholdForm(ModelForm):
    class Meta:
        model = KitThreshold
        exclude = ['create', 'lastmodified']
    threshold = IntegerField(min_value=0, max_value=9223372036854775807)

class KitThresholdNotifyForm(ModelForm):
    class Meta:
        model = KitThresholdNotify
        fields = '__all__'

class KitThresholdTemplateForm(ModelForm):
    class Meta:
        model = KitThresholdTemplate
        fields = '__all__'

class PartThresholdForm(ModelForm):
    class Meta:
        model = PartThreshold
        exclude = ['created', 'lastmodified']
    threshold = IntegerField(min_value=0, max_value=9223372036854775807)

class PartThresholdNotifyForm(ModelForm):
    class Meta:
        model = PartThresholdNotify
        fields = '__all__'

class PartThresholdTemplateForm(ModelForm):
    class Meta:
        model = PartThresholdTemplate
        fields = '__all__'

class PartStorageThresholdForm(ModelForm):
    class Meta:
        model = PartStorageThreshold
        exclude = ['created', 'lastmodified']
    threshold = IntegerField(min_value=0, max_value=9223372036854775807)

class PartStorageThresholdNotifyForm(ModelForm):
    class Meta:
        model = PartStorageThresholdNotify
        fields = '__all__'

class PartStorageThresholdTemplateForm(ModelForm):
    class Meta:
        model = PartStorageThresholdTemplate
        fields = '__all__'

class ObjectActionForm(ModelForm):
    class Meta:
        model = ObjectAction
        fields = '__all__'

class ObjectActionNotifyForm(ModelForm):
    class Meta:
        model = ObjectActionNotify
        fields = '__all__'

class ObjectActionTemplateForm(ModelForm):
    class Meta:
        model = ObjectActionTemplate
        fields = '__all__'

class EmailTemplateForm(ModelForm):
    class Meta:
        model = EmailTemplate
        exclude = ['created', 'lastmodified']

class EmailRecipientForm(ModelForm):
    class Meta:
        model = EmailRecipient
        exclude = ['created', 'lastmodified']

