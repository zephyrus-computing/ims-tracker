from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required, permission_required
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
import logging

from inventory.models import Part, PartStorage, Kit
from inventory.search import Search
from ims.addons.tracker.admin.forms import EmailRecipientForm, EmailTemplateForm, PartThresholdForm, PartThresholdNotifyForm, PartThresholdTemplateForm, PartStorageThresholdForm, PartStorageThresholdNotifyForm, PartStorageThresholdTemplateForm, ObjectActionForm, ObjectActionNotifyForm, ObjectActionTemplateForm, KitThresholdForm, KitThresholdNotifyForm, KitThresholdTemplateForm
from ims.addons.tracker.models import EmailRecipient, EmailTemplate, PartThreshold, PartThresholdNotify, PartThresholdTemplate, PartStorageThreshold, PartStorageThresholdNotify, PartStorageThresholdTemplate, ObjectAction, ObjectActionNotify, ObjectActionTemplate, KitThreshold, KitThresholdNotify, KitThresholdTemplate
from ims.addons.tracker.helpers import parse_querytype

logger = logging.getLogger(__name__)
from log.logger import getLogger
logger2 = getLogger('tracker-admin')

@permission_required('tracker.add_partthresholdnotify')
def admin_partthresholdnotify_create(request):
    cname = 'admin_partthresholdnotify_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = PartThresholdNotifyForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new PartThresholdNotify "{}-{}"'.format(form.fields['partthreshold'], form.fields['emailrecipient']))
            inst = form.save()
            logger2.info(request.user, 'Created PartThresholdNotify id {}'.format(inst.id))
            return redirect('/tracker/admin/partthresholdnotify/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create PartThresholdNotify Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'PartThresholdNotify', 'form': form, 'message': 'Form is not valid.'})
    else:
        form = PartThresholdNotifyForm()
        logger.debug('Generated a new {} and checking for provided variables'.format(form.__class__.__name__))
        er = request.GET.get('emailrecipient')
        pt = request.GET.get('partthreshold')
        if er != None and pt != None:
            form = PartThresholdNotifyForm(initial={'emailrecipient': get_object_or_404(EmailRecipient, id=er), 'partthreshold': get_object_or_404(PartThreshold, id=pt)})
        else:
            if er != None:
                form = PartThresholdNotifyForm(initial={'emailrecipient': get_object_or_404(EmailRecipient, id=er)})
            if pt != None:
                form = PartThresholdNotifyForm(initial={'partthreshold': get_object_or_404(PartThreshold, id=pt)})
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create PartThresholdNotify page')
        return render(request, 'tracker/admin/create.html', {'name': 'PartThresholdNotify', 'form': form})

@permission_required('tracker.add_partstoragethresholdnotify')
def admin_partstoragethresholdnotify_create(request):
    cname = 'admin_partstoragethresholdnotify_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = PartStorageThresholdNotifyForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new PartStorageThresholdNotify "{}-{}"'.format(form.fields['partstoragethreshold'], form.fields['emailrecipient']))
            inst = form.save()
            logger2.info(request.user, 'Created PartStorageThresholdNotify id {}'.format(inst.id))
            return redirect('/tracker/admin/partstoragethresholdnotify/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create PartStorageThresholdNotify Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'PartStorageThresholdNotify', 'form': form, 'message': 'Form is not valid.'})
    else:
        form = PartStorageThresholdNotifyForm()
        logger.debug('Generated a new {} and checking for provided variables'.format(form.__class__.__name__))
        er = request.GET.get('emailrecipient')
        pst = request.GET.get('partstoragethreshold')
        if er != None and pst != None:
            form = PartStorageThresholdNotifyForm(initial={'emailrecipient': get_object_or_404(EmailRecipient, id=er), 'partstoragethreshold': get_object_or_404(PartStorageThreshold, id=pst)})
        else:
            if er != None:
                form = PartStorageThresholdNotifyForm(initial={'emailrecipient': get_object_or_404(EmailRecipient, id=er)})
            if pst != None:
                form = PartStorageThresholdNotifyForm(initial={'partstoragethreshold': get_object_or_404(PartStorageThreshold, id=pst)})
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create PartStorageThresholdNotify page')
        return render(request, 'tracker/admin/create.html', {'name': 'PartStorageThresholdNotify', 'form': form})

@permission_required('tracker.add_objectactionnotify')
def admin_objectactionnotify_create(request):
    cname = 'admin_objectactionnotify_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = ObjectActionNotifyForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new ObjectActionNotify "{}-{}"'.format(form.fields['objectaction'], form.fields['emailrecipient']))
            inst = form.save()
            logger2.info(request.user, 'Created ObjectActionNotify id {}'.format(inst.id))
            return redirect('/tracker/admin/objectactionnotify/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create ObjectActionNotify Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'ObjectActionNotify', 'form': form, 'message': 'Form is not valid.'})
    else:
        form = ObjectActionNotifyForm()
        logger.debug('Generated a new {} and checking for provided variables'.format(form.__class__.__name__))
        er = request.GET.get('emailrecipient')
        oa = request.GET.get('objectaction')
        if er != None and oa != None:
            form = ObjectActionNotifyForm(initial={'emailrecipient': get_object_or_404(EmailRecipient, id=er), 'objectaction': get_object_or_404(ObjectAction, id=oa)})
        else:
            if er != None:
                form = ObjectActionNotifyForm(initial={'emailrecipient': get_object_or_404(EmailRecipient, id=er)})
            if oa != None:
                form = ObjectActionNotifyForm(initial={'objectaction': get_object_or_404(ObjectAction, id=oa)})
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create ObjectActionNotify page')
        return render(request, 'tracker/admin/create.html', {'name': 'ObjectActionNotify', 'form': form})

@permission_required('tracker.add_objectaction')
def admin_objectaction_create(request):
    cname = 'admin_objectaction_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = ObjectActionForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new ObjectAction "{}"'.format(form.fields['name']))
            inst = form.save()
            logger2.info(request.user, 'Created ObjectAction id {}'.format(inst.id))
            return redirect('/tracker/admin/objectaction/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create ObjectAction Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'ObjectAction', 'form': form, 'message': 'Form is not valid.'})
    else:
        logger.debug('Checking for provided variables')
        t = request.GET.get('type')
        pk = request.GET.get('id')
        a = request.GET.get('action')
        init_dict = {'objtype':'','objid':'0','action':''}
        if t != None:
            init_dict['objtype'] = t
        if pk != None:
            init_dict['objid'] = pk
        if a != None:
            init_dict['action'] = a
        form = ObjectActionForm(initial=init_dict)
        logger.debug('Generated a new {}'.format(form.__class__.__name__))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create ObjectAction page')
        return render(request, 'tracker/admin/create.html', {'name': 'ObjectAction', 'form': form})

@permission_required('tracker.add_partthreshold')
def admin_partthreshold_create(request):
    cname = 'admin_partthreshold_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = PartThresholdForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new PartThreshold "{}"'.format(form.fields['name']))
            inst = form.save()
            logger2.info(request.user, 'Created PartThreshold id {}'.format(inst.id))
            return redirect('/tracker/admin/partthreshold/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create PartThreshold Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'PartThreshold', 'form': form, 'message': 'Form is not valid.'})
    else:
        logger.debug('Checking for provided variables')
        form = PartThresholdForm()
        p = request.GET.get('part')
        if p != None:
            form = PartThresholdForm(initial={'part':get_object_or_404(Part, id=p)})
        logger.debug('Generated a new {}'.format(form.__class__.__name__))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create PartThreshold page')
        return render(request, 'tracker/admin/create.html', {'name': 'PartThreshold', 'form': form})

@permission_required('tracker.add_partstoragethreshold')
def admin_partstoragethreshold_create(request):
    cname = 'admin_partstoragethreshold_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = PartStorageThresholdForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new PartStorageThreshold "{}"'.format(form.fields['name']))
            inst = form.save()
            logger2.info(request.user, 'Created PartStorageThreshold id {}'.format(inst.id))
            return redirect('/tracker/admin/partstoragethreshold/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create PartStorageThreshold Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'PartStorageThreshold', 'form': form, 'message': 'Form is not valid.'})
    else:
        logger.debug('Checking for provided variables')
        form = PartStorageThresholdForm()
        ps = request.GET.get('partstorage')
        if ps != None:
            form = PartStorageThresholdForm(initial={'partstorage':get_object_or_404(PartStorage, id=ps)})
        logger.debug('Generated a new {}'.format(form.__class__.__name__))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create PartStorageThreshold page')
        return render(request, 'tracker/admin/create.html', {'name': 'PartStorageThreshold', 'form': form})

@permission_required('tracker.add_objectactiontemplate')
def admin_objectactiontemplate_create(request):
    cname = 'admin_objectactiontemplate_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = ObjectActionTemplateForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new ObjectActionTemplate "{}-{}"'.format(form.fields['objectaction'], form.fields['emailtemplate']))
            inst = form.save()
            logger2.info(request.user, 'Created ObjectActionTemplate id {}'.format(inst.id))
            return redirect('/tracker/admin/objectactiontemplate/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create ObjectActionTemplate Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'ObjectActionTemplate', 'form': form, 'message': 'Form is not valid.'})
    else:
        form = ObjectActionTemplateForm()
        logger.debug('Generated a new {} and checking for provided variables'.format(form.__class__.__name__))
        et = request.GET.get('emailtemplate')
        oa = request.GET.get('objectaction')
        if et != None and oa != None:
            form = ObjectActionTemplateForm(initial={'emailtemplate': get_object_or_404(EmailTemplate,id=et), 'objectaction': get_object_or_404(ObjectAction, id=oa)})
        else:
            if et != None:
                form = ObjectActionTemplateForm(initial={'emailtemplate': get_object_or_404(EmailTemplate,id=et)})
            if oa != None:
                form = ObjectActionTemplateForm(initial={'objectaction': get_object_or_404(ObjectAction, id=oa)})
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create ObjectActionTemplate page')
        return render(request, 'tracker/admin/create.html', {'name': 'ObjectActionTemplate', 'form': form})

@permission_required('tracker.add_partthresholdtemplate')
def admin_partthresholdtemplate_create(request):
    cname = 'admin_partthresholdtemplate_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = PartThresholdTemplateForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new PartThresholdTemplate "{}-{}"'.format(form.fields['partthreshold'], form.fields['emailtemplate']))
            inst = form.save()
            logger2.info(request.user, 'Created PartThresholdTemplate id {}'.format(inst.id))
            return redirect('/tracker/admin/partthresholdtemplate/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create PartThresholdTemplate Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'PartThresholdTemplate', 'form': form, 'message': 'Form is not valid.'})
    else:
        form = PartThresholdTemplateForm()
        logger.debug('Generated a new {} and checking for provided variables'.format(form.__class__.__name__))
        et = request.GET.get('emailtemplate')
        pt = request.GET.get('partthreshold')
        if et != None and pt != None:
            form = PartThresholdTemplateForm(initial={'emailtemplate': get_object_or_404(EmailTemplate,id=et), 'partthreshold': get_object_or_404(PartThreshold, id=pt)})
        else:
            if et != None:
                form = PartThresholdTemplateForm(initial={'emailtemplate': get_object_or_404(EmailTemplate,id=et)})
            if pt != None:
                form = PartThresholdTemplateForm(initial={'partthreshold': get_object_or_404(PartThreshold, id=pt)})
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create PartThresholdTemplate page')
        return render(request, 'tracker/admin/create.html', {'name': 'PartThresholdTemplate', 'form': form})

@permission_required('tracker.add_partstoragethresholdtemplate')
def admin_partstoragethresholdtemplate_create(request):
    cname = 'admin_partstoragethresholdtemplate_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = PartStorageThresholdTemplateForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new PartStorageThresholdTemplate "{}-{}"'.format(form.fields['partstoragethreshold'], form.fields['emailtemplate']))
            inst = form.save()
            logger2.info(request.user, 'Created PartStorageThresholdTemplate id {}'.format(inst.id))
            return redirect('/tracker/admin/partstoragethresholdtemplate/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create PartStorageThresholdTemplate Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'PartStorageThresholdTemplate', 'form': form, 'message': 'Form is not valid.'})
    else:
        form = PartStorageThresholdTemplateForm()
        logger.debug('Generated a new {} and checking for provided variables'.format(form.__class__.__name__))
        et = request.GET.get('emailtemplate')
        pst = request.GET.get('partstoragethreshold')
        if et != None and pst != None:
            form = PartStorageThresholdTemplateForm(initial={'emailtemplate': get_object_or_404(EmailTemplate,id=et), 'partstoragethreshold': get_object_or_404(PartStorageThreshold, id=pst)})
        else:
            if et != None:
                form = PartStorageThresholdTemplateForm(initial={'emailtemplate': get_object_or_404(EmailTemplate,id=et)})
            if pst != None:
                form = PartStorageThresholdTemplateForm(initial={'partstoragethreshold': get_object_or_404(PartStorageThreshold, id=pst)})
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create PartStorageThresholdTemplate page')
        return render(request, 'tracker/admin/create.html', {'name': 'PartStorageThresholdTemplate', 'form': form})

@permission_required('tracker.add_emailrecipient')
def admin_emailrecipient_create(request):
    cname = 'admin_emailrecipient_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = EmailRecipientForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new EmailRecipient "{}"'.format(form.fields['name']))
            inst = form.save()
            logger2.info(request.user, 'Created EmailRecipient id {}'.format(inst.id))
            return redirect('/tracker/admin/emailrecipient/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create EmailRecipient Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'EmailRecipient', 'form': form, 'message': 'Form is not valid.'})
    else:
        form = EmailRecipientForm()
        logger.debug('Generated a new {}'.format(form.__class__.__name__))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create EmailRecipient page')
        return render(request, 'tracker/admin/create.html', {'name': 'EmailRecipient', 'form': form})

@permission_required('tracker.add_emailtemplate')
def admin_emailtemplate_create(request):
    cname = 'admin_emailtemplate_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = EmailTemplateForm(request.POST, request.FILES)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new EmailTemplate "{}"'.format(form.fields['name']))
            inst = form.save()
            logger2.info(request.user, 'Created EmailTemplate id {}'.format(inst.id))
            return redirect('/tracker/admin/emailtemplate/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create EmailTemplate Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'EmailTemplate', 'form': form, 'message': 'Form is not valid.', 'templateform': True})
    else:
        form = EmailTemplateForm()
        logger.debug('Generated a new {}'.format(form.__class__.__name__))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create EmailTemplate page')
        return render(request, 'tracker/admin/create.html', {'name': 'EmailTemplate', 'form': form, 'templateform': True})

@permission_required('tracker.add_kitthreshold')
def admin_kitthreshold_create(request):
    cname = 'admin_kitthreshold_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = KitThresholdForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new KitThreshold "{}"'.format(form.fields['name']))
            inst = form.save()
            logger2.info(request.user, 'Created KitThreshold id {}'.format(inst.id))
            return redirect('/tracker/admin/kitthreshold/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create KitThreshold Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'KitThreshold', 'form': form, 'message': 'Form is not valid.'})
    else:
        logger.debug('Checking for provided variables')
        k = request.GET.get('kit')
        form = KitThresholdForm()
        if k != None:
            form = KitThresholdForm(initial={'kit': get_object_or_404(Kit, id=k)})
        logger.debug('Generated a new {}'.format(form.__class__.__name__))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create KitThreshold page')
        return render(request, 'tracker/admin/create.html', {'name': 'KitThreshold', 'form': form})

@permission_required('tracker.add_kitthresholdnotify')
def admin_kitthresholdnotify_create(request):
    cname = 'admin_kitthresholdnotify_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = KitThresholdNotifyForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new KitThresholdNotify "{}-{}"'.format(form.fields['kitthreshold'], form.fields['emailrecipient']))
            inst = form.save()
            logger2.info(request.user, 'Created KitThresholdNotify id {}'.format(inst.id))
            return redirect('/tracker/admin/kitthresholdnotify/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create KitThresholdNotify Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'KitThresholdNotify', 'form': form, 'message': 'Form is not valid.'})
    else:
        form = KitThresholdNotifyForm()
        logger.debug('Generated a new {} and checking for provided variables'.format(form.__class__.__name__))
        er = request.GET.get('emailrecipient')
        kt = request.GET.get('kitthreshold')
        if er != None and kt != None:
            form = KitThresholdNotifyForm(initial={'emailrecipient': get_object_or_404(EmailRecipient, id=er), 'kitthreshold': get_object_or_404(KitThreshold, id=kt)})
        else:
            if er != None:
                form = KitThresholdNotifyForm(initial={'emailrecipient': get_object_or_404(EmailRecipient, id=er)})
            if kt != None:
                form = KitThresholdNotifyForm(initial={'kitthreshold': get_object_or_404(KitThreshold, id=kt)})
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create KitThresholdNotify page')
        return render(request, 'tracker/admin/create.html', {'name': 'KitThresholdNotify', 'form': form})

@permission_required('tracker.add_kitthresholdtemplate')
def admin_kitthresholdtemplate_create(request):
    cname = 'admin_kitthresholdtemplate_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        form = KitThresholdTemplateForm(request.POST)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Creating new KitThresholdTemplate "{}-{}"'.format(form.fields['kitthreshold'], form.fields['emailtemplate']))
            inst = form.save()
            logger2.info(request.user, 'Created KitThresholdTemplate id {}'.format(inst.id))
            return redirect('/tracker/admin/kitthresholdtemplate/')
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create KitThresholdTemplate Form not valid')
        return render(request, 'tracker/admin/create.html', {'name': 'KitThresholdTemplate', 'form': form, 'message': 'Form is not valid.'})
    else:
        form = KitThresholdTemplateForm()
        logger.debug('Generated a new {} and checking for provided variables'.format(form.__class__.__name__))
        et = request.GET.get('emailtemplate')
        kt = request.GET.get('kitthreshold')
        if et != None and kt != None:
            form = KitThresholdTemplateForm(initial={'emailtemplate': get_object_or_404(EmailTemplate,id=et), 'kitthreshold': get_object_or_404(KitThreshold, id=kt)})
        else:
            if et != None:
                form = KitThresholdTemplateForm(initial={'emailtemplate': get_object_or_404(EmailTemplate,id=et)})
            if kt != None:
                form = KitThresholdTemplateForm(initial={'kitthreshold': get_object_or_404(KitThreshold, id=kt)})
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Create KitThresholdTemplate page')
        return render(request, 'tracker/admin/create.html', {'name': 'KitThresholdTemplate', 'form': form})


@permission_required('tracker.change_kitthreshold')
def admin_kitthreshold_change(request, id):
    cname = 'admin_kitthreshold_change'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    obj = KitThreshold.objects.get(id=id)
    if request.method == 'POST':
        form = KitThresholdForm(request.POST, instance=obj)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Updating KitThreshold id {}'.format(id))
            message = 'KitThreshold updated successfully'
            try:
                form.save()
            except Exception as err:
                logger.warning(err)
                logger2.warning(request.user, err)
                message = 'Failed to update KitThreshold'
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info(request.user, '{} for id {}'.format(message, id))
            return render(request, 'tracker/admin/update.html', {'objtype': 'KitThreshold', 'name': obj.name, 'form': form, 'message': message})
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Change KitThreshold Form not valid for id {}'.format(id))
        return render(request, 'tracker/admin/update.html', {'objtype': 'KitThreshold', 'name': obj.name, 'form': form, 'message': 'Form is not valid.'})
    else:
        form = KitThresholdForm(instance=obj)
        logger.debug('Populated {} for id {}'.format(form.__class__.__name__, id))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Change KitThreshold page')
        return render(request, 'tracker/admin/update.html', {'objtype': 'KitThreshold', 'name': obj.name, 'form': form})

@permission_required('tracker.delete_kitthreshold')
def ajax_kitthreshold_delete(request, id):
    cname = 'ajax_kitthreshold_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested KitThreshold')
    kt = get_object_or_404(KitThreshold, id=id)
    logger.info('DELETE request for KitThreshold id {}'.format(id))
    kt.delete()
    logger2.info(request.user, 'Deleted KitThreshold id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_kitthresholdnotify')
def ajax_kitthresholdnotify_delete(request, id):
    cname = 'ajax_kitthresholdnotify_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested KitThresholdNotify')
    ktn = get_object_or_404(KitThresholdNotify, id=id)
    logger.info('DELETE request for KitThresholdNotify id {}'.format(id))
    ktn.delete()
    logger2.info(request.user, 'Deleted KitThresholdNotify id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_kitthresholdtemplate')
def ajax_kitthresholdtemplate_delete(request, id):
    cname = 'ajax_kitthresholdtemplate_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested KitThresholdTemplate')
    ktt = get_object_or_404(KitThresholdTemplate, id=id)
    logger.info('DELETE request for KitThresholdTemplate id {}'.format(id))
    ktt.delete()
    logger2.info(request.user, 'Deleted KitThresholdTemplate id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.view_kitthreshold')
def admin_kitthreshold_view(request):
    cname = 'admin_kitthreshold_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed KitThrehold List'
    object_list = KitThreshold.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of KitThreshold with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['name', 'threshold', 'operator', 'alarm', 'kit__name', 'kit__sku', 'kit__description'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'KitThreshold', 'objs': object_list, 'allowupdate': True})

@permission_required('tracker.view_kitthresholdtemplate')
def admin_kitthresholdtemplate_view(request):
    cname = 'admin_kitthresholdtemplate_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed KitThresholdTemplate List'
    object_list = KitThresholdTemplate.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of KitThresholdTemplate with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['kitthreshold__name', 'emailtemplate__name', 'emailtemplate__subject'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'KitThresholdTemplate', 'objs': object_list})

@permission_required('tracker.view_kitthresholdnotify')
def admin_kitthresholdnotify_view(request):
    cname = 'admin_kitthresholdnotify_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed KitThresholdNotify List'
    object_list = KitThresholdNotify.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of KitThresholdNotify with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['kitthreshold__name', 'emailrecipient__name', 'emailrecipient__email'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'KitThresholdNotify', 'objs': object_list})

@permission_required('tracker.change_emailrecipient')
def admin_emailrecipient_change(request, id):
    cname = 'admin_emailrecipient_change'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    obj = EmailRecipient.objects.get(id=id)
    if request.method == 'POST':
        form = EmailRecipientForm(request.POST, instance=obj)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Updating EmailRecipient id {}'.format(id))
            message = 'EmailRecipient updated successfully'
            try:
                form.save()
            except Exception as err:
                logger.warning(err)
                logger2.warning(request.user, err)
                message = 'Failed to update EmailRecipient'
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info(request.user, '{} for id {}'.format(message, id))
            return render(request, 'tracker/admin/update.html', {'objtype': 'EmailRecipient', 'name': obj.name, 'form': form, 'message': message})
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Change EmailRecipient Form not valid for id {}'.format(id))
        return render(request, 'tracker/admin/update.html', {'objtype': 'EmailRecipeint', 'name': obj.name, 'form': form, 'message': 'Form is not valid.'})
    else:
        form = EmailRecipientForm(instance=obj)
        logger.debug('Populated {} for id {}'.format(form.__class__.__name__, id))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Change EmailRecipient page')
        return render(request, 'tracker/admin/update.html', {'objtype': 'EmailRecipient', 'name': obj.name, 'form': form})

@permission_required('tracker.change_emailtemplate')
def admin_emailtemplate_change(request, id):
    cname = 'admin_emailtemplate_change'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    obj = EmailTemplate.objects.get(id=id)
    if request.method == 'POST':
        form = EmailTemplateForm(request.POST, request.FILES, instance=obj)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Updating EmailTemplate id {}'.format(id))
            message = 'EmailTemplate updated successfully'
            try:
                form.save()
            except Exception as err:
                logger.warning(err)
                logger2.warning(request.user, err)
                message = 'Failed to update EmailTemplate'
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info(request.user, '{} for id {}'.format(message, id))
            return render(request, 'tracker/admin/update.html', {'objtype': 'EmailTemplate', 'name': obj.name, 'form': form, 'message': message})
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Change EmailTemplate Form not valid for id {}'.format(id))
        return render(request, 'tracker/admin/update.html', {'objtype': 'EmailTemplate', 'name': obj.name, 'form': form, 'message': 'Form is not valid.'})
    else:
        form = EmailTemplateForm(instance=obj)
        logger.debug('Populated {} for id {}'.format(form.__class__.__name__, id))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Change EmailTemplate page')
        return render(request, 'tracker/admin/update.html', {'objtype': 'EmailTemplate', 'name': obj.name, 'form': form})

@permission_required('tracker.change_partthreshold')
def admin_partthreshold_change(request, id):
    cname = 'admin_partthreshold_change'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    obj = PartThreshold.objects.get(id=id)
    if request.method == 'POST':
        form = PartThresholdForm(request.POST, instance=obj)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Updating PartThreshold id {}'.format(id))
            message = 'PartThreshold updated successfully'
            try:
                form.save()
            except Exception as err:
                logger.warning(err)
                logger2.warning(request.user, err)
                message = 'Failed to update PartThreshold'
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info(request.user, '{} for id {}'.format(message, id))
            return render(request, 'tracker/admin/update.html', {'objtype': 'PartThreshold', 'name': obj.name, 'form': form, 'message': message})
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Change PartThreshold Form not valid for id {}'.format(id))
        return render(request, 'tracker/admin/update.html', {'objtype': 'PartThreshold', 'name': obj.name, 'form': form, 'message': 'Form is not valid.'})
    else:
        form = PartThresholdForm(instance=obj)
        logger.debug('Populated {} for id {}'.format(form.__class__.__name__, id))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Change PartThreshold page')
        return render(request, 'tracker/admin/update.html', {'objtype': 'PartThreshold', 'name': obj.name, 'form': form})

@permission_required('tracker.change_partstoragethreshold')
def admin_partstoragethreshold_change(request, id):
    cname = 'admin_partstoragethreshold_change'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    obj = PartStorageThreshold.objects.get(id=id)
    if request.method == 'POST':
        form = PartStorageThresholdForm(request.POST, instance=obj)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Updating PartThreshold id {}'.format(id))
            message = 'PartStorageThreshold updated successfully'
            try:
                form.save()
            except Exception as err:
                logger.warning(err)
                logger2.warning(request.user, err)
                message = 'Failed to update PartStorageThreshold'
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info(request.user, '{} for id {}'.format(message, id))
            return render(request, 'tracker/admin/update.html', {'objtype': 'PartStorageThreshold', 'name': obj.name, 'form': form, 'message': message})
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Change PartStorageThreshold Form not valid for id {}'.format(id))
        return render(request, 'tracker/admin/update.html', {'objtype': 'PartStorageThreshold', 'name': obj.name, 'form': form, 'message': 'Form is not valid.'})
    else:
        form = PartStorageThresholdForm(instance=obj)
        logger.debug('Populated {} for id {}'.format(form.__class__.__name__, id))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Change PartStorageThreshold page')
        return render(request, 'tracker/admin/update.html', {'objtype': 'PartStorageThreshold', 'name': obj.name, 'form': form})

@permission_required('tracker.change_objectaction')
def admin_objectaction_change(request, id):
    cname = 'admin_objectaction_change'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    obj = ObjectAction.objects.get(id=id)
    if request.method == 'POST':
        form = ObjectActionForm(request.POST, instance=obj)
        if form.is_valid():
            logger.debug('{} is valid.'.format(form.__class__.__name__))
            logger.info('Updating ObjectAction id {}'.format(id))
            message = 'ObjectAction updated successfully'
            try:
                form.save()
            except Exception as err:
                logger.warning(err)
                logger2.warning(request.user, err)
                message = 'Failed to update ObjectAction'
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info(request.user, '{} for id {}'.format(message, id))
            return render(request, 'tracker/admin/update.html', {'objtype': 'ObjectAction', 'name': obj.name, 'form': form, 'message': message})
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Change ObjectAction Form not valid for id {}'.format(id))
        return render(request, 'tracker/admin/update.html', {'objtype': 'ObjectAction', 'name': obj.name, 'form': form, 'message': 'Form is not valid.'})
    else:
        form = ObjectActionForm(instance=obj)
        logger.debug('Populated {} for id {}'.format(form.__class__.__name__, id))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Admin Change ObjectAction page')
        return render(request, 'tracker/admin/update.html', {'objtype': 'ObjectAction', 'name': obj.name, 'form': form})

@permission_required('tracker.delete_partthreshold')
def ajax_partthreshold_delete(request, id):
    cname = 'admin_partthreshold_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested PartThreshold')
    pt = get_object_or_404(PartThreshold, id=id)
    logger.info('DELETE request for PartThreshold id {}'.format(id))
    pt.delete()
    logger2.info(request.user, 'Deleted PartThreshold id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_partthresholdtemplate')
def ajax_partthresholdtemplate_delete(request, id):
    cname = 'admin_partthresholdtemplate_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested PartThresholdTemplate')
    ptt = get_object_or_404(PartThresholdTemplate, id=id)
    logger.info('DELETE request for PartThresholdTemplate id {}'.format(id))
    ptt.delete()
    logger2.info(request.user, 'Deleted PartThresholdTemplate id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_partthresholdnotify')
def ajax_partthresholdnotify_delete(request, id):
    cname = 'admin_partthresholdnotify_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested PartThresholdNotify')
    ptn = get_object_or_404(PartThresholdNotify, id=id)
    logger.info('DELETE request for PartThresholdNotify id {}'.format(id))
    ptn.delete()
    logger2.info(request.user, 'Deleted PartThresholdNotify id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_partstoragethreshold')
def ajax_partstoragethreshold_delete(request, id):
    cname = 'admin_partstoragethreshold_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested PartStorageThreshold')
    pst = get_object_or_404(PartStorageThreshold, id=id)
    logger.info('DELETE request for PartStorageThreshold id {}'.format(id))
    pst.delete()
    logger2.info(request.user, 'Deleted PartStorageThreshold id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_partstoragethresholdtemplate')
def ajax_partstoragethresholdtemplate_delete(request, id):
    cname = 'admin_partstoragethresholdtemplate_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested PartStorageThresholdTemplate')
    pstt = get_object_or_404(PartStorageThresholdTemplate, id=id)
    logger.info('DELETE request for PartStorageThresholdTemplate id {}'.format(id))
    pstt.delete()
    logger2.info(request.user, 'Deleted PartStorageThresholdTemplate id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_partstoragethresholdnotify')
def ajax_partstoragethresholdnotify_delete(request, id):
    cname = 'admin_partstoragethresholdnotify_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested PartStorageThresholdNotify')
    pstn = get_object_or_404(PartStorageThresholdNotify, id=id)
    logger.info('DELETE request for PartStorageThresholdNotify id {}'.format(id))
    pstn.delete()
    logger2.info(request.user, 'Deleted PartStorageThresholdNotify id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_objectaction')
def ajax_objectaction_delete(request, id):
    cname = 'admin_objectaction_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested ObjectAction')
    oa = get_object_or_404(ObjectAction, id=id)
    logger.info('DELETE request for ObjectAction id {}'.format(id))
    oa.delete()
    logger2.info(request.user, 'Deleted ObjectAction id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_objectactionnotify')
def ajax_objectactionnotify_delete(request, id):
    cname = 'admin_objectactionnotify_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested ObjectActionNotify')
    oan = get_object_or_404(ObjectActionNotify, id=id)
    logger.info('DELETE request for ObjectActionNotify id {}'.format(id))
    oan.delete()
    logger2.info(request.user, 'Deleted ObjectActionNotify id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_objectactiontemplate')
def ajax_objectactiontemplate_delete(request, id):
    cname = 'admin_objectactiontemplate_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested ObjectActionTemplate')
    oat = get_object_or_404(ObjectActionTemplate, id=id)
    logger.info('DELETE request for ObjectActionTemplate id {}'.format(id))
    oat.delete()
    logger2.info(request.user, 'Deleted ObjectActionTemplate id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_emailrecipient')
def ajax_emailrecipient_delete(request, id):
    cname = 'admin_emailrecipient_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested EmailRecipient')
    er = get_object_or_404(EmailRecipient, id=id)
    logger.info('DELETE request for EmailRecipient id {}'.format(id))
    er.delete()
    logger2.info(request.user, 'Deleted EmailRecipient id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.delete_emailtemplate')
def ajax_emailtemplate_delete(request, id):
    cname = 'admin_emailtemplate_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested EmailTemplate')
    et = get_object_or_404(EmailTemplate, id=id)
    logger.info('DELETE request for EmailTemplate id {}'.format(id))
    et.delete()
    logger2.info(request.user, 'Deleted EmailTemplate id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@login_required
def home(request):
    cname = 'home'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed Admin Home'
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/home.html')

@permission_required('tracker.view_partthreshold')
def admin_partthreshold_view(request):
    cname = 'admin_partthreshold_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed PartThrehold List'
    object_list = PartThreshold.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of PartThreshold with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['name', 'threshold', 'operator', 'alarm', 'part__name', 'part__sku', 'part__description'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'PartThreshold', 'objs': object_list, 'allowupdate': True})

@permission_required('tracker.view_partthresholdtemplate')
def admin_partthresholdtemplate_view(request):
    cname = 'admin_partthresholdtemplate_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed PartThresholdTemplate List'
    object_list = PartThresholdTemplate.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of PartThresholdTemplate with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['partthreshold__name', 'emailtemplate__name', 'emailtemplate__subject'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'PartThresholdTemplate', 'objs': object_list})

@permission_required('tracker.view_partthresholdnotify')
def admin_partthresholdnotify_view(request):
    cname = 'admin_partthresholdnotify_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed PartThresholdNotify List'
    object_list = PartThresholdNotify.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of PartThresholdNotify with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['partthreshold__name', 'emailrecipient__name', 'emailrecipient__email'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'PartThresholdNotify', 'objs': object_list})

@permission_required('tracker.view_partstoragethreshold')
def admin_partstoragethreshold_view(request):
    cname = 'admin_partstoragethreshold_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed PartStorageThreshold List'
    object_list = PartStorageThreshold.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of PartStorageThreshold with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['name', 'threshold', 'operator', 'alarm', 'partstorage__part__name', 'partstorage__part__sku', 'partstorage__part__description', 'partstorage__storage__name', 'partstorage__storage__description'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'PartStorageThreshold', 'objs': object_list, 'allowupdate': True})

@permission_required('tracker.view_partstoragethresholdtemplate')
def admin_partstoragethresholdtemplate_view(request):
    cname = 'admin_partstoragethresholdtemplate_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed PartStorageThresholdTemplate List'
    object_list = PartStorageThresholdTemplate.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of PartStorageThresholdTemplate with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['partstoragethreshold__name', 'emailtemplate__name', 'emailtemplate__subject'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'PartStorageThresholdTemplate', 'objs': object_list})

@permission_required('tracker.view_partstoragethresholdnotify')
def admin_partstoragethresholdnotify_view(request):
    cname = 'admin_partstoragethresholdnotify_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed PartStoragedThresholdNotify List'
    object_list = PartStorageThresholdNotify.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of PartStorageThresholdNotify with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['partstoragethreshold__name', 'emailrecipient__name', 'emailrecipient__email'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'PartStorageThresholdNotify', 'objs': object_list})

@permission_required('tracker.view_objectaction')
def admin_objectaction_view(request):
    cname = 'admin_objectaction_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed ObjectAction List'
    object_list = ObjectAction.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of ObjectAction with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['name', 'objtype', 'action', 'objid'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'ObjectAction', 'objs': object_list, 'allowupdate': True})

@permission_required('tracker.view_objectactiontemplate')
def admin_objectactiontemplate_view(request):
    cname = 'admin_objectactiontemplate_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed ObjectActionTemplate List'
    object_list = ObjectActionTemplate.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of ObjectActionTemplate with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['objectaction__name', 'emailtemplate__name', 'emailtemplate__subject'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'ObjectActionTemplate', 'objs': object_list})

@permission_required('tracker.view_objectactionnotify')
def admin_objectactionnotify_view(request):
    cname = 'admin_objectactionnotify_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed ObjectActionNotify List'
    object_list = ObjectActionNotify.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of ObjectActionNotify with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['objectaction__name', 'emailrecipient__name', 'emailrecipient__email'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'ObjectActionNotify', 'objs': object_list})

@permission_required('tracker.view_emailrecipient')
def admin_emailrecipient_view(request):
    cname = 'admin_emailrecipient_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed EmailRecipient List'
    object_list = EmailRecipient.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of EmailRecipient with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['name', 'email'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'EmailRecipient', 'objs': object_list, 'allowupdate': True})

@permission_required('tracker.view_emailtemplate')
def admin_emailtemplate_view(request):
    cname = 'admin_emailtemplate_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logmsg = 'Accessed EmailTemplate List'
    object_list = EmailTemplate.objects.all()
    query = request.GET.get('q')
    if query != None:
        logmsg = '{} with query \'{}\''.format(logmsg, query)
        logger.debug('Filtering object_list of EmailTemplate with query')
        querytype = request.GET.get('t')
        qt = parse_querytype(querytype)
        sf = Search().filter(['name', 'subject'], qt + query)
        object_list = object_list.filter(sf)
    paginator = Paginator(object_list, settings.ADMIN_PAGE_SIZE)
    page = request.GET.get('page')
    # Try to display the page or catch and process the errors
    try:
        object_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver the first page
        logger.debug('PageNotAnInteger, show page 1')
        object_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range, deliver the last page
        logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
        object_list = paginator.page(paginator.num_pages)
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, logmsg)
    return render(request, 'tracker/admin/view.html', {'query': query, 'name': 'EmailTemplate', 'objs': object_list, 'allowupdate': True})
