from datetime import datetime, timedelta, timezone
from django.conf import settings
from django.core.mail import send_mail
from django.db.models import Q
from django.shortcuts import render
from django_celery_beat.models import IntervalSchedule, PeriodicTask
from itertools import chain
from os import listdir, remove
from os.path import join
import base64
import logging

from ims.celery import app
from ims.addons.tracker.models import PartThreshold, PartThresholdNotify, PartThresholdTemplate, PartStorageThreshold, PartStorageThresholdNotify, PartStorageThresholdTemplate, KitThreshold, KitThresholdNotify, KitThresholdTemplate, EmailTemplate, EmailRecipient, ObjectAction, ObjectActionNotify, ObjectActionTemplate, Notification, Threshold, ReportNotify
from log.logger import getLogger
from log.models import Log,INFO
import ims.addons.tracker.helpers as helpers
import inventory.models


logger = logging.getLogger('ims.celery')
logger2 = getLogger('tracker')

@app.task(name='part_threshold_check')
def part_threshold_check():
    cname = 'part_threshold_check'
    logger.debug('{} called'.format(cname))
    lastlog = Log.objects.filter(module='tracker',severity=INFO,message='Started Processing Part Threshold Check').last()
    if lastlog is None:
        dt = datetime(1,1,1,tzinfo=timezone.utc)
    else:
        dt = lastlog.created
    logger.debug('Determined the last run as {}'.format(dt))
    logs = Log.objects.filter(module__in='inventory-admin',message__istartswith='partstorage updated',created__gt=dt)
    logs = chain(logs, Log.objects.filter(Q(module__in=['inventory','inventory-api']),Q(message__istartswith='added')|Q(message__istartswith='subtracted'),created__gt=dt).filter(message__icontains='partstorage'))
    logger2.info(0,'Started Processing Part Threshold Check')
    parts = []
    for log in logs:
        try:
            psid = log.message.split('id')[1].strip()
            ps = inventory.models.PartStorage.objects.get(id=psid)
            if ps.part not in parts:
                parts.append(ps.part)
        except inventory.models.PartStorage.DoesNotExist as err:
            logger.warning('Unable to locate PartStorage with id {}. Error message: {}'.format(psid, err))
            logger2.warning(0,'Unable to process Part Threshold Check for PartStorage id {}'.format(psid))
        except IndexError as err:
            logger.warning('Unable to extract PartStorage id from message of Log record id {}'.format(log.id))
            logger2.warning(0,'Unable to extract PartStorage id from message of Log record id {}'.format(log.id))
        except Exception as err:
            logger.error('Unknown error while trying to process Part Threshold Check for PartStorage id {}: {}'.format(psid, err))
            logger2.error(0,'Unknown error while trying to process Part Threshold Check for PartStorage id {}: {}'.format(psid, err))
    for part in parts:
        logger.debug('{} for part id {}'.format(cname, part.id))
        logger2.debug(0,'{} for part id {}'.format(cname, part.id))
        thresholds = PartThreshold.objects.filter(part=part)
        for t in thresholds:
            total = part.get_total()
            logger.debug('checking threshold: {} {} {}'.format(total, t.operator, t.threshold))
            if helpers.check_threshold(total, t.operator, t.threshold):
                logger.debug('threshold violation. triggering threshold alarm')
                logger2.info(0,'Status Alarm for PartThreshold id {}'.format(t.id))
                threshold = Threshold.objects.create(thresholdtype=helpers.PARTTHRESHOLD, thresholdid=t.id, count=t.threshold, current=total, status=True)
                if not t.alarm or not settings.ADDON_TRACKER_NOTIFY_ONCE:
                    notifies = PartThresholdNotify.objects.filter(partthreshold=t)
                    for notify in notifies:
                        logger.debug('queuing email for {}'.format(notify.emailrecipient.email))
                        Notification.objects.create(notifytype=helpers.PARTTHRESHOLD, notifyid=t.id, threshold=threshold.id, emailrecipient=notify.emailrecipient, status=helpers.QUEUED)
                    logger2.info(0,'Queued messages to {} recipients for PartThreshold id {}'.format(notifies.count(), t.id))
                t.alarm = True
                t.save()
            else:
                if t.alarm:
                    logger.debug('threshold no longer in alarm')
                    logger2.info(0,'Status Clear for PartThreshold id {}'.format(t.id))
                    t.alarm = False
                    t.save()
                    threshold = Threshold.objects.create(thresholdtype=helpers.PARTTHRESHOLD, thresholdid=t.id, count=t.threshold, current=total, status=False)
                    notifies = PartThresholdNotify.objects.filter(partthreshold=t)
                    for notify in notifies:
                        logger.debug('queuing email for {}'.format(notify.emailrecipient.email))
                        Notification.objects.create(notifytype=helpers.PARTTHRESHOLD, notifyid=t.id, threshold=threshold.id, emailrecipient=notify.emailrecipient, status=helpers.QUEUED)
                    logger2.info(0,'Queued messages to {} recipients for PartThreshold id {}'.format(notifies.count(), t.id))


@app.task(name='partstorage_threshold_check')
def partstorage_threshold_check():
    cname = 'partstorage_threshold_check'
    logger.debug('{} called'.format(cname))
    lastlog = Log.objects.filter(module='tracker',severity=INFO,message='Started Processing PartStorage Threshold Check').last()
    if lastlog is None:
        dt = datetime(1,1,1,tzinfo=timezone.utc)
    else:
        dt = lastlog.created
    logger.debug('Determined the last run as {}'.format(dt))
    logs = Log.objects.filter(module__in='inventory-admin',message__istartswith='partstorage updated',created__gt=dt)
    logs = chain(logs, Log.objects.filter(Q(module__in=['inventory','inventory-api']),Q(message__istartswith='added')|Q(message__istartswith='subtracted'),created__gt=dt).filter(message__icontains='partstorage'))
    logger2.info(0,'Started Processing PartStorage Threshold Check')
    partstorages = []
    for log in logs:
        try:
            psid = log.message.split('id')[1].strip()
            ps = inventory.models.PartStorage.objects.get(id=psid)
            if ps not in partstorages:
                partstorages.append(ps)
        except inventory.models.PartStorage.DoesNotExist as err:
            logger.warning('Unable to locate PartStorage with id {}. Error message: {}'.format(psid, err))
            logger2.warning(0,'Unable to process PartStorage Threshold Check for PartStorage id {}'.format(psid))
        except IndexError as err:
            logger.warning('Unable to extract PartStorage id from message of Log record id {}'.format(log.id))
            logger2.warning(0,'Unable to extract PartStorage id from message of Log record id {}'.format(log.id))
        except Exception as err:
            logger.error('Unknown error while trying to process PartStorage Threshold Check for Log id {}: {}'.format(log.id, err))
            logger2.error(0,'Unknown error while trying to process PartStorage Threshold Check for Log id {}: {}'.format(log.id, err))
    for ps in partstorages:
        logger.debug('{} for partstorage id {}'.format(cname, ps.id))
        logger2.debug(0,'{} for partstorage id {}'.format(cname, ps.id))
        thresholds = PartStorageThreshold.objects.filter(partstorage=ps)
        for t in thresholds:
            total = ps.count
            logger.debug('checking threshold: {} {} {}'.format(total, t.operator, t.threshold))
            if helpers.check_threshold(total, t.operator, t.threshold):
                logger.debug('threshold violation. triggering threshold alarm')
                logger2.info(0,'Status Alarm for PartStorageThreshold id {}'.format(t.id))
                threshold = Threshold.objects.create(thresholdtype=helpers.PARTSTORAGETHRESHOLD, thresholdid=t.id, count=t.threshold, current=total, status=True)
                if not t.alarm or not settings.ADDON_TRACKER_NOTIFY_ONCE:
                    notifies = PartStorageThresholdNotify.objects.filter(partstoragethreshold=t)
                    for notify in notifies:
                        logger.debug('queuing email for {}'.format(notify.emailrecipient.email))
                        Notification.objects.create(notifytype=helpers.PARTSTORAGETHRESHOLD, notifyid=t.id, threshold=threshold.id, emailrecipient=notify.emailrecipient, status=helpers.QUEUED)
                    logger2.info(0,'Queued messages to {} recipients for PartStorageThreshold id {}'.format(notifies.count(), t.id))
                t.alarm = True
                t.save()
            else:
                if t.alarm:
                    logger.debug('threshold no longer in alarm')
                    logger2.info(0,'Status Clear for PartStorageThreshold id {}'.format(t.id))
                    t.alarm = False
                    t.save()
                    threshold = Threshold.objects.create(thresholdtype=helpers.PARTSTORAGETHRESHOLD, thresholdid=t.id, count=t.threshold, current=total, status=False)
                    notifies = PartStorageThresholdNotify.objects.filter(partstoragethreshold=t)
                    for notify in notifies:
                        logger.debug('queuing email for {}'.format(notify.emailrecipient.email))
                        Notification.objects.create(notifytype=helpers.PARTSTORAGETHRESHOLD, notifyid=t.id, threshold=threshold.id, emailrecipient=notify.emailrecipient, status=helpers.QUEUED)
                    logger2.info(0,'Queued messages to {} recipients for PartStorageThreshold id {}'.format(notifies.count(), t.id))

@app.task(name='kit_threshold_check')
def kit_threshold_check():
    cname = 'kit_threshold_check'
    logger.debug('{} called'.format(cname))
    lastlog = Log.objects.filter(module='tracker',severity=INFO,message='Started Processing Kit Threshold Check').last()
    if lastlog is None:
        dt = datetime(1,1,1,tzinfo=timezone.utc)
    else:
        dt = lastlog.created
    logger.debug('Determined the last run as {}'.format(dt))
    logs = Log.objects.filter(module='inventory-admin',message__istartswith='kit updated',created__gt=dt)
    logs = chain(logs, Log.objects.filter(Q(module__in=['inventory','inventory-api']),Q(message__istartswith='added')|Q(message__istartswith='subtracted'),message__icontains='kit',created__gt=dt))
    logs2 = Log.objects.filter(module='inventory-admin',message__istartswith='partstorage updated',created__gt=dt)
    logs2 = chain(logs, Log.objects.filter(Q(module__in=['inventory','inventory-api']),Q(message__istartswith='added')|Q(message__istartswith='subtracted'),message__icontains='partstorage',created__gt=dt))
    logger2.info(0,'Started Processing Kit Threshold Check')
    kits = []
    for log in logs:
        try:
            kid = log.message.split('id')[1].strip()
            k = inventory.models.Kit.objects.get(id=kid)
            if k not in kits:
                kits.append(k)
        except inventory.models.Kit.DoesNotExist as err:
            logger.warning('Unable to locate Kit with id {}. Error message: {}'.format(kid, err))
            logger2.warning(0,'Unable to process Kit Threshold Check for Kit id {}'.format(kid))
        except IndexError as err:
            logger.warning('Unable to extract Kit id from message of Log record id {}'.format(log.id))
            logger2.warning(0,'Unable to extract Kit id from message of Log record id {}'.format(log.id))
        except Exception as err:
            logger.error('Unknown error while trying to process Kit Threshold Check for Log id {}: {}'.format(log.id, err))
            logger2.error(0,'Unknown error while trying to process Kit Threshold Check for Log id {}: {}'.format(log.id, err))
    for log in logs2:
        try:
            psid = log.message.split('id')[1].strip()
            ps = inventory.models.PartStorage.objects.get(id=psid)
            kpsa = inventory.models.KitPartStorage.objects.filter(partstorage=ps)
            for kps in kpsa:
                if kps.kit not in kits:
                    kits.append(kps.kit)
        except inventory.models.PartStorage.DoesNotExist as err:
            logger.warning('Unable to locate PartStorage with id {}. Error message: {}'.format(psid, err))
            logger2.warning(0,'Unable to process Kit Threshold Check for PartStorage id {}'.format(psid))
        except IndexError as err:
            logger.warning('Unable to extract PartStorage id from message of Log record id {}'.format(log.id))
            logger2.warning(0,'Unable to extract PartStorage id from message of Log record id {}'.format(log.id))
        except Exception as err:
            logger.error('Unknown error while trying to process Kit Threshold Check for Log id {}: {}'.format(log.id, err))
            logger2.error(0,'Unknown error while trying to process Kit Threshold Check for Log id {}: {}'.format(log.id, err))
    for kit in kits:
        logger.debug('{} for kit id {}'.format(cname, kit.id))
        logger2.debug(0,'{} for kit id {}'.format(cname, kit.id))
        thresholds = KitThreshold.objects.filter(kit=kit)
        for t in thresholds:
            total = kit.get_available_count()
            logger.debug('checking threshold: {} {} {}'.format(total, t.operator, t.threshold))
            if helpers.check_threshold(total, t.operator, t.threshold):
                logger.debug('threshold violation. triggering threshold alarm')
                logger2.info(0,'Status Alarm for KitThreshold id {}'.format(t.id))
                threshold = Threshold.objects.create(thresholdtype=helpers.KITTHRESHOLD, thresholdid=t.id, count=t.threshold, current=total, status=True)
                if not t.alarm or not settings.ADDON_TRACKER_NOTIFY_ONCE:
                    notifies = KitThresholdNotify.objects.filter(kitthreshold=t)
                    for notify in notifies:
                        logger.debug('queuing email for {}'.format(notify.emailrecipient.email))
                        Notification.objects.create(notifytype=helpers.KITTHRESHOLD, notifyid=t.id, threshold=threshold.id, emailrecipient=notify.emailrecipient, status=helpers.QUEUED)
                        logger2.info(0,'Queued messages to {} recipients for KitThreshold id {}'.format(notifies.count(), t.id))
                    t.alarm = True
                    t.save()
            else:
                if t.alarm:
                    logger.debug('threshold no longer in alarm')
                    logger2.info(0,'Status Clear for KitThreshold id {}'.format(t.id))
                    t.alarm = False
                    t.save()
                    threshold = Threshold.objects.create(thresholdtype=helpers.KITTHRESHOLD, thresholdid=t.id, count=t.threshold, current=total, status=False)
                    notifies = KitThresholdNotify.objects.filter(kitthreshold=t)
                    for notify in notifies:
                        logger.debug('queuing email for {}'.format(notify.emailrecipient.email))
                        Notification.objects.create(notifytype=helpers.KITTHRESHOLD, notifyid=t.id, threshold=threshold.id, emailrecipient=notify.emailrecipient, status=helpers.QUEUED)
                    logger2.info(0,'Queued messages to {} recipients for KitThreshold id {}'.format(notifies.count(), t.id))

@app.task(name='send_email')
def send_email(subject, message, fromaddr, tolist):
    mail_sent = -1
    cname = 'send_email'
    logger.debug('{} called'.format(cname))
    logger2.debug(0, '{} called'.format(cname))
    try:
        if hasattr(settings, 'EMAIL_HOST_USER') and hasattr(settings, 'EMAIL_HOST_PASSWORD'):
            mail_sent = send_mail(subject, '', fromaddr, tolist, auth_user=settings.EMAIL_HOST_USER, auth_password=settings.EMAIL_HOST_PASSWORD, html_message=message)
        else:
            mail_sent = send_mail(subject, '', fromaddr, tolist, html_message=message)
    except Exception as err:
        logger.warning('There was an Error in processing {}. Error Message: {}'.format(cname, err))
        logger2.error(0,'There was an Error in processing {}. Error Message: {}'.format(cname, err))
        return -1
    finally:
        logger.debug('{} completed'.format(cname))
        logger2.debug(0,'{} completed'.format(cname))
        return mail_sent

@app.task(name='object_action_check')
def object_action_check():
    cname = 'object_action_check'
    logger.debug('{} called'.format(cname))
    lastlog = Log.objects.filter(module='tracker',severity=INFO,message='Started Processing Object Action Check').last()
    if lastlog is None:
        dt = datetime(1,1,1,tzinfo=timezone.utc)
    else:
        dt = lastlog.created
    logger.debug('Determined the last run as {}'.format(dt))
    logs = Log.objects.filter(Q(module__in=['inventory-admin','inventory-api']),Q(message__istartswith='created')|Q(message__istartswith='deleted')|Q(message__icontains='updated')|Q(message__istartswith='added')|Q(message__istartswith='subtracted'),created__gt=dt)
    logs = chain(logs, Log.objects.filter(Q(module='inventory'),Q(message__istartswith='added')|Q(message__istartswith='subtracted'),created__gt=dt))
    logger2.info(0,'Started Processing Object Action Check')
    for log in logs:
        try:
            objid = log.message.split('id')[1].strip()
            first = log.message.split()[0]
            actionname = obj = objtypename = ''
            logger.debug('parsing log data for action, object type, and id')
            if first in ['Created','Deleted']:
                actionname = first
                objtypename = log.message.split()[1]
            elif first in ['Added','Subtracted']:
                actionname = first.split('ed')[0]
                objtypename = log.message.split()[3]
            else:
                actionname = 'Modified'
                objtypename = first
            objtype = helpers.convert_name_type(objtypename)
            action = helpers.convert_name_action(actionname)
            if objtype == '':
                logger.warning('Unable to parse log message for ObjectAction data for Log id {}'.format(log.id))
                logger2.warning(0,'Unable to parse log message for ObjectAction data for Log id {}'.format(log.id))
                continue
            logger.debug('checking for ObjectActions for type {} and action {}'.format(objtype, action))
            alerts = ObjectAction.objects.filter(objtype=objtype, action=action)
            for alert in alerts:
                if alert.objid == 0 or int(objid) == alert.objid:
                    logger.debug('Attempting to retrieve affected object')
                    try:
                        obj = helpers.get_object(objtype, objid)
                    except:
                        logger.debug('Failed to retrieve affected object')
                    notifies = ObjectActionNotify.objects.filter(objectaction=alert)
                    for notify in notifies:
                        logger.debug('adding recipient {} and executing trigger'.format(notify.emailrecipient.email))
                        Notification.objects.create(notifytype=helpers.OBJECTACTION, notifyid=alert.id, emailrecipient=notify.emailrecipient, status=helpers.QUEUED)
        except Exception as err:
            logger.error('Unknown error while trying to process ObjectAction Check for object id {}: {}'.format(objid, err))
            logger2.error(0,'Unknown error while trying to process ObjectAction Check for object id {}: {}'.format(objid, err))

@app.task(name='process_notifications')
def process_notifications():
    try:
        cname = 'process_notifiations'
        logger.debug('{} called'.format(cname))
        logger2.debug(0,'{} called'.format(cname))
        notifications = Notification.objects.filter(status=helpers.QUEUED)
        logger.debug('generating list of emailrecipients')
        emailrecipients = ''
        for notification in notifications:
            if not notification.emailrecipient.email in emailrecipients:
                logger.debug('adding {} to list of emailrecipients'.format(notification.emailrecipient.id))
                emailrecipients += '{},'.format(notification.emailrecipient.id)
        if not emailrecipients == '':
            for email in emailrecipients.rstrip(',').split(','):
                logger.debug('processing notifications for emailrecipient id {}'.format(email))
                notifies = notifications.filter(emailrecipient__id=email)        
                fullmessage = message = subject = template = ''
                notifyids = ()
                for notify in notifies:
                    logger.debug('updating notification id {} to Pending'.format(notify.id))
                    notify.status = helpers.PENDING
                    notify.save()
                    logger.debug('locating the email template and rendering message')
                    try:
                        if notify.notifytype == helpers.PARTTHRESHOLD:
                            logger.debug('notifytype is {}'.format(helpers.PARTTHRESHOLD))
                            partthreshold = PartThreshold.objects.get(id=notify.notifyid)
                            template = PartThresholdTemplate.objects.get(partthreshold=partthreshold).emailtemplate
                            message = render(None, template.template.url[7:], {'partthreshold': partthreshold, 'threshold': Threshold.objects.get(id=notify.threshold), 'created': notify.created}).content.decode('utf-8')
                            logger.debug('message rendered')
                        if notify.notifytype == helpers.PARTSTORAGETHRESHOLD:
                            logger.debug('notifytype is {}'.format(helpers.PARTSTORAGETHRESHOLD))
                            partstoragethreshold = PartStorageThreshold.objects.get(id=notify.notifyid)
                            template = PartStorageThresholdTemplate.objects.get(partstoragethreshold=partstoragethreshold).emailtemplate
                            message = render(None, template.template.url[7:], {'partstoragethreshold': partstoragethreshold, 'threshold': Threshold.objects.get(id=notify.threshold), 'created': notify.created}).content.decode('utf-8')
                            logger.debug('message rendered')
                        if notify.notifytype == helpers.OBJECTACTION:
                            logger.debug('notifytype is {}'.format(helpers.OBJECTACTION))
                            objectaction = ObjectAction.objects.get(id=notify.notifyid)
                            template = ObjectActionTemplate.objects.get(objectaction=objectaction).emailtemplate
                            obj = helpers.get_object(objectaction.objtype, objectaction.objid)
                            message = render(None, template.template.url[7:], {'action': objectaction.action, 'obj': obj, 'created': notify.created}).content.decode('utf-8')
                            logger.debug('message rendered')
                        if notify.notifytype == helpers.KITTHRESHOLD:
                            logger.debug('notifytype is {}'.format(helpers.KITTHRESHOLD))
                            kitthreshold = KitThreshold.objects.get(id=notify.notifyid)
                            template = KitThresholdTemplate.objects.get(kitthreshold=kitthreshold).emailtemplate
                            message = render(None, template.template.url[7:], {'kitthreshold': kitthreshold, 'threshold': Threshold.objects.get(id=notify.threshold), 'created': notify.created}).content.decode('utf-8')
                            logger.debug('message rendered')
                        subject = template.subject
                        if not message == '':
                            logger.debug('message generated')
                            fullmessage += message
                            notifyids += (notify.id,)
                        else:
                            logger.debug('failed to generate message')
                            notify.status = helpers.ERROR
                    except PartThreshold.DoesNotExist as err:
                        logger.warning('Unable to locate PartThreshold with id {}. Error message: {}'.format(notify.notifyid, err))
                        logger2.warning(0,'Unable to locate PartThreshold with id {}. Error message: {}'.format(notify.notifyid, err))
                        notify.status = helpers.ERROR
                    except PartStorageThreshold.DoesNotExist as err:
                        logger.warning('Unable to locate PartStorageThreshold with id {}. Error message: {}'.format(notify.notifyid, err))
                        logger2.warning(0,'Unable to locate PartStorageThreshold with id {}. Error message: {}'.format(notify.notifyid, err))
                        notify.status = helpers.ERROR
                    except KitThreshold.DoesNotExist as err:
                        logger.warning('Unable to locate KitThreshold with id {}. Error message: {}'.format(notify.notifyid, err))
                        logger2.warning(0,'Unable to locate KitThreshold with id {}. Error message: {}'.format(notify.notifyid, err))
                        notify.status = helpers.ERROR
                    except Threshold.DoesNotExist as err:
                        logger.warning('Unable to locate Threshold with id {}. Error Message: {}'.format(notify.threshold, err))
                        logger2.warning(0,'Unable to locate Threshold with id {}. Error Message: {}'.format(notify.threshold, err))
                        notify.status = helpers.ERROR
                    except ObjectAction.DoesNotExist as err:
                        logger.warning('Unable to locate ObjectAction with id {}. Error message: {}'.format(notify.notifyid, err))
                        logger2.warning(0,'Unable to locate ObjectAction with id {}. Error message: {}'.format(notify.notifyid, err))
                        notify.status = helpers.ERROR
                    except PartThresholdTemplate.DoesNotExist as err:
                        logger.warning('Unable to locate EmailTemplate for PartThreshold with id {}. Error message: {}'.format(partthreshold.id, err))
                        logger2.warning(0,'Unable to locate EmailTemplate for PartThreshold with id {}. Error message: {}'.format(partthreshold.id, err))
                        notify.status = helpers.ERROR
                    except PartStorageThresholdTemplate.DoesNotExist as err:
                        logger.warning('Unable to locate EmailTemplate for PartStorageThreshold with id {}. Error message: {}'.format(partstoragethreshold.id, err))
                        logger2.warning(0,'Unable to locate EmailTemplate for PartStorageThreshold with id {}. Error message: {}'.format(partstoragethreshold.id, err))
                        notify.status = helpers.ERROR
                    except KitThresholdTemplate.DoesNotExist as err:
                        logger.warning('Unable to locate EmailTemplate for KitThreshold with id {}. Error message: {}'.format(kitthreshold.id, err))
                        logger2.warning(0,'Unable to locate EmailTemplate for KitThreshold with id {}. Error message: {}'.format(kitthreshold.id, err))
                        notify.status = helpers.ERROR
                    except ObjectActionTemplate.DoesNotExist as err:
                        logger.warning('Unable to locate EmailTemplate for ObjectAction with id {}. Error message: {}'.format(objectaction.id, err))
                        logger2.warning(0,'Unable to locate EmailTemplate for ObjectAction with id {}. Error message: {}'.format(objectaction.id, err))
                        notify.status = helpers.ERROR
                    except Exception as err:
                        logger.warning('There was an unknown problem in processing notification id {}'.format(notify.id))
                        logger.debug(err)
                        logger2.error(0,'There was an unknown problem in processing notification id {}. Error message: {}'.format(notify.id, err))
                        notify.status = helpers.ERROR
                    finally:
                        logger.debug('processing notification id {} complete'.format(notify.id))
                        logger2.debug(0,'Processed Notification id {}'.format(notify.id))
                        notify.save()
                if len(notifies) > 1:
                    subject = settings.EMAIL_DEFAULT_SUBJECT
                if not notifyids == ():
                    send_notifications.delay(email, subject, fullmessage, notifyids)
    except Exception as err:
        logger.warning('There was an unknown problem in processing notifications')
        logger.debug(err)
        logger2.error(0,'There was an unknown problem in processing notifications. Error Message: {}'.format(err))
    finally:
        logger.debug('{} completed'.format(cname))
        logger2.debug(0,'{} completed'.format(cname))

@app.task(name='send_notifications')
def send_notifications(emailid, subject, message, notifyids):
    try:
        cname = 'send_notifications'
        logger.debug('{} called'.format(cname))
        logger2.debug(0,'{} called'.format(cname))
        logger.debug('{} {} {} {}'.format(emailid, subject, notifyids, message))
        emailrecipient = EmailRecipient.objects.get(id=emailid)
        result = send_email(subject, message, settings.DEFAULT_FROM_EMAIL, [emailrecipient.email])
        if result == 1:
            notifications = Notification.objects.filter(id__in=notifyids)
            for notification in notifications:
                logger.debug('marking status as SENT for notification id {}'.format(notification.id))
                logger2.debug(0,'Updating status to SENT for notification id {}'.format(notification.id))
                notification.status = helpers.SENT
                notification.save()
        else:
            notifications = Notification.objects.filter(id__in=notifyids)
            for notification in notifications:
                logger.debug('marking status as ERROR for notification id {}'.format(notification.id))
                logger2.debug(0,'Updating status to ERROR for notification id {}'.format(notification.id))
                notification.status = helpers.ERROR
                notification.save()
    except Exception as err:
        logger.warning('There was an unknown problem in sending notifications')
        logger.debug(err)
        logger2.warning(0,'There was an unknown problem in sending notifications. Error Message {}'.format(err))
    finally:
        logger.debug('{} completed'.format(cname))
        logger2.debug(0,'{} completed'.format(cname))

@app.task(name='clean_tracker_graphs')
def clean_tracker_graphs(dir_path = join(settings.BASE_DIR, 'media/tracker/graphs')):
    cname = 'clean_tracker_graphs'
    logger.debug('{} called'.format(cname))
    try:
        files = listdir(dir_path)
        for file in files:
            if file.endswith('.png'):
                remove(join(dir_path, file))
    except Exception as err:
        logger.warning('There was an unknown problem while cleaning tracker graphs')
        logger.debug(err)
        logger2.warning(0,'There was an unknown problem in sending notifications. Error Message {}'.format(err))

@app.task(name='scheduled_report')
def scheduled_report(*args, **kwargs):
    cname = 'scheduled_report'
    logger.debug('{} called'.format(cname))
    report = kwargs['report']
    ids = kwargs['ids']
    subject = kwargs['subject']
    if 'duration' in kwargs:
        duration = kwargs['duration']
        span = kwargs['span']
    try:
        if report == helpers.RECENTLYADDEDTABLE:
            results = helpers.recent_adds_list(0, ids, duration, span)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_table.html'), {'title':'Recently Added','results': results}).content.decode('utf-8')
        if report == helpers.RECENTLYMODIFIEDTABLE:
            results = helpers.recent_mods_list(0, ids, duration, span)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_table.html'), {'title':'Recently Modified','results': results}).content.decode('utf-8')
        if report == helpers.USERACTIVITYTABLE:
            results = helpers.recent_user_list(0, ids[0], duration, span)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_table.html'), {'title':'Recent User Activity','results': results}).content.decode('utf-8')
        if report == helpers.PARTCOUNTTABLE:
            results = helpers.part_counts(0, ids)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_table.html'), {'title':'Part Counts Table','results': results}).content.decode('utf-8')
        if report == helpers.PARTCOUNTBAR:
            image = helpers.part_counts_graph(0, ids)
            data_uri = base64.b64encode(open(join(settings.BASE_DIR,image.lstrip('/')), 'rb').read()).decode('utf-8')
            img_tag = '<img src="data:image/png;base64,{0}" alt="Bar Graph of Part Counts">'.format(data_uri)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_graph.html'), {'title':'Part Count Bar Graph','img':img_tag}).content.decode('utf-8')
        if report == helpers.STORAGECONTENTSTABLE:
            results = helpers.storage_counts(0, ids[0])
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_table.html'), {'title':'Storage Counts Table','results':results}).content.decode('utf-8')
        if report == helpers.STORAGECONTENTSBAR:
            image = helpers.storage_counts_graph(0, ids[0])
            data_uri = base64.b64encode(open(join(settings.BASE_DIR,image.lstrip('/')), 'rb').read()).decode('utf-8')
            img_tag = '<img src="data:image/png;base64,{0}" alt="Bar Graph of Storage Contents">'.format(data_uri)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_graph.html'), {'title':'Storage Contents Bar Graph','img':img_tag}).content.decode('utf-8')
        if report == helpers.KITCOUNTTABLE:
            results = helpers.kit_counts(0, ids)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_table.html'), {'title':'Kit Counts Table','results':results}).content.decode('utf-8')
        if report == helpers.KITCOUNTBAR:
            image = helpers.kit_counts_graph(0, ids)
            data_uri = base64.b64encode(open(join(settings.BASE_DIR,image.lstrip('/')), 'rb').read()).decode('utf-8')
            img_tag = '<img src="data:image/png;base64,{0}" alt="Bar Graph of Kit Counts">'.format(data_uri)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_graph.html'), {'title':'Kit Count Bar Graph','img':img_tag}).content.decode('utf-8')
        if report == helpers.HISTORICPARTCOUNTLINE:
            image = helpers.historic_part_count_graph(0, ids[0], duration, span)
            data_uri = base64.b64encode(open(join(settings.BASE_DIR,image.lstrip('/')), 'rb').read()).decode('utf-8')
            img_tag = '<img src="data:image/png;base64,{0}" alt="Line Graph of Historic Part Counts">'.format(data_uri)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_graph.html'), {'title':'Historic Part Count Line Graph','img':img_tag}).content.decode('utf-8')
        if report == helpers.HISTORICPARTCOUNTTABLE:
            results = helpers.historic_part_count(0, ids[0], duration, span)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_table.html'), {'title':'Historic Part Count Table','results': results}).content.decode('utf-8')
        if report == helpers.HISTORICKITCOUNTLINE:
            image = helpers.historic_kit_count_graph(0, ids[0], duration, span)
            data_uri = base64.b64encode(open(join(settings.BASE_DIR,image.lstrip('/')), 'rb').read()).decode('utf-8')
            img_tag = '<img src="data:image/png;base64,{0}" alt="Line Graph of Historic Kit Counts">'.format(data_uri)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_graph.html'), {'title':'Historic Kit Count Line Graph','img':img_tag}).content.decode('utf-8')
        if report == helpers.HISTORICKITCOUNTTABLE:
            results = helpers.historic_kit_count(0, ids[0], duration, span)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_table.html'), {'title':'Historic Kit Count Table','results': results}).content.decode('utf-8')
        if report == helpers.HISTORICPARTSTORAGECOUNTLINE:
            image = helpers.historic_partstorage_count_graph(0, ids[0], duration, span)
            data_uri = base64.b64encode(open(join(settings.BASE_DIR,image.lstrip('/')), 'rb').read()).decode('utf-8')
            img_tag = '<img src="data:image/png;base64,{0}" alt="Line Graph of Historic PartStorage Counts">'.format(data_uri)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_graph.html'), {'title':'Historic PartStorage Count Line Graph','img':img_tag}).content.decode('utf-8')
        if report == helpers.HISTORICPARTSTORAGECOUNTTABLE:
            results = helpers.historic_partstorage_count(0, ids[0], duration, span)
            message = render(None, join(settings.MEDIA_ROOT,'tracker/templates/report_table.html'), {'title':'Historic PartStorage Count Table','results': results}).content.decode('utf-8')
    except IsADirectoryError:
        logger.warning('Scheduled Report named "{}" failed to generate a graph!'.format(subject))
        logger2.warning(0,'Scheduled Report named "{}" failed to generate a graph!'.format(subject))
    task = PeriodicTask.objects.filter(name=subject).first()
    if task == None:
        logger.warning('PeriodicTask named "{}" not found!'.format(subject))
        logger2.warning(0,'PeriodicTask named "{}" not found!'.format(subject))
    else:
        recipients = ReportNotify.objects.filter(report=task)
        for recipient in recipients:
            send_notifications(recipient.emailrecipient.id, subject, message, [0])
        logger.debug('{} completed'.format(cname))