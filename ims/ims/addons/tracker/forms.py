from datetime import datetime
from django import forms
from django.contrib.auth.models import User

from inventory.models import Part, Kit, Storage
from ims.addons.tracker.helpers import GRAPH_CHOICES, MODEL_CHOICES, PERIOD_CHOICES, REPORTS, SCHEDULER_CHOICES, SOLAR_CHOICES, TIMESPAN_CHOICES
from ims.addons.tracker.widgets import DateInput, TimeInput

class ModelSelectForm(forms.Form):
    id = forms.MultipleChoiceField(choices = MODEL_CHOICES)

class GraphSelectForm(forms.Form):
    graph = forms.ChoiceField(choices = GRAPH_CHOICES, initial = GRAPH_CHOICES[2])
    id = forms.ModelChoiceField(Part.objects.all())

class MultipurposeSelectForm(forms.Form):
    id = forms.ModelChoiceField(queryset=None)

    def __init__(self, model, *args, **kwargs):
        super(forms.Form, self).__init__(*args, **kwargs)
        self.fields['id'] = forms.ModelChoiceField(queryset=model.objects.all())

class PartMultiSelectForm(forms.Form):
    part = forms.ModelMultipleChoiceField(Part.objects.all())

class KitMultiSelectForm(forms.Form):
    kit = forms.ModelMultipleChoiceField(Kit.objects.all())

class StorageMultiSelectForm(forms.Form):
    storage = forms.ModelMultipleChoiceField(Storage.objects.all())

class TimeSpanForm(forms.Form):
    duration = forms.IntegerField(min_value = 1, initial = 1, max_value = 9223372036854775807)
    span = forms.ChoiceField(choices = TIMESPAN_CHOICES, initial = TIMESPAN_CHOICES[1])

class UserSelectForm(forms.Form):
    user = forms.ModelChoiceField(User.objects.all())

class ReportSchedulerForm(forms.Form):
    name = forms.CharField(max_length = 50)
    description = forms.CharField(max_length = 250)
    report = forms.ChoiceField(choices = REPORTS, initial = REPORTS[9])
    schedule = forms.ChoiceField(choices = SCHEDULER_CHOICES, initial = SCHEDULER_CHOICES[2])

class ClockedScheduleForm(forms.Form):
    date = forms.DateField(required = False, initial = datetime.now(), widget = DateInput)
    time = forms.TimeField(required = False, initial = datetime.now(), widget = TimeInput)
    timezone = forms.CharField(required = False, initial = 'UTC')

class IntervalScheduleForm(forms.Form):
    number = forms.IntegerField(min_value = 1, initial = 1, max_value = 9223372036854775807)
    period = forms.ChoiceField(choices = PERIOD_CHOICES, initial = PERIOD_CHOICES[3])

class CronScheduleForm(forms.Form):
    minute = forms.CharField(required = False, initial = '*')
    hour = forms.CharField(required = False, initial = '*')
    day = forms.CharField(required = False, initial = '*')
    month = forms.CharField(required = False, initial = '*')
    day_of_week = forms.CharField(required = False, initial = '*')
    timezone = forms.CharField(required = False, initial = 'UTC')

class SolarScheduleForm(forms.Form):
    event = forms.ChoiceField(required = False, choices = SOLAR_CHOICES, initial = SOLAR_CHOICES[7])
    latitude = forms.FloatField(required = False, min_value = -90, max_value = 90)
    longitude = forms.FloatField(required = False, min_value = -180, max_value = 180)