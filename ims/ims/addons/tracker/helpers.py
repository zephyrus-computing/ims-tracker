from datetime import datetime, timedelta
from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import FieldError
from django.db.models import Q
from django.db.models.base import ModelBase
from django.utils.timezone import now
from itertools import chain
import logging
import matplotlib.dates as mdates
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from inventory.models import Part, Storage, AlternateSKU, Kit, Assembly, PartStorage, PartAlternateSKU, KitPartStorage
from inventory.search import Search
from log.logger import getLogger
from log.models import Log

logger = logging.getLogger(__name__)
logger2 = getLogger('tracker')

# Consts
LESSTHAN = '<'
LESSTHANOREQUALTO = '<='
GREATERTHAN = '>'
GREATERTHANOREQUALTO = '>='
EQUALTO = '=='
NOTEQUALTO = '!='
OPERATORS = [
    (LESSTHAN, 'Less Than'),
    (LESSTHANOREQUALTO, 'Less Than or Equal To'),
    (GREATERTHAN, 'Greater Than'),
    (GREATERTHANOREQUALTO, 'Greater Than or Equal To'),
    (EQUALTO, 'Equal To'),
    (NOTEQUALTO, 'Not Equal To'),
]

ALTSKU = 'as'
ASSEMBLY = 'a'
PART = 'p'
PARTALTSKU = 'pa'
PARTSTORAGE = 'ps'
STORAGE = 's'
KIT = 'k'
KITPARTSTORAGE = 'kp'
OBJTYPES = [
    (ALTSKU, 'AlternateSKU'),
    (ASSEMBLY, 'Assembly'),
    (PART, 'Part'),
    (PARTALTSKU, 'PartAltSKU'),
    (PARTSTORAGE, 'PartStorage'),
    (STORAGE, 'Storage'),
    (KIT, 'Kit'),
    (KITPARTSTORAGE, 'KitPartStorage'),
]

ADD = 'a'
CREATED = 'c'
DELETED = 'd'
MODIFIED = 'm'
SUBTRACT = 's'
ACTIONS = [
    (ADD, 'Add'),
    (CREATED, 'Created'),
    (DELETED, 'Deleted'),
    (MODIFIED, 'Modified'),
    (SUBTRACT, 'Subtract'),
]

PARTTHRESHOLD = 'pt'
PARTSTORAGETHRESHOLD = 'pst'
OBJECTACTION = 'oa'
KITTHRESHOLD = 'kt'
NOTIFYTYPES = [
    (PARTTHRESHOLD, 'Part Threshold'),
    (PARTSTORAGETHRESHOLD, 'PartStorage Threshold'),
    (OBJECTACTION, 'Object Action'),
    (KITTHRESHOLD, 'Kit Threshold'),
]

QUEUED = 'q'
SENT = 's'
ERROR = 'e'
PENDING = 'p'
STATUSES = [
    (QUEUED, 'In Queue'),
    (PENDING, 'Pending'),
    (SENT, 'Sent'),
    (ERROR, 'Error')
]

RECENTLYADDEDTABLE = 'recent_adds_list'
RECENTLYMODIFIEDTABLE = 'recent_mods_list'
USERACTIVITYTABLE = 'recent_user_list'
PARTCOUNTTABLE = 'part_counts'
PARTCOUNTBAR = 'part_counts_graph'
STORAGECONTENTSTABLE = 'storage_counts'
STORAGECONTENTSBAR = 'storage_counts_graph'
KITCOUNTTABLE = 'kit_counts'
KITCOUNTBAR = 'kit_counts_graph'
HISTORICPARTCOUNTLINE = 'historic_part_count_graph'
HISTORICPARTCOUNTTABLE = 'historic_part_count'
HISTORICKITCOUNTLINE = 'historic_kit_count_graph'
HISTORICKITCOUNTTABLE = 'historic_kit_count'
HISTORICPARTSTORAGECOUNTLINE = 'historic_partstorage_count_graph'
HISTORICPARTSTORAGECOUNTTABLE = 'historic_partstorage_count'
REPORTS = [
    (RECENTLYADDEDTABLE, 'Recently Added Table'),
    (RECENTLYMODIFIEDTABLE, 'Recently Modified Table'),
    (USERACTIVITYTABLE, 'User Activity Table'),
    (PARTCOUNTTABLE, 'Part Count Table'),
    (PARTCOUNTBAR, 'Part Count Bar Graph'),
    (STORAGECONTENTSTABLE, 'Storage Contents Table'),
    (STORAGECONTENTSBAR, 'Storage Contents Bar Graph'),
    (KITCOUNTTABLE, 'Kit Count Table'),
    (KITCOUNTBAR, 'Kit Count Bar Graph'),
    (HISTORICPARTCOUNTLINE, 'Historic Part Count Line Graph'),
    (HISTORICPARTCOUNTTABLE, 'Historic Part Count Table'),
    (HISTORICKITCOUNTLINE, 'Historic Kit Count Line Graph'),
    (HISTORICKITCOUNTTABLE, 'Historic Kit Count Table'),
    (HISTORICPARTSTORAGECOUNTLINE, 'Historic PartStorage Count Line Graph'),
    (HISTORICPARTSTORAGECOUNTTABLE, 'Historic PartStorage Count Table'),
]

REPORT_LIST_TIMESPAN_FIELD = [
    RECENTLYADDEDTABLE, RECENTLYMODIFIEDTABLE, USERACTIVITYTABLE, HISTORICPARTCOUNTLINE,
    HISTORICPARTCOUNTTABLE, HISTORICKITCOUNTLINE, HISTORICKITCOUNTTABLE,
    HISTORICPARTSTORAGECOUNTLINE, HISTORICPARTSTORAGECOUNTTABLE
]

MODEL_CHOICES =(
    ('a','Assembly'),
    ('k','Kit'),
    ('p','Part'),
    ('s','Storage'),
    ('ps','PartStorage'),
    ('as','AlternateSKU'),
    ('pa','PartAlternateSKU'),
    ('kp','KitPartStorage'),
)

GRAPH_CHOICES =(
    ('hkcl','Historic Kit Count (L)'),
    ('hpcl','Historic Part Count (L)'),
    ('hpscl','Historic PartStorage Count (L)'),
    ('scb','Storage Contents (B)'),
    ('pcb','Part Counts (B)'),
    ('kcb','Kit Counts (B)'),
)

TIMESPAN_CHOICES =(
    ('m','Minutes'),
    ('h','Hours'),
    ('d','Days'),
    ('w','Weeks'),
)

PERIOD_CHOICES = (('s','Seconds'),)
PERIOD_CHOICES += TIMESPAN_CHOICES

SCHEDULER_CHOICES =(
    ('c','Clocked'),
    ('t','Crontab'),
    ('i','Interval'),
    ('s','Solar Event'),
)

SOLAR_CHOICES =(
    ('ad','Astronomical Dawn'),
    ('cd','Civil Dawn'),
    ('nd','Nautical Dawn'),
    ('da','Astronomical Dusk'),
    ('dc','Civil Dusk'),
    ('dn','Nautical Dusk'),
    ('sn','Solar Noon'),
    ('sr','Sunrise'),
    ('ss','Sunset'),
)

def get_model_from_logs(user, model, logs):
    cname = 'get_model_from_logs'
    logger.debug('called helper method {} for model {}'.format(cname,model))
    logger2.debug(user, 'Accessed helper method {} for model {}'.format(cname,model))
    if model.__class__ is not ModelBase:
        logger.warning('Provided model "{}" is of unknown type'.format(model))
        logger2.warning(user,'Unable to process {} for {}; unknown type'.format(cname,model))
        return []
    arr = []
    for log in logs:
        try:
            id = log.message.split('id')[1].strip()
            obj = model.objects.get(id=id)
            if obj not in arr:
                arr.append(obj)
        except model.DoesNotExist as err:
            logger.warning('Unable to locate {} with id {}. Error message: {}'.format(model,id,err))
            logger2.warning(user,'Unable to process {} for {} id {}'.format(cname,model,id))
        except IndexError as err:
            logger.warning('Unable to extract {} id from message of Log record id {}'.format(model,log.id))
            logger2.warning(user,'Unable to extract {} id from message of Log record id {}'.format(model,log.id))
        except Exception as err:
            logger.error('Unknown error while trying to process {} for Log id {}: {}'.format(log.id,err))
            logger2.error(user,'Unknown error while trying to process {} for Log id {}: {}'.format(log.id,err))
    return arr

def recent_adds_list(user, models, length, span):
    cname = 'recent_adds_list'
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    since = _get_time_ago(length, span)
    raw = ''
    if 'p' in models:
        logger.debug('Searching Logs for {} Parts'.format(cname))
        logs = Log.objects.filter(Q(module__in=['inventory-admin','inventory-api']),message__istartswith='created part ',created__gt=since)
        raw = chain(raw, get_model_from_logs(user, Part, logs))
    if 's' in models:
        logger.debug('Searching Logs for {} Storage'.format(cname))
        logs = Log.objects.filter(Q(module__in=['inventory-admin','inventory-api']),message__istartswith='created storage ',created__gt=since)
        raw = chain(raw, get_model_from_logs(user, Storage, logs))
    if 'as' in models:
        logger.debug('Searching Logs for {} AlternateSKU'.format(cname))
        logs = Log.objects.filter(Q(module__in=['inventory-admin','inventory-api']),message__istartswith='created alternatesku ',created__gt=since)
        raw = chain(raw, get_model_from_logs(user, AlternateSKU, logs))
    if 'k' in models:
        logger.debug('Searching Logs for {} Kits'.format(cname))
        logs = Log.objects.filter(Q(module__in=['inventory-admin','inventory-api']),message__istartswith='created kit ',created__gt=since)
        raw = chain(raw, get_model_from_logs(user, Kit, logs))
    if 'a' in models:
        logger.debug('Searching Logs for {} Assemblies'.format(cname))
        logs = Log.objects.filter(Q(module__in=['inventory-admin','inventory-api']),message__istartswith='created assembly ',created__gt=since)
        raw = chain(raw, get_model_from_logs(user, Assembly, logs))
    if 'ps' in models:
        logger.debug('Searching Logs for {} PartStorage'.format(cname))
        logs = Log.objects.filter(Q(module__in=['inventory-admin','inventory-api']),message__istartswith='created partstorage ',created__gt=since)
        raw = chain(raw, get_model_from_logs(user, PartStorage, logs))
    if 'pa' in models:
        logger.debug('Searching Logs for {} PartAlternateSKU'.format(cname))
        logs = Log.objects.filter(Q(module__in=['inventory-admin','inventory-api']),message__istartswith='created partalternatesku ',created__gt=since)
        raw = chain(raw, get_model_from_logs(user, PartAlternateSKU, logs))
    if 'kp' in models:
        logger.debug('Searching Logs for {} KitPartStorage'.format(cname))
        logs = Log.objects.filter(Q(module__in=['inventory-admin','inventory-api']),message__istartswith='created kitpartstorage ',created__gt=since)
        raw = chain(raw, get_model_from_logs(user, KitPartStorage, logs))
    logger.debug('Removing duplicates and sorting results')
    unique = set(raw)
    return sorted(list(unique), key=lambda r: r.created)

def recent_mods_list(user, models, length, span):
    cname = 'recent_mods_list'
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    since = _get_time_ago(length, span)
    raw = ''
    if 'p' in models:
        logger.debug('Searching Logs for {} Parts'.format(cname))
        logs = Log.objects.filter(module='inventory-api',message__istartswith='updated partdetail ',created__gt=since)
        logs = chain(Log.objects.filter(module='inventory-admin',message__istartswith='part updated ',created__gt=since))
        raw = chain(raw, get_model_from_logs(user, Part, logs))
    if 's' in models:
        logger.debug('Searching Logs for {} Storage'.format(cname))
        logs = Log.objects.filter(module='inventory-api',message__istartswith='updated storagedetail ',created__gt=since)
        logs = chain(Log.objects.filter(module='inventory-admin',message__istartswith='storage updated ',created__gt=since))
        raw = chain(raw, get_model_from_logs(user, Storage, logs))
    if 'as' in models:
        logger.debug('Searching Logs for {} AlternateSKU'.format(cname))
        logs = Log.objects.filter(module='inventory-api',message__istartswith='updated alternateskudetail ',created__gt=since)
        logs = chain(Log.objects.filter(module='inventory-admin',message__istartswith='alternatesku updated ',created__gt=since))
        raw = chain(raw, get_model_from_logs(user, AlternateSKU, logs))
    if 'k' in models:
        logger.debug('Searching Logs for {} Kits'.format(cname))
        logs = Log.objects.filter(module='inventory-api',message__istartswith='updated kitdetail ',created__gt=since)
        logs = chain(Log.objects.filter(module='inventory-admin',message__istartswith='kit updated ',created__gt=since))
        raw = chain(raw, get_model_from_logs(user, Kit, logs))
    if 'a' in models:
        logger.debug('Searching Logs for {} Assemblies'.format(cname))
        logs = Log.objects.filter(module='inventory-api',message__istartswith='updated assemblydetail ',created__gt=since)
        logs = chain(Log.objects.filter(module='inventory-admin',message__istartswith='assembly updated ',created__gt=since))
        raw = chain(raw, get_model_from_logs(user, Assembly, logs))
    if 'ps' in models:
        logger.debug('Searching Logs for {} PartStorage'.format(cname))
        logs = Log.objects.filter(module='inventory-api',message__istartswith='updated partstoragedetail ',created__gt=since)
        logs = chain(Log.objects.filter(module='inventory-admin',message__istartswith='partstorage updated ',created__gt=since))
        raw = chain(raw, get_model_from_logs(user, PartStorage, logs))
    if 'kp' in models:
        logger.debug('Searching Logs for {} KitPartStorage'.format(cname))
        logs = Log.objects.filter(module='inventory-api',message__istartswith='updated kitpartstoragedetail ',created__gt=since)
        logs = chain(Log.objects.filter(module='inventory-admin',message__istartswith='kitpartstorage updated ',created__gt=since))
        raw = chain(raw, get_model_from_logs(user, KitPartStorage, logs))
    logger.debug('Removing duplicates and sorting results')
    unique = set(raw)
    return sorted(list(unique), key=lambda r: r.created)

def recent_user_list(user, target, length, span):
    cname = 'recent_user_list'
    since = _get_time_ago(length, span)
    if is_int(target):
        try:
            target = User.objects.get(id=target)
        except User.DoesNotExist:
            logger.debug('{} helper method failed. Target user with id {} does not exist.'.format(cname,target))
            return '', ''
        except:
            logger.debug('{} helper method failed. Unknown error trying to find user id {}.'.format(cname,target))
            return '', ''
    logger.debug('Searching Logs for {} activity for id {}'.format(cname,(target.id or 0)))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    logs = Log.objects.filter(user=target,created__gt=since)
    return sorted(list(logs), key=lambda r: r.created)

def historic_part_count(user, part, length, span):
    cname = 'historic_part_count'
    since = _get_time_ago(length, span)
    if is_int(part):
        try:
            part = Part.objects.get(id=part)
        except Part.DoesNotExist:
            logger.debug('{} helper method failed. Target part with id {} does not exist.'.format(cname,part))
            return '', ''
        except:
            logger.debug('{} helper method failed. Unknown error trying to find part id {}.'.format(cname,part))
            return '', ''
    logger.debug('Searching Logs for {} of Part id {}'.format(cname, part.id))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    logs = Log.objects.filter(Q(message__istartswith='added')|Q(message__istartswith='subtracted'),Q(module='inventory')|Q(module='inventory-api'),message__icontains='partstorage',created__gt=since).order_by('-created')
    # Get the PartStorage objects for Part
    partstoreages = part.get_storage()
    ids = []
    points = [{'time':now().timestamp(),'count':part.get_total()}]
    # Grab IDs of PartStorage for comparing
    for ps in partstoreages:
        if ps.id not in ids:
            ids.append(ps.id)
    # For each log, check if the ID matches one for Part
    for log in logs:
        try:
            psid = log.message.split('id')[1].split(';')[0].strip()
            ps = PartStorage.objects.get(id=psid)
            if ps.id in ids:
                count = log.message.split('current count:')[1].strip()
                if not is_int(count):
                    logger.warning('Unable to parse count from log message with id {}'.format(log.id))
                    logger2.warning(0,'Unable to parse count from log message with id {}'.format(log.id))
                    continue
                points.append({'time':log.created.timestamp(),'count':int(count)})
        except PartStorage.DoesNotExist as err:
            logger.warning('Unable to locate PartStorage with id {}. Error message: {}'.format(psid, err))
            logger2.warning(0,'Unable to process log message for {}; PartStorage id {} not found'.format(cname,psid))
        except IndexError as err:
            logger.warning('Unable to extract PartStorage id from message of Log record id {}'.format(log.id))
            logger2.warning(0,'Unable to extract PartStorage id from message of Log record id {}'.format(log.id))
        except Exception as err:
            logger.error('Unknown error while trying to process Historic Part Count for PartStorage id {}: {}'.format(psid, err))
            logger2.error(0,'Unknown error while trying to process Historic Part Count for PartStorage id {}: {}'.format(psid, err))
    points.append({'time':since.timestamp(),'count':points[-1]['count']})
    # Organize the data and return
    timestamps = [point['time'] for point in points]
    counts = [point['count'] for point in points]
    data = sorted(zip(timestamps, counts))
    return zip(*data)

def historic_part_count_graph(user, part, length, span):
    cname = 'historic_part_count_graph'
    logger.debug('{} called. Retrieving data...'.format(cname))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    if is_int(part):
        try:
            part = Part.objects.get(id=part)
        except Part.DoesNotExist:
            logger.debug('{} helper method failed. Target part with id {} does not exist.'.format(cname,part))
            return ''
        except:
            logger.debug('{} helper method failed. Unknown error trying to find part id {}.'.format(cname,part))
            return ''
    fig, ax = plt.subplots()
    timestamps, counts = historic_part_count(user, part, length, span)
    datetimes, values = _summarize_counts(timestamps, counts, length, span, settings.ADDON_TRACKER_SUMMARIZE)
    formatter = _get_date_formatter(length, span)
    ax.plot(datetimes, values)
    ax.set(xlabel='Time', ylabel='Count', title='Historic Count for {}'.format(part.name))
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_minor_formatter(formatter)
    ax.grid()
    fig.autofmt_xdate()
    filename = 'Count-{}-{}.png'.format(part.name,timestamps[0])
    plt.savefig('{}/tracker/graphs/{}'.format(settings.MEDIA_ROOT,filename))
    plt.close(fig)
    logger.debug('Returning file path of newly generated plot chart')
    return '{}tracker/graphs/{}'.format(settings.MEDIA_URL,filename)

def historic_kit_count(user, kit, length, span):
    cname = 'historic_kit_count'
    since = _get_time_ago(length, span)
    if is_int(kit):
        try:
            kit = Kit.objects.get(id=kit)
        except Kit.DoesNotExist:
            logger.debug('{} helper method failed. Target kit with id {} does not exist.'.format(cname,kit))
            return '', ''
        except:
            logger.debug('{} helper method failed. Unknown error trying to find kit id {}.'.format(cname,kit))
            return '', ''
    logger.debug('Searching Logs for {} of Kit id {}'.format(cname, kit.id))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    logs = Log.objects.filter(Q(message__istartswith='added')|Q(message__istartswith='subtracted'),Q(module='inventory')|Q(module='inventory-api'),message__icontains='kit',created__gt=since).order_by('-created')
    # Get the KitPartStorage objects for Kit
    points = [{'time':now().timestamp(),'count':kit.get_available_count()}]
    # For each log, check if the ID matches one for Part
    for log in logs:
        try:
            kid = log.message.split('id')[1].split(';')[0].strip()
            if kid == kit.id:
                count = log.message.split('current count:')[1].strip()
                if not is_int(count):
                    logger.warning('Unable to parse count from log message with id {}'.format(log.id))
                    logger2.warning(0,'Unable to parse count from log message with id {}'.format(log.id))
                    continue
                points.append({'time':log.created.timestamp(),'count':int(count)})
        except IndexError as err:
            logger.warning('Unable to extract Kit id from message of Log record id {}'.format(log.id))
            logger2.warning(0,'Unable to extract Kit id from message of Log record id {}'.format(log.id))
        except Exception as err:
            logger.error('Unknown error while trying to process Historic Kit Count for Kit id {}: {}'.format(kid, err))
            logger2.error(0,'Unknown error while trying to process Historic Kit Count for Kit id {}: {}'.format(kid, err))
    points.append({'time':since.timestamp(),'count':points[-1]['count']})
    timestamps = [point['time'] for point in points]
    counts = [point['count'] for point in points]
    data = sorted(zip(timestamps, counts))
    return zip(*data)

def historic_kit_count_graph(user, kit, length, span):
    cname = 'historic_kit_count_graph'
    logger.debug('{} called. Retrieving data...'.format(cname))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    if is_int(kit):
        try:
            kit = Kit.objects.get(id=kit)
        except Kit.DoesNotExist:
            logger.debug('{} helper method failed. Target kit with id {} does not exist.'.format(cname,kit))
            return ''
        except:
            logger.debug('{} helper method failed. Unknown error trying to find kit id {}.'.format(cname,kit))
            return ''
    fig, ax = plt.subplots()
    timestamps, counts = historic_kit_count(user, kit, length, span)
    datetimes, values = _summarize_counts(timestamps, counts, length, span, settings.ADDON_TRACKER_SUMMARIZE)
    formatter = _get_date_formatter(length, span)
    ax.plot(datetimes, values)
    ax.set(xlabel='Time', ylabel='Count', title='Historic Count for {}'.format(kit.name))
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_minor_formatter(formatter)
    ax.grid()
    fig.autofmt_xdate()
    filename = 'Count-{}-{}.png'.format(kit.name,timestamps[0])
    plt.savefig('{}/tracker/graphs/{}'.format(settings.MEDIA_ROOT,filename))
    plt.close(fig)
    logger.debug('Returning file path of newly generated plot chart')
    return '{}tracker/graphs/{}'.format(settings.MEDIA_URL,filename)

def historic_partstorage_count(user, partstorage, length, span):
    cname = 'historic_partstorage_count'
    since = _get_time_ago(length, span)
    if is_int(partstorage):
        try:
            partstorage = PartStorage.objects.get(id=partstorage)
        except PartStorage.DoesNotExist:
            logger.debug('{} helper method failed. Target partstorage with id {} does not exist.'.format(cname,partstorage))
            return None
        except:
            logger.debug('{} helper method failed. Unknown error trying to find partstorage id {}.'.format(cname,partstorage))
            return None
    logger.debug('Searching Logs for {} of PartStorage id {}'.format(cname, partstorage.id))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    logs = Log.objects.filter(Q(message__istartswith='added')|Q(message__istartswith='subtracted'),Q(module='inventory')|Q(module='inventory-api'),message__icontains='partstorage',created__gt=since).order_by('-created')
    # Get the PartStorage objects for Part
    points = [{'time':now().timestamp(),'count':partstorage.count}]
    # For each log, check if the ID matches one for Part
    for log in logs:
        try:
            psid = log.message.split('id')[1].split(';')[0].strip()
            if int(psid) == partstorage.id:
                count = log.message.split('current count:')[1].strip()
                if not is_int(count):
                    logger.warning('Unable to parse count from log message with id {}'.format(log.id))
                    logger2.warning(0,'Unable to parse count from log message with id {}'.format(log.id))
                    continue
                points.append({'time':log.created.timestamp(),'count':count})
        except IndexError as err:
            logger.warning('Unable to extract PartStorage id from message of Log record id {}'.format(log.id))
            logger2.warning(0,'Unable to extract PartStorage id from message of Log record id {}'.format(log.id))
        except Exception as err:
            logger.error('Unknown error while trying to process Historic PartStorage Count for PartStorage id {}: {}'.format(psid, err))
            logger2.error(0,'Unknown error while trying to process Historic PartStorage Count for PartStorage id {}: {}'.format(psid, err))
    points.append({'time':since.timestamp(),'count':points[-1]['count']})
    # Organize the data and return
    timestamps = [point['time'] for point in points]
    counts = [point['count'] for point in points]
    data = sorted(zip(timestamps, counts))
    return zip(*data)

def historic_partstorage_count_graph(user, partstorage, length, span):
    cname = 'historic_partstorage_count_graph'
    logger.debug('{} called. Retrieving data...'.format(cname))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    if is_int(partstorage):
        try:
            partstorage = PartStorage.objects.get(id=partstorage)
        except PartStorage.DoesNotExist:
            logger.debug('{} helper method failed. Target partstorage with id {} does not exist.'.format(cname,partstorage))
            return ''
        except:
            logger.debug('{} helper method failed. Unknown error trying to find partstorage id {}.'.format(cname,partstorage))
            return ''
    fig, ax = plt.subplots()
    timestamps, counts = historic_partstorage_count(user, partstorage, length, span)
    datetimes, values = _summarize_counts(timestamps, counts, length, span, settings.ADDON_TRACKER_SUMMARIZE)
    formatter = _get_date_formatter(length, span)
    ax.plot(datetimes, values)
    ax.set(xlabel='Time', ylabel='Count', title='Historic Count for {}'.format(partstorage))
    ax.xaxis.set_major_formatter(formatter)
    ax.xaxis.set_minor_formatter(formatter)
    ax.grid()
    fig.autofmt_xdate()
    filename = 'Count-{}-{}.png'.format(partstorage,timestamps[0])
    plt.savefig('{}/tracker/graphs/{}'.format(settings.MEDIA_ROOT,filename))
    plt.close(fig)
    logger.debug('Returning file path of newly generated plot chart')
    return '{}tracker/graphs/{}'.format(settings.MEDIA_URL,filename)

def part_counts(user, parts):
    cname = 'part_counts'
    logger.debug('{} called. Retrieving data...'.format(cname))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    counts = []
    try:
        for part in parts:
            if is_int(part):
                part = get_object('p', part)
            counts.append({'name':part.name,'count':part.get_total()})
    except TypeError:
        logger.warning('Unable to check part counts for the specified object')
        logger2.warning(0,'Unable to check part counts for the specified object')
    except AttributeError:
        logger.warning('Unable to check part counts for one of the specified objects')
        logger2.warning(0,'Unable to check part counts for one of the specified objects')
    return counts

def part_counts_graph(user, parts):
    cname = 'part_counts_graph'
    logger.debug('{} called. Retrieving data...'.format(cname))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    data = part_counts(user, parts)
    index = np.arange(len(parts))
    for row in range(len(data)):
        plt.bar(index[row], data[row]['count'], 0.4)
    plt.ylabel('Amount')
    plt.xticks(ticks=index,labels=[x['name'] for x in data],rotation=90)
    plt.title('Part Counts')
    filename = 'PartCounts-{}.png'.format(now().timestamp())
    plt.savefig('{}/tracker/graphs/{}'.format(settings.MEDIA_ROOT,filename),bbox_inches='tight')
    plt.close()
    logger.debug('Returning file path of newly generated plot chart')
    return '{}tracker/graphs/{}'.format(settings.MEDIA_URL,filename)
    
def kit_counts(user, kits):
    cname = 'kit_counts'
    logger.debug('{} called. Retrieving data...'.format(cname))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    counts = []
    try:
        for kit in kits:
            if is_int(kit):
                kit = get_object('k', kit)
            counts.append({'name':kit.name,'count':kit.get_available_count()})
    except TypeError:
        logger.warning('Unable to check kit counts for the specified object')
        logger2.warning(0,'Unable to check kit counts for the specified object')
    except AttributeError:
        logger.warning('Unable to check kit counts for one of the specified objects')
        logger2.warning(0,'Unable to check kit counts for one of the specified objects')
    return counts

def kit_counts_graph(user, kits):
    cname = 'kit_counts_graph'
    logger.debug('{} called. Retrieving data...'.format(cname))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    data = kit_counts(user, kits)
    index = np.arange(len(kits))
    for row in range(len(data)):
        plt.bar(index[row], data[row]['count'], 0.4)
    plt.ylabel('Amount')
    plt.xticks(ticks=index,labels=[x['name'] for x in data],rotation=90)
    plt.title('Kit Counts')
    filename = 'KitCounts-{}.png'.format(now().timestamp())
    plt.savefig('{}/tracker/graphs/{}'.format(settings.MEDIA_ROOT,filename),bbox_inches='tight')
    plt.close()
    logger.debug('Returning file path of newly generated plot chart')
    return '{}tracker/graphs/{}'.format(settings.MEDIA_URL,filename)

def storage_counts(user, storage):
    cname = 'storage_counts'
    logger.debug('{} called. Retrieving data...'.format(cname))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    counts = []
    try:
        if is_int(storage):
            storage = get_object('s', storage)
        partstorages = storage.get_parts_storage()
        for partstorage in partstorages:
            counts.append({'name':partstorage.part.name,'count':partstorage.count})
    except TypeError:
        logger.warning('Unable to check storage counts for the specified object')
        logger2.warning(0,'Unable to check storage counts for the specified object')
    except AttributeError:
        logger.warning('Unable to check storage counts for one of the specified objects')
        logger2.warning(0,'Unable to check storage counts for one of the specified objects')
    return counts

def storage_counts_graph(user, storage):
    cname = 'storage_counts_graph'
    logger.debug('{} called. Retrieving data...'.format(cname))
    logger2.debug(user, 'Accessed helper method {}'.format(cname))
    data = storage_counts(user, storage)
    index = np.arange(len(data))
    for row in range(len(data)):
        plt.bar(index[row], data[row]['count'], 0.4)
    plt.ylabel('Amount')
    plt.xticks(ticks=index,labels=[x['name'] for x in data],rotation=90)
    plt.title('Storage Counts')
    filename = 'StorageCounts-{}.png'.format(now().timestamp())
    plt.savefig('{}/tracker/graphs/{}'.format(settings.MEDIA_ROOT,filename),bbox_inches='tight')
    plt.close()
    logger.debug('Returning file path of newly generated plot chart')
    return '{}tracker/graphs/{}'.format(settings.MEDIA_URL,filename)

def lookup_dictionary(dic, v):
    for k in dic:
        if k[0] == v:
            return k[1]
    return 'unknown'

def rlookup_dictionary(dic, v):
    for k in dic:
        if k[1] == v:
            return k[0]
    return ''

def get_operator(v):
    return lookup_dictionary(OPERATORS, v)

def get_objtype(v):
    return lookup_dictionary(OBJTYPES, v)

def get_action(v):
    return lookup_dictionary(ACTIONS, v)

def get_notifytype(v):
    return lookup_dictionary(NOTIFYTYPES, v)

def get_status(v):
    return lookup_dictionary(STATUSES, v)

def get_thresholdtype(v):
    return lookup_dictionary(NOTIFYTYPES, v)

def get_model_choice(v):
    return lookup_dictionary(MODEL_CHOICES, v)

def get_timespan_type(v):
    return lookup_dictionary(TIMESPAN_CHOICES, v)

def get_solar_type(v):
    return lookup_dictionary(SOLAR_CHOICES, v)

def convert_name_type(name):
    return rlookup_dictionary(OBJTYPES, name)

def convert_name_action(name):
    return rlookup_dictionary(ACTIONS, name)

def get_object(objtype, objid):
    logger.debug('get_object called')
    try:
        if objtype == 'as':
            return AlternateSKU.objects.get(id=objid)
        if objtype == 'a':
            return Assembly.objects.get(id=objid)
        if objtype == 'p':
            return Part.objects.get(id=objid)
        if objtype == 'pa':
            return PartAlternateSKU.objects.get(id=objid)
        if objtype == 'ps':
            return PartStorage.objects.get(id=objid)
        if objtype == 's':
            return Storage.objects.get(id=objid)
        if objtype == 'k':
            return Kit.objects.get(id=objid)
        if objtype == 'kp':
            return KitPartStorage.objects.get(id=objid)
        return ''
    except AlternateSKU.DoesNotExist as err:
        logger.warning('Unable to locate AlternateSKU with id {}. Error message: {}'.format(objid, err))
        raise
    except Assembly.DoesNotExist as err:
        logger.warning('Unable to locate Assembly with id {}. Error message: {}'.format(objid, err))
        raise
    except Part.DoesNotExist as err:
        logger.warning('Unable to locate Part with id {}. Error message: {}'.format(objid, err))
        raise
    except PartAlternateSKU.DoesNotExist as err:
        logger.warning('Unable to locate PartAlternateSKU with id {}. Error message: {}'.format(objid, err))
        raise
    except PartStorage.DoesNotExist as err:
        logger.warning('Unable to locate PartStorage with id {}. Error message: {}'.format(objid, err))
        raise
    except Storage.DoesNotExist as err:
        logger.warning('Unable to locate Storage with id {}. Error message: {}'.format(objid, err))
        raise
    except Kit.DoesNotExist as err:
        logger.warning('Unable to locate Kit with id {}. Error message: {}'.format(objid, err))
        raise
    except KitPartStorage.DoesNotExist as err:
        logger.warning('Unable to locate KitPartStorage with id {}. Error message: {}'.format(objid, err))
        raise
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_object')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_object completed')

def check_threshold(total, operator, threshold):
    if operator == '<':
        return total < threshold
    if operator == '<=':
        return total <= threshold
    if operator == '>':
        return total > threshold
    if operator == '>=':
        return total >= threshold
    if operator == '==':
        return total == threshold
    if operator == '!=':
        return total != threshold
    return False
    
def is_int(i):
    try:
        int(i)
        return True
    except ValueError:
        return False
    except TypeError:
        return False
    
def parse_querytype(value):
    if value == 1 or value == '1':
        return "^"
    if value == 2 or value == '2':
        return "="
    return ""

def _get_date_formatter(length, span_type):
    t = get_timespan_type(span_type)
    if not is_int(length):
        return mdates.DateFormatter("%Y-%m-%d")
    length = int(length)
    if t == 'Weeks':
        length *= 10080
    if t == 'Days':
        length *= 1440
    if t == 'Hours':
        length *= 60
    if length < 1441:
        return mdates.DateFormatter("%H:%M:%S")
    if length < 2881:
        return mdates.DateFormatter("%Y-%m-%d %H:%M:%S")
    return mdates.DateFormatter("%Y-%m-%d")

def _get_time_ago(num, span_type):
    t = get_timespan_type(span_type)
    delta = timedelta(hours=1)
    if not is_int(num):
        return now() - delta
    num = int(num)
    if t == 'Days':
        delta = timedelta(days=num)
    if t == 'Hours':
        delta = timedelta(hours=num)
    if t == 'Minutes':
        delta = timedelta(minutes=num)
    if t == 'Weeks':
        delta = timedelta(weeks=num)
    return now() - delta

def _get_sample_size(length, span_type):
    t = get_timespan_type(span_type)
    if not is_int(length):
        return ''
    length = int(length)
    if t == 'Weeks':
        length *= 10080
    if t == 'Days':
        length *= 1440
    if t == 'Hours':
        length *= 60
    if length < 16:
        return ''
    if length < 61:
        return '5T'
    if length < 361:
        return '30T'
    if length < 1441:
        return 'H'
    if length < 2881:
        return '3H'
    if length < 4321:
        return '6H'
    return 'D'

def _summarize_counts(timestamps, counts, length, span, summarize):
    sample = _get_sample_size(length, span)
    datetimes = [datetime.fromtimestamp(ts) for ts in timestamps]
    if sample == '':
        return datetimes, counts
    data = { 'timestamp': datetimes, 'count': counts }
    df = pd.DataFrame(data)
    df.set_index('timestamp', inplace=True)
    if summarize == 'max':
        resampled_df = df.resample(sample).max()
    if summarize == 'min':
        resampled_df = df.resample(sample).min()
    if summarize == 'mean':
        resampled_df = df.resample(sample).mean()
    if summarize == 'median':
        resampled_df = df.resample(sample).median()
    # 'last' is deprecated and 'min' should be used; to be removed in v1.3
    if summarize == 'last':
        resampled_df = df.resample(sample).last()
    resampled_df = resampled_df.ffill()
    resampled_df = resampled_df.reset_index()
    return resampled_df['timestamp'], resampled_df['count']

def get_search_fields(model):
    logger.debug('get_search_fields called')
    try:
        if model == 'EmailRecipient':
            return ['name','email']
        if model == 'EmailTemplate':
            return ['name','subject']
        if model == 'PartThreshold':
            return ['name','part__name','operator','alarm','threshold']
        if model == 'PartThresholdNotify':
            return ['partthreshold__part__name','partthreshold__name','emailrecipient__name','emailrecipient__email']
        if model == 'PartThresholdTemplate':
            return ['partthreshold__part__name','partthreshold__name','emailtemplate__name','emailtemplate__subject']
        if model == 'PartStorageThreshold':
            return ['name','partstorage__part__name','partstorage__storage__name','operator','alarm','threshold']
        if model == 'PartStorageThresholdNotify':
            return ['partstoragethreshold__partstorage__part__name','partstoragethreshold__partstorage__storage__name','partstoragethreshold__name','emailrecipient__name','emailrecipient__email']
        if model == 'PartStorageThresholdTemplate':
            return ['partstoragethreshold__partstorage__part__name','partstoragethreshold__partstorage__storage__name','partstoragethreshold__name','emailtemplate__name','emailtemplate__subject']
        if model == 'KitThreshold':
            return ['name','kit__name','operator','alarm','threshold']
        if model == 'KitThresholdNotify':
            return ['kitthreshold__kit__name','kitthreshold__name','emailrecipient__name','emailrecipient__email']
        if model == 'KitThresholdTemplate':
            return ['kitthreshold__kit__name','kitthreshold__name','emailtemplate__name','emailtemplate__subject']
        if model == 'ObjectAction':
            return ['name','objtype','action','objid']
        if model == 'ObjectActionNotify':
            return ['objectaction__name','emailrecipient__name','emailrecipient__email']
        if model == 'ObjectActionTemplate':
            return ['objectaction__name','emailtemplate__name','emailtemplate__subject']
        return []
    except Exception as err:
        logger.warning('There was an unknown problem in processing get_search_fields')
        logger.debug(err)
        raise
    finally:
        logger.debug('get_search_fields completed')

def filter_api_request(request, logmsg, model, objects):
    logger.debug('filter_api_request called')
    fields = get_search_fields(model)
    query = request.GET.get('search')
    if query != None:
        logmsg += ' query={},'.format(query)
        logger.debug('Filtering list of {} with query'.format(model))
        sf = Search().filter(fields, query)
        objects = objects.filter(sf)
    for field in fields:
        value = request.GET.get(field)
        if value != None:
            logmsg += ' {}={},'.format(field, value)
            logger.debug('Filtering list of {} with query on field "{}"'.format(model, field))
            sf = Search().filter([field], value)
            objects = objects.filter(sf)
    sort = request.GET.get('sort')
    if sort != None:
        try:
            objects = objects.order_by(sort)
            logmsg += ' sort={},'.format(sort)
        except FieldError:
            logger.warning('Unable to sort {} based on field "{}". Request made by {}'.format(model, sort, (request.user or 0)))
    limit = request.GET.get('limit')
    if not is_int(limit): limit = settings.DEFAULT_API_RESULTS
    else: limit = int(limit)
    offset = request.GET.get('offset')
    if not is_int(offset): offset = 0
    else: offset = int(offset)
    logmsg += ' limit={}, offset={}'.format(limit, offset)
    limit += offset
    objects = objects[offset:limit]
    logger.debug('filter_api_request completed')
    return logmsg, objects