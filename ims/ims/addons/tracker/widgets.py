from django.forms.widgets import Input
from django.utils import formats

class DateTimeBaseInput(Input):
    format_key = ""
    supports_microseconds = True

    def __init__(self, attrs={"step":"1"}, format=None):
        super().__init__(attrs)
        self.format = format or None

    def format_value(self, value):
        return formats.localize_input(
            value, self.format or formats.get_format(self.format_key)[0]
        )

class DateInput(DateTimeBaseInput):
    input_type = "date"
    template_name = "django/forms/widgets/date.html"
    format_key = "DATE_INPUT_FORMATS"

class TimeInput(DateTimeBaseInput):
    input_type = "time"
    template_name = "django/forms/widgets/time.html"
    format_key = "TIME_INPUT_FORMATS"
