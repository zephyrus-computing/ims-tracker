from django.urls import re_path
from ims.addons.tracker.api import views

app_name='ims.addon.tracker.api'
urlpatterns = [
    re_path(r'^partthreshold/$', views.PartThresholdList.as_view(), name='partthreshold_list'),
    re_path(r'^partthreshold/(?P<id>[\d]+)/$', views.PartThresholdDetail.as_view(), name='partthreshold_detail'),
    re_path(r'^partthresholdnotify/$', views.PartThresholdNotifyList.as_view(), name='partthresholdnotify_list'),
    re_path(r'^partthresholdnotify/(?P<id>[\d]+)/$', views.PartThresholdNotifyDetail.as_view(), name='partthresholdnotify_detail'),
    re_path(r'^partthresholdtemplate/$', views.PartThresholdTemplateList.as_view(), name='partthresholdtemplate_list'),
    re_path(r'^partthresholdtemplate/(?P<id>[\d]+)/$', views.PartThresholdTemplateDetail.as_view(), name='partthresholdtemplate_detail'),
    re_path(r'^partstoragethreshold/$', views.PartStorageThresholdList.as_view(), name='partstoragethreshold_list'),
    re_path(r'^partstoragethreshold/(?P<id>[\d]+)/$', views.PartStorageThresholdDetail.as_view(), name='partstoragethreshold_detail'),
    re_path(r'^partstoragethresholdnotify/$', views.PartStorageThresholdNotifyList.as_view(), name='partstoragethresholdnotify_list'),
    re_path(r'^partstoragethresholdnotify/(?P<id>[\d]+)/$', views.PartStorageThresholdNotifyDetail.as_view(), name='partstoragethresholdnotify_detail'),
    re_path(r'^partstoragethresholdtemplate/$', views.PartStorageThresholdTemplateList.as_view(), name='partstoragethresholdtemplate_list'),
    re_path(r'^partstoragethresholdtemplate/(?P<id>[\d]+)/$', views.PartStorageThresholdTemplateDetail.as_view(), name='partstoragethresholdtemplate_detail'),
    re_path(r'^kitthreshold/$', views.KitThresholdList.as_view(), name='kitthreshold_list'),
    re_path(r'^kitthreshold/(?P<id>[\d]+)/$', views.KitThresholdDetail.as_view(), name='kitthreshold_detail'),
    re_path(r'^kitthresholdnotify/$', views.KitThresholdNotifyList.as_view(), name='kitthresholdnotify_list'),
    re_path(r'^kitthresholdnotify/(?P<id>[\d]+)/$', views.KitThresholdNotifyDetail.as_view(), name='kitthresholdnotify_detail'),
    re_path(r'^kitthresholdtemplate/$', views.KitThresholdTemplateList.as_view(), name='kitthresholdtemplate_list'),
    re_path(r'^kitthresholdtemplate/(?P<id>[\d]+)/$', views.KitThresholdTemplateDetail.as_view(), name='kitthresholdtemplate_detail'),
    re_path(r'^objectaction/$', views.ObjectActionList.as_view(), name='objectaction_list'),
    re_path(r'^objectaction/(?P<id>[\d]+)/$', views.ObjectActionDetail.as_view(), name='objectaction_detail'),
    re_path(r'^objectactionnotify/$', views.ObjectActionNotifyList.as_view(), name='objectactionnotify_list'),
    re_path(r'^objectactionnotify/(?P<id>[\d]+)/$', views.ObjectActionNotifyDetail.as_view(), name='objectactionnotify_detail'),
    re_path(r'^objectactiontemplate/$', views.ObjectActionTemplateList.as_view(), name='objectactiontemplate_list'),
    re_path(r'^objectactiontemplate/(?P<id>[\d]+)/$', views.ObjectActionTemplateDetail.as_view(), name='objectactiontemplate_detail'),
    re_path(r'^emailrecipient/$', views.EmailRecipientList.as_view(), name='emailrecipient_list'),
    re_path(r'^emailrecipient/(?P<id>[\d]+)/$', views.EmailRecipientDetail.as_view(), name='emailrecipient_detail'),
    re_path(r'^emailtemplate/$', views.EmailTemplateList.as_view(), name='emailtemplate_list'),
    re_path(r'^emailtemplate/(?P<id>[\d]+)/$', views.EmailTemplateDetail.as_view(), name='emailtemplate_detail'),
]
