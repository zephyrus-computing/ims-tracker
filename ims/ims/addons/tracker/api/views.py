from django.conf import settings
from django.shortcuts import get_object_or_404
from inspect import currentframe
from os.path import join
from rest_framework import generics, status
from rest_framework.parsers import MultiPartParser, FormParser, FileUploadParser
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView
import logging

from inventory.api.serializers import PartSerializer, PartStorageSerializer, KitSerializer
from ims.addons.tracker.api.serializers import EmailRecipientSerializer, EmailTemplateSerializer, PartThresholdSerializer, PartThresholdReadSerializer, PartStorageThresholdSerializer, PartStorageThresholdReadSerializer, PartThresholdNotifySerializer, PartThresholdNotifyReadSerializer, PartThresholdTemplateSerializer, PartThresholdTemplateReadSerializer, PartStorageThresholdNotifySerializer, PartStorageThresholdNotifyReadSerializer, PartStorageThresholdTemplateSerializer, PartStorageThresholdTemplateReadSerializer, KitThresholdSerializer, KitThresholdReadSerializer, KitThresholdNotifySerializer, KitThresholdNotifyReadSerializer, KitThresholdTemplateSerializer, KitThresholdTemplateReadSerializer, ObjectActionSerializer, ObjectActionNotifySerializer, ObjectActionNotifyReadSerializer, ObjectActionTemplateSerializer, ObjectActionTemplateReadSerializer, NotificationSerializer, ThresholdSerializer
from ims.addons.tracker.models import PartThreshold, PartStorageThreshold, PartThresholdNotify, PartThresholdTemplate, PartStorageThresholdNotify, PartStorageThresholdTemplate, KitThreshold, KitThresholdNotify, KitThresholdTemplate, ObjectAction, ObjectActionNotify, ObjectActionTemplate, Threshold, Notification, EmailRecipient, EmailTemplate
from log.logger import getLogger
import ims.addons.tracker.helpers as helpers

logger = logging.getLogger(__name__)
logger2 = getLogger('tracker-api')


# PartThreshold API Views
class PartThresholdList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = PartThreshold
    serializer_classes = (PartThresholdReadSerializer, PartThresholdSerializer)
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_classes[0], 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_classes[1], 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class PartThresholdDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = PartThresholdSerializer
    model_class = PartThreshold
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# PartStorageThreshold API Views
class PartStorageThresholdList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = PartStorageThreshold
    serializer_classes = (PartStorageThresholdReadSerializer, PartStorageThresholdSerializer)
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_classes[0], 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_classes[1], 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class PartStorageThresholdDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = PartStorageThresholdSerializer
    model_class = PartStorageThreshold
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# PartThresholdNotify API Views
class PartThresholdNotifyList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = PartThresholdNotify
    serializer_classes = (PartThresholdNotifyReadSerializer, PartThresholdNotifySerializer)
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_classes[0], 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_classes[1], 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class PartThresholdNotifyDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = PartThresholdNotifyReadSerializer
    model_class = PartThresholdNotify
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# PartThresholdTemplate API Views
class PartThresholdTemplateList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = PartThresholdTemplate
    serializer_classes = (PartThresholdTemplateReadSerializer, PartThresholdTemplateSerializer)
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_classes[0], 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_classes[1], 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class PartThresholdTemplateDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = PartThresholdTemplateReadSerializer
    model_class = PartThresholdTemplate
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# PartStorageThresholdNotify API Views
class PartStorageThresholdNotifyList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = PartStorageThresholdNotify
    serializer_classes = (PartStorageThresholdNotifyReadSerializer, PartStorageThresholdNotifySerializer)
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_classes[0], 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_classes[1], 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class PartStorageThresholdNotifyDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = PartStorageThresholdNotifyReadSerializer
    model_class = PartStorageThresholdNotify
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# PartStorageThresholdTemplate API Views
class PartStorageThresholdTemplateList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = PartStorageThresholdTemplate
    serializer_classes = (PartStorageThresholdTemplateReadSerializer, PartStorageThresholdTemplateSerializer)
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_classes[0], 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_classes[1], 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class PartStorageThresholdTemplateDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = PartStorageThresholdTemplateReadSerializer
    model_class = PartStorageThresholdTemplate
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# KitThreshold API Views
class KitThresholdList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = KitThreshold
    serializer_classes = (KitThresholdReadSerializer, KitThresholdSerializer)
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_classes[0], 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_classes[1], 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class KitThresholdDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = KitThresholdSerializer
    model_class = KitThreshold
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# KitThresholdNotify API Views
class KitThresholdNotifyList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = KitThresholdNotify
    serializer_classes = (KitThresholdNotifyReadSerializer, KitThresholdNotifySerializer)
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_classes[0], 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_classes[1], 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class KitThresholdNotifyDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = KitThresholdNotifyReadSerializer
    model_class = KitThresholdNotify
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# KitThresholdTemplate API Views
class KitThresholdTemplateList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = KitThresholdTemplate
    serializer_classes = (KitThresholdTemplateReadSerializer, KitThresholdTemplateSerializer)
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_classes[0], 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_classes[1], 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class KitThresholdTemplateDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = KitThresholdTemplateReadSerializer
    model_class = KitThresholdTemplate
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# ObjectAction API Views
class ObjectActionList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = ObjectAction
    serializer_class = ObjectActionSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class ObjectActionDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = ObjectActionSerializer
    model_class = ObjectAction
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# ObjectActionNotify API Views
class ObjectActionNotifyList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = ObjectActionNotify
    serializer_classes = (ObjectActionNotifyReadSerializer, ObjectActionNotifySerializer)
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_classes[0], 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_classes[1], 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class ObjectActionNotifyDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = ObjectActionNotifyReadSerializer
    model_class = ObjectActionNotify
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# ObjectActionTemplate API Views
class ObjectActionTemplateList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = ObjectActionTemplate
    serializer_classes = (ObjectActionTemplateReadSerializer, ObjectActionTemplateSerializer)
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_classes[0], 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_classes[1], 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class ObjectActionTemplateDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = ObjectActionTemplateReadSerializer
    model_class = ObjectActionTemplate
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# EmailRecipient API Views
class EmailRecipientList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    model_class = EmailRecipient
    serializer_class = EmailRecipientSerializer
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class EmailRecipientDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    serializer_class = EmailRecipientSerializer
    model_class = EmailRecipient
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

# EmailTemplate API Views
class EmailTemplateList(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)
    serializer_class = EmailTemplateSerializer
    model_class = EmailTemplate
    def get(self, request):
        return _get(request, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def post(self, request):
        return _post(request, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.add_{}'.format(self.model_class.__name__.lower()))

class EmailTemplateDetail(APIView):
    if settings.ALLOW_ANONYMOUS:
        permission_classes = (IsAuthenticatedOrReadOnly,)
    else:
        permission_classes = (IsAuthenticated,)
    parser_classes = (MultiPartParser, FormParser, FileUploadParser)
    serializer_class = EmailTemplateSerializer
    model_class = EmailTemplate
    def get(self, request, id):
        return _pk_get(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.view_{}'.format(self.model_class.__name__.lower()))
    def put(self, request, id):
        return _pk_put(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def patch(self, request, id):
        return _pk_patch(request, id, self.__class__.__name__, self.model_class, self.serializer_class, 'tracker.change_{}'.format(self.model_class.__name__.lower()))
    def delete(self, request, id):
        return _pk_delete(request, id, self.__class__.__name__, self.model_class, 'tracker.delete_{}'.format(self.model_class.__name__.lower()))

def _get(request, cname, model, serializer, permission):
    fname = 'GET'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug((request.user or 0), '{} Request to {} API'.format(fname, cname))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logmsg = 'Accessed {} API'.format(cname)
    logger.debug('Generating list of {}'.format(model.__name__))
    templates = model.objects.all()
    logmsg, templates = helpers.filter_api_request(request, logmsg, model.__name__, templates)
    logger.debug('Responding (200) to request for {} APIView'.format(cname))
    logger2.info((request.user or 0), logmsg)
    return Response(serializer(templates, many=True).data, status.HTTP_200_OK)
def _post(request, cname, model, serializer, permission):
    fname = 'POST'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug(request.user, '{} Request to {} API'.format(fname, cname))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Serializing data in request')
    serializer = serializer(data=request.data)
    if serializer.is_valid():
        logger.debug('Serializer is valid')
        logger.info('Creating new {} "{}"'.format(model.__name__, serializer.validated_data.get('name')))
        inst = serializer.save()
        logger.debug('Responding (201) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Created {} id {}'.format(model.__name__, inst.id))
        return Response(serializer.data, status.HTTP_201_CREATED)
    logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
    logger.debug('Responding (400) to request for {} APIView'.format(cname))
    logger2.warning(request.user, 'Bad {} Request to {} API'.format(fname, cname))
    return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
def _pk_get(request, id, cname, model, serializer, permission):
    fname = 'GET'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug((request.user or 0), '{} Request to {} API for id {}'.format(fname, cname, id))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {}'.format(model.__name__))
    template = get_object_or_404(model, id=id)
    logger.debug('Responding (200) to request for {} APIView'.format(cname))
    logger2.info((request.user or 0), 'Accessed {} API for id {}'.format(cname, id))
    return Response(serializer(template).data, status.HTTP_200_OK)
def _pk_put(request, id, cname, model, serializer, permission):
    fname = 'PUT'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug(request.user, '{} Request to {} API for id {}'.format(fname, cname, id))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {}'.format(model))
    template = get_object_or_404(model, id=id)
    logger.debug('Serializing data in request')
    serializer = serializer(template, data=request.data)
    if serializer.is_valid():
        logger.debug('Serializer is valid')
        serializer.save()
        logger.info('{} request successfully updated {} id {}'.format(fname, model.__name__, id))
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Updated {} for id {}'.format(cname, id))
        return Response(serializer.data, status.HTTP_200_OK)
    logger.debug(serializer.errors)
    logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
    logger.debug('Responding (400) to request for {} APIView'.format(cname))
    logger2.warning(request.user, 'Bad {} Request to {} API'.format(fname, cname))
    return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
def _pk_patch(request, id, cname, model, serializer, permission):
    fname = 'PATCH'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug(request.user, '{} Request to {} API for id {}'.format(fname, cname, id))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {}'.format(model.__name__))
    template = get_object_or_404(model, id=id)
    logger.debug('Serializing data in request')
    serializer = serializer(template, data=request.data, partial=True)
    if serializer.is_valid():
        logger.debug('Serializer is valid')
        serializer.save()
        logger.info('{} request successfully updated {} id {}'.format(fname, model.__name__, id))
        logger.debug('Responding (200) to request for {} APIView'.format(cname))
        logger2.info(request.user, 'Updated {} for id {}'.format(cname, id))
        return Response(serializer.data, status.HTTP_200_OK)
    logger.info('HTTP_400_BAD_REQUEST response for {} APIView'.format(cname))
    logger.debug('Responding (400) to request for {} APIView'.format(cname))
    logger2.warning(request.user, 'Bad {} Request to {} API'.format(fname, cname))
    return Response(serializer.errors, status.HTTP_400_BAD_REQUEST)
def _pk_delete(request, id, cname, model, permission):
    fname = 'DELETE'
    logger.info('{} Request made to {} APIView'.format(fname, cname))
    logger2.debug(request.user, '{} Request to {} API for id {}'.format(fname, cname, id))
    if not request.user.has_perm(permission):
        logger.debug('User does not have {} permissions to {} APIView'.format(fname, cname))
        logger2.warning(request.user, 'Access Denied for {} request to {} API'.format(fname, cname))
        return Response({}, status.HTTP_403_FORBIDDEN)
    logger.debug('Getting requested {}'.format(model.__name__))
    obj = get_object_or_404(model, id=id)
    message = {
        'message': '{} has sucessfully been deleted'.format(model.__name__),
    }
    if (model.__name__ == 'PartThreshold'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'part': PartSerializer(obj.part).data,
        }
    if (model.__name__ == 'PartThresholdNotify'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'partthreshold': PartThresholdSerializer(obj.partthreshold).data,
            'emailrecipient': EmailRecipientSerializer(obj.emailrecipient).data,
        }
    if (model.__name__ == 'PartThresholdTemplate'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'partthreshold': PartThresholdSerializer(obj.partthreshold).data,
            'emailtemplate': EmailTemplateSerializer(obj.emailtemplate).data,
        }
    if (model.__name__ == 'PartStorageThreshold'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'partstorage': PartStorageSerializer(obj.partstorage).data,
        }
    if (model.__name__ == 'PartStorageThresholdNotify'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'partstoragethreshold': PartStorageThresholdSerializer(obj.partstoragethreshold).data,
            'emailrecipient': EmailRecipientSerializer(obj.emailrecipient).data,
        }
    if (model.__name__ == 'PartStorageThresholdTemplate'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'partstoragethreshold': PartStorageThresholdSerializer(obj.partstoragethreshold).data,
            'emailtemplate': EmailTemplateSerializer(obj.emailtemplate).data,
        }
    if (model.__name__ == 'KitThreshold'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'kit': KitSerializer(obj.kit).data,
        }
    if (model.__name__ == 'KitThresholdNotify'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'kitthreshold': KitThresholdSerializer(obj.kitthreshold).data,
            'emailrecipient': EmailRecipientSerializer(obj.emailrecipient).data,
        }
    if (model.__name__ == 'KitThresholdTemplate'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'kitthreshold': KitThresholdSerializer(obj.kitthreshold).data,
            'emailtemplate': EmailTemplateSerializer(obj.emailtemplate).data,
        }
    if (model.__name__ == 'ObjectAction'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'objtype': obj.objtype,
            'objid': obj.objid,
        }
    if (model.__name__ == 'ObjectActionNotify'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'objectaction': ObjectActionSerializer(obj.objectaction).data,
            'emailrecipient': EmailRecipientSerializer(obj.emailrecipient).data,
        }
    if (model.__name__ == 'ObjectActionTemplate'):
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model.__name__),
            'objectaction': ObjectActionSerializer(obj.objectaction).data,
            'emailtemplate': EmailTemplateSerializer(obj.emailtemplate).data,
        }
    if (model.__name__ == 'EmailRecipient'):
        partthresholds = obj.get_partthresholds()
        partstoragethresholds = obj.get_partstoragethresholds()
        kitthresholds = obj.get_kitthresholds()
        objectactions = obj.get_objectactions()
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model),
            'partthreshold': PartThresholdSerializer(partthresholds, many=True).data,
            'partstoragethreshold': PartStorageThresholdSerializer(partstoragethresholds, many=True).data,
            'kitthreshold': KitThresholdSerializer(kitthresholds, many=True).data,
            'objectaction': ObjectActionSerializer(objectactions, many=True).data,
        }
    if (model.__name__ == 'EmailTemplate'):
        partthresholds = obj.get_partthresholds()
        partstoragethresholds = obj.get_partstoragethresholds()
        kitthresholds = obj.get_kitthresholds()
        objectactions = obj.get_objectactions()
        message = {
            'message': '{} has sucessfully been deleted. It was connected to the following objects:'.format(model),
            'partthreshold': PartThresholdSerializer(partthresholds, many=True).data,
            'partstoragethreshold': PartStorageThresholdSerializer(partstoragethresholds, many=True).data,
            'kitthreshold': KitThresholdSerializer(kitthresholds, many=True).data,
            'objectaction': ObjectActionSerializer(objectactions, many=True).data,
        }
    logger.info('DELETE request for {} id {}'.format(model.__name__, id))
    obj.delete()
    logger.debug('Responding (200) to request for {} APIView'.format(cname))
    logger2.info(request.user, 'Deleted {} id {}'.format(model.__name__, id))
    return Response(message, status.HTTP_200_OK)