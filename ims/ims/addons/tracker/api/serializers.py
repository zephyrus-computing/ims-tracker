from django.conf import settings
from rest_framework import serializers
from ims.addons.tracker.models import PartThreshold, PartStorageThreshold, EmailRecipient, EmailTemplate, PartThresholdTemplate, PartThresholdNotify, PartStorageThresholdTemplate, PartStorageThresholdNotify, KitThreshold, KitThresholdTemplate, KitThresholdNotify, ObjectAction, ObjectActionTemplate, ObjectActionNotify, Notification, Threshold

class KitThresholdSerializer(serializers.ModelSerializer):
    class Meta:
        model = KitThreshold
        fields = ('id','name','kit','threshold','operator','alarm')

    def create(self, validated_data):
        return KitThreshold.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.kit = validated_data.get('kit', instance.kit)
        instance.threshold = validated_data.get('threshold', instance.threshold)
        instance.operator = validated_data.get('operator', instance.operator)
        instance.save()
        return instance

class KitThresholdReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = KitThreshold
        fields = ('id','name','kit','threshold','operator','alarm')
        depth = 1

class KitThresholdNotifySerializer(serializers.ModelSerializer):
    class Meta:
        model = KitThresholdNotify
        fields = ('id','kitthreshold','emailrecipient')

    def create(self, validated_data):
        return KitThresholdNotify.objects.create(**validated_data)

class KitThresholdNotifyReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = KitThresholdNotify
        fields = ('id','kitthreshold','emailrecipient')
        depth = 1

class KitThresholdTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = KitThresholdTemplate
        fields = ('id','kitthreshold','emailtemplate')

    def create(self, validated_data):
        return KitThresholdTemplate.objects.create(**validated_data)

class KitThresholdTemplateReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = KitThresholdTemplate
        fields = ('id','kitthreshold','emailtemplate')
        depth = 1

class PartThresholdSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartThreshold
        fields = ('id','name','part','threshold','operator','alarm')

    def create(self, validated_data):
        return PartThreshold.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.part = validated_data.get('part', instance.part)
        instance.threshold = validated_data.get('threshold', instance.threshold)
        instance.operator = validated_data.get('operator', instance.operator)
        instance.save()
        return instance

class PartThresholdReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartThreshold
        fields = ('id','name','part','threshold','operator','alarm')
        depth = 1

class PartStorageThresholdSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorageThreshold
        fields = ('id','name','partstorage','threshold','operator','alarm')

    def create(self, validated_data):
        return PartStorageThreshold.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.partstorage = validated_data.get('partstorage', instance.partstorage)
        instance.threshold = validated_data.get('threshold', instance.threshold)
        instance.operator = validated_data.get('operator', instance.operator)
        instance.save()
        return instance

class PartStorageThresholdReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorageThreshold
        fields = ('id','name','partstorage','threshold','operator','alarm')
        depth = 1

class EmailRecipientSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailRecipient
        fields = ('id','name','email')

    def create(self, validated_data):
        return EmailRecipient.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.email = validated_data.get('email', instance.email)
        instance.save()
        return instance

class EmailTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailTemplate
        fields = ('id','name','subject','template')

    def create(self, validated_data):
        return EmailTemplate.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.subject = validated_data.get('subject', instance.subject)
        instance.template = validated_data.get('template', instance.template)
        instance.save()
        return instance

class PartThresholdNotifySerializer(serializers.ModelSerializer):
    class Meta:
        model = PartThresholdNotify
        fields = ('id','partthreshold','emailrecipient')

    def create(self, validated_data):
        return PartThresholdNotify.objects.create(**validated_data)

class PartThresholdNotifyReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartThresholdNotify
        fields = ('id','partthreshold','emailrecipient')
        depth = 1

class PartThresholdTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartThresholdTemplate
        fields = ('id','partthreshold','emailtemplate')

    def create(self, validated_data):
        return PartThresholdTemplate.objects.create(**validated_data)

class PartThresholdTemplateReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartThresholdTemplate
        fields = ('id','partthreshold','emailtemplate')
        depth = 1

class PartStorageThresholdNotifySerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorageThresholdNotify
        fields = ('id','partstoragethreshold','emailrecipient')

    def create(self, validated_data):
        return PartStorageThresholdNotify.objects.create(**validated_data)

class PartStorageThresholdNotifyReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorageThresholdNotify
        fields = ('id','partstoragethreshold','emailrecipient')
        depth = 1

class PartStorageThresholdTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorageThresholdTemplate
        fields = ('id','partstoragethreshold','emailtemplate')

    def create(self, validated_data):
        return PartStorageThresholdTemplate.objects.create(**validated_data)

class PartStorageThresholdTemplateReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = PartStorageThresholdTemplate
        fields = ('id','partstoragethreshold','emailtemplate')
        depth = 1

class ObjectActionSerializer(serializers.ModelSerializer):
    class Meta:
         model = ObjectAction
         fields = ('id','name','objtype','action','objid')

    def create(self, validated_data):
        return ObjectAction.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.action = validated_data.get('action', instance.action)
        instance.save()
        return instance

class ObjectActionNotifySerializer(serializers.ModelSerializer):
    class Meta:
        model = ObjectActionNotify
        fields = ('id','objectaction','emailrecipient')

    def create(self, validated_data):
        return ObjectActionNotify.objects.create(**validated_data)

class ObjectActionNotifyReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObjectActionNotify
        fields = ('id','objectaction','emailrecipient')
        depth = 1

class ObjectActionTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObjectActionTemplate
        fields = ('id','objectaction','emailtemplate')

    def create(self, validated_data):
        return ObjectActionTemplate.objects.create(**validated_data)

class ObjectActionTemplateReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObjectActionTemplate
        fields = ('id','objectaction','emailtemplate')
        depth = 1

class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ('id','notifytype','notifyid','threshold','emailrecipient','status')

class ThresholdSerializer(serializers.ModelSerializer):
    class Meta:
        model = Threshold
        fields = ('id','thresholdtype','thresholdid','count','current','status')

