from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required, permission_required
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
import logging

from inventory.search import Search
from ims.addons.tracker.models import EmailRecipient, PartThreshold, PartThresholdNotify, PartStorageThreshold, PartStorageThresholdNotify, ObjectAction, ObjectActionNotify, ReportNotify, KitThreshold, KitThresholdNotify
import ims.addons.tracker.helpers as helpers

logger = logging.getLogger(__name__)
from log.logger import getLogger
logger2 = getLogger('tracker')

def status_partthreshold_list(request):
    cname = 'status_partthreshold_list'
    logger.debug('Request made to {} view'.format(cname))
    logmsg = 'Accessed PartThreshold List'
    logger2.debug((request.user or 0), 'Request to view {}'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm('tracker.view_partthreshold'):
        # Get all PartThreshold
        logger.debug('Generating object_list of PartThreshold')
        object_list = PartThreshold.objects.all()

        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None: 
            logger.debug('Filtering object_list of PartThreshold with query')
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            querytype = request.GET.get('t')
            qt = helpers.parse_querytype(querytype)
            sf = Search().filter(helpers.get_search_fields('PartThreshold'), qt + query)
            object_list = object_list.filter(sf)

        # Create the Paginator and get the existing page
        logger.debug('Paginate object_list of PartThreshold')
        paginator = Paginator(object_list, settings.ADDON_TRACKER_THRESHOLD_PAGE_SIZE)
        page = request.GET.get('page')
        try:
            partthresholds = paginator.page(page)
        except PageNotAnInteger:
            logger.debug('PageNotAnInteger, show page 1')
            partthresholds = paginator.page(1)
        except EmptyPage:
            logger.debug('EmptyPage, show page ' + str(paginator.num_pages))
            partthresholds = paginator.page(paginator.num_pages)

        # Return the list of partthresholds
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return render(request, 'tracker/partthreshold/list.html', {'query': query, 'page': page, 'partthresholds': partthresholds})
    else:
        logger.debug('User was not authorized and ALLOW_ANONYMOUS is set to False')
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def status_partthreshold_detail(request, id):
    cname = 'status_partthreshold_detail'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view PartThreshold Details for id {}'.format(id))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm('tracker.view_partthreshold'):
        # Get the PartThreshold object to display in detail
        subscribed = False
        pt = get_object_or_404(PartThreshold, id=id)
        if request.user.is_authenticated:
            logger.debug('Checking if user is subscribed to "{}"'.format(pt.name))
            try:
                er = EmailRecipient.objects.filter(email=request.user.email).first()
                ptn = PartThresholdNotify.objects.filter(partthreshold=pt, emailrecipient=er).first()
                if ptn != None:
                    logger.debug('User is subscribed to "{}"'.format(pt.name))
                    subscribed = True
                else:
                    logger.debug('User is not subscribed to "{}"'.format(pt.name))
            except Exception as err:
                logger.debug('Problem checking for subscription')
                logger.error(err)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), 'Accessed PartThreshold id {}'.format(id))
        return render(request, 'tracker/partthreshold/detail.html', {'partthreshold': pt, 'subscribed': subscribed})
    else:
        logger.debug('User was not authorized and ALLOW_ANONYMOUS is set to False')
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def status_partstoragethreshold_list(request):
    cname = 'status_partstoragethreshold_list'
    logger.debug('Request made to {} view'.format(cname))
    logmsg = 'Accessed PartStorageThreshold List'
    logger2.debug((request.user or 0), 'Request to view {}'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm('tracker.view_partstoragethreshold'):
        # Get all PartStorageThreshold
        logger.debug('Generating object_list of PartStorageThreshold')
        object_list = PartStorageThreshold.objects.all()

        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None:
            logger.debug('Filtering object_list of PartStorageThreshold with query')
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            querytype = request.GET.get('t')
            qt = helpers.parse_querytype(querytype)
            sf = Search().filter(helpers.get_search_fields('PartStorageThreshold'), qt + query)
            object_list = object_list.filter(sf)

        # Create the Paginator and get the existing page
        logger.debug('Paginate object_list of PartStorageThreshold')
        paginator = Paginator(object_list, settings.ADDON_TRACKER_THRESHOLD_PAGE_SIZE)
        page = request.GET.get('page')
        try:
            partstoragethresholds = paginator.page(page)
        except PageNotAnInteger:
            logger.debug('PageNotAnInteger, show page 1')
            partstoragethresholds = paginator.page(1)
        except EmptyPage:
            logger.debug('EmptyPage, show page ' + str(paginator.num_pages))
            partstoragethresholds = paginator.page(paginator.num_pages)
        
        # Return the list of partstoragethreshold
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return render(request, 'tracker/partstoragethreshold/list.html', {'query': query, 'page': page, 'partstoragethresholds': partstoragethresholds})
    else:
        logger.debug('User was not authorized and ALLOW_ANONYMOUS is set to False')
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def status_partstoragethreshold_detail(request, id):
    cname = 'status_partstoragethreshold_detail'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view PartStorageThreshold Details for id {}'.format(id))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm('tracker.view_partstoragethreshold'):
        # Get the PartStorageThreshold object to display in detail
        subscribed = False
        pst = get_object_or_404(PartStorageThreshold, id=id)
        if request.user.is_authenticated:
            logger.debug('Checking if user is subscribed to "{}"'.format(pst.name))
            try:
                er = EmailRecipient.objects.filter(email=request.user.email).first()
                pstn = PartStorageThresholdNotify.objects.filter(partstoragethreshold=pst, emailrecipient=er).first()
                if pstn != None:
                    logger.debug('User is subscribed to "{}"'.format(pst.name))
                    subscribed = True
                else:
                    logger.debug('User is not subscribed to "{}"'.format(pst.name))
            except Exception as err:
                logger.debug('Problem checking for subscription')
                logger.error(err)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), 'Accessed PartStorageThreshold id {}'.format(id))
        return render(request, 'tracker/partstoragethreshold/detail.html', {'partstoragethreshold': pst, 'subscribed': subscribed})
    else:
        logger.debug('User was not authorized and ALLOW_ANONYMOUS is set to False')
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def status_kitthreshold_list(request):
    cname = 'status_kitthreshold_list'
    logger.debug('Request made to {} view'.format(cname))
    logmsg = 'Accessed KitThreshold List'
    logger2.debug((request.user or 0), 'Request to view {}'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm('tracker.view_kitthreshold'):
        # Get all KitThreshold
        logger.debug('Generating object_list of KitThreshold')
        object_list = KitThreshold.objects.all()

        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None:
            logger.debug('Filtering object_list of KitThreshold with query')
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            querytype = request.GET.get('t')
            qt = helpers.parse_querytype(querytype)
            sf = Search().filter(helpers.get_search_fields('KitThreshold'), qt + query)
            object_list = object_list.filter(sf)

        # Create the Paginator and get the existing page
        logger.debug('Paginate object_list of KitThreshold')
        paginator = Paginator(object_list, settings.ADDON_TRACKER_THRESHOLD_PAGE_SIZE)
        page = request.GET.get('page')
        try:
            kitthresholds = paginator.page(page)
        except PageNotAnInteger:
            logger.debug('PageNotAnInteger, show page 1')
            kitthresholds = paginator.page(1)
        except EmptyPage:
            logger.debug('EmptyPage, show page ' + str(paginator.num_pages))
            kitthresholds = paginator.page(paginator.num_pages)

        # Return the list of partthresholds
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return render(request, 'tracker/kitthreshold/list.html', {'query': query, 'page': page, 'kitthresholds': kitthresholds})
    else:
        logger.debug('User was not authorized and ALLOW_ANONYMOUS is set to False')
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def status_kitthreshold_detail(request, id):
    cname = 'status_kitthreshold_detail'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view KitThreshold Details for id {}'.format(id))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm('tracker.view_kitthreshold'):
        # Get the KitThreshold object to display in detail
        subscribed = False
        kt = get_object_or_404(KitThreshold, id=id)
        if request.user.is_authenticated:
            logger.debug('Checking if user is subscribed to "{}"'.format(kt.name))
            try:
                er = EmailRecipient.objects.filter(email=request.user.email).first()
                ktn = KitThresholdNotify.objects.filter(kitthreshold=kt, emailrecipient=er).first()
                if ktn != None:
                    logger.debug('User is subscribed to "{}"'.format(kt.name))
                    subscribed = True
                else:
                    logger.debug('User is not subscribed to "{}"'.format(kt.name))
            except Exception as err:
                logger.debug('Problem checking for subscription')
                logger.error(err)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), 'Accessed KitThreshold id {}'.format(id))
        return render(request, 'tracker/kitthreshold/detail.html', {'kitthreshold': kt, 'subscribed': subscribed})
    else:
        logger.debug('User was not authorized and ALLOW_ANONYMOUS is set to False')
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def status_objectaction_list(request):
    cname = 'status_objectaction_list'
    logger.debug('Request made to {} view'.format(cname))
    logmsg = 'Accessed ObjectAction List'
    logger2.debug((request.user or 0), 'Request to view {}'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm('tracker.view_objectaction'):
        # Get all KitThreshold
        logger.debug('Generating object_list of ObjectAction')
        object_list = ObjectAction.objects.all()

        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None:
            logger.debug('Filtering object_list of ObjectAction with query')
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            querytype = request.GET.get('t')
            qt = helpers.parse_querytype(querytype)
            sf = Search().filter(helpers.get_search_fields('ObjectAction'), qt + query)
            object_list = object_list.filter(sf)

        # Create the Paginator and get the existing page
        logger.debug('Paginate object_list of ObjectAction')
        paginator = Paginator(object_list, settings.ADDON_TRACKER_THRESHOLD_PAGE_SIZE)
        page = request.GET.get('page')
        try:
            objectactions = paginator.page(page)
        except PageNotAnInteger:
            logger.debug('PageNotAnInteger, show page 1')
            objectactions = paginator.page(1)
        except EmptyPage:
            logger.debug('EmptyPage, show page ' + str(paginator.num_pages))
            objectactions = paginator.page(paginator.num_pages)

        # Return the list of partthresholds
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return render(request, 'tracker/objectaction/list.html', {'query': query, 'page': page, 'objectactions': objectactions})
    else:
        logger.debug('User was not authorized and ALLOW_ANONYMOUS is set to False')
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def status_objectaction_detail(request, id):
    cname = 'status_objectaction_detail'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view ObjectAction Details for id {}'.format(id))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm('tracker.view_objectaction'):
        # Get the ObjectAction object to display in detail
        subscribed = False
        oa = get_object_or_404(ObjectAction, id=id)
        if request.user.is_authenticated:
            logger.debug('Checking if user is subscribed to "{}"'.format(oa.name))
            try:
                er = EmailRecipient.objects.filter(email=request.user.email).first()
                oan = ObjectActionNotify.objects.filter(objectaction=oa, emailrecipient=er).first()
                if oan != None:
                    logger.debug('User is subscribed to "{}"'.format(oa.name))
                    subscribed = True
                else:
                    logger.debug('User is not subscribed to "{}"'.format(oa.name))
            except Exception as err:
                logger.debug('Problem checking for subscription')
                logger.error(err)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), 'Accessed ObjectAction id {}'.format(id))
        return render(request, 'tracker/objectaction/detail.html', {'objectaction': oa, 'subscribed': subscribed})
    else:
        logger.debug('User was not authorized and ALLOW_ANONYMOUS is set to False')
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def home(request):
    cname = 'home'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view {}'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.is_authenticated:
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), 'Accessed Home view')
        return render(request, 'tracker/home.html')
    else:
        logger.debug('User was not authenticated and ALLOW_ANONYMOUS is set to False')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def emailrecipient_list(request):
    cname = 'emailrecipient_list'
    logger.debug('Request made to {} view'.format(cname))
    logmsg = 'Accessed EmailRecipient List'
    logger2.debug((request.user or 0), 'Request to view {}'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm('tracker.view_emailrecipient'):
        # Get all emailrecipient
        logger.debug('Generating object_list of EmailRecipient')
        object_list = EmailRecipient.objects.all()

        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None:
            logger.debug('Filtering object_list of EmailRecipient with query')
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            querytype = request.GET.get('t')
            qt = helpers.parse_querytype(querytype)
            sf = Search().filter(helpers.get_search_fields('EmailRecipient'), qt + query)
            object_list = object_list.filter(sf)

        # Create the Paginator and get the existing page
        logger.debug('Paginate object_list of EmailRecipient')
        paginator = Paginator(object_list, settings.ADDON_TRACKER_THRESHOLD_PAGE_SIZE)
        page = request.GET.get('page')
        try:
            emailrecipients = paginator.page(page)
        except PageNotAnInteger:
            logger.debug('PageNotAnInteger, show page 1')
            emailrecipients = paginator.page(1)
        except EmptyPage:
            logger.debug('EmptyPage, show page ' + str(paginator.num_pages))
            emailrecipients = paginator.page(paginator.num_pages)

        # Return the list of emailrecipients
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), logmsg)
        return render(request, 'tracker/emailrecipient/list.html', {'query': query, 'page': page, 'emailrecipients': emailrecipients})
    else:
        logger.debug('User was not authorized and ALLOW_ANONYMOUS is set to False')
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def emailrecipient_detail(request, id):
    cname = 'emailrecipient_detail'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view EmailRecipient Details for id {}'.format(id))
    if settings.ALLOW_ANONYMOUS or request.user.has_perm('tracker.view_emailrecipient'):
        # Get the EmailRecipient object to display in detail
        er = get_object_or_404(EmailRecipient, id=id)
        ptn = PartThresholdNotify.objects.filter(emailrecipient=er)
        pstn = PartStorageThresholdNotify.objects.filter(emailrecipient=er)
        ktn = KitThresholdNotify.objects.filter(emailrecipient=er)
        oan = ObjectActionNotify.objects.filter(emailrecipient=er)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), 'Accessed EmailRecipient id {}'.format(id))
        return render(request, 'tracker/emailrecipient/detail.html', {'emailrecipient': er, 'partthresholdnotify': ptn, 'partstoragethresholdnotify': pstn, 'kitthresholdnotify': ktn, 'objectactionnotify': oan})
    else:
        logger.debug('User was not authorized and ALLOW_ANONYMOUS is set to False')
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def emailtemplate_list(request):
    return render(request, 'tracker/emailtemplate/list.html')

def emailtemplate_detail(request, id):
    return render(request, 'tracker/emailtemplate/detail.html')

def ajax_partthreshold_subscribe(request, id):
    return ajax_subscribe(request, id, 'ajax_partthreshold_subscribe', PartThreshold, PartThresholdNotify, 'tracker.view_partthreshold')

def ajax_partthreshold_unsubscribe(request, id):
    return ajax_unsubscribe(request, id, 'ajax_partthreshold_unsubscribe', PartThreshold, PartThresholdNotify, 'tracker.view_partthreshold')

def ajax_partstoragethreshold_subscribe(request, id):
    return ajax_subscribe(request, id, 'ajax_partstoragethreshold_subscribe', PartStorageThreshold, PartStorageThresholdNotify, 'tracker.view_partstoragethreshold')

def ajax_partstoragethreshold_unsubscribe(request, id):
    return ajax_unsubscribe(request, id, 'ajax_partstoragethreshold_unsubscribe', PartStorageThreshold, PartStorageThresholdNotify, 'tracker.view_partstoragethreshold')

def ajax_kitthreshold_subscribe(request, id):
    return ajax_subscribe(request, id, 'ajax_kitthreshold_subscribe', KitThreshold, KitThresholdNotify, 'tracker.view_kitthreshold')

def ajax_kitthreshold_unsubscribe(request, id):
    return ajax_unsubscribe(request, id, 'ajax_kitthreshold_unsubscribe', KitThreshold, KitThresholdNotify, 'tracker.view_kitthreshold')

def ajax_objectaction_subscribe(request, id):
    return ajax_subscribe(request, id, 'ajax_objectaction_subscribe', ObjectAction, ObjectActionNotify, 'tracker.view_objectaction')

def ajax_objectaction_unsubscribe(request, id):
    return ajax_unsubscribe(request, id, 'ajax_objectaction_unsubscribe', ObjectAction, ObjectActionNotify, 'tracker.view_objectaction')

def ajax_subscribe(request, id, cname, model, notify, permission):
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view {}'.format(cname))
    if request.user.has_perm(permission):
        logger.debug('User is authenticated, checking for objects')
        obj = get_object_or_404(model, id=id)
        er = EmailRecipient.objects.filter(email=request.user.email).first()
        if er == None:
            logger.debug('User does not have a EmailRecipient object')
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.debug(request.user, 'No matching EmailRecipient found for the User')
            return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'No matching EmailRecipient found.'})
        objnotify = _get_objnotify(notify, obj, er)
        if objnotify == None:
            logger.info('Creating new {} "{} to {}"'.format(notify.__name__, obj.name, er.email))
            _create_objnotify(notify, obj, er)
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info(request.user, 'Subscribed to {} id {}'.format(model.__name__, id))
            return render(request, 'tracker/ajax_result.html', {'color': 'green', 'result': 'Successfully Subscribed to {}.'.format(obj.name)})
        else:
            logger.debug('{} for "{} to {}" already exists'.format(notify.__name__, obj.name, er.email))
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.debug(request.user, 'User already subscribed to {} id {}'.format(model.__name__, id))
            return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'Subscription already exists.'})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
    return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'Unable to create Notification. Access is denied'})

def ajax_unsubscribe(request, id, cname, model, notify, permission):
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view {}'.format(cname))
    if request.user.has_perm(permission):
        logger.debug('User is authenticated, checking for objects')
        obj = get_object_or_404(model, id=id)
        er = EmailRecipient.objects.filter(email=request.user.email).first()
        if er == None:
            logger.debug('User does not have a EmailRecipient object')
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.debug(request.user, 'No matching EmailRecipient found for the User')
            return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'No matching EmailRecipient found.'})
        objnotify = _get_objnotify(notify, obj, er)
        if objnotify == None:
            logger.debug('{} for "{} to {}" does not exist'.format(notify.__name__, obj.name, er.email))
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.debug(request.user, 'User not subscribed to {} id {}'.format(model.__name__, id))
            return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'Subscription does not exist.'})
        else:
            logger.info('Deleting {} "{} to {}"'.format(notify.__name__, obj.name, er.email))
            objnotify.delete()
            logger2.info(request.user, 'Unsubscribed from {} id {}'.format(model.__name__, id))
            logger.debug('Rendering request for {} view'.format(cname))
            return render(request, 'tracker/ajax_result.html', {'color': 'green', 'result': 'Successfully Unsubscribed from {}.'.format(obj.name)})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
    return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'Unable to delete Notification. Access is denied'})

def ajax_partthresholdnotify_delete(request, id):
    return ajax_notify_delete(request, id, 'ajax_partthresholdnotify_delete', PartThresholdNotify, 'tracker.delete_partthresholdnotify')

def ajax_partstoragethresholdnotify_delete(request, id):
    return ajax_notify_delete(request, id, 'ajax_partstoragethresholdnotify_delete', PartStorageThresholdNotify, 'tracker.delete_partstoragethresholdnotify')

def ajax_objectactionnotify_delete(request, id):
    return ajax_notify_delete(request, id, 'ajax_objectactionnotify_delete', ObjectActionNotify, 'tracker.delete_objectactionnotify')

def ajax_kitthresholdnotify_delete(request, id):
    return ajax_notify_delete(request, id, 'ajax_kitthresholdnotify_delete', KitThresholdNotify, 'tracker.delete_kitthresholdnotify')

def ajax_notify_delete(request, id, cname, model, permission):
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug((request.user or 0), 'Request to view {}'.format(cname))
    if request.user.is_authenticated:
        obj = get_object_or_404(model, id=id)
        if request.user.email == obj.emailrecipient.email or request.user.has_perm(permission):
            logger.debug('Requestor is either the recipient or has permission to delete object')
            logger.info('Deleting {} with id {}'.format(model.__name__, id))
            color = 'green'
            message = 'KitThreshold Notification {} removed.'.format(obj)
            try:
                obj.delete()
                logger2.info(request.user, 'Deleted KitThresholdNotify id {}'.format(id))
            except Exception as err:
                message = 'There was a problem trying to remove KitThreshold Notification {}.'.format(obj)
                color = 'red'
                logger.error(err)
                logger2.warning(request.user, err)
            logger.debug('Rendering request for {} view'.format(cname))
            return render(request, 'tracker/ajax_result.html', {'color': color, 'result': message})
        logger2.warning(request.user, 'Unauthorized request to {}'.format(request.path))
        return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'Unable to remove Notification. Access is denied'})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
    return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'Unable to remove Notification. Access is denied'})

@login_required()
def ajax_profile(request):
    cname = 'ajax_profile'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.user.email != None:
        logger.debug('Gathering notify objects for user')
        er = EmailRecipient.objects.filter(email=request.user.email).first()
        ptn = PartThresholdNotify.objects.filter(emailrecipient=er)
        pstn = PartStorageThresholdNotify.objects.filter(emailrecipient=er)
        ktn = KitThresholdNotify.objects.filter(emailrecipient=er)
        oan = ObjectActionNotify.objects.filter(emailrecipient=er)
        rn = ReportNotify.objects.filter(emailrecipient=er)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed User Subscriptions')
        return render(request, 'tracker/ajax_profile.html', {'partthresholdnotify': ptn, 'partstoragethresholdnotify': pstn, 'kitthresholdnotify': ktn, 'objectactionnotify': oan, 'reportnotify': rn})
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, 'Accessed User Subscriptions')
    return render(request, 'tracker/ajax_profile.html')

def _get_objnotify(notify, obj, er):
    if notify.__name__ == 'ObjectActionNotify':
        return ObjectActionNotify.objects.filter(objectaction=obj, emailrecipient=er).first()
    if notify.__name__ == 'PartThresholdNotify':
        return PartThresholdNotify.objects.filter(partthreshold=obj, emailrecipient=er).first()
    if notify.__name__ == 'PartStorageThresholdNotify':
        return PartStorageThresholdNotify.objects.filter(partstoragethreshold=obj, emailrecipient=er).first()
    if notify.__name__ == 'KitThresholdNotify':
        return KitThresholdNotify.objects.filter(kitthreshold=obj, emailrecipient=er).first()
    
def _create_objnotify(notify, obj, er):
    if notify.__name__ == 'ObjectActionNotify':
        return ObjectActionNotify.objects.get_or_create(objectaction=obj, emailrecipient=er)[0]
    if notify.__name__ == 'PartThresholdNotify':
        return PartThresholdNotify.objects.get_or_create(partthreshold=obj, emailrecipient=er)[0]
    if notify.__name__ == 'PartStorageThresholdNotify':
        return PartStorageThresholdNotify.objects.get_or_create(partstoragethreshold=obj, emailrecipient=er)[0]
    if notify.__name__ == 'KitThresholdNotify':
        return KitThresholdNotify.objects.get_or_create(kitthreshold=obj, emailrecipient=er)[0]
