from datetime import datetime, timedelta
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib.auth.decorators import login_required, permission_required
from django.contrib.auth.models import User
from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, get_object_or_404, redirect
from django_celery_beat.models import ClockedSchedule, CrontabSchedule, IntervalSchedule, PeriodicTask, SolarSchedule, MINUTES, HOURS, now
from pytz import timezone, utc
import json
import logging

from ims.addons.tracker.forms import ClockedScheduleForm, CronScheduleForm, GraphSelectForm, IntervalScheduleForm, KitMultiSelectForm, PartMultiSelectForm, ModelSelectForm, SolarScheduleForm, StorageMultiSelectForm, ReportSchedulerForm, TimeSpanForm, UserSelectForm, MultipurposeSelectForm 
from ims.addons.tracker.models import EmailRecipient, ReportNotify
from inventory.models import Part, Storage, PartStorage, AlternateSKU, PartAlternateSKU, Assembly, Kit, KitPartStorage
from inventory.search import Search
from log.logger import getLogger
import ims.addons.tracker.helpers as helpers

logger = logging.getLogger(__name__)
logger2 = getLogger('tracker')

def reporting_home(request):
    cname = 'reporting_home'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.is_authenticated:
        logger2.debug((request.user or 0), 'Request made to {} view'.format(cname))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info((request.user or 0), 'Accessed {}'.format(cname))
        return render(request, 'tracker/reporting/home.html')
    else:
        logger.debug('User was not authenticated')
        logger2.warning(0, 'Unauthenticated request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def reporting_recent_adds(request):
    cname = 'reporting_recent_adds'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm('tracker.can_view_recentadd'):
        logger2.debug((request.user or 0), 'Request made to {} view'.format(cname))
        model_form = ModelSelectForm(data=request.GET)
        timespan_form = TimeSpanForm(data=request.GET)
        if model_form.is_valid() and timespan_form.is_valid():
            logger.debug('Selecting specified models: {}'.format(model_form.cleaned_data['id']))
            objecttypes = request.GET.getlist('id')
            i = request.GET.get('duration')
            span = request.GET.get('span')
            results = helpers.recent_adds_list((request.user or 0), objecttypes, i, span)
            logger.info('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {} results for previous {}{} for objects {}'.format(cname,i,span,objecttypes))
            return render(request, 'tracker/reporting/recent/adds-mods.html', {'title': 'Recently Added', 'model_form': model_form, 'timespan_form': timespan_form, 'results': results})
        else:
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {}'.format(cname))
            model_form = ModelSelectForm()
            timespan_form = TimeSpanForm()
            return render(request, 'tracker/reporting/recent/adds-mods.html', {'title': 'Recently Added', 'model_form': model_form, 'timespan_form': timespan_form})
    else:
        logger.debug('User was not authorized for view {}'.format(cname))
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def reporting_recent_mods(request):
    cname = 'reporting_recent_mods'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm('tracker.can_view_recentmod'):
        logger2.debug((request.user or 0), 'Request made to {} view'.format(cname))
        model_form = ModelSelectForm(data=request.GET)
        timespan_form = TimeSpanForm(data=request.GET)
        if model_form.is_valid() and timespan_form.is_valid():
            logger.debug('Selecting specified models: {}'.format(model_form.cleaned_data['id']))
            objecttypes = request.GET.getlist('id')
            i = request.GET.get('duration')
            span = request.GET.get('span')
            results = helpers.recent_mods_list((request.user or 0), objecttypes, i, span)
            logger.info('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {} results for previous {}{} for objects {}'.format(cname,i,span,objecttypes))
            return render(request, 'tracker/reporting/recent/adds-mods.html', {'title': 'Recently Modified', 'model_form': model_form, 'timespan_form': timespan_form, 'results': results})
        else:
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {}'.format(cname))
            model_form = ModelSelectForm()
            timespan_form = TimeSpanForm()
            return render(request, 'tracker/reporting/recent/adds-mods.html', {'title': 'Recently Modified', 'model_form': model_form, 'timespan_form': timespan_form})
    else:
        logger.debug('User was not authorized for view {}'.format(cname))
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def reporting_recent_user(request):
    cname = 'reporting_recent_user'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm('tracker.can_view_useract'):
        logger2.debug((request.user or 0), 'Request made to {} view'.format(cname))
        user_form = UserSelectForm(data=request.GET)
        timespan_form = TimeSpanForm(data=request.GET)
        if user_form.is_valid() and timespan_form.is_valid():
            logger.debug('Selecting specified User: {}'.format(user_form.cleaned_data['user']))
            user = request.GET.get('user')
            i = request.GET.get('duration')
            span = request.GET.get('span')
            results = helpers.recent_user_list((request.user or 0), user, i, span)
            logger.info('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {} results for previous {}{} for objects {}'.format(cname,i,span,user))
            return render(request, 'tracker/reporting/recent/user-act.html', {'user_form': user_form, 'timespan_form': timespan_form, 'results': results})
        else:
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {}'.format(cname))
            user_form = UserSelectForm()
            timespan_form = TimeSpanForm()
            return render(request, 'tracker/reporting/recent/user-act.html', {'user_form': user_form, 'timespan_form': timespan_form})
    else:
        logger.debug('User was not authorized for view {}'.format(cname))
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def reporting_counts_part(request):
    cname = 'reporting_counts_part'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm('tracker.can_view_partcount'):
        logger2.debug((request.user or 0), 'Request made to {} view'.format(cname))
        parts_form = PartMultiSelectForm(data=request.GET)
        if parts_form.is_valid():
            logger.debug('Selecting specified Parts: {}'.format(parts_form.cleaned_data['part']))
            results = helpers.part_counts((request.user or 0), parts_form.cleaned_data['part'])
            logger.info('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {} results for Part ids {}'.format(cname,parts_form.cleaned_data['part']))
            return render(request, 'tracker/reporting/count/parts.html', {'parts_form': parts_form, 'results': results})
        else:
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {}'.format(cname))
            parts_form = PartMultiSelectForm()
            return render(request, 'tracker/reporting/count/parts.html', {'parts_form': parts_form})
    else:
        logger.debug('User was not authorized for view {}'.format(cname))
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def reporting_counts_kit(request):
    cname = 'reporting_counts_kit'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm('tracker.can_view_kitcount'):
        logger2.debug((request.user or 0), 'Request made to {} view'.format(cname))
        kits_form = KitMultiSelectForm(data=request.GET)
        if kits_form.is_valid():
            logger.debug('Selecting specified Parts: {}'.format(kits_form.cleaned_data['kit']))
            results = helpers.kit_counts((request.user or 0), kits_form.cleaned_data['kit'])
            logger.info('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {} results for Kit ids {}'.format(cname,kits_form.cleaned_data['kit']))
            return render(request, 'tracker/reporting/count/kits.html', {'kits_form': kits_form, 'results': results})
        else:
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {}'.format(cname))
            kits_form = KitMultiSelectForm()
            return render(request, 'tracker/reporting/count/kits.html', {'kits_form': kits_form})
    else:
        logger.debug('User was not authorized for view {}'.format(cname))
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def reporting_counts_storage(request):
    cname = 'reporting_counts_storage'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm('tracker.can_view_contents'):
        logger2.debug((request.user or 0), 'Request made to {} view'.format(cname))
        select_form = MultipurposeSelectForm(Storage,data=request.GET)
        if select_form.is_valid():
            logger.debug('Selecting specified Parts: {}'.format(select_form.cleaned_data['id']))
            results = helpers.storage_counts((request.user or 0), select_form.cleaned_data['id'])
            logger.info('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {} results for Storage id {}'.format(cname,select_form.cleaned_data['id']))
            return render(request, 'tracker/reporting/count/storage.html', {'select_form': select_form, 'results': results})
        else:
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info((request.user or 0), 'Accessed {}'.format(cname))
            select_form = MultipurposeSelectForm(Storage)
            return render(request, 'tracker/reporting/count/storage.html', {'select_form': select_form})
    else:
        logger.debug('User was not authorized for view {}'.format(cname))
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)

def reporting_graph(request):
    cname = 'reporting_graph'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm('tracker.can_view_graph'):
        logger2.debug(request.user, 'Request made to {} view'.format(cname))
        model_form = GraphSelectForm(data=request.GET)
        timespan_form = TimeSpanForm(data=request.GET)
        if timespan_form.is_valid():
            graphtype = request.GET.get('graph')
            i = request.GET.get('duration')
            span = request.GET.get('span')
            graph = obj = ''
            try:
                if graphtype == 'hpcl':
                    id = request.GET.get('id')
                    logger.debug('Selecting specified model: Part id {}'.format(id))
                    obj = helpers.get_object('p', id)
                    graph = helpers.historic_part_count_graph(request.user,obj,i,span)
                if graphtype == 'hpscl':
                    id = request.GET.get('id')
                    logger.debug('Selecting specified model: PartStorage id {}'.format(id))
                    obj = helpers.get_object('ps', id)
                    graph = helpers.historic_partstorage_count_graph(request.user,obj,i,span)
                if graphtype == 'scb':
                    id = request.GET.get('id')
                    logger.debug('Selecting specified model: Storage id {}'.format(id))
                    obj = helpers.get_object('s', id)
                    graph = helpers.storage_counts_graph(request.user,obj)
                if graphtype == 'hkcl':
                    id = request.GET.get('id')
                    logger.debug('Selecting specified model: Kit id {}'.format(id))
                    obj = helpers.get_object('k', id)
                    graph = helpers.historic_kit_count_graph(request.user,obj,i,span)
                if graphtype == 'kcb':
                    id = request.GET.getlist('id')
                    logger.debug('Selecting specified model: Kit id {}'.format(id))
                    objs = []
                    for i in id:
                        obj = helpers.get_object('k', i)
                        objs.append(obj)
                    if objs == []:
                        logger.error('There was an error retrieving the selected object. Error: {}'.format(err))
                        logger2.warning(request.user, 'Error retrieving selected object in method {}: {}'.format(cname, err))
                        return render(request, 'tracker/reporting/graph.html', {'model_form': model_form, 'timespan_form': timespan_form})
                    graph = helpers.kit_counts_graph((request.user or 0), objs)
                if graphtype == 'pcb':
                    id = request.GET.getlist('id')
                    logger.debug('Selecting specified model: Part id {}'.format(id))
                    objs = []
                    for i in id:
                        obj = helpers.get_object('p', i)
                        objs.append(obj)
                    if objs == []:
                        logger.error('There was an error retrieving the selected object. Error: {}'.format(err))
                        logger2.warning(request.user, 'Error retrieving selected object in method {}: {}'.format(cname, err))
                        return render(request, 'tracker/reporting/graph.html', {'model_form': model_form, 'timespan_form': timespan_form})
                    graph = helpers.part_counts_graph(request.user, objs)
            except Exception as err:
                logger.error('There was an error retrieving the selected object. Error: {}'.format(err))
                logger2.warning(request.user, 'Error retrieving selected object in method {}: {}'.format(cname, err))
                return render(request, 'tracker/reporting/graph.html', {'model_form': model_form, 'timespan_form': timespan_form})
            model_form = GraphSelectForm()
            timespan_form = TimeSpanForm()
            alt = 'Historic Counts for {} {} spanning {} {}'.format(obj.__class__.__name__,obj, i, helpers.get_timespan_type(span))
            return render(request, 'tracker/reporting/graph.html', {'model_form': model_form, 'timespan_form': timespan_form, 'graph': graph, 'alt': alt})
        else:
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info(request.user, 'Accessed {}'.format(cname))
            model_form = GraphSelectForm()
            timespan_form = TimeSpanForm()
            return render(request, 'tracker/reporting/graph.html', {'model_form': model_form, 'timespan_form': timespan_form})
    else:
        logger.debug('User was not authorized for view {}'.format(cname))
        logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
        return redirect('/accounts/login/?next=' + request.path)
    
def ajax_graph_models(request, model):
    cname = 'ajax_graph_models'
    logger.debug('Request made to {} view'.format(cname))
    items = ''
    if request.user.is_authenticated:
        logger2.debug(request.user, 'Request to {} view for list of {}'.format(cname, model))
        if model == 'p':
            logger2.error(request.user, 'WHY IS THIS BEING CALLED: ajax_graph_models = p')
            logger.debug('Generating a list of Part objects')
            items = Part.objects.all()
        if model == 'hpcl' and request.user.has_perm('tracker.can_view_histpart'):
            logger.debug('Generating a list of Part objects')
            items = Part.objects.all()
        if model == 'pcb' and request.user.has_perm('tracker.can_view_partcount'):
            logger.debug('Generating a list of Part objects')
            items = Part.objects.all()
        if model == 's':
            logger2.error(request.user, 'WHY IS THIS BEING CALLED: ajax_graph_models = s')
            logger.debug('Generating a list of Storage objects')
            items = Storage.objects.all()
        if model == 'scb' and request.user.has_perm('tracker.can_view_contents'):
            logger.debug('Generating a list of Storage objects')
            items = Storage.objects.all()
        if model == 'k':
            logger2.error(request.user, 'WHY IS THIS BEING CALLED: ajax_graph_models = k')
            logger.debug('Generating a list of Kit objects')
            items = Kit.objects.all()
        if model == 'hkcl' and request.user.has_perm('tracker.can_view_histkit'):
            logger.debug('Generating a list of Kit objects')
            items = Kit.objects.all()
        if model == 'kcb' and request.user.has_perm('tracker.can_view_kitcount'):
            logger.debug('Generating a list of Kit objects')
            items = Kit.objects.all()
        if model == 'ps':
            logger2.error(request.user, 'WHY IS THIS BEING CALLED: ajax_graph_models = ps')
            logger.debug('Generating a list of PartStorage objects')
            items = PartStorage.objects.all()
        if model == 'hpscl' and request.user.has_perm('tracker.can_view_histps'):
            logger.debug('Generating a list of PartStorage objects')
            items = PartStorage.objects.all()
        if model == 'as':
            logger2.error(request.user, 'WHY IS THIS BEING CALLED: ajax_graph_models = as')
            logger.debug('Generating a list of AlternateSKU objects')
            items = AlternateSKU.objects.all()
        if model == 'pa':
            logger2.error(request.user, 'WHY IS THIS BEING CALLED: ajax_graph_models = pa')
            logger.debug('Generating a list of PartAlternateSKU objects')
            items = PartAlternateSKU.objects.all()
        if model == 'a':
            logger2.error(request.user, 'WHY IS THIS BEING CALLED: ajax_graph_models = a')
            logger.debug('Generating a list of Assembly objects')
            items = Assembly.objects.all()
        if model == 'hacl' and request.user.has_perm('tracker.can_view_histpart'):
            logger.debug('Generating a list of Assembly objects')
            items = Assembly.objects.all()
        if model == 'kps':
            logger2.error(request.user, 'WHY IS THIS BEING CALLED: ajax_graph_models = kps')
            logger.debug('Generating a list of KitPartStorage objects')
            items = KitPartStorage.objects.all()
        logger.debug('Rendering request for ajax view')
        logger2.info(request.user, 'Accessed list of {}'.format(model))
        return render(request, 'tracker/dropdown_list_options.html', {'items': items})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
    return render(request, 'tracker/dropdown_list_options.html', {'items': items})

def ajax_report_dropdown_options(request, view):
    cname = 'ajax_report_dropdown_options'
    logger.debug('Request made to {} view'.format(cname))
    items = ''
    if request.user.has_perm('tracker.can_view_recentadd') or request.user.has_perm('tracker.can_view_recentmod') or request.user.has_perm('tracker.can_view_useract'):
        logger2.debug((request.user or 0), 'Request to {} view for {} options'.format(cname, view))
        if (view == 'recent_adds_list' and request.user.has_perm('tracker.can_view_recentadd')) or (view == 'recent_mods_list' and request.user.has_perm('tracker.can_view_recentmod')):
            logger.debug('Generating a list of object types')
            items = []
            for obj in helpers.MODEL_CHOICES:
                items.append({'id':obj[0],'name':obj[1]})
        if view == 'recent_user_list' and request.user.has_perm('tracker.can_view_useract'):
            logger.debug('Generating a list of User objects')
            items = User.objects.all()
        logger.debug('Rendering request for ajax view')
        logger2.info(request.user, 'Accessed options for {}'.format(view))
        return render(request, 'tracker/dropdown_list_options.html', {'items': items})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
    return render(request, 'tracker/dropdown_list_options.html', {'items': items})

""" # This is path forward with generating graphs and will convert them all in v1.3
def ajax_get_historic_part_count(request, id):
    cname = 'ajax_get_historic_part_count'
    logger.debug('Request made to {} view'.format(cname))
    if settings.ALLOW_ANONYMOUS or request.user.is_authenticated:
        logger2.debug((request.user or 0), 'Request made to {} view'.format(cname))
        logger.debug('Selecting specified Part: id {}'.format(id))
        start = request.GET.get('s')
        end = request.GET.get('e')
        try:
            obj = get_object_or_404(Part, id=id)
        except Exception as err:
            logger.error('There was an error retrieving the selected object. Error: {}'.format(err))
            logger2.warning((request.user or 0), 'Error retrieving selected object in method {}: {}'.format(cname, err))
            return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to access image. Object not found'})
        graph = historic_part_count_graph((request.user or 0),obj,start,end)
        return render(request, 'inventory/ajax_request.html', {'color': 'green', 'result': graph})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
    return render(request, 'inventory/ajax_result.html', {'color': 'red', 'result': 'Unable to access image. Access is denied'})
 """

def reporting_scheduler(request):
    cname = 'reporting_scheduler'
    logger.debug('Request made to {} view'.format(cname))
    if request.user.has_perm('tracker.can_view_schdreport'):
        logger.debug('Generating object_list of PeriodicTask')
        logmsg = 'Accessed Scheduled Reports List'
        object_list = PeriodicTask.objects.filter(task='scheduled_report')
        # Create the Search filter and apply to the existing results
        query = request.GET.get('q')
        if query != None:
            logger.debug('Filtering object_list of PeriodicTask with query')
            logmsg = '{} with query \'{}\''.format(logmsg, query)
            querytype = request.GET.get('t')
            qt = helpers.parse_querytype(querytype)
            sf = Search().filter(['name', 'kwargs'], qt + query)
            object_list = object_list.filter(sf)

        # Create the Paginator and get the existing page
        logger.debug('Paginate object_list of PeriodicTask')
        paginator = Paginator(object_list, settings.ADDON_TRACKER_THRESHOLD_PAGE_SIZE)
        page = request.GET.get('page')
        try:
            tasks = paginator.page(page)
        except PageNotAnInteger:
            logger.debug('PageNotAnInteger, show page 1')
            tasks = paginator.page(1)
        except EmptyPage:
            logger.debug('EmptyPage, show page {}'.format(paginator.num_pages))
            tasks = paginator.page(paginator.num_pages)

        # Return the list of tasks
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, logmsg)
        return render(request, 'tracker/reporting/scheduler/list.html', {'query': query, 'page': page, 'tasks': tasks})
    logger.debug('Access was denied to {} view'.format(cname))
    logger2.warning((request.user or 0), 'Unauthorized request to {}'.format(request.path))
    return redirect('/accounts/login/?next={}'.format(request.path))

@permission_required('tracker.can_add_schdreport')
def reporting_scheduler_create(request):
    cname = 'reporting_scheduler_create'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        reportform = ReportSchedulerForm(request.POST)
        solarform = SolarScheduleForm(request.POST)
        cronform = CronScheduleForm(request.POST)
        intervalfrom = IntervalScheduleForm(request.POST)
        clockform = ClockedScheduleForm(request.POST)
        modelform = GraphSelectForm(request.POST)
        timespanform = TimeSpanForm(request.POST)
        if reportform.is_valid():
            logger.debug('ReportSchedulerForm is valid.')
            logger.info('Creating new Scheduled Report "{}"'.format(reportform.cleaned_data['name']))
            task = ''
            kwargs = '{'
            kwargs += '"report":"{}",'.format(reportform.cleaned_data['report'])
            ids = request.POST.getlist('id')
            kwargs += '"ids":{},'.format(ids)
            if reportform.cleaned_data['report'] in helpers.REPORT_LIST_TIMESPAN_FIELD:
                if timespanform.is_valid():
                    kwargs += '"duration":"{}",'.format(timespanform.cleaned_data['duration'])
                    kwargs += '"span":"{}",'.format(timespanform.cleaned_data['span'])
            kwargs += '"subject":"{}",'.format(reportform.cleaned_data['name'])
            kwargs = kwargs.strip(',')
            kwargs += '}'
            kwargs = kwargs.replace("'",'"')
            if reportform.cleaned_data['schedule'] == 'c':
                if clockform.is_valid() and clockform.cleaned_data['date'] != None and clockform.cleaned_data['time'] != None and clockform.cleaned_data['timezone'] != None :
                    logger.debug('Getting or creating ClockedSchedule; clocked_time: {}, clocked_date: {}, timezone: {}'.format(clockform.cleaned_data['time'], clockform.cleaned_data['date'], clockform.cleaned_data['timezone']))
                    try:
                        tzinfo = timezone(clockform.cleaned_data['timezone'])
                    except:
                        tzinfo = utc
                    date_time = datetime.combine(clockform.cleaned_data['date'],clockform.cleaned_data['time'],tzinfo=tzinfo)
                    schedule = ClockedSchedule.objects.get_or_create(clocked_time=date_time)[0]
                    task = PeriodicTask.objects.create(enabled=True,name=reportform.cleaned_data['name'],description=reportform.cleaned_data['description'],clocked=schedule,task='scheduled_report',start_time=now(),one_off=True,kwargs=kwargs)
                else:
                    logger.debug('Clocked Form invalid. Rendering request for {} view'.format(cname))
                    logger2.warning(request.user, 'Create Scheduled Report Clocked Form is not valid')
                    return render(request, 'tracker/reporting/scheduler/create.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': modelform, 'timespanform': timespanform, 'message': 'Schedule Form invalid.'})
            if reportform.cleaned_data['schedule'] == 't':
                if cronform.is_valid() and cronform.cleaned_data['minute'] != '' and cronform.cleaned_data['hour'] != '' and cronform.cleaned_data['day'] != '' and cronform.cleaned_data['month'] != '' and cronform.cleaned_data['day_of_week'] != '' and cronform.cleaned_data['timezone'] != '':
                    logger.debug('Getting or creating CrontabSchedule; minute: {}, hour: {}, days: {}, months: {}, day_of_week: {}'.format(cronform.cleaned_data['minute'], cronform.cleaned_data['hour'], cronform.cleaned_data['day'], cronform.cleaned_data['month'], cronform.cleaned_data['day_of_week']))
                    try:
                        tzinfo = timezone(clockform.cleaned_data['timezone'])
                    except:
                        tzinfo = utc
                    schedule = CrontabSchedule.objects.get_or_create(minute=cronform.cleaned_data['minute'],hour=cronform.cleaned_data['hour'],day_of_month=cronform.cleaned_data['day'],month_of_year=cronform.cleaned_data['month'],day_of_week=cronform.cleaned_data['day_of_week'],timezone=tzinfo)[0]
                    task = PeriodicTask.objects.create(enabled=True,name=reportform.cleaned_data['name'],description=reportform.cleaned_data['description'],crontab=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs)
                else:
                    logger.debug('Crontab Form invalid. Rendering request for {} view'.format(cname))
                    logger2.warning(request.user, 'Create Scheduled Report Crontab Form is not valid')
                    return render(request, 'tracker/reporting/scheduler/create.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': modelform, 'timespanform': timespanform, 'message': 'Schedule Form invalid.'})
            if reportform.cleaned_data['schedule'] == 's':
                if solarform.is_valid() and solarform.cleaned_data['event'] != None and solarform.cleaned_data['latitude'] != None and solarform.cleaned_data['longitude'] != None:
                    logger.debug('Getting or creating SolarSchedule; event: {}, latitude: {}, longitude: {}'.format(solarform.cleaned_data['event'], solarform.cleaned_data['latitude'], solarform.cleaned_data['longitude']))
                    event = helpers.get_solar_type(solarform.cleaned_data['event']).lower()
                    schedule = SolarSchedule.objects.get_or_create(event=event,latitude=solarform.cleaned_data['latitude'],longitude=solarform.cleaned_data['longitude'])[0]
                    task = PeriodicTask.objects.create(enabled=True,name=reportform.cleaned_data['name'],description=reportform.cleaned_data['description'],solar=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs)
                else:
                    logger.debug('Solar Form invalid. Rendering request for {} view'.format(cname))
                    logger2.warning(request.user, 'Create Scheduled Report Solar Form is not valid')
                    return render(request, 'tracker/reporting/scheduler/create.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': modelform, 'timespanform': timespanform, 'message': 'Schedule Form invalid.'})
            if reportform.cleaned_data['schedule'] == 'i':
                if intervalfrom.is_valid() and intervalfrom.cleaned_data['number'] != None and intervalfrom.cleaned_data['period'] != None:
                    logger.debug('Getting or creating IntervalSchedule; every: {}, period: {}'.format(intervalfrom.cleaned_data['number'], intervalfrom.cleaned_data['period']))
                    period = helpers.get_timespan_type(intervalfrom.cleaned_data['period']).lower()
                    number = intervalfrom.cleaned_data['number']
                    if period == 'weeks':
                        period = 'days'
                        number *= 7
                    schedule = IntervalSchedule.objects.get_or_create(every=number,period=period)[0]
                    task = PeriodicTask.objects.create(enabled=True,name=reportform.cleaned_data['name'],description=reportform.cleaned_data['description'],interval=schedule,task='scheduled_report',start_time=now(),kwargs=kwargs)
                else:
                    logger.debug('Interval Form invalid. Rendering request for {} view'.format(cname))
                    logger2.warning(request.user, 'Create Scheduled Report Interval Form is not valid')
                    return render(request, 'tracker/reporting/scheduler/create.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': modelform, 'timespanform': timespanform, 'message': 'Schedule Form invalid.'})
            logger.info('Created new Scheduled Report id {}'.format(task.id))
            logger2.info(request.user, 'Created PeriodicTask id {}'.format(task.id))
            return redirect('/tracker/reporting/scheduler/')
        logger.debug('Form(s) invalid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Create Scheduled Report Forms not valid')
        return render(request, 'tracker/reporting/scheduler/create.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': modelform, 'timespanform': timespanform, 'message': 'Form(s) invalid.'})
    else:
        reportform = ReportSchedulerForm()
        solarform = SolarScheduleForm()
        cronform = CronScheduleForm()
        intervalfrom = IntervalScheduleForm()
        clockform = ClockedScheduleForm()
        modelform = MultipurposeSelectForm(Part)
        timespanform = TimeSpanForm()
        logger.debug('Generated a new forms')
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Create Scheduled Report page')
        return render(request, 'tracker/reporting/scheduler/create.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': modelform, 'timespanform': timespanform})


@permission_required('tracker.can_view_schdreport')
def reporting_scheduler_view(request, id):
    cname = 'reporting_scheduler_view'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Collecting objects')
    obj = PeriodicTask.objects.get(id=id)
    sub = False
    try:
        er = EmailRecipient.objects.filter(email=request.user.email).first()
        rn = ReportNotify.objects.filter(report=obj, emailrecipient=er).first()
        if rn != None:
            logger.debug('User is subscribed to report "{}"'.format(obj.name))
            sub = True
        else:
            logger.debug('User is not subscribed to report "{}"'.format(obj.name))
    except Exception as err:
        logger.debug('Problem checking for report subscription')
        logger.error(err)
    logger.debug('Loading forms data for {} view'.format(cname))
    kwargs = json.loads(obj.kwargs)
    report = kwargs['report']
    reportform = ReportSchedulerForm(initial={'name':obj.name,'description':obj.description,'report':report})
    for field in reportform.fields: reportform.fields[field].disabled = True
    if obj.solar_id != None:
        scheduleform = SolarScheduleForm(initial={'event':obj.solar.event,'latitude':obj.solar.latitude,'longitude':obj.solar.longitude})
        reportform.initial['schedule'] = 's'
    if obj.crontab_id != None:
        scheduleform = CronScheduleForm(initial={'minute':obj.crontab.minute,'hour':obj.crontab.hour,'day':obj.crontab.day_of_month,'month':obj.crontab.month_of_year,'day_of_week':obj.crontab.day_of_week,'timezone':obj.crontab.timezone})
        reportform.initial['schedule'] = 't'
    if obj.interval_id != None:
        if obj.interval.period == 'days' and obj.interval.every % 7 == 0:
            scheduleform = IntervalScheduleForm(initial={'number':int(obj.interval.every / 7),'period':'w'})
        else:
            scheduleform = IntervalScheduleForm(initial={'number':obj.interval.every,'period':obj.interval.period[0]})
        reportform.initial['schedule'] = 'i'
    if obj.clocked_id != None:
        scheduleform = ClockedScheduleForm(initial={'time':obj.clocked.clocked_time.time,'date':obj.clocked.clocked_time.date})
        reportform.initial['schedule'] = 'c'
    for field in scheduleform.fields: scheduleform.fields[field].disabled = True
    idform = _get_report_model_form(kwargs['report'])
    idform.initial = {'id':kwargs['ids']}
    for field in idform.fields: idform.fields[field].disabled = True
    timespanform = ''
    if 'duration' in kwargs:
        timespanform = TimeSpanForm(initial={'duration':kwargs['duration'],'span':kwargs['span']})
        for field in timespanform.fields: timespanform.fields[field].disabled = True
    logger.debug('Rendering request for {} view'.format(cname))
    logger2.info(request.user, 'Accessed View Scheduled Report page for id {}'.format(id))
    if timespanform:
        return render(request, 'tracker/reporting/scheduler/view.html', {'reportform': reportform, 'scheduleform': scheduleform, 'ids': idform, 'timespanform': timespanform, 'subscribed': sub})
    else:
        return render(request, 'tracker/reporting/scheduler/view.html', {'reportform': reportform, 'scheduleform': scheduleform, 'ids': idform, 'subscribed': sub})

@permission_required('tracker.can_change_schdreport')
def reporting_scheduler_change(request, id):
    cname = 'reporting_scheduler_change'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    obj = get_object_or_404(PeriodicTask,id=id)
    recipients = EmailRecipient.objects.all()
    subscription_status = []
    for recipient in recipients:
        sub = False
        if ReportNotify.objects.filter(report=obj,emailrecipient=recipient).count() > 0:
            sub = True
        subscription_status.append({'id':recipient.id,'name':recipient.name,'subscribed':sub})
    if request.method == 'POST':
        logger.debug('Serializing the request data')
        reportform = ReportSchedulerForm(request.POST)
        solarform = SolarScheduleForm(request.POST)
        cronform = CronScheduleForm(request.POST)
        intervalfrom = IntervalScheduleForm(request.POST)
        clockform = ClockedScheduleForm(request.POST)
        idform = _get_report_model_form(request.POST.get('report'))
        idform.initial = request.POST
        timespanform = TimeSpanForm(request.POST)
        if reportform.is_valid():
            logger.debug('ReportSchedulerForm is valid.')
            logger.info('Updating Scheduled Report id {}'.format(id))
            kwargs = '{'
            kwargs += '"report":"{}",'.format(reportform.cleaned_data['report'])
            ids = request.POST.getlist('id')
            kwargs += '"ids":{},'.format(ids)
            if reportform.cleaned_data['report'] in helpers.REPORT_LIST_TIMESPAN_FIELD:
                if timespanform.is_valid():
                    kwargs += '"duration":"{}",'.format(timespanform.cleaned_data['duration'])
                    kwargs += '"span":"{}",'.format(timespanform.cleaned_data['span'])
            kwargs += '"subject":"{}",'.format(reportform.cleaned_data['name'])
            kwargs = kwargs.strip(',')
            kwargs += '}'
            kwargs = kwargs.replace("'",'"')
            message = 'Scheduled Report updated successfully'
            try:
                obj.name = reportform.cleaned_data['name']
                obj.description = reportform.cleaned_data['description']
                obj.kwargs = kwargs
                if reportform.cleaned_data['schedule'] == 'c':
                    visible = 'clocked'
                    if clockform.is_valid() and clockform.cleaned_data['date'] != None and clockform.cleaned_data['time'] != None and clockform.cleaned_data['timezone'] != None :
                        logger.debug('Getting or creating ClockedSchedule; clocked_time: {}, clocked_date: {}, timezone: {}'.format(clockform.cleaned_data['time'], clockform.cleaned_data['date'], clockform.cleaned_data['timezone']))
                        try:
                            tzinfo = timezone(clockform.cleaned_data['timezone'])
                        except:
                            tzinfo = utc
                        date_time = datetime.combine(clockform.cleaned_data['date'],clockform.cleaned_data['time'],tzinfo=tzinfo)
                        schedule = ClockedSchedule.objects.get_or_create(clocked_time=date_time)[0]
                        obj.solar = None
                        obj.crontab = None
                        obj.interval = None
                        obj.clocked = schedule
                        obj.one_off = True
                    else:
                        logger.debug('Clocked Form invalid. Rendering request for {} view'.format(cname))
                        logger2.warning(request.user, 'Create Scheduled Report Clocked Form is not valid')
                        return render(request, 'tracker/reporting/scheduler/create.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': idform, 'timespanform': timespanform, 'recipients': subscription_status, 'message': 'Schedule Form invalid.'})
                if reportform.cleaned_data['schedule'] == 't':
                    visible = 'crontab'
                    if cronform.is_valid() and cronform.cleaned_data['minute'] != '' and cronform.cleaned_data['hour'] != '' and cronform.cleaned_data['day'] != '' and cronform.cleaned_data['month'] != '' and cronform.cleaned_data['day_of_week'] != '' and cronform.cleaned_data['timezone'] != '':
                        logger.debug('Getting or creating CrontabSchedule; minute: {}, hour: {}, days: {}, months: {}, day_of_week: {}'.format(cronform.cleaned_data['minute'], cronform.cleaned_data['hour'], cronform.cleaned_data['day'], cronform.cleaned_data['month'], cronform.cleaned_data['day_of_week']))
                        try:
                            tzinfo = timezone(clockform.cleaned_data['timezone'])
                        except:
                            tzinfo = utc
                        schedule = CrontabSchedule.objects.get_or_create(minute=cronform.cleaned_data['minute'],hour=cronform.cleaned_data['hour'],day_of_month=cronform.cleaned_data['day'],month_of_year=cronform.cleaned_data['month'],day_of_week=cronform.cleaned_data['day_of_week'],timezone=tzinfo)[0]
                        obj.solar = None
                        obj.clocked = None
                        obj.interval = None
                        obj.crontab = schedule
                        obj.one_off = False
                    else:
                        logger.debug('Crontab Form invalid. Rendering request for {} view'.format(cname))
                        logger2.warning(request.user, 'Create Scheduled Report Crontab Form is not valid')
                        return render(request, 'tracker/reporting/scheduler/create.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': idform, 'timespanform': timespanform, 'recipients': subscription_status, 'visible': visible, 'message': 'Schedule Form invalid.'})
                if reportform.cleaned_data['schedule'] == 's':
                    visible = 'solar'
                    if solarform.is_valid() and solarform.cleaned_data['event'] != None and solarform.cleaned_data['latitude'] != None and solarform.cleaned_data['longitude'] != None:
                        logger.debug('Getting or creating SolarSchedule; event: {}, latitude: {}, longitude: {}'.format(solarform.cleaned_data['event'], solarform.cleaned_data['latitude'], solarform.cleaned_data['longitude']))
                        event = helpers.get_solar_type(solarform.cleaned_data['event']).lower()
                        schedule = SolarSchedule.objects.get_or_create(event=event,latitude=solarform.cleaned_data['latitude'],longitude=solarform.cleaned_data['longitude'])[0]
                        obj.clocked = None
                        obj.crontab = None
                        obj.interval = None
                        obj.solar = schedule
                        obj.one_off = False
                    else:
                        logger.debug('Solar Form invalid. Rendering request for {} view'.format(cname))
                        logger2.warning(request.user, 'Create Scheduled Report Solar Form is not valid')
                        return render(request, 'tracker/reporting/scheduler/create.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': idform, 'timespanform': timespanform, 'recipients': subscription_status, 'visible': visible, 'message': 'Schedule Form invalid.'})
                if reportform.cleaned_data['schedule'] == 'i':
                    visible = 'interval'
                    if intervalfrom.is_valid() and intervalfrom.cleaned_data['number'] != None and intervalfrom.cleaned_data['period'] != None:
                        logger.debug('Getting or creating IntervalSchedule; every: {}, period: {}'.format(intervalfrom.cleaned_data['number'], intervalfrom.cleaned_data['period']))
                        period = helpers.get_timespan_type(intervalfrom.cleaned_data['period']).lower()
                        number = intervalfrom.cleaned_data['number']
                        if period == 'weeks':
                            period = 'days'
                            number *= 7
                        schedule = IntervalSchedule.objects.get_or_create(every=number,period=period)[0]
                        obj.solar = None
                        obj.crontab = None
                        obj.clocked = None
                        obj.interval = schedule
                        obj.one_off = False
                    else:
                        logger.debug('Interval Form invalid. Rendering request for {} view'.format(cname))
                        logger2.warning(request.user, 'Create Scheduled Report Interval Form is not valid')
                        return render(request, 'tracker/reporting/scheduler/create.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': idform, 'timespanform': timespanform, 'recipients': subscription_status, 'visible': visible, 'message': 'Schedule Form invalid.'})
                obj.save()
            except Exception as err:
                logger.warning(err)
                logger2.warning(request.user, err)
                message = 'Failed to update Scheduled Report'
            logger.debug('Rendering request for {} view'.format(cname))
            logger2.info(request.user, '{} for id {}'.format(message, id))
            return render(request, 'tracker/reporting/scheduler/update.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': idform, 'timespanform': timespanform, 'recipients': subscription_status, 'visible': visible, 'message': message})
        if obj.solar_id != None: visible = 'solar'
        if obj.crontab_id != None: visible = 'crontab'
        if obj.interval_id != None: visible = 'interval'
        if obj.clocked_id != None: visible = 'clocked'
        logger.debug('Form is not valid. Rendering request for {} view'.format(cname))
        logger2.warning(request.user, 'Change KitThreshold Form not valid for id {}'.format(id))
        return render(request, 'tracker/reporting/scheduler/update.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': idform, 'timespanform': timespanform, 'recipients': subscription_status, 'visible': visible, 'message': 'Form is not valid.'})
    else:
        logger.debug('Loading forms data for {} view'.format(cname))
        kwargs = json.loads(obj.kwargs)
        reportform = ReportSchedulerForm(initial={'name':obj.name,'description':obj.description,'report':kwargs['report']})
        if obj.solar_id != None:
            solarform = SolarScheduleForm(initial={'event':obj.solar.event,'latitude':obj.solar.latitude,'longitude':obj.solar.longitude})
            reportform.fields['schedule'].initial = 's'
            visible = 'solar'
        else:
            solarform = SolarScheduleForm()
        if obj.crontab_id != None:
            cronform = CronScheduleForm(initial={'minute':obj.crontab.minute,'hour':obj.crontab.hour,'day':obj.crontab.day_of_month,'month':obj.crontab.month_of_year,'day_of_week':obj.crontab.day_of_week,'timezone':obj.crontab.timezone})
            reportform.fields['schedule'].initial = 't'
            visible = 'crontab'
        else:
            cronform = CronScheduleForm()
        if obj.interval_id != None:
            if obj.interval.period[0] == 'days' and obj.interval.every % 7 == 0:
                intervalfrom = IntervalScheduleForm(initial={'number':(obj.interval.every / 7),'period':'weeks'})
            else:
                intervalfrom = IntervalScheduleForm(initial={'number':obj.interval.every,'period':obj.interval.period[0]})
            reportform.fields['schedule'].initial = 'i'
            visible = 'interval'
        else:
            intervalfrom = IntervalScheduleForm()
        if obj.clocked_id != None:
            clockform = ClockedScheduleForm(initial={'time':obj.clocked.clocked_time.time,'date':obj.clocked.clocked_time.date})
            reportform.fields['schedule'].initial = 'c'
            visible = 'clocked'
        else:
            clockform = ClockedScheduleForm()
        idform = _get_report_model_form(kwargs['report'])
        idform.initial = {'id':kwargs['ids']}
        if 'duration' in kwargs:
            timespanform = TimeSpanForm(initial={'duration':kwargs['duration'],'span':kwargs['span']})
        else:
            timespanform = ''
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Accessed Update Scheduled Report page for id {}'.format(id))
        if timespanform:
            return render(request, 'tracker/reporting/scheduler/update.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': idform, 'timespanform': timespanform, 'recipients': subscription_status, 'visible': visible})
        else:
            return render(request, 'tracker/reporting/scheduler/update.html', {'reportform': reportform, 'solarform': solarform, 'cronform': cronform, 'intervalform': intervalfrom, 'clockform': clockform, 'modelform': idform, 'recipients': subscription_status, 'visible': visible})

@permission_required('tracker.can_delete_schdreport')
def ajax_scheduler_delete(request, id):
    cname = 'ajax_scheduler_delete'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested PeriodicTask')
    pt = get_object_or_404(PeriodicTask, id=id)
    logger.info('DELETE request for PeriodicTask id {}'.format(id))
    pt.delete()
    logger2.info(request.user, 'Deleted PeriodicTask id {}'.format(id))
    logger.debug('Rendering request for {} view'.format(cname))
    return render(request, 'tracker/ajax_result.html', {'color': 'green', 'message': 'Successfully Deleted'})

@permission_required('tracker.can_change_schdreport')
def ajax_scheduler_assign(request, rid, uid):
    cname = 'ajax_scheduler_assign'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested PeriodicTask and Recipient')
    pt = get_object_or_404(PeriodicTask, id=rid)
    er = get_object_or_404(EmailRecipient, id=uid)
    rn = ReportNotify.objects.filter(report=pt,emailrecipient=er).first()
    if rn == None:
        logger.info('Assigning notification for PeriodicTask id {} to EmailRecipient id {}'.format(rid, uid))
        ReportNotify.objects.create(report=pt, emailrecipient=er)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Assigned notifications for Report id {} to EmailRecipient id {}'.format(rid, uid))
        return render(request, 'tracker/ajax_result.html', {'color': 'green', 'result': 'Successfully assigned {} to {}.'.format(er.name, pt.name)})
    else:
        logger.debug('ReportNotify for "{} to {}" already exists'.format(pt.name, er.email))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.debug(request.user, 'User already subscribed to PeriodicTask id {}'.format(rid))
        return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'Subscription already exists.'})

@permission_required('tracker.can_change_schdreport')
def ajax_scheduler_unassign(request, rid, uid):
    cname = 'ajax_scheduler_unassign'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Getting requested PeriodicTask and Recipient')
    pt = get_object_or_404(PeriodicTask, id=rid)
    er = get_object_or_404(EmailRecipient, id=uid)
    rn = ReportNotify.objects.filter(report=pt,emailrecipient=er).first()
    if rn == None:
        logger.debug('ReportNotify for "{} to {}" does not exist'.format(pt.name, er.email))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.debug(request.user, 'User is not subscribed to PeriodicTask id {}'.format(rid))
        return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'Subscription does not exist.'})
    else:
        logger.info('Unassigning notification for PeriodicTask id {} to EmailRecipient id {}'.format(rid, uid))
        rn.delete()
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Unassigned notifications for Report id {} to EmailRecipient id {}'.format(rid, uid))
        return render(request, 'tracker/ajax_result.html', {'color': 'green', 'result': 'Successfully unassigned {} from {}.'.format(er.name, pt.name)})

@permission_required('tracker.can_subscribe_schdreport')
def ajax_scheduler_subscribe(request, id):
    cname = 'ajax_scheduler_subscribe'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('Checking for objects')
    pt = get_object_or_404(PeriodicTask, id=id)
    er = EmailRecipient.objects.filter(email=request.user.email).first()
    if er == None:
        logger.debug('User does not have a EmailRecipient object')
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.debug(request.user, 'No matching EmailRecipient found for the User')
        return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'No matching EmailRecipient found.'})
    rn = ReportNotify.objects.filter(report=pt, emailrecipient=er).first()
    if rn == None:
        logger.info('Creating new ReportNotify "{} to {}"'.format(pt.name, er.email))
        ReportNotify.objects.create(report=pt, emailrecipient=er)
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.info(request.user, 'Subscribed to Report id {}'.format(id))
        return render(request, 'tracker/ajax_result.html', {'color': 'green', 'result': 'Successfully Subscribed to {}.'.format(pt.name)})
    else:
        logger.debug('ReportNotify for "{} to {}" already exists'.format(pt.name, er.email))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.debug(request.user, 'User already subscribed to Report id {}'.format(id))
        return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'Subscription already exists.'})

@permission_required('tracker.can_subscribe_schdreport')
def ajax_scheduler_unsubscribe(request, id):
    cname = 'ajax_scheduler_unsubscribe'
    logger.debug('Request made to {} view'.format(cname))
    logger2.debug(request.user, 'Request to view {}'.format(cname))
    logger.debug('User is authenticated, checking for objects')
    pt = get_object_or_404(PeriodicTask, id=id)
    er = EmailRecipient.objects.filter(email=request.user.email).first()
    if er == None:
        logger.debug('User does not have a EmailRecipient object')
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.debug(request.user, 'No matching EmailRecipient found for the User')
        return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'No matching EmailRecipient found.'})
    rn = ReportNotify.objects.filter(report=pt, emailrecipient=er).first()
    if rn == None:
        logger.debug('ReportNotify for "{} to {}" does not exist'.format(pt.name, er.email))
        logger.debug('Rendering request for {} view'.format(cname))
        logger2.debug(request.user, 'User not subscribed to Report id {}'.format(id))
        return render(request, 'tracker/ajax_result.html', {'color': 'red', 'result': 'Subscription does not exist.'})
    else:
        logger.info('Deleting ReportNotify "{} to {}"'.format(pt.name, er.email))
        rn.delete()
        logger2.info(request.user, 'Unsubscribed from Report id {}'.format(id))
        logger.debug('Rendering request for {} view'.format(cname))
        return render(request, 'tracker/ajax_result.html', {'color': 'green', 'result': 'Successfully Unsubscribed from {}.'.format(pt.name)})

def _get_report_model_form(report):
    if report == 'recent_user_list':
        logger.debug('Generating a list of User objects')
        form = MultipurposeSelectForm(User)
    elif 'part_count' in report:
        logger.debug('Generating a list of Part objects')
        form = MultipurposeSelectForm(Part)
        if 'historic' not in report:
            form.fields['id'].multiple = True
    elif report == 'storage_counts' or report == 'storage_counts_graph':
        logger.debug('Generating a list of Storage objects')
        form = MultipurposeSelectForm(Storage)
    elif 'kit_count' in report == 'kit_counts_graph':
        logger.debug('Generating a list of Kit objects')
        form = MultipurposeSelectForm(Kit)
        if 'historic' not in report:
            form.fields['id'].multiple = True
    elif 'partstorage_count' in report:
        logger.debug('Generating a list of PartStorage objects')
        form = MultipurposeSelectForm(PartStorage)
    else:
        logger.debug('Generating a list of objects types')
        form = ModelSelectForm()
        form.fields['id'].multiple = True
    return form