from __future__ import absolute_import
from celery import Celery
from celery.signals import after_setup_logger
from django.conf import settings
import logging
import os

from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ims.settings')

logger = logging.getLogger(__name__)

app = Celery('ims')

app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()

@after_setup_logger.connect
def setup_loggers(logger, *args, **kwargs):
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    # FileHandler
    rfh = logging.handlers.RotatingFileHandler(os.path.join(settings.LOG_DIR, 'tasks.log'))
    rfh.backupCount = 3
    rfh.maxBytes = 1024*1024*5
    rfh.setFormatter(formatter)
    logger.addHandler(rfh)
