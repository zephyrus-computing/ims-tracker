amqp==5.2.0
asgiref==3.7.2
billiard==4.2.0
celery==5.3.6
click==8.1.7
click-didyoumean==0.3.0
click-plugins==1.1.1
click-repl==0.3.0
contourpy==1.2.0
cron-descriptor==1.4.3
cycler==0.12.1
Deprecated==1.2.14
django-celery-beat==2.5.0
django-timezone-field==6.1.0
fonttools==4.49.0
kiwisolver==1.4.5
kombu==5.3.5
matplotlib==3.8.3
numpy==1.26.4
pandas==2.2.1
pillow==10.2.0
prompt-toolkit==3.0.43
pyparsing==3.1.1
python-crontab==3.0.0
python-dateutil==2.8.2
redis==5.0.2
tzdata==2024.1
vine==5.1.0
wcwidth==0.2.13
wrapt==1.16.0
