# IMS-Tracker
The Tracker add-on module for IMS enables email notifications for changes to storage contents, part information, or assembly details and generate reports for tracking inventory levels. Users are able to manage their own subscriptions for notifications and reports or an administrator can assign subscription as needed.

## Installation
Quick installation instructions can be found in the Quick Install Wiki section.

Detailed instructions can be found in the Admin Guide Wiki section.

The Debian package can be download from the Package Registry.

## Roadmap
- Add an admin page for view Threshold and Notification history.

- Update and standardize the methods used for triggers and reports.

- Remove deprecated tasks from the old IMS tightly coupled integration.

- Optimize module pages for small screens.

- Security patching and bug fixes as necessary.

## Support
Tickets may be submitted to this repo for support requests or bug reports. You may also email support@zephyruscomputing.com.

## Contributing
Requests to help contribute are welcome. For major changes, please open an issue ticket first to discuss what you would like to change.

Please make sure to update tests as appropriate.

### Development Environment Setup
```
git clone https://gitlab.com/zephyrus-computing/inventory-management-system.git ims
cd ims
./dev.sh setup
cd ..
git clone https://gitlab.com/zephyrus-computing/ims-tracker.git ims-tracker
cd ims-tracker/
./dev-tracker.sh setup
source venv/bin/activate
```
The dev.sh script with the setup option will configure a python virtual environment and install the packages specified in requirements.txt, create a dev db, and execute the superuser creation process.

The dev-tracker.sh script with the setup option will configure the python virtual environment and install the packages specified in the requirements.txt, then add all of the necessary file changes to imitate the installation of IMS-Tracker module.

When you have finished your work, you can run ```deactivate``` to close the environment.

If you need to reset your environment, run the following:
```
./dev-tracker.sh delete
./dev.sh delete
./dev.sh setup
./dev-tracker.sh setup 
```

## License
[GPL3](http://choosealicense.com/licenses/gpl-3.0/)
